﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Ninject;
using System.Reflection;

[assembly: OwinStartup(typeof(Scoolfirst.Api.Startup))]

namespace Scoolfirst.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            
            ConfigureAuth(app);
            
        }

        
    }
}
