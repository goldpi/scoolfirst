﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoolfirst.Api.Models
{
    public class PaReportModel
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public long ReasoningId { get; set; }
        public DateTime On { get; set; }

        public int RightAns { get; set; }
        public int WrongAns { get; set; }
        public int SkippedAns { get; set; }

        public int Total => RightAns + WrongAns + SkippedAns;
        public double Month { get; set; }
    }
}