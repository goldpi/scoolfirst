﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Scoolfirst.Api.Models;
using System.Net;

namespace Scoolfirst.Api.Providers
{


    public class ExceptionIntrceptAttribute :ExceptionFilterAttribute 
    {
        public override void OnException(HttpActionExecutedContext context)
    {
            context.Exception.Debug();
            context.Response = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        
    }
}

    public class ModulerCheckerAPIAttribute : Attribute, IActionFilter
    {

        
        private User UserData;
        public ModulerCheckerAPIAttribute()
        {
            
        }


        public bool AllowMultiple =>true;

        public Task<HttpResponseMessage> ExecuteActionFilterAsync(HttpActionContext filterContext, 
            CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
           
                
                using (ScoolfirstContext dc = new ScoolfirstContext())
                {
                    if (filterContext.RequestContext.Principal.Identity.IsAuthenticated)
                    {
                        var user = filterContext.RequestContext.Principal.Identity.Name;
                        var UserData1 = (from l in dc.Users
                                         where l.UserName == user || l.Email == user

                                         select new { p = l, School = l.School, @class = l.Class }).FirstOrDefault();

                    var userdata = dc.Users.FirstOrDefault(i => i.Email == user);
                    if (userdata != null)
                    {
                        string Text = $"User Name: {userdata.FullName} ,Phone No: {userdata.PhoneNumber} ,Email: {userdata.Email} ,Points:{userdata.Points}";
                        Text.UserDetails();
                    }

                        if (UserData1 != null)
                        {
                            UserData = UserData1.p;
                            UserData.School = UserData1.School;
                            UserData.Class = UserData1.@class;

                            var a = filterContext.ActionDescriptor.ActionName;
                            var c = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                            ActivityLogger logger = new ActivityLogger
                            {
                                Url = filterContext.Request.RequestUri.ToString(),
                                Action = a,
                                Controller = c,
                                Source = "Mobile",
                                UserName = UserData != null ? UserData.UserName : "No user",
                                Id = Guid.NewGuid(),
                                OnDateOffset = DateTimeOffset.Now
                            };
                            logger.Debug();
                            dc.ActivityLogger.Add(logger);
                            dc.SaveChanges();
                           
                        }


                    }

                    var result = continuation();
                    
                    return result;
                }
           
            
           
        }

        
    }
}