﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;
//using Microsoft.AspNet.Identity.Owin;
//using Microsoft.Owin.Security;
//using Microsoft.Owin.Security.Cookies;
//using Microsoft.Owin.Security.OAuth;
//using Scoolfirst.Api.Models;
//using Scoolfirst.Model.Identity;
//using System.Text.RegularExpressions;
//using Microsoft.Owin.Security.Infrastructure;
//using System.Collections.Concurrent;

//namespace Scoolfirst.Api.Providers
//{
//    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
//    {
//        private readonly string _publicClientId;
//        private Scoolfirst.Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();
//        public ApplicationOAuthProvider(string publicClientId)
//        {
//            if (publicClientId == null)
//            {
//                throw new ArgumentNullException("publicClientId");
//            }
//            _publicClientId = publicClientId;
//        }

//        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
//        {
//            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
//            var user = dc.Users.FirstOrDefault(i => i.PhoneNumber == context.UserName || i.UserName==context.UserName || i.Email==context.UserName); 
//            if (user == null)
//            {
//                context.SetError("invalid_grant", "The user name or password is incorrect.");
//                return;
//            }
//            var results=await userManager.CheckPasswordAsync(user, context.Password);
//            if (results==false)
//            {
//                context.SetError("invalid_grant", "The user name or password is incorrect.");
//                return;
//            }

//            //if (user.Expiry < DateTime.Now)
//            //{
//            //    context.SetError("invalid_grant", "Subscription Expired");
//            //}

//                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
//               OAuthDefaults.AuthenticationType);
           
//            ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
//                CookieAuthenticationDefaults.AuthenticationType);

//            if(dc.Booster.Any(i => i.UserId == user.Id))
//            {
//                var Booster = dc.Booster.OrderByDescending(i=>i.ExpiryOn).FirstOrDefault(i => i.UserId == user.Id);
//                if (Booster != null && Booster.ExpiryOn < DateTime.Now)
//                {
//                    user.Factor = 2;
//                }
//                else
//                {
//                    user.Factor = Booster.Factor;
//                }

//            }
//            else
//            {
//                user.Factor = 2;
//            }
            
            
//            dc.SaveChanges();
//            AuthenticationProperties properties = CreateProperties(user.UserName,user);

//            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            
//            context.Validated(ticket);
            
//            context.Request.Context.Authentication.SignIn(cookiesIdentity);
//        }

//        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
//        {
//            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
//            {
//                context.AdditionalResponseParameters.Add(property.Key, property.Value);
//            }

//            return Task.FromResult<object>(null);
//        }

//        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
//        {
//            // Resource owner password credentials does not provide a client ID.
//            if (context.ClientId == null)
//            {
//                context.Validated();
//            }

//            return Task.FromResult<object>(null);
//        }

//        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
//        {
//            if (context.ClientId == _publicClientId)
//            {
//                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

//                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
//                {
//                    context.Validated();
//                }
//            }

//            return Task.FromResult<object>(null);
//        }

//        public static AuthenticationProperties CreateProperties(string userName,User user)
//        {
//            IDictionary<string, string> data = new Dictionary<string, string>
//            {
//                { "userName", userName },
                
//            };
//            data.Add("Points",user.Points.ToString());
//            data.Add("Factor", user.Factor.ToString());
//            data.Add("SchoolId", user.SchoolId.ToString());
//            data.Add("Subscription", user.Subscribed.ToString());
//           // data.Add("SubscriptionName",user.)
//            return new AuthenticationProperties(data);
//        }
//        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
//        {
//           if (context.Ticket == null || context.Ticket.Identity == null || !context.Ticket.Identity.IsAuthenticated)
//    {
//        context.SetError("invalid_grant", "Refresh token is not valid");
//    }
//    else
//    {
//        var userIdentity = context.Ticket.Identity;
 
//        var authenticationTicket =new AuthenticationTicket(userIdentity,context.Ticket.Properties);
 
//        //Additional claim is needed to separate access token updating from authentication 
//        //requests in RefreshTokenProvider.CreateAsync() method
//        authenticationTicket.Identity.AddClaim(new Claim("refreshToken", "refreshToken"));
 
//        context.Validated(authenticationTicket);
//    }
 
//    return Task.FromResult<object>(null);
//        }
//    }


//    public class UpgrdeRefreshTokenProvier : IAuthenticationTokenProvider
//    {
//        private static ConcurrentDictionary<string, AuthenticationTicket> _refreshTokens = new ConcurrentDictionary<string, AuthenticationTicket>();
//        public void Create(AuthenticationTokenCreateContext context)
//        {
//            var guid = Guid.NewGuid().ToString();

//            // copy all properties and set the desired lifetime of refresh token  
//            var refreshTokenProperties = new AuthenticationProperties(context.Ticket.Properties.Dictionary)
//            {
//                IssuedUtc = context.Ticket.Properties.IssuedUtc,
//                ExpiresUtc = DateTime.UtcNow.AddDays(14)
//            };
//            var refreshTokenTicket = new AuthenticationTicket(context.Ticket.Identity, refreshTokenProperties);

//            _refreshTokens.TryAdd(guid, refreshTokenTicket);

//            // consider storing only the hash of the handle  
//            context.SetToken(guid);
//        }

//        public async  Task CreateAsync(AuthenticationTokenCreateContext context)
//        {
//            var guid = Guid.NewGuid().ToString();

//            // copy all properties and set the desired lifetime of refresh token  
//            var refreshTokenProperties = new AuthenticationProperties(context.Ticket.Properties.Dictionary)
//            {
//                IssuedUtc = context.Ticket.Properties.IssuedUtc,
//                ExpiresUtc = DateTime.UtcNow.AddDays(14)
//            };
//             var refreshTokenTicket = new AuthenticationTicket(context.Ticket.Identity, refreshTokenProperties);

            
//             _refreshTokens.TryAdd(guid, refreshTokenTicket);
//            //context.Ticket.
//            // consider storing only the hash of the handle  
//            context.SetToken(guid);
//            //return
//            await Task.FromResult(0);

//            //throw new NotImplementedException();
//        }

//        //public void Create(AuthenticationTokenCreateContext context)
//        //{
//        //    var guid = Guid.NewGuid().ToString();

//        //    // copy all properties and set the desired lifetime of refresh token  
//        //    var refreshTokenProperties = new AuthenticationProperties(context.Ticket.Properties.Dictionary)
//        //    {
//        //        IssuedUtc = context.Ticket.Properties.IssuedUtc,
//        //        ExpiresUtc = DateTime.UtcNow.AddMinutes(9)//.AddDays(14)
//        //    };
//        //    var refreshTokenTicket = new AuthenticationTicket(context.Ticket.Identity, refreshTokenProperties);

//        //    _refreshTokens.TryAdd(guid, refreshTokenTicket);

//        //    // consider storing only the hash of the handle  
//        //    context.SetToken(guid);
//        //}

//        public void Receive(AuthenticationTokenReceiveContext context)
//        {
//            AuthenticationTicket ticket;
//            string header = context.OwinContext.Request.Headers["Authorization"];

//            if (_refreshTokens.TryRemove(context.Token, out ticket))
//            {
//               context.SetTicket(ticket);
//            }
//        }

//        public Task ReceiveAsync(AuthenticationTokenReceiveContext context)
//        {
//            throw new NotImplementedException();
//        }


//        //public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
//        //{
//        //    AuthenticationTicket ticket;
//        //    string header = context.OwinContext.Request.Headers["Authorization"];

//        //    if ( _refreshTokens.TryRemove(context.Token, out ticket))
//        //    {

//        //       context.SetTicket(ticket);
//        //    }
//        //}
//    }
//}