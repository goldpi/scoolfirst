﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.KDC;
using Scoolfirst.ViewModel.Feed;
using Scoolfirst.ViewModel.KDC;
using Scoolfirst.ViewModel.KDC.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class KDCController : ApiController
    {
        ScoolfirstContext dc = new ScoolfirstContext();
        
        ArticlesItems ArtilceHandler = new ArticlesItems();
        [Route("api/askanswer")]
        public IEnumerable<KDCApiVM> Get(int page=0,int size=4,bool my = true)
        {
            if (my)
            {
                return ArtilceHandler.GetAPIMyItems(dc, User.Identity.Name, page, size);
            }
            else
            {
                return ArtilceHandler.GetApiItems(dc, User.Identity.Name, page, size);
            }
        }

        [HttpGet]
        [Route("api/Solution/{id}")]
        public object ShowKdcData(Guid id)
        {
            return dc.ArticleComments.Where(i => i.ArticleId == id).ToList();
        }
        [HttpPost]
        [Route("api/AskandAnsSave")]
        public object SaveData(Article a)
        {
            var user = User.Identity.Name;
            var id = dc.Users.FirstOrDefault(i => i.UserName == user);
            a.ID = Guid.NewGuid();
            a.UserId = id.Id;
            a.On = DateTime.UtcNow;
            dc.Articles.Add(a);
            dc.SaveChanges();
            return a;
        }
       
    }
}
