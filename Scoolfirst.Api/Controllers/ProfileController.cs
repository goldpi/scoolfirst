﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using System.Web.Http;
using Scoolfirst.ViewModel.User.ProfileUpdateWeb;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Linq;
namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class ProfileController : ApiController
    {
      
        private ScoolfirstContext dc = new ScoolfirstContext();
        [HttpPost]
        [Route("api/ProfilePostData")]
        public ProfileVM ProfileDataPost(ProfileVM profile)
        {
            
            var userId = profile.UserId;
            var user = dc.Users.FirstOrDefault(i=>i.Id==userId);

            user.FullName = profile.Name;
            user.Gender = profile.Gender;
            user.DOB = profile.Dob;
            user.Board = profile.Board;
            user.ClassId = profile.ClassId;          
            user.SchoolId = profile.SchoolId;
            user.Address = profile.Address;          
            user.AboutMe = profile.AboutMe;
            dc.Entry(user).State = System.Data.Entity.EntityState.Modified;
            dc.SaveChanges();
            return profile;
          
        }

        [HttpGet]
        [Route("api/mysubscriptiondetail")]
        public object GetSubscriptionDetail()
        {

            var userId = User.Identity.Name;
            var user = dc.Users.Include(i => i.Class).Include(i => i.School).FirstOrDefault(i => i.UserName == userId);
            return new 
            {
                Email = User.Identity.GetUserName(),

                UserName = user.UserName,
                Name = user.FullName,
                Picture = user.PictureUrl,
                ClassName = user.Class.RomanDisplay,
                SchoolName = user.School.SchoolName,
                PhoneNumber = user.PhoneNumber,
                ExpiryDate = user.Expiry,
                Trial = user.Trial,
                RegisterDate = user.RegisterdOn,
                Modules = user.Modules,
                Points = user.Points,
                subscription = user.Subscribed,
                ClassId = user.ClassId,
                SchoolId=user.SchoolId

        };


            
        }
    }
}