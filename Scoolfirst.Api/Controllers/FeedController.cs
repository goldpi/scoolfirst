﻿using Newtonsoft.Json;
using Scoolfirst.ViewModel.Feed;
using Scoolfirst.ViewModel.Feed.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class FeedController : ApiController
    {
        Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();
        FeedItems FeedHandler = new FeedItems();
        /// <summary>
        /// Brings feed for the logged user.
        /// </summary>
        /// <param name="page">Page No</param>
        /// <param name="size">No of Items</param>
        /// <returns></returns>
        public IEnumerable<ApiFeedModel> Get(int page=0,int size=3)
        {
           
            FeedItems itemss = new FeedItems();

            // var feeds = dc.Feeds.Include(o => o.Post).Include(i => i.Comments).Include(i=>i.Likes).OrderByDescending(i => i.OnDateTime).Where(i => i.OnDateTime <= Date).ToList();
            
           var items= itemss.GetApiItems(dc, User.Identity.Name, page,size);

            return items;
        }

        /// <summary>
        /// Likes or Unlikes the post
        /// </summary>
        /// <param name="Id">Feed Id</param>
        /// <returns>No of Likes</returns>
        [Route("Like")]
        public int Like(Guid Id)
        {
            return FeedHandler.AddRemoveFeedLike(dc, Id, User.Identity.Name);
        }
        /// <summary>
        /// Add or remmoves from Stared post.
        /// </summary>
        /// <param name="Id">Feed Id</param>
        /// <returns>The Status that post is in starred</returns>
        [Route("Fav")]
        public bool Fav(Guid Id)
        {
            return FeedHandler.AddRemoveFeedStar(dc, Id, User.Identity.Name);
        }
        [Route("api/Feed/Calander")]
        [HttpGet]
        public IEnumerable<FeedCalander> Calander(int Month = 0,int year=2015)
        {
            if (Month == 0)
            {
                Month = DateTime.Now.Month;
            }
            
             
            var dt = dc.Feeds.Where(i => i.OnDateTime.Month >= Month&&i.OnDateTime.Year>=year);
            List<FeedCalander> fd = new List<FeedCalander>();
            foreach (var item in dt)
            {
                fd.Add(new FeedCalander().FromFeed(item));
            }
            return fd;
        }

        [Route("api/Feed/BreakBrainFast"),HttpGet]
        public IEnumerable<KnowledgeNutritionViewModel> GetBreakBrainFast(int page=0,int size=10)
        {
            return dc.BreakBrainFastsForUser(User.Identity.Name, page, size);
        }


        //[Route("api/Feed/ExperimentalLearning"), HttpGet]
        //public IEnumerable<ExperimentalLearningViewModel> getExperimentalLearning(int page = 0, int size = 10)
        //{
        //    return dc.ExperimentalLearningsForUser(User.Identity.Name, page, size);
        //}
    }
}
