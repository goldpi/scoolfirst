﻿#region Imports
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.SqlClient;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Scoolfirst.Api.Models;
using Scoolfirst.Api.Providers;
using Scoolfirst.Api.Results;
using Scoolfirst.Model.Identity;
using System.Linq;
using System.Net;

//using Microsoft.Ajax.Utilities;
using Scoolfirst.Model.Provider;
using Scoolfirst.ViewModel;
using System.Data.Entity;
using Newtonsoft.Json.Linq;
using Scoolfirst.Model.Context;
using System.Text.RegularExpressions;

#endregion
namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;
        private ScoolfirstContext dc = new ScoolfirstContext();
        private IShortMessageSender MessageProvider;


        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat,IShortMessageSender messageSender)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
            MessageProvider = messageSender;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [System.Web.Http.Route("UserInfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [System.Web.Http.Route("UserDetalis")]
        public Object GetUserInformation()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);
            var user = User.Identity.Name;
            var dc = new Scoolfirst.Model.Context.ScoolfirstContext();
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.AutoDetectChangesEnabled = false;
            var userInfo = dc.Users.Include(i=>i.Class).Include(i=>i.School).FirstOrDefault(i=>i.Email==user);
            
            return new
            {
                Email = User.Identity.GetUserName(),
                UserId = userInfo.Id,
                UserName = userInfo.UserName,
                Name = userInfo.FullName,
                Picture = userInfo.PictureUrl,
                ClassId = userInfo.ClassId,
                Class=userInfo.Class,
                SchoolId = userInfo.SchoolId,
                ClassName = userInfo.Class.RomanDisplay,
                SchoolName = userInfo.School.SchoolName,
                PhoneNumber = userInfo.PhoneNumber,
                ExpiryDate = userInfo.Expiry,
                Trial = userInfo.Trial,
                RegisterDate = userInfo.RegisterdOn,
                Dob = userInfo.DOB,
                AboutMe = userInfo.AboutMe,
                OTP=userInfo.ConnectionId,
                Board=userInfo.Board,
                Address=userInfo.Address,
                Gender=userInfo.Gender,
                Modules = userInfo.Modules,
                Points = userInfo.Points,
                subscription = userInfo.Subscribed,
                Booster=userInfo.Factor
            };
        }


        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [System.Web.Http.Route("UserEmail")]
        public string GetUserEmail()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);
            return User.Identity.GetUserId();
        }
        // POST api/Account/Logout
        [System.Web.Http.Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [System.Web.Http.Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        // POST api/Account/ChangePassword
        [System.Web.Http.Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            
           
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/SetPassword
        [System.Web.Http.Authorize]
        [System.Web.Http.Route("SetNewPassword")]
        public async Task<IHttpActionResult> SetNewPassword(ChangePasswordBindingModel model)
        {
           
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(),model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }


        // POST api/Account/SetPassword
        [System.Web.Http.Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var username = User.Identity.Name;
            var currentUser = dc.Users.FirstOrDefault(i => i.UserName == username);
            var token = UserManager.GeneratePasswordResetToken(currentUser.Id);
            IdentityResult result = await UserManager.ResetPasswordAsync(currentUser.Id, token, model.NewPassword);
            // IdentityResult result = await UserManager.ChangePasswordAsync(currentUser.Id,currentUser.ConnectionId,model.NewPassword);

            if (!result.Succeeded)
            {
                
                return GetErrorResult(result);
            }

            return Ok(new { Message="Success"});
        }

        // POST api/Account/AddExternalLogin
        [System.Web.Http.Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [System.Web.Http.Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogin
        [System.Web.Http.OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            User user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                   OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName,user);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }

        // POST api/Account/Register
        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModelChild model)
        {
            #region validation
            string UserID = string.Empty;
            if (UserManager.Users.Any(i => i.PhoneNumber.Equals(model.PhoneNo, StringComparison.InvariantCultureIgnoreCase)))
            {
                ModelState.AddModelError("Mobile", "Please try another mobile no.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            #endregion
            #region Register
            string code = string.Empty;
            Random generator = new Random();
            if (model.Email == null)
            {
                model.Email = "";
            }
            var reResult = Regex.Match(model.Email, @"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$").Success;
            model.Email = string.IsNullOrEmpty(model.Email) || (!reResult) ? model.PhoneNo + model.Name.Replace(' ', '_') + "@upgradejr.com" : model.Email;
            code = generator.Next(0, 1000000).ToString("D6");
            User user = CreateUser(model, code);
            #endregion
            try
            {
                IdentityResult result = await UserManager.CreateAsync(user, code);
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                else
                {
                    RoleAssign(user);
                    UserID = user.Id;
                    if (!Request.IsLocal())
                        await SmsProviderInstance.SendOTPAsync(user.FullName, code, user.PhoneNumber);
                 
                    return Ok(new
                   {
                       Email = user.Email,
                       UserId = user.Id,
                       UserName = user.UserName,
                       Name = user.FullName,
                       Picture = user.PictureUrl,
                       ClassId = user.ClassId,
                       SchoolId = user.SchoolId,
                       PhoneNumber = user.PhoneNumber,
                       ExpiryDate = user.Expiry,
                       Trial = user.Trial,
                       RegisterDate = user.RegisterdOn,
                       AboutMe = user.AboutMe,
                       Address = user.Address,
                       Gender = user.Gender,
                       Points = user.Points,
                       subscription = user.Subscribed,
                       Booster=user.Factor
                       
                   });
                }   
            }
            catch (Exception ex)
            {
               throw HandleException(ex);    
            }
        }

        private  User CreateUser(RegisterBindingModelChild model, string code)
        {
            if (dc.Schools.Any(i => i.Id == model.SchoolId && i.IsMasterContent))
            {
                if (!string.IsNullOrEmpty(model.Board))
                {
                    var schoolId = dc.Schools.FirstOrDefault(i => i.Board.Name == model.Board && i.IsMasterContent == true);
                    model.SchoolId = schoolId.Id;
                }
            }
            
            return new User()
            {
                UserName = model.Email,
                Email = model.Email,
                FullName = model.Name,
                PhoneNumber = model.PhoneNo,
                SchoolId = model.SchoolId,
                ClassId = model.ClassId,
                PictureUrl = HostingServerInfo.Picture.GetCompleteUrl() + "/content/default.gif",
                Address = "",
                AboutMe = "About me",
                Gender = "",
                RegisterdOn = DateTime.UtcNow,
                Expiry = DateTime.UtcNow.AddDays(7),
                Trial = true,
                Subscribed = false,
                ConnectionId = code,
                RegisterAs = model.RegisterAs,
                Factor=2,
                RequestedforBrandAmbesdor = model.RequestedforBrandAmbesdor,
                Points=0,
                Board=model.Board,
                Student_Parent_Admin=model.WhoIAm
            };
        }

        private HttpResponseException HandleException(Exception ex)
        {
            Exception exx = ex.InnerException != null
                                ? ex.InnerException.InnerException != null ? ex.InnerException.InnerException : new Exception(ex.Message)
                                : new Exception(ex.Message);
            if (ex is EntityException && ex.Message.Contains("The underlying provider failed on Open"))
                return new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Unable to connect to server."));

            else if (exx is SqlException && exx.Message.Contains("Schools"))
                return new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Please select a existing school."));
            else if (exx is SqlException && exx.Message.Contains("Class"))
                return new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Please select a existing class."));
            return new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error occured in server."));
        }


        private void RoleAssign(User user)
        {
            var store = new RoleStore<IdentityRole>(dc);
            var manager = new RoleManager<IdentityRole>(store);
            if (!manager.RoleExists("student"))
            {
                var role = new IdentityRole { Name = "student" };
                var res = manager.Create(role);
                if (res.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "student");
                }
            }
            else
            {
                UserManager.AddToRole(user.Id, "student");
            }
        }



        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.Route("VerifyPhone")]
        public  IHttpActionResult VerifyOTP(VerifyPhoneNumberViewModel model)
        {
            var p = UserManager.Users.FirstOrDefault(i => i.PhoneNumber.Equals(model.PhoneNumber, StringComparison.InvariantCultureIgnoreCase) && i.ConnectionId.Equals(model.Code, StringComparison.InvariantCultureIgnoreCase));
            if (p!=null)
            {
                var result = ApplicationOAuthProvider.CreateProperties(p.UserName,p);
                
                var tokenExpiration =  TimeSpan.FromDays(15);

                ClaimsIdentity identity = new ClaimsIdentity("CustomType", ClaimTypes.Email, ClaimTypes.Role);

                identity.AddClaim(new Claim(ClaimTypes.Email, p.UserName));

                var props = new AuthenticationProperties()
                {
                    IssuedUtc = DateTime.UtcNow,
                    ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration),
                    AllowRefresh=true
                };

                var ticket = new AuthenticationTicket(identity, props);

                var accessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);
                var refresh = Startup.OAuthOptions.RefreshTokenFormat.Protect(ticket);

                JObject tokenResponse = new JObject(
                                            new JProperty("email", p.Email),
                                            new JProperty("access_token", accessToken),
                                            new JProperty("expiresIn", tokenExpiration.TotalSeconds),
                                            new JProperty("issuedUtc", ticket.Properties.IssuedUtc),
                                            new JProperty("expiresUtc", ticket.Properties.ExpiresUtc),
                                            new JProperty("refresh",refresh),
                                            new JProperty("Points", p.Points.ToString()),
                                            new JProperty("Factor", p.Factor.ToString()),
                                            new JProperty("SchoolId", p.SchoolId.ToString()),
                                            new JProperty("Subscription", p.Subscribed.ToString())
            );
                return Ok(tokenResponse);
            }
            else
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid OTP. Please try another one."));
            }

        }

        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.Route("ResendOTP")]
        public async Task<IHttpActionResult> ResendOTPAsync(VerifyPhoneNumberViewModel model)
        {

            if (UserManager.Users.Any(i => i.PhoneNumber.Equals(model.PhoneNumber, StringComparison.InvariantCultureIgnoreCase)))
            {
                var User =
                    UserManager.Users.FirstOrDefault(
                        i => i.PhoneNumber.Equals(model.PhoneNumber, StringComparison.InvariantCultureIgnoreCase));
                //SmsProviderInstance.SendMessage(User.PhoneNumber, HttpUtility.UrlEncode("Hi " + User.FullName + "! your verification code is:" + User.ConnectionId+". you get this sms on your re-send OTP request."));
                await SmsProviderInstance.SendOTPAsync(User.FullName, User.ConnectionId, User.PhoneNumber, "Resend");

                return Ok(new { MessageEvents="ok" });
            }
            else
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "You are not register with us. Plese register first to verify your account."));
            }

        }


        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.Route("ResetOTP")]
        public async Task<IHttpActionResult> ResetOTPAsync(VerifyPhoneNumberViewModel model)
        {

            if (dc.Users.Any(i => i.PhoneNumber.Equals(model.PhoneNumber, StringComparison.InvariantCultureIgnoreCase)))
            {
                var User =
                    dc.Users.FirstOrDefault(
                        i => i.PhoneNumber.Equals(model.PhoneNumber, StringComparison.InvariantCultureIgnoreCase));
                string code = string.Empty;
                Random generator = new Random();
                code = generator.Next(0, 1000000).ToString("D6");
                User.ConnectionId = code;
                dc.SaveChanges();
                //SmsProviderInstance.SendMessage(User.PhoneNumber, HttpUtility.UrlEncode("Hi " + User.FullName + "! your verification code is:" + User.ConnectionId + ". you get this sms on your re-send OTP request."));
                await SmsProviderInstance.SendOTPAsync(User.FullName, User.ConnectionId, User.PhoneNumber, "Resend");

                return Ok(new { MessageEvents = "ok" });
            }
            else
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "You are not register with us. Plese register first to verify your account."));
            }

        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("api/UserPaymentInfo")]
        public object GetUserPaymentInfo()
        {
            var userName = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i=>i.UserName==userName);
            if (user.Factor == 0)
            {
                user.Factor = 2;
            }
            if (dc.Booster.Any(i => i.UserId == user.Id))
            {
                var Booster = dc.Booster.OrderByDescending(i => i.ExpiryOn).FirstOrDefault(i => i.UserId == user.Id);
                if (Booster != null && Booster.ExpiryOn < DateTime.Now)
                {
                    user.Factor = 2;
                }
                else
                {
                    user.Factor = Booster.Factor;
                }

            }
            return user;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
