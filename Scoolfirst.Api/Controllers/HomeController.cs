﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace Scoolfirst.Api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }


        public ActionResult Notice()
        {
            return  Json(new { Notice = System.IO.File.ReadAllText($@"{AppDomain.CurrentDomain.BaseDirectory}\App_data\Notice.txt") },JsonRequestBehavior.AllowGet);
        }
    }
}
