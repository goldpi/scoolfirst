﻿using Scoolfirst.Model.Common;
using Scoolfirst.Model.Projects;
using Scoolfirst.ViewModel.Common;
using Scoolfirst.ViewModel.Common.Api;
using Scoolfirst.ViewModel.Feed;
using Scoolfirst.ViewModel.Products;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using Scoolfirst.ViewModel.WorkBook.Api;
using System;
using Scoolfirst.Model.RNM;
using Scoolfirst.Model.WorkBook;
using System.Text.RegularExpressions;
using Scoolfirst.ViewModel.Feed.Api;
using Scoolfirst.Model.Utility;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Scoolfirst.Api.Models;
using Scoolfirst.Model.Notifications;
using Scoolfirst.Model.MongoDatabase;

namespace Scoolfirst.Api.Controllers
{
    
    public class CommonController : ApiController
    {
        Scoolfirst.Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();

        [Route("api/Schools")]
        [HttpGet]
        public IEnumerable<Schools> GetSchools()
        {
            var list = dc.Schools;
            return Schools.FromListSchool(list);

        }

        [Route("api/activeAccount")]
        [HttpGet,Authorize]
        public object AccountActive()
        {
            var user = User.Identity.Name;
            //int schoolId = 0;
            var CurrentUser = dc.Users.Include(i => i.School)
                .FirstOrDefault(i => i.UserName == user);
            user.Debug();
            return new { Active = CurrentUser.Expiry > DateTime.Now ,Day=(CurrentUser.Expiry-DateTime.Now).Days};
        }


        [Route("api/SamplePapers/")]
        [HttpGet]
        public object SamplePapers(int ClassId,int SubjectId,int SchoolId)
        {
            return dc.SamplePapers.Where(i => i.ClassId == ClassId && i.SchoolId == SchoolId & i.SubjectId == SubjectId).
                Select(i=>new { Id= i.Id , ImageUrl = i.ImageUrl ,Title= i.Title, Description=i.Description}).ToList();
        }

        [Route("api/Resource")]
        [HttpGet]
        public Resource RegisterResource()
        {
            var r = new Resource();
            r.SchoolList = Schools.FromListSchool(dc.Schools.Include(i=>i.Area.City).Where(i=>i.Active==true)).ToList();
            r.Clasess = Class.FromListClasses(dc.Classes).ToList();
            return r;
        }

        [Route("api/SchoolBoard")]
        [HttpGet]
        [AllowAnonymous]
        public object SchoolBasedOnBoard()
        {
            var board = dc.Boards.ToList();
            return board;

        }

        [Route("api/Subscription")]
        [HttpGet]
        public object Subscription()
        {
            string sURL = "http://upgradejr.com/umbraco/api/subscription/getall";
            WebRequest wrGETURL = WebRequest.Create(sURL);
            Stream objStream = wrGETURL.GetResponse().GetResponseStream();
            StreamReader objReader = new StreamReader(objStream);
            var value = objReader.ReadToEnd();
            var tmp = JsonConvert.DeserializeObject<List<Subscription>>(value);


            return tmp;
        }

        [Route("api/Class")]
        [HttpGet]
        public IEnumerable<Class> GetClass()
        {
            var list = dc.Classes;
            return Class.FromListClasses(list);

        }

        [Route("api/Product")]
        [HttpGet]
        public IEnumerable<Product> GetProduct()
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            return dc.Products.Where(i=>i.Active==true).OrderByDescending(i=>i.AddedOn).ToList();
        }

        [Route("api/RoleModel")]
        [HttpGet]
        public IEnumerable<RoleModelVM> GetRoleModel(int Page=0,int size=4)
        {
            return dc.RoleModels
                .OrderByDescending(i=>i.On)
                .Skip(Page*size).Take(size)
                .ToRoleModeViewModel();
        }

        [Route("api/RoleModelByClass/{Id}")]
        [HttpGet]
        public IEnumerable<RoleModelVM> GetRoleModelByClass(int Id,int Page = 0, int size = 4)
        {
            return dc.RoleModels
                .Where(i=>i.Id==Id)
                .OrderByDescending(i => i.On)
                .Skip(Page * size).Take(size)
                .ToRoleModeViewModel();
        }


        [Route("api/BreakBrainFast/")]
        [HttpGet]
        public IEnumerable<KnowledgeNutritionViewModel> BreakBrainFast(int Page = 0, int size = 4)
        {
            //ToDo: Free feeds work [imran]
            return dc.BreakBrainFastsForAPI(Page, size);
        }
        

        [Route("api/ProductCategories/")]
        [HttpGet]
        public IEnumerable<ProductCategoryVM> getCategories()
        {
            dc.Configuration.ProxyCreationEnabled = false;
            return dc.ProductCategories.FromProductCategories();
        }

        [Route("api/Goals/")]
        [HttpGet]
        public IEnumerable<Goals> GetGoals()
        {
            dc.Configuration.ProxyCreationEnabled = false;
            return dc.Goals;
        }

        [Route("api/Projects/")]
        [HttpGet]
        public IEnumerable<Project> GetProjects(int classId)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            return dc.Projects.Where(i => i.CategoryId == classId);
        }

        [Route("api/ProjectsCategory/")]
        [HttpGet]
        public IEnumerable<ProjectCategory> GetProjectCat()
        {
            
            dc.Configuration.ProxyCreationEnabled = false;
            return dc.ProjectCategories.Include(i=>i.Projects).ToList();
        }


        [Route("api/SummaryUpgrade/")]
        [HttpGet]
        public IEnumerable<Summary> GetUpgrade(int? month)
        {
            int year = DateTime.UtcNow.Year;
            int thisMonth = DateTime.UtcNow.Month;
            month = month ?? thisMonth;
            if (month != thisMonth)
            {
                if (thisMonth < 4)
                {
                    year = month > 4 ? year - 1 : year;
                }
                else
                {
                    year = month < 4 ? year + 1 : year;
                }

            }
            return dc.Summary.Where(i => i.Type == SummaryType.UPGRADE && i.Month == month && i.Year == year).ToList();
        }


        [Route("api/Summary/")]
        [HttpGet]
        public IEnumerable<object> GetSummary(int sid,int cid,int? month)
        {
            int year = DateTime.UtcNow.Year;
            int thisMonth = DateTime.UtcNow.Month;
            month = month ?? thisMonth;
            if (month != thisMonth)
            {
                if (thisMonth < 4)
                {
                    year = month > 4 ? year - 1 : year;
                }
                else
                {
                    year = month < 4 ? year + 1 : year;
                }

            }
            var Groups = dc.Classes.Find(cid).Groups.Select(i => i.Id);
            // DateTime Date = DateTimeHelper.GetIST();
            var DateOnAbove = DateTimeHelper.GetIST().AddMonths(-1);
            var DateBelow = DateTimeHelper.GetIST().AddMonths(1);

            var ff = dc.Feeds
                .Where(i => i.OnDateTime >= DateOnAbove && i.OnDateTime <= DateBelow && Groups.Any(o => o == i.GroupId))
                .OrderBy(i => i.OnDateTime).ToList();

            foreach(var i in ff)
            {
                yield return new { Id = i.Id, Title = i.Title, Date = i.OnDateTime, PostId = i.PostId, Past = DateTimeHelper.GetIST() >= i.OnDateTime,Future =DateTimeHelper.GetIST() <= i.OnDateTime ,Content=i.Post.Tags};
            }
            //return ff;
        }

        [Route("api/WB/Subjects/")]
        [HttpGet]
        public object GetSubjects(int sid,int cid)
        {
            
            var subjects = dc.SchoolClassSubject.Where(i => i.ClassId == cid && i.SchoolId == sid)
                .Select(i => i.Subject).Select(i => new { Id = i.Id, DisplayName = i.DisplayName, Color = i.Color, SubjectName = i.SubjectName }).ToList();
            return new { Subjects = subjects, ClassId = cid, SchoolId = sid };
        }

        [Route("api/WB/GetBooksBySchoolClass/")]
        [HttpGet]
        public object GetBooks(int sid, int cid, string SubjectName,bool newver=false)
        {
            var scohool = dc.Schools.FirstOrDefault(i => i.Id == sid);
            if(scohool.UseMasterContent)
            {
                sid = dc.Schools.FirstOrDefault(i => i.IsMasterContent && i.BoardId == scohool.BoardId).Id;
            }
            var Subject = dc.Subjects.FirstOrDefault(i => i.DisplayName == SubjectName||i.SubjectName==SubjectName);
            BookClassSchoolSyllabus ret = null;
            if (dc.BookClassSchoolSyllabus.Any(i => i.SubjectId == Subject.Id && i.ClassId == cid && i.SchoolId == sid))
                ret = dc.BookClassSchoolSyllabus.FirstOrDefault(i => i.SubjectId == Subject.Id && i.ClassId == cid && i.SchoolId == sid);
            else
                ret = new BookClassSchoolSyllabus { /*BookId = 0*/ ClassId = cid, Map = "[]", SchoolId = sid, SubjectId = Subject.Id };
            var ob= JsonConvert.DeserializeObject(ret.Map);
            if (newver)
                if (User.Identity.IsAuthenticated)
                {
                    var userLog = dc.Users.FirstOrDefault(i => i.UserName == User.Identity.Name);
                    var results = dc.TestTrackings.Where(i => i.UserId == userLog.Id && i.Test.SubjectId == Subject.Id && i.IsPassable == true && i.ViewSolution == true).Select(o => new { o.Month }).ToList();
                    return new { Months = results, Book = ob };
                }

                else
                    return new { Book = ob };
            else
                return ob;
        }

       



        [Route("api/WB/GetBooksOfClass/")]
        [HttpGet]
        public object GetBookQuestion(int sid, int cid, string SubjectName)
        {

            return WB.GetBooksBySubjectSQL(dc, sid, cid, SubjectName);
        }


        [Route("api/WB/Question/")]
        [HttpGet]
        public object GetQuestions(int UnitId)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            var re= dc.Questions.Where(i => i.ChapterId == UnitId)
                .OrderBy(i=>i.TypeOrder).ThenBy(i=>i.Order)
                .Select(i=>new {Id=i.Id, Title=i.Title, Answer=i.Answer, Refrence =i.Refrence , Type=i.QuestionType,Order=i.Order,TO=i.TypeOrder })
                .GroupBy(i=>new { i.Type ,i.TO}).OrderBy(i=>i.Key.TO).Select(i=> new {Type=i.Key.Type,Questions=i.OrderBy(io=>io.Order) }).ToList();
            return re;
        }


        [Route("api/NAO")]
        [HttpGet]
        public IEnumerable<object> NAO(int level,int @class)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            return dc.Olympiads.Where(i=>i.Class==@class&&i.Level==level).ToList();
        }
        [Route("api/Rnm")]
        [HttpGet]
        public IEnumerable<ReasoningRoot> RnmPost(string @class)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            return dc.ReasoningRoots.Where(i => i.Class == @class).ToList();
        }

        [Route("api/SyllabusDate")]
        [HttpGet]
        public IEnumerable<object> SyllabusDate(int @class,int school)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            return dc.SyllabusDates.Include(i=>i.Quiz).Where(i => i.Class == @class && i.School == school).ToList();
        }

        [Route("api/Sub")]
        [HttpGet]
        public IEnumerable<Subject> Sub()
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            return dc.Subjects.ToList();
        }

        [Route("api/QuizesBySCSub")]
        [HttpGet]
        public IEnumerable<Quiz> Quiz(string SchoolId,string ClassId,string Subject)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            return dc.Quizes.Where(i=>i.School==SchoolId&&i.Class==ClassId&&i.Subject==Subject).ToList();
        }


        [Route("api/QuizesBySC")]
        [HttpGet]
        public IEnumerable<Quiz> Quiz(string SchoolId, string ClassId)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            return dc.Quizes.Where(i => i.School == SchoolId && i.Class == ClassId ).ToList();
        }

       
        
        [Route("api/points/update"),HttpPost,Authorize]
        public object Update(Point Points)
        {
            var user = User.Identity.Name;

            var CurrentUser = dc.Users
                .FirstOrDefault(i => i.UserName == user);
            CurrentUser.Points += Points.Points;
            dc.SaveChanges();
            return Points;
        }

        [Route("api/Notification/date"), HttpGet]
        public object Notification(DateTime date)
        {
            var notificationtest = new NotificationManagerRepo().ListAllPer();
            var dat = DateTimeHelper.GetIST();
            var dat_2 = dat.AddMinutes(-15);
            var p = new List<Notification>();
            foreach (var i in notificationtest)
            {
               
                var dat2 = DateTimeHelper.GetIST().Subtract(new TimeSpan(0, 0, 20));
                if (i.Date <= dat2 && i.Date >= date)
                {                 
                    p.Add(i);   
                }

            }
            
            return new { Notice = p , Sync = dat};
        }
        [Route("api/Notification"), HttpGet]
        public object Notification()
        {
            var notificationtest = new NotificationManagerRepo().ListAllPer();
            var dat = DateTimeHelper.GetIST();
            var dat_2 = dat.AddMinutes(-15);
            var p = new List<Notification>();
            foreach (var i in notificationtest)
            {
                
                var dat2 = DateTimeHelper.GetIST().Subtract(new TimeSpan(0, 0, 20));
                if (i.Date <= dat2&& i.Date>=dat_2)
                {
                    p.Add(i);
                }

            }

            return new { Notice = p, Sync = dat };
        }

        [Route("api/LeaderBoard"),HttpGet]
        public object Leader(int size=100)
        {
          return  dc.Users.OrderByDescending(i => i.Points).Take(size).Select(o => new { Name = o.FullName, Points = o.Points, Picture = o.PictureUrl }).ToList();
        }



        [Route("api/Date"), HttpGet]
        public object Date()
        {
            return "26/11/2018 21:00:00";
        }





        [Route("api/Updates")]
        [HttpGet]
        public object Updates(DateTime date)
        {
            if (dc.AppUpdates.Any(i => i.AddedOn <= date))
            {
                var data = dc.AppUpdates.Where(i => i.AddedOn <= date).ToList().Last();
                if (data != null)
                    return data;


            }

            return new HttpError("400");
        }
        string[] ListofSpam = new[] { "is", "a", "an", "the", "or", "and", "on", "like", "that","no","who","why","where","when","what", "not",  "now", "non" };
   // }
    }
}
