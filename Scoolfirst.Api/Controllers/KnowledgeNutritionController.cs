﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Scoolfirst.Model.Context;
using Scoolfirst.ViewModel.Feed;
using System.Data.Entity;
using Scoolfirst.Model.Utility;

namespace Scoolfirst.Api.Controllers
{
    
    public class KnowledgeNutritionController : ApiController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();

        [Route("api/knowledgenutrition")]
        [HttpGet]
        public object GetFeeds(int page = 0,int size=3)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            //var User= this.User.Identity.Name;
            //var UserId = dc.Users.FirstOrDefault(i => i.UserName == User);
            
            var Catergory = PostType.KNOWLEDGE_NUTRITION;

            //var Groups = dc.Classes.Include(i=>i.Groups).FirstOrDefault(i=>i.Id==UserId.ClassId).Groups.Select(i => i.Id);
            DateTime Date = DateTimeHelper.GetIST();

           var items= dc.Feeds.Include(i => i.Post)
                .Include(i=>i.Post.Videos)
                .Where(i => i.OnDateTime <= Date  && i.Post.Type == Catergory)
                .OrderByDescending(i => i.OnDateTime)
                .Skip(size * page)
                .Take(size)
                .Select(i=>new {Titlie=i.Title,Post=new { Content=i.Post.Content, Title=i.Post.Title, Videos=i.Post.Videos } })
                .ToList();

            return items;

        }
    }
}
