﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Projects;
using System.Data.Entity;
namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class CreativeCanvasController : ApiController
    {
       
        private ScoolfirstContext dc = new ScoolfirstContext();

        [Route("api/creativecanvas")]
        [HttpGet]

        public IEnumerable<Project> GetProject(int page = 0,int size=3)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var data = dc.Projects.Include(i => i.Category).Where(i=>i.Active).OrderBy(i=>i.AddedOn).Skip(size * page).Take(size).ToList();
            return data;
        }
    }
}
