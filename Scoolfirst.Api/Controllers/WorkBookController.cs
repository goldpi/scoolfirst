﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.ViewModel.ConnectedSchool;
using Scoolfirst.ViewModel.WorkBook.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using Scoolfirst.Api.Models;

namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class WorkBookController : ApiController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();

        [HttpGet]
        [AllowAnonymous]
        [Route("api/workbook/Subjects")]
        public object Subjects()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity.Name;
                int schoolId = 0;
                var CurrentUser = dc.Users.Include(i => i.School)
                    .FirstOrDefault(i => i.UserName == user);
                schoolId = CurrentUser.SchoolId ?? 0;
                if (CurrentUser.School.UseMasterContent)
                {
                    schoolId = dc.Schools.FirstOrDefault(i => i.IsMasterContent == true).Id;
                }
                var subjects = dc.SchoolClassSubject.Where(i => i.ClassId == CurrentUser.ClassId && i.SchoolId == schoolId)
                    .Select(i => i.Subject).Select(i => new { Id = i.Id, DisplayName = i.DisplayName, Color = i.Color, SubjectName = i.SubjectName }).ToList();
                return new { Subjects = subjects, ClassId = CurrentUser.ClassId, SchoolId = schoolId };
            }
            else
            {
                var classid = dc.Classes.ToList();
                var subjects = dc.SchoolClassSubject.Where(i => i.ClassId == classid.First().Id && i.SchoolId == 1036)
                    .Select(i => i.Subject).Select(i => new { Id = i.Id, DisplayName = i.DisplayName, Color = i.Color, SubjectName = i.SubjectName }).ToList();
                return new { Subjects = subjects, ClassId = classid.First().Id, SchoolId = 1036 };
            }
        }

        [Route("api/MySubjects")]
        [HttpGet]
        [AllowAnonymous]
        public object MySubjects()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity.Name;
                int schoolId = 0;
                var CurrentUser = dc.Users.Include(i => i.School)
                    .FirstOrDefault(i => i.UserName == user);
                schoolId = CurrentUser.SchoolId ?? 0;
                if (CurrentUser.School.UseMasterContent || schoolId == 0)
                {
                    schoolId = dc.Schools.FirstOrDefault(i => i.IsMasterContent == true).Id;
                }
                var Subjects = dc.SchoolClassSubject.Where(i => i.ClassId == CurrentUser.ClassId && i.SchoolId == schoolId)
                    .Select(i => i.Subject).Select(i => new { i.Id, i.DisplayName, i.Color, i.SubjectName }).ToList();
                return new { Subjects, CurrentUser.ClassId, SchoolId = schoolId };
            }
            else
            {
                var classid = dc.Classes.FirstOrDefault(i => i.NumericDisplay == 6);
                var Subjects = dc.SchoolClassSubject.Where(i => i.ClassId == classid.Id && i.SchoolId == 1036)
                    .Select(i => i.Subject).Select(i => new { i.Id, i.DisplayName, i.Color, i.SubjectName }).ToList();
                return new { Subjects, ClassId = classid.Id, SchoolId = 1036 };
            }
        }

        [Route("api/workbook/book/{Id}")]
        [HttpGet]
        public QuizVM GetByBook(int Id)
        {
            return QuizVM.BookQuestion(dc, Id);
        }

        [Route("api/WorkBook/Boards")]
        [HttpGet]
        [AllowAnonymous]
        public object GetBoards()
        {
            return dc.Boards.Select(i => new { BoardName = i.Name, BoardId = i.Id }).ToList();
        }

        [Route("api/WorkBook/Schools/{Id}")]
        [HttpGet]
        [AllowAnonymous]
        public object GetSchoolss(int Id)
        {
            return dc.Schools.Where(i => i.BoardId == Id).Select(i => new { SchoolName = i.SchoolName, SchoolId = i.Id }).ToList();
        }

        [Route("api/WorkBook/GetBooksBySchoolClass/")]
        [HttpGet]
        public object GetBooks(string SubjectName)
        {
            var uid = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == uid);

            return WB.GetBooksBySubject(dc, user.SchoolId.Value, user.ClassId, SubjectName);
        }

        [Route("api/workbook/bookUnit/{Id}")]
        [HttpGet]
        public IEnumerable<Object> BookUnit(int Id)
        {
            return dc.Books.Find(Id).Chapters.Select(i => new { ChapterName = i.Name, Id = i.Id /*,Month=i.Month*/});
        }

        [Route("api/workbook/Quiz/{Id}")]
        [HttpGet]
        public QuizVM getByQuiz(Guid Id)
        {
            return QuizVM.Quiz(dc, Id);
        }

        [Route("api/Book/")]
        [HttpGet]
        public IEnumerable<Books> GetBbooks()
        {
            return dc.Books;
        }

        [Route("api/Books/")]
        [HttpGet]
        public object GetBbooks(int Board, int classId)
        {
            var daya = dc.Books.Include(i => i.Chapters)
                .Where(i => i.Global == true && i.BoardId == Board && i.ClassesId == classId)
                 .Select(o => new
                 {
                     ImageUrl = o.ImageUrl,
                     Title = o.Title,
                     Id = o.Id,
                     Subject = o.Subject.DisplayName,
                     Author = o.Author,
                     Chapters = o.Chapters.Select(p => new { Month = p.Month, Id = p.Id, Name = p.Name }).GroupBy(i => i.Month).Select(or => new { Month = or.Key, Chapters = or })
                 }).ToList();
            return daya;
        }

        [Route("api/TossTopic/{SubjectId}")]
        [HttpGet]
        public object TossTopic(int SubjectId)
        {
            var uid = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == uid);
            return TossTopicQuery(user.ClassId, user.SchoolId.Value, SubjectId);
        }

        [Route("api/TossTopic/{SubjectId}/{search}")]
        [HttpGet]
        public object TossTopicSearch(int SubjectId, string Search)
        {
            var uid = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == uid);
            List<ConnectedSchoolAggregate> list = new List<ConnectedSchoolAggregate>();
            string[] ListofSpam = new[] { "is", "a", "an", "the", "or", "and", "on", "like", "that", "no", "who", "why", "where", "when", "what", "not", "now", "non", "of" };
            var searcterm = Search.Split(' ').ToArray().Where(i => !ListofSpam.Any(o => i.ToLower() == o.ToLower())).ToArray();
            searcterm.Debug();
            foreach (var i in searcterm)
            {
                var result = TossSearch(user.ClassId, SubjectId, i);
                i.Debug();
                "Class".Debug();
                user.ClassId.Debug();
                result.Debug();
                list.AddRange(result);
            }
            return list.Distinct();
        }

        private List<ConnectedSchoolAggregate> TossSearch(int ClassId, int SubjectId, string search)
        {
            return dc.Database.SqlQuery<Chapter>($@"SELECT Distinct
                                                        Chapters.* ,Books.*
                                                        FROM SchoolClassSubjects INNER JOIN
                                                        Books ON SchoolClassSubjects.ClassId = Books.ClassesId And SchoolClassSubjects.SubjectId = Books.SubjectId INNER JOIN
                                                        ChapterBooks On Books.Id = ChapterBooks.Books_Id Inner Join
                                                        Chapters ON ChapterBooks.Chapter_Id = Chapters.Id
                                                        WHERE (Chapters.ClassesId= {ClassId} AND SchoolClassSubjects.SubjectId={SubjectId}) AND Chapters.SearchKeyWord Like '%{search}%'")
                                                     .Select(cpr => new ConnectedSchoolAggregate { Id = cpr.Id.ToString(), Title = cpr.Name, Books = cpr.Books }).ToList();
        }

        private List<ConnectedSchoolAggregate> TossTopicSearch(int ClassId, int schoolId, int SubjectId, string search)
        {
            if (schoolId == 0)
            {
                return dc.Database.SqlQuery<Chapter>($@"SELECT Distinct
                                                        Chapters.* ,Books.*
                                                        FROM SchoolClassSubjects INNER JOIN
                                                        Books ON SchoolClassSubjects.ClassId = Books.ClassesId And SchoolClassSubjects.SubjectId = Books.SubjectId INNER JOIN
                                                        ChapterBooks On Books.Id = ChapterBooks.Books_Id Inner Join
                                                        Chapters ON ChapterBooks.Chapter_Id = Chapters.Id
                                                        WHERE (Chapters.ClassesId = {ClassId} AND SchoolClassSubjects.SubjectId={SubjectId}) AND Chapters.SearchKeyWord Like '%{search}%'")
                                                         .Where(i => i.Trending == true)
                                                         .Select(cpr => new ConnectedSchoolAggregate { Id = cpr.Id.ToString(), Title = cpr.Name, Books = cpr.Books }).ToList();
            }
            else
            {
                return dc.Database
                    .SqlQuery<Chapter>($@"SELECT Distinct
                                   Chapters.* ,Books.*
                                        FROM SchoolClassSubjects INNER JOIN
                                        Books ON SchoolClassSubjects.ClassId = Books.ClassesId
                                                And
                                                SchoolClassSubjects.SubjectId = Books.SubjectId INNER JOIN
                                        ChapterBooks On Books.Id = ChapterBooks.Books_Id Inner Join
                                        Chapters ON ChapterBooks.Chapter_Id = Chapters.Id
                                    WHERE (Chapters.ClassesId={schoolId} AND SchoolClassSubjects.ClassId = {ClassId} AND SchoolClassSubjects.SubjectId={SubjectId}) AND Chapters.SearchKeyWord Like '%{search}%'")
                                .Where(i => i.Trending == true)
                                .Select(cpr => new ConnectedSchoolAggregate { Id = cpr.Id.ToString(), Title = cpr.Name, Books = cpr.Books }).ToList();
            }
        }

        private List<ConnectedSchoolAggregate> TossTopicQuery(int ClassId, int schoolId, int SubjectId)
        {
            if (schoolId == 0)
            {
                return dc.Database.SqlQuery<Chapter>($@"SELECT Distinct
                                                        Chapters.*, Books.*
                                                        FROM SchoolClassSubjects INNER JOIN
                                                        Books ON SchoolClassSubjects.ClassId = Books.ClassesId And SchoolClassSubjects.SubjectId = Books.SubjectId INNER JOIN
                                                        ChapterBooks On Books.Id = ChapterBooks.Books_Id Inner Join
                                                        Chapters ON ChapterBooks.Chapter_Id = Chapters.Id
                                                        WHERE (Chapters.ClassesId = {ClassId} AND SchoolClassSubjects.SubjectId={SubjectId})")
                                                         .Where(i => i.GlobalTrending == true)
                                                         .Select(cpr => new ConnectedSchoolAggregate { Id = cpr.Id.ToString(), Title = cpr.Name, Books = cpr.Books }).ToList();
            }
            else
            {
                return dc.Database.SqlQuery<Chapter>($@"SELECT Distinct
                                                            Chapters.* ,Books.*
                                                                FROM SchoolClassSubjects INNER JOIN
                                                                Books ON SchoolClassSubjects.ClassId = Books.ClassesId
                                                                      And
                                                                        SchoolClassSubjects.SubjectId = Books.SubjectId INNER JOIN
                                                                ChapterBooks On Books.Id = ChapterBooks.Books_Id Inner Join
                                                                Chapters ON ChapterBooks.Chapter_Id = Chapters.Id
                                                            WHERE (Chapters.ClassesId={schoolId} AND SchoolClassSubjects.ClassId = {ClassId} AND SchoolClassSubjects.SubjectId={SubjectId})")
                                                        .Where(i => i.GlobalTrending == true)
                                                        .Select(cpr => new ConnectedSchoolAggregate { Id = cpr.Id.ToString(), Title = cpr.Name, Books = cpr.Books }).ToList();
            }
        }
    }
}