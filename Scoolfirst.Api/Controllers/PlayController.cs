﻿using System;
using System.Linq;
using System.Web.Http;
using Scoolfirst.Model.PlayReasoning;
using Scoolfirst.Model.Context;
using System.Data.Entity;
using System.Web.Http.Description;
using Newtonsoft.Json.Linq;
using Scoolfirst.Api.Models;
using Scoolfirst.Model.VirtualCompetetion;

namespace Scoolfirst.Api.Controllers
{
    [System.Web.Http.Authorize]
    public class PlayController : ApiController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();
        [Route("api/playassessment")]
        [AllowAnonymous]
        public object GetPlay()
        {
            if (User.Identity.IsAuthenticated) {
                var UserName = User.Identity.Name;
                var user = dc.Users.FirstOrDefault(i => i.UserName == UserName);
                dc.Configuration.LazyLoadingEnabled = false;
                dc.Configuration.ProxyCreationEnabled = false;
                var play = dc.PlayRoots.OrderByDescending(i => i.Id).GroupBy(i => i.Subject)
                    .Select(i => new { Subject = i.Key, Value = i }).ToList();
                play.Debug();
                var attempted = dc.PlayAttempts.Where(i => i.UserId == user.Id).Select(i => i.PlayId).Distinct();
                attempted.Debug();
                return new {  play,  attempted };
            }
            else
            {
                dc.Configuration.LazyLoadingEnabled = false;
                dc.Configuration.ProxyCreationEnabled = false;
                var play = dc.PlayRoots.OrderByDescending(i => i.Id).GroupBy(i => i.Subject)
                    .Select(i => new { Subject = i.Key, Value = i }).ToList();
                return new {  play };
            }
            
           
        }

        // todo: pull data according to date time
        /// <summary>
        /// To get the question of subject.
        /// Model:- <see cref="Scoolfirst.Model.PlayReasoning.PlayRoot"/> 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Play/Test/{name}")]
        [AllowAnonymous]
        public object  GetPlayQuestion(string name)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
           // var test = dc.PlayRoots.OrderByDescending(i => i.Id).Where(i => i.Subject == name).ToList();

           var test = dc.PlayRoots.OrderByDescending(i => i.Id).Where(i=>i.Subject==name && (i.StartsOn<=DateTimeOffset.UtcNow || i.StartsOn==null)).ToList();
            return test;
        }
        [HttpGet]
        [Route("api/StartTest/{id}")]
        public object StartTest(long id)
        {
            var user = User.Identity.Name;
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var test = dc.PlayRoots.Include(i => i.QuestionsSet)
                .Include(i => i.QuestionsSet.Select(i_ => i_.Option))
                .FirstOrDefault(i => i.Id == id);

           var result= dc.PlayAttempts.Any(i => i.User.UserName == user && i.PlayId == id);
            result.Debug();
            return new { test = test, appeared = result };
        }

        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("api/play/SaveResult")]
        public IHttpActionResult SaveAnswers([FromBody]JObject data)//Guid[] AnswerId,int[] ReasoningId,Guid[] QuesionId)
        {
           
            Guid[] AnswerId;
            int[] ReasoningId;
            Guid[] QuesionId;
            var Iden = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == Iden);
            if (data["AnswerId"].Count() > 1)
            {
                AnswerId = data["AnswerId"].ToObject<Guid[]>();
                ReasoningId = data["ReasoningId"].ToObject<int[]>();
                QuesionId = data["QuesionId"].ToObject<Guid[]>();
            }
            else
            {
                AnswerId = new Guid[] { data["AnswerId"].ToObject<Guid>() };
                ReasoningId = new int[] { data["ReasoningId"].ToObject<int>() };
                QuesionId = new Guid[] { data["QuesionId"].ToObject<Guid>() };
            }
            var Attempt = new PlayAttempt
            {
                Id = Guid.NewGuid(),
                On = DateTime.UtcNow,
                PlayId = ReasoningId[0],
                UserId = user.Id
            };

            dc.PlayAttempts.Add(Attempt);
            dc.SaveChanges();
            PlayAnswers[] Answer = new PlayAnswers[AnswerId.Count()];
            for (int i = 0; i < AnswerId.Count(); i++)
            {
                Answer[i] = new PlayAnswers
                {
                    Id = Guid.NewGuid(),
                    AttemptId = Attempt.Id,
                    OptionId  = AnswerId[i],
                    QuestionId = QuesionId[i],
                    PlayId = ReasoningId[i]
                };
            }
            dc.PlayAnswers.AddRange(Answer);
            dc.SaveChanges();

            
            
            return Ok(1);
        }

        [HttpGet]
        [Route("api/playReview/{id}")]
        public object Result(int Id)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            var UserName = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == UserName);
            var result = dc.PlayAttempts.Include(i=>i.Play).FirstOrDefault(i => i.UserId == user.Id && i.PlayId == Id);
            var attempts = dc.PlayAnswers.Include(i=>i.Question)
                .Include(i=>i.Question.Option).Where(i => i.AttemptId == result.Id)
                .Select(i=> new { Question=new { Query= i.Question.Query , Option=i.Question
                .Option.Select(p=>new { Answer=p.Answer,Correct=p.Correct ,Id=p.Id}),
                    Solution =i.Question.Solution, ShowSolution=i.Question.ShowSolution } , OptionId = i.OptionId })
                .ToList();
            attempts.Debug();
            return new { result = result, attempts = attempts };
        }


        [HttpGet]
        [Route("api/playPoints")]
        public object Points()
        {
            var user = User.Identity.Name;
            var userInfo = dc.Users.FirstOrDefault(i => i.Email == user);
            return new { userInfo.Points, Booster = userInfo.Factor };
        }


        
    }


   
}
