﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;

namespace Scoolfirst.Api.Controllers
{
    public class TextNoticeController : ApiController
    {
        // GET api/<controller>
        public object Get()
        {
            return File.ReadAllText($@"{AppDomain.CurrentDomain.BaseDirectory}\App_data\Notice.txt");
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public string Post(TextNotice value)
        {
            //if (RequestContext.Principal.Identity.IsAuthenticated)
            File.WriteAllText($@"{AppDomain.CurrentDomain.BaseDirectory}\App_data\Notice.txt", value.Value);
            return value.Value;

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }

    public class TextNotice
    {
        public string Value { get; set; }
    }
}