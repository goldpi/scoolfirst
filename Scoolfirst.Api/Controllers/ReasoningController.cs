﻿using Scoolfirst.Model.RNM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Web.Http.Description;
using Scoolfirst.ViewModel.Reasoning;
using Newtonsoft.Json.Linq;

namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class ReasoningController : ApiController
    {

        private Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();


        [HttpGet]
        [Route("api/Reasoning/")]
        [AllowAnonymous]
        public Scoolfirst.ViewModel.Reasoning.ReasoningApiViewModel ReasoningRoots(string level)
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity.Name;
                var LoggedUser = dc.Users.FirstOrDefault(i => i.UserName == user);
                dc.Configuration.LazyLoadingEnabled = false;
                dc.Configuration.ProxyCreationEnabled = false;
                var re = dc.ReasoningRoots.Where(i => i.Level == level).OrderBy(o => o.Order).ToList();
                var ret = new Scoolfirst.ViewModel.Reasoning.ReasoningApiViewModel { Data = re };

                var tracker = dc.ReasoningTracker
                        .Where(i => i.UserId == LoggedUser.Id && i.Level == level)
                        .OrderBy(i => i.OnDateTime)
                        .ToList();

                var last = tracker.LastOrDefault();
                ret.Tracker = last;
                ret.Current = tracker.Count();
                return ret;

            }
            else
            {
                
                dc.Configuration.LazyLoadingEnabled = false;
                dc.Configuration.ProxyCreationEnabled = false;
                var re = dc.ReasoningRoots.Where(i => i.Level == level).OrderBy(o => o.Order).ToList();
                var ret = new Scoolfirst.ViewModel.Reasoning.ReasoningApiViewModel { Data = re }; 
                ret.Tracker = null;
                ret.Current = 0;
                return ret;
            }
            
        }
        [HttpGet]
        [Route("api/Reasoning/Start/{id}")]
        public Model.RNM.ReasoningTracker StartLesseon(string Level,long id)
        {
            var user = User.Identity.Name;
            var LoggedUser = dc.Users.FirstOrDefault(i => i.UserName == user);
            ReasoningTracker trcker = null;
            var rmnid = dc.ReasoningRoots
                   .Where(i => i.Level == Level && i.IsLessonOrExam != ReasoningRootType.Single)
                   .OrderBy(i => i.Order)
                   .Skip((int)id)
                   .Take(1)
                   .FirstOrDefault()
                   .Id;
            if (dc.ReasoningTracker.Count(i => i.UserId == LoggedUser.Id && i.Level == Level) > 0)
            {
                var tracker = dc.ReasoningTracker
                    .Include(i => i.RnmPost)
                    .Where(i => i.UserId == LoggedUser.Id && i.Level == Level)
                    .OrderBy(i => i.OnDateTime)
                    .ToList();
                var last = tracker.LastOrDefault();
                
                if (last.Complete)
                {
                    trcker = new ReasoningTracker { OnDateTime = DateTime.UtcNow, RnmPostId = rmnid, UserId = LoggedUser.Id, Level = Level };
                    dc.ReasoningTracker.Add(trcker);

                }
            }
            else
            {

                trcker = new ReasoningTracker { OnDateTime = DateTime.UtcNow, RnmPostId = rmnid, UserId = LoggedUser.Id, Level = Level };
                dc.ReasoningTracker.Add(trcker);
            }
            dc.SaveChanges();

            return trcker;
        }

        [HttpGet]
        [Route("api/Reasoning/Test/{id}")]
        public object Test(long id)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var Iden = User.Identity.Name;
            var test = dc.ReasoningRoots.Include(i => i.Questions)
                .Include(i => i.Questions.Select(i_ => i_.Option))
                .FirstOrDefault(i => i.Id == id);
            var user = dc.Users.FirstOrDefault(i => i.UserName == Iden);
            if (dc.ReasoningAttemp.Any(a=>a.ReasoningId==id&&a.UserId==user.Id))
            {
                var po = dc.ReasoningAttemp.FirstOrDefault(i => i.ReasoningId == id && i.UserId == user.Id).Id;
                var reson = dc.ReasoningAnswers
                .Include(i => i.Option)
                .Where(i => i.ReasoningId == id && i.UserId == user.Id&&i.AttempId==po);

                return new
                {
                    Test = true,
                    Reasoning = test,
                    Correct = reson.Count(p => p.Option.Correct),
                    Wrong = reson.Count(p => !p.Option.Correct),
                    Skipped = test.Questions.Count() - reson.Count()
                };
            }

            return new
            {
                Test = false,
                Reasoning = test,
            };
        }

        [Route("api/Reasoning/result/{Id}")]
        [HttpGet]
        public object GetRootResult(long Id)
        {
            
           // this.Configuration.Formatters.
            var Iden = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == Iden);
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var po = dc.ReasoningAttemp.FirstOrDefault(i => i.ReasoningId == Id && i.UserId == user.Id).Id;
            var reson = dc.ReasoningAnswers
                .Where(i => i.ReasoningId == Id && i.UserId == user.Id&&i.AttempId==po)
                .Select(i=> new {QuestionId=i.QuestionId,OptionId=i.OptionId ,Correct=i.Option.Correct }).ToList();
            var test = dc.ReasoningRoots.Include(i => i.Questions)
               .Include(i => i.Questions.Select(i_ => i_.Option))
               .FirstOrDefault(i => i.Id == Id);
            var resonResult = test.Questions.Select(i => new { Question = new {Solution=i.Solution,ShortOrder=i.ShortOrder,Id=i.Id,Option= i.Option.Select(_=> new {Id=_.Id,Answer=_.Answer,Correct=_.Correct, QuestionId=_.QuestionId,ShortOrder=_.ShortOrder }), Query=i.Query }, Ans = reson.FirstOrDefault(p => p.QuestionId == i.Id) });
            return new { Test = true, Reasoning = reson,  render = resonResult };

        }



        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("api/Reasoning/SaveResult")]
        public IHttpActionResult SaveAnswers([FromBody]JObject data)//Guid[] AnswerId,int[] ReasoningId,Guid[] QuesionId)
        {
            Guid[] AnswerId;
            int[] ReasoningId  ;
            Guid[] QuesionId  ;
            if (data["AnswerId"].Count()>1)
            {
            AnswerId = data["AnswerId"].ToObject<Guid[]>();
            ReasoningId = data["ReasoningId"].ToObject<int[]>();
            QuesionId = data["QuesionId"].ToObject<Guid[]>();
            }
            else
            {
            AnswerId = new Guid[] { data["AnswerId"].ToObject<Guid>() };
            ReasoningId =new int[] { data["ReasoningId"].ToObject<int>() };
            QuesionId =new Guid[] { data["QuesionId"].ToObject<Guid>() };
            }

            ReasnionAnswerBack[] Answer = new ReasnionAnswerBack[AnswerId.Count()];
            for (int i = 0; i < AnswerId.Count(); i++)
            {
                Answer[i] = new ReasnionAnswerBack
                {
                    AnswerId = AnswerId[i],
                    QuesionId = QuesionId[i],
                    ReasoningId = ReasoningId[i]
                };
            }
            //this.ActionContext.Request.
           // ReasnionAnswerBack[] Answer = data["ReasnionAnswerBack"].ToObject<ReasnionAnswerBack[]>();
            var Iden = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == Iden);
            var first = Answer[0];
            var Attempt = new ReasoningAttemp
            {
                Id = Guid.NewGuid(),
                On = DateTime.UtcNow,
                ReasoningId = first.ReasoningId,
                UserId = user.Id
            };



            dc.ReasoningAttemp.Add(Attempt);
            dc.SaveChanges();

            List<ReasoningAnswers> ans = new List<ReasoningAnswers>();
            //if (db.ReasoningAnswers.Any(i => i.ReasoningId==first.ReasoningId&& i.UserId == user.Id))
            //{
            //    return Ok(0);
            //}
            foreach (var i in Answer)
            {
                ans.Add(new ReasoningAnswers { Id = Guid.NewGuid(), OptionId = i.AnswerId, UserId = user.Id, QuestionId = i.QuesionId, ReasoningId = i.ReasoningId, AttempId = Attempt.Id });
            }
            dc.ReasoningAnswers.AddRange(ans);
            dc.SaveChanges();

            var reasoinig = dc.ReasoningRoots.Find(first.ReasoningId);
            var tracking = dc.ReasoningTracker.FirstOrDefault(i => i.UserId == user.Id && i.RnmPostId == reasoinig.Id);
            Attempt = dc.ReasoningAttemp.Include(i => i.Ans).Include(i => i.Ans.Select(o => o.Option)).FirstOrDefault(o => o.Id == Attempt.Id);



            if (Attempt.Ans.Count(i => i.Option.Correct == true) >= reasoinig.MinQuestionToPass)
            { tracking.TestPassed = true; dc.SaveChanges(); }
            return Ok(1);
        }
    }
}
