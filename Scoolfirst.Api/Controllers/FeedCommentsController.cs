﻿using Scoolfirst.ViewModel.Feed;
using Scoolfirst.ViewModel.Feed.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class FeedCommentsController : ApiController
    {
        Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();
        
        public IEnumerable<ApiFeedComment> Get(Guid? Id,int page = 0, int size = 3)
        {
            if (Id == null)
            {
                BadRequest();
            }
            FeedItems itemss = new FeedItems();

            var items = itemss.GetComments(dc, User.Identity.Name, Id.Value, page, size);

            return items;
        }

        public ApiFeedComment Post(ApiFeedComment model)   
        {
            FeedItems itemss = new FeedItems();
            return ApiFeedComment.FromComment(itemss.AddComment(dc, model.Comment, model.FeedId, User.Identity.Name), User.Identity.Name);
        }
        /// <summary>
        /// Removes the Comment
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool Delete(Guid Id)
        {
            FeedItems itemss = new FeedItems();
            return itemss.RemoveComment(dc, Id, User.Identity.Name);
        }
    }
}
