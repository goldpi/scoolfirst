﻿using Scoolfirst.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using Scoolfirst.Model.Camp;
using System.Web.Http.Description;
using Newtonsoft.Json.Linq;
using Scoolfirst.ViewModel.Camp;

namespace Scoolfirst.Api.Controllers
{
    public class CampController : ApiController
    {
        private ScoolfirstContext _context = new ScoolfirstContext();

        [Route("api/Camp")]
        public Object GetCamps()
        {
            if (User.Identity.IsAuthenticated)
            {
                var UserName = this.User.Identity.Name;
                var UserId = _context.Users.FirstOrDefault(i => i.UserName == UserName);
                var Subscribed = _context.SubscribedCamps.Any(i => i.UserId == UserId.Id) ? _context.SubscribedCamps.Include(i => i.Camp).Include(i => i.Camp.CampPosts).Where(i => i.UserId == UserId.Id).Select(i => i.Camp).ToList() : (Object)false;
                var Categories = _context.CampCategories.Where(i => i.Active == true).ToList();
                return new { Subscribed, Categories };
            }
            else
            {
               
                var Categories = _context.CampCategories.Where(i => i.Active == true).ToList();
                return new { Categories };
            }
            
        }

        [Route("api/Camp/save")]
        [HttpPost]
        public Object GetCamps(VmCampTo CategoriesSubs)
        {
           
            var UserName = this.User.Identity.Name;
            var UserId = _context.Users.FirstOrDefault(i => i.UserName == UserName);
            if (CategoriesSubs.CategoriesSubs.Count() > 0 && _context.SubscribedCamps.Any(i => i.UserId == UserId.Id))
            {
                var sub = _context.SubscribedCamps.Where(i => i.UserId == UserId.Id);
                _context.SubscribedCamps.RemoveRange(sub);
                _context.SaveChanges();
            }
            
            foreach (var item in CategoriesSubs.CategoriesSubs)
            {
                var i = new SubscribedCamp { UserId = UserId.Id, CampId = item };
                _context.SubscribedCamps.Add(i);
                _context.SaveChanges();
            }

            var Subscribed = _context.SubscribedCamps.Any(i => i.UserId == UserId.Id) ? _context.SubscribedCamps.Include(i => i.Camp).Include(i => i.Camp.CampPosts).Where(i => i.UserId == UserId.Id).Select(i => i.Camp).ToList() : (Object)false;
            var Categories = _context.CampCategories.Where(i => i.Active == true).ToList();
            return new { Subscribed, Categories };
        }


        [Route("api/Camp/camp/{Id}")]
        
        [Authorize]
        public Object GetCamps(int Id)
        {

            var Categories = _context.CampPosts.Where(i => i.Active == true&& i.CampCategoryId==Id).ToList();
            return Categories;
        }
        [Route("api/Camp/start/{Id}")]

        [Authorize]
        public Object Camps(int Id)
        {

            var Categories = _context.CampPosts.Where(i => i.Active == true && i.CampCategoryId == Id).ToList();
            return Categories;
        }

        [HttpGet]
        [Route("api/Camp/Test/{id}")]
        public object Test(long id)
        {
            _context.Configuration.LazyLoadingEnabled = false;
            _context.Configuration.ProxyCreationEnabled = false;
            var Iden = User.Identity.Name;
            var test = _context.CampQuestions.Include(i => i.Option)
                .Where(i => i.SetId == id).ToList();
            var user = _context.Users.FirstOrDefault(i => i.UserName == Iden);
            if (_context.CampAttempt.Any(a => a.CampPostId == id && a.UserId == user.Id))
            {
                var po = _context.CampAttempt.FirstOrDefault(i => i.CampPostId == id && i.UserId == user.Id).Id;
                var reson = _context.ReasoningAnswers
                .Include(i => i.Option)
                .Where(i => i.ReasoningId == id && i.UserId == user.Id && i.AttempId == po);

                return new
                {
                    Test = true,
                    Reasoning = test,
                    Correct = reson.Count(p => p.Option.Correct),
                    Wrong = reson.Count(p => !p.Option.Correct),
                    Skipped = test.Count() - reson.Count()
                };
            }

            return new
            {
                Test = false,
                Reasoning = test,
                Time=20
            };
        }

        [Route("api/Camp/result/{Id}")]
        [HttpGet]
        public object GetRootResult(long Id)
        {

            // this.Configuration.Formatters.
            var Iden = User.Identity.Name;
            var user = _context.Users.FirstOrDefault(i => i.UserName == Iden);
            _context.Configuration.LazyLoadingEnabled = false;
            _context.Configuration.ProxyCreationEnabled = false;
            var po = _context.ReasoningAttemp.FirstOrDefault(i => i.ReasoningId == Id && i.UserId == user.Id).Id;
            var reson = _context.ReasoningAnswers
                .Where(i => i.ReasoningId == Id && i.UserId == user.Id && i.AttempId == po)
                .Select(i => new { QuestionId = i.QuestionId, OptionId = i.OptionId, Correct = i.Option.Correct }).ToList();
            var test = _context.ReasoningRoots.Include(i => i.Questions)
               .Include(i => i.Questions.Select(i_ => i_.Option))
               .FirstOrDefault(i => i.Id == Id);
            var resonResult = test.Questions.Select(i => new { Question = new { Solution = i.Solution, ShortOrder = i.ShortOrder, Id = i.Id, Option = i.Option.Select(_ => new { Id = _.Id, Answer = _.Answer, Correct = _.Correct, QuestionId = _.QuestionId, ShortOrder = _.ShortOrder }), Query = i.Query }, Ans = reson.FirstOrDefault(p => p.QuestionId == i.Id) });
            return new { Test = true, Reasoning = reson, render = resonResult };

        }



        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("api/Camp/SaveResult")]
        public IHttpActionResult SaveAnswers([FromBody]JObject data)//Guid[] AnswerId,int[] ReasoningId,Guid[] QuesionId)
        {
            Guid[] AnswerId;
            int[] ReasoningId;
            Guid[] QuesionId;
            if (data["AnswerId"].Count() > 1)
            {
                AnswerId = data["AnswerId"].ToObject<Guid[]>();
                ReasoningId = data["ReasoningId"].ToObject<int[]>();
                QuesionId = data["QuesionId"].ToObject<Guid[]>();
            }
            else
            {
                AnswerId = new Guid[] { data["AnswerId"].ToObject<Guid>() };
                ReasoningId = new int[] { data["ReasoningId"].ToObject<int>() };
                QuesionId = new Guid[] { data["QuesionId"].ToObject<Guid>() };
            }

            CampAnswerBack[] Answer = new CampAnswerBack[AnswerId.Count()];
            for (int i = 0; i < AnswerId.Count(); i++)
            {
                Answer[i] = new CampAnswerBack
                {
                    AnswerId = AnswerId[i],
                    QuesionId = QuesionId[i],
                    ReasoningId = ReasoningId[i]
                };
            }
            //this.ActionContext.Request.
            // ReasnionAnswerBack[] Answer = data["ReasnionAnswerBack"].ToObject<ReasnionAnswerBack[]>();
            var Iden = User.Identity.Name;
            var user = _context.Users.FirstOrDefault(i => i.UserName == Iden);
            var first = Answer[0];
            var Attempt = new CampAttempt
            {
                Id = Guid.NewGuid(),
                On = DateTime.UtcNow,
                CampPostId = first.ReasoningId,
                UserId = user.Id
            };



            _context.CampAttempt.Add(Attempt);
            _context.SaveChanges();

            List<CampAnswers> ans = new List<CampAnswers>();
            //if (db.ReasoningAnswers.Any(i => i.ReasoningId==first.ReasoningId&& i.UserId == user.Id))
            //{
            //    return Ok(0);
            //}
            foreach (var i in Answer)
            {
                ans.Add(new CampAnswers { Id = Guid.NewGuid(), OptionId = i.AnswerId, UserId = user.Id, QuestionId = i.QuesionId, CampId = i.ReasoningId, AttemptId = Attempt.Id });
            }
            _context.CampAnswers.AddRange(ans);
            _context.SaveChanges();

            var reasoinig = _context.ReasoningRoots.Find(first.ReasoningId);
            var tracking = _context.ReasoningTracker.FirstOrDefault(i => i.UserId == user.Id && i.RnmPostId == reasoinig.Id);
            Attempt = _context.CampAttempt.Include(i => i.Ans).Include(i => i.Ans.Select(o => o.Option)).FirstOrDefault(o => o.Id == Attempt.Id);



            if (Attempt.Ans.Count(i => i.Option.Correct == true) >= reasoinig.MinQuestionToPass)
            { tracking.TestPassed = true; _context.SaveChanges(); }
            return Ok(1);
        }
    }

    public class VmCampTo
    {
        public int[] CategoriesSubs { get; set; }
    }
}
