﻿using Scoolfirst.Api.Models;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.PeriodicAssesment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace Scoolfirst.Api.Controllers
{
    public class ReportController : ApiController
    {
        private ScoolfirstContext _context;

        public ReportController(ScoolfirstContext scoolfirstContext)
        {
            _context = scoolfirstContext;
        }

        [HttpGet]
        [Route("api/report/")]
        public object SchoolFoundationReport()
        {

            var user = _context.Users.FirstOrDefault(i => i.UserName == User.Identity.Name);
            var Tracking = (from TestTracking in _context.TestTrackings.Include(i => i.User)
                            where TestTracking.UserId == user.Id 
                            select TestTracking.TestId).ToList();

            var result1 =
                _context.PARoots
                .Where(i => Tracking.Any(o => i.Id == o))
                .Join(_context.PAAttemp, (d) => d.Id, o => o.ReasoningId, (d, o) => 
                new ReportVM { RightAns=o.RightAns,Subject=d.Subject.DisplayName 
                ,On=o.On, SkippedAns= o.SkippedAns, WrongAns=o.WrongAns,AddedOn=  d.AddedOn, Title=d.Title,
                    Level=d.Level,RId=d.Id , Attemp=o.Id,Month=d.Month })
                    .Select(i=> i).ToList();
            var reasoning_Attemtp = _context.ReasoningAttemp.Include(i=>i.Ans).Include(i=>i.Ans.Select(io=>io.Reasoning.Questions)).Where(i => i.UserId == user.Id).ToList();
            var List = new List<ReportVM>();
            foreach(var i in reasoning_Attemtp)
            {
                var reasoing = i.Ans.First().Reasoning;
                var vm = new ReportVM();
                vm.RId = i.ReasoningId;
                vm.Month = "-11";
                vm.On = i.On;
                vm.Subject = "Reasoning "+reasoing.Level;
                vm.Title = reasoing.Title;
                vm.Level = reasoing.Level;
                vm.RightAns = i.Ans.Count(io => io.Option.Correct == true);
                vm.WrongAns = i.Ans.Count(io => io.Option.Correct == false);
                vm.SkippedAns = reasoing.Questions.Count() - (vm.RightAns + vm.WrongAns);
                List.Add(vm);
            }

            var virtualComp=_context.VirtualCompetetionAttempts.Include(i => i.Ans).Include(i => i.Ans.Select(io => io.Exam.QuestionsSet)).Where(i => i.UserId == user.Id).ToList();
            var List2 = new List<ReportVM>();
            foreach (var i in virtualComp)
            {
                var reasoing = i.Ans.First().Exam;
                var vm = new ReportVM();
                vm.RId = i.ExamId;
                vm.Month = "-12";
                vm.On = i.On;
                vm.Subject = "Virtual " + reasoing.Level;
                vm.Title = reasoing.Title;
                vm.Level = reasoing.Level;
                vm.RightAns = i.Ans.Count(io => io.Option.Correct == true);
                vm.WrongAns = i.Ans.Count(io => io.Option.Correct == false);
                vm.SkippedAns = reasoing.QuestionsSet.Count() - (vm.RightAns + vm.WrongAns);
                List2.Add(vm);
            }
            return new { Virtual=List2,Reasoning= List, School=result1};
        }
    }

    public class ReportVM
    {
        public int RightAns { get; set; }
        public string Subject { get;set;}
        public DateTime On{get;set;}
        public int SkippedAns{get;set;}
        public int WrongAns{get;set;}
        public DateTime AddedOn{get;set;}
        public string Title{get;set;}
        public string Level{get;set;}
        public long RId { get; set; }
        public Guid Attemp { get; set; }
        public string Month { get; set; }
    }
}