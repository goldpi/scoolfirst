﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Slides;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Scoolfirst.ViewModel.Slider;
using System.Data.Entity;
namespace Scoolfirst.Api.Controllers
{
    
    public class SliderController : ApiController
    {
        ScoolfirstContext dc = new ScoolfirstContext();
        [Route("api/slider/")]
        [HttpGet]
        public IEnumerable<SliderVM> getSlider()
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            return dc.Sliders.Include(i=>i.Slices).Select(x=>new SliderVM { Title = x.SliderName, Slices =x.Slices.Select(k=>new SliderSliceVM {Title=k.Title,Description=k.Description,Image=k.Image, SlidingOrder=k.slidingOrder }).ToList() }).ToList();
        }

        [Route("api/slides/{SlideName}")]
        [HttpGet]
        public IEnumerable<SliderSliceVM> getSlider(string SlideName)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            return dc.SliderSlices.Where(i=> i.Slider.SliderName == SlideName)                
                .Select(x => new SliderSliceVM { Title = x.Title, Description = x.Description, Image = x.Image, SlidingOrder = x.slidingOrder }).ToList();
                
        }

        [Route("api/slider/dashboard/")]
        [HttpGet]
        public IEnumerable<SliderSliceVM> getSliderAndroidDashboard()
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            return dc.SliderSlices.Where(i=>i.Slider.SliderName== "Camp").Select(x => new SliderSliceVM { Title = x.Title, Description = x.Description, Image = x.Image, SlidingOrder = x.slidingOrder }).ToList();
        }

        [Route("api/slider/RoleModel/")]
        [HttpGet]
        public IEnumerable<SliderSliceVM> getSliderAndroidRoleModel()
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            return dc.SliderSlices.Where(i => i.Slider.SliderName == "Android RoleModel").Select(x => new SliderSliceVM { Title = x.Title, Description = x.Description, Image = x.Image, SlidingOrder = x.slidingOrder }).ToList();
        }

        [Route("api/slider/ComputerNCreative/")]
        [HttpGet]
        public IEnumerable<SliderSliceVM> getSliderComputerNCreative()
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            return dc.SliderSlices.Where(i => i.Slider.SliderName == "Computer & Creative").Select(x => new SliderSliceVM { Title = x.Title, Description = x.Description, Image = x.Image, SlidingOrder = x.slidingOrder }).ToList();
        }
        [Route("api/slider/ProductCategory/")]
        [HttpGet]
        public IEnumerable<SliderSliceVM> getSliderProductCategory()
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            return dc.SliderSlices.Where(i => i.Slider.SliderName == "Product Category").Select(x=>new SliderSliceVM {Title= x.Title,Description= x.Description,Image= x.Image, SlidingOrder = x.slidingOrder }).ToList();
            
        }

    }
}
