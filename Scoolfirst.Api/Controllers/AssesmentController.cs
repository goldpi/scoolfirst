﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using Scoolfirst.Model.PeriodicAssesment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Web.Http.Description;
using Scoolfirst.ViewModel.Assement;
using Newtonsoft.Json.Linq;
using Scoolfirst.Model.WorkBook;

namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class AssesmentController : ApiController
    {

        private ScoolfirstContext dc;
        private User UserData;
        public AssesmentController()
        {
            dc = new ScoolfirstContext();
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
        }

        [Route("api/Assesment")]
        [HttpGet]
        public object GetRoots(int subjectId,PAType type = PAType.WorkBook, int page = 0, int size = 30)
        {
            GetUserDetails();
            var testAppeared = dc.PAAttemp.Where(i => i.UserId == UserData.Id && i.Reasoning.TypeOfAssement == type).ToList();
            var schoolId = UserData.SchoolId;
            if (UserData.School.UseMasterContent)
            {
                schoolId = dc.Schools.FirstOrDefault(i => i.IsMasterContent == true).Id;
            }
            var Resoing = dc.PARoots
                .Where(i => i.SchoolId == schoolId && i.ClassId == UserData.ClassId && i.TypeOfAssement == type && i.SubjectId == subjectId).AsQueryable();
            
            //if (type == PAType.WorkBook || type==PAType.SingleTest)
            //{
                var returnList = Resoing.Select(u => new {data = u, count = u.QuestionsSet.Count() }).GroupBy(i => i.data.Level).Select(u => new { data = u, Key=u.Key });
                return new { appearedLIst = testAppeared, TestList = returnList.OrderByDescending(i => i.Key).Skip(page * size).Take(size).ToList() };
            //}
            //else
            //{
            //    var returnList = Resoing.Select(u => new { data = u, count = u.QuestionsSet.Count() });
            //    return new { appearedLIst = testAppeared, TestList = returnList.OrderByDescending(i => i.data.Order).Skip(page * size).Take(size).ToList() };
            //}
               

           
            
            
        }



        [Route("api/Assesment/byMonth")]
        [HttpGet]
        public object GetRootsByMonth(int subjectId, PAType type = PAType.WorkBook,string Month="1", int page = 0, int size = 30)
        {
            GetUserDetails();
            var testAppeared = dc.PAAttemp.Where(i => i.UserId == UserData.Id && i.Reasoning.TypeOfAssement == type).ToList();
            var schoolId = UserData.SchoolId;
            if (UserData.School.UseMasterContent)
            {
                schoolId = dc.Schools.FirstOrDefault(i => i.IsMasterContent == true).Id;
            }
            var Resoing = dc.PARoots
                .Where(i => i.SchoolId == schoolId && i.ClassId == UserData.ClassId && i.TypeOfAssement == type && i.SubjectId == subjectId && i.Month==Month)
                .OrderBy(i=>i.Order)
                .AsQueryable();
     
            return new { appearedLIst = testAppeared, TestList =Resoing.Skip(page * size).Take(size).ToList() };
            
        }

        [Route("api/Assesment/{Id}")]
        [HttpGet]
        public object GetRoot(long Id)
        {
            GetUserDetails();
            var Resoing = dc
                    .PARoots
                    .Include(i => i.QuestionsSet)
                    .Include(i => i.QuestionsSet
                    .Select(r => r.Option))

                    .FirstOrDefault(i => i.Id == Id);
            if (dc.PAAttemp.Count(i => i.ReasoningId == Id && i.UserId == UserData.Id) == 0)
            {   
                return new { Test = false, Reasoning = Resoing };
            }
            else
            {
                var reson = dc.PAAttemp
                    .Select(i => new { i = new { RightAns = i.RightAns, SkippedAns = i.SkippedAns, WrongAns = i.WrongAns  }, Reasoning = i.Reasoning, Ans = i.Ans, ReasoningId = i.ReasoningId, UserId = i.UserId })
                    .FirstOrDefault(i => i.ReasoningId == Id && i.UserId == UserData.Id);
                return new { Test = true, Reasoning = Resoing, result=reson.i };
            }    
        }
        [Route("api/Assesment/result/{Id}")]
        [HttpGet]
        public object GetRootResult(long Id)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            GetUserDetails();
            var resonId = dc.PAAttemp
                    .FirstOrDefault(i => i.ReasoningId == Id && i.UserId == UserData.Id).Id;
            var reson = dc.PAAnswers.Where(i => i.AttempId == resonId).ToList();
            var reasoning = dc.PARoots.Include(o=>o.QuestionsSet).Include(i=>i.QuestionsSet.Select(p=>p.Option))
                .FirstOrDefault(i => i.Id == Id);
            var resonResult = reasoning.QuestionsSet
                .Select(o=>new { ShortOrder = o.ShortOrder, Query=o.Query,Solution=o.Solution,Id=o.Id,Option=o.Option.Select(i=>new { Answer=i.Answer,Correct=i.Correct,Id=i.Id }) })
                .Select(i => new { Question = i, Ans = reson
                .Select(ip=>new { OptionId=ip.OptionId, Correct=ip.Option.Correct, QuestionId=ip.QuestionId })
                .FirstOrDefault(p => p.QuestionId == i.Id) });
            return new { Test = true,  render=resonResult };
           
        }

        [HttpPost]
        [Route("api/Assement/SaveResult")]
        [ResponseType(typeof(object))]
        public IHttpActionResult SaveAnswers( [FromBody]JObject data)
        {
            Guid[] AnswerId;
            int[] ReasoningId;
            Guid[] QuesionId;
           // bool passed = data["pass"].ToObject<bool>();
            AnswerGiven answer = new AnswerGiven();
            answer.Correct = data["Correct"].ToObject<int>();
            answer.Wrong = data["Wrong"].ToObject<int>();
            answer.Skiped = data["Skiped"].ToObject<int>();
            if (data["AnswerId"].Count() > 1)
            {
                AnswerId = data["AnswerId"].ToObject<Guid[]>();
                ReasoningId = data["ReasoningId"].ToObject<int[]>();
                QuesionId = data["QuesionId"].ToObject<Guid[]>();
            }
            else
            {
                AnswerId = new Guid[] { data["AnswerId"].ToObject<Guid>() };
                ReasoningId = new int[] { data["ReasoningId"].ToObject<int>() };
                QuesionId = new Guid[] { data["QuesionId"].ToObject<Guid>() };
            }
            answer.Answer = new List<PAAnswerBack>();
            for (int i = 0; i < AnswerId.Count(); i++)
            {
                answer.Answer.Add(new PAAnswerBack
                {
                    AnswerId = AnswerId[i],
                    QuesionId = QuesionId[i],
                    ReasoningId = ReasoningId[i]
                });
            }
            var points = 0;
            var Iden = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == Iden);
            var first = answer.Answer[0];
            var proots = dc.PARoots.Include(i=>i.QuestionsSet).FirstOrDefault(i=>i.Id==first.ReasoningId);

            var passed = proots.MinQuestionToPass <= answer.Correct;
            if(proots!=null&& !dc.TestTrackings.Any(i=>i.UserId==user.Id && i.TestId==proots.Id))
            {
                //var firstAttempt = dc.PAAttemp.Any(i => i.UserId == user.Id && i.ReasoningId == proots.Id);
               
                var p = new TestTracking {
                    Score=answer.Correct,
                    UserId=user.Id,
                    TestId=proots.Id,
                    Month=float.Parse(proots.Month),
                    IsPassable=proots.IsPassable,
                    PointsGiven=proots.IsPassable?false:answer.Correct==proots.QuestionsSet.Count(),
                    ViewSolution=passed
                };
                dc.TestTrackings.Add(p);
                if (p.PointsGiven)
                {
                    user.Points += 30;
                    points = 30;
                }
                dc.SaveChanges();
                
            }
            var Attempt = new PAAttemp
            {
                Id = Guid.NewGuid(),
                On = DateTime.UtcNow,
                ReasoningId = first.ReasoningId,
                UserId = user.Id,
                WrongAns = answer.Wrong,
                RightAns = answer.Correct,
                SkippedAns = answer.Skiped
            };
            dc.PAAttemp.Add(Attempt);
            dc.SaveChanges();
            List<PAAnswers> ans = new List<PAAnswers>();
            foreach (var i in answer.Answer)
            {
                ans.Add(new PAAnswers { Id = Guid.NewGuid(), OptionId = i.AnswerId, UserId = user.Id, QuestionId = i.QuesionId, ReasoningId = i.ReasoningId, AttempId = Attempt.Id });
            }
            dc.PAAnswers.AddRange(ans);
            dc.SaveChanges();
            return Ok(new {points });
        }

        private void GetUserDetails()
        {
            var userId = User.Identity.Name;
            UserData = dc.Users.Include(i=>i.School).FirstOrDefault(i=>i.UserName==userId);
        }

    }
}
