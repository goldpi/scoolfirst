﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Payments;

namespace Scoolfirst.Api.Controllers
{
    public class OfflinePaymentController : ApiController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();


        [Authorize]
        [Route("api/OfflinePayment/request")]
        [HttpPost]
        public object RequestPayment(OfflinePayment payment)
        {
            var uid_name = User.Identity.Name;
            var uid = dc.Users.FirstOrDefault(u => u.UserName == uid_name);
            var userInfo = uid;
            payment.Id = Guid.NewGuid();
            payment.UserId = uid.Id;
            payment.RequestDate = DateTimeOffset.Now;
            uid.Expiry = DateTime.Now.AddDays(payment.Days);
            dc.OfflinePayments.Add(payment);
            dc.SaveChanges();
            return new { payment,user = new
            {
                Email = User.Identity.Name,
                UserId = userInfo.Id,
                UserName = userInfo.UserName,
                Name = userInfo.FullName,
                Picture = userInfo.PictureUrl,
                ClassId = userInfo.ClassId,
                Class = userInfo.Class,
                SchoolId = userInfo.SchoolId,
                ClassName = userInfo.Class.RomanDisplay,
                SchoolName = userInfo.School.SchoolName,
                PhoneNumber = userInfo.PhoneNumber,
                ExpiryDate = userInfo.Expiry,
                Trial = userInfo.Trial,
                RegisterDate = userInfo.RegisterdOn,
                Dob = userInfo.DOB,
                AboutMe = userInfo.AboutMe,
                OTP = userInfo.ConnectionId,
                Board = userInfo.Board,
                Address = userInfo.Address,
                Gender = userInfo.Gender,
                Modules = userInfo.Modules,
                Points = userInfo.Points,
                subscription = userInfo.Subscribed,

            }};
        }
    }
}
