﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class MustRemberTricksController : ApiController
    {
        Scoolfirst.Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();

        [HttpGet]
        [Route("api/MustRememberTricks")]
        public IEnumerable<Model.Feed.Post> Post(int page = 0)
        {

            var Feeds = dc.Posts.Where(i => i.Type == PostType.MUST_REMEMBER_TRICKS)
                .OrderBy(i => i.OnDateTIme)
                .Skip(page * 3)
                .Take(3).ToList();
            return Feeds;
        }
    }
}
