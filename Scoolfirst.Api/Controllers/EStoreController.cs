﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Projects;
namespace Scoolfirst.Api.Controllers
{
    
    public class EStoreController : ApiController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();
        [HttpGet]
        [Route("api/estoredata")]
        public IEnumerable<Product> GetProduct(int page = 0,int size=4)
        {
            var Products = dc
                .Products
                .Where(i=>i.Active==true)
                .OrderByDescending(i => i.AddedOn)
                .Skip(page * size)
                .Take(size).ToList();
            return Products;
        }
    }
}
