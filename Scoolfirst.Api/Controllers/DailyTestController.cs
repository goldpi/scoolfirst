﻿using Newtonsoft.Json.Linq;
using Scoolfirst.Api.Models;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.DaliyTestNameSpace;
using Scoolfirst.Model.Identity;
using Scoolfirst.Model.Provider;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Scoolfirst.Api.Controllers
{
    
    public class DailyTestController : ApiController
    {
        private ScoolfirstContext _context;
        public DailyTestController()
        {
            _context = new ScoolfirstContext();
        }

        private User GetUser()
        {
            var UserName = this.User.Identity.Name;
            return _context.Users.Include(i=>i.Class).FirstOrDefault(i => i.UserName == UserName);
        }

        
        [Route("api/NextDailyTest")]
        [HttpGet]
        public DaliyTest NextTest()
        {
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");


            var Now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            var Test = _context.DaliyTests.OrderBy(i=>i.EndsOn).Where(i => i.StartsOn > Now).FirstOrDefault();
            return Test;
        }


        [HttpGet]
        [Route("api/StartTestDaily/")]
        public object StartTest()
        {
            var dc = _context;
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");


            var Now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
           
            var Test = _context.DaliyTests.Where(i => i.EndsOn > Now).FirstOrDefault();
            User user = this.GetUser();
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var test = dc.DTQuestionSet.Include(i => i.Questions)
                .Include(i => i.Questions.Select(i_ => i_.Answers))
                .FirstOrDefault(i => i.DaliyTestId == Test.Id && i.ClassId==user.ClassId);
            if (test == null)
                return NotFound();

            var result = dc.DTAppearences.Any(i => i.UserId == user.Id && i.DaliyTestId == Test.Id);
            result.Debug();
            return new { test = test, appeared = result, Id=Test.Id };
        }

        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("api/DailyTest/SaveResult")]
        public IHttpActionResult SaveAnswers([FromBody]JObject data)//Guid[] AnswerId,int[] ReasoningId,Guid[] QuesionId)
        {
            var dc = _context;
            Guid[] AnswerId;
            Guid[] DailyTestId;
            Guid[] QuesionId;
            var Iden = User.Identity.Name;
            var user = this.GetUser();
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");


            var Now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            if (data["AnswerId"].Count() > 1)
            {
                AnswerId = data["AnswerId"].ToObject<Guid[]>();
                DailyTestId = data["DailyTestId"].ToObject<Guid[]>();
                QuesionId = data["QuesionId"].ToObject<Guid[]>();
            }
            else
            {
                AnswerId = new Guid[] { data["AnswerId"].ToObject<Guid>() };
                DailyTestId = new Guid[] { data["DailyTestId"].ToObject<Guid>() };
                QuesionId = new Guid[] { data["QuesionId"].ToObject<Guid>() };
            }
           
            var Attempt = new DTAppeared
            {
                Id = Guid.NewGuid(),
                On = Now,
                DaliyTestId = DailyTestId[0],
                UserId = user.Id,
                 
            };
            var test = dc.DTQuestionSet.Include(i => i.Questions)
              .Include(i => i.Questions.Select(i_ => i_.Answers))
              .FirstOrDefault(i => i.DaliyTestId == Attempt.DaliyTestId && i.ClassId == user.ClassId);
            Attempt.DTQuestionSetId = test.Id;
            dc.DTAppearences.Add(Attempt);
            dc.SaveChanges();
            DTAnswered[] Answer = new DTAnswered[AnswerId.Count()];
            for (int i = 0; i < AnswerId.Count(); i++)
            {
                var ansId = AnswerId[i];
                var ans = test.Questions.SelectMany(iy => iy.Answers).FirstOrDefault(o=>o.Id==ansId);
                Answer[i] = new DTAnswered
                {
                    Id = Guid.NewGuid(),
                    AppearedId = Attempt.Id,
                    AnswerId = AnswerId[i],
                    DTQuestionSetId=Attempt.DTQuestionSetId,
                    Correct = ans.IsCorrect, UserId=user.Id   
                };
            }
            dc.DTAnswereds.AddRange(Answer);
            dc.SaveChanges();


            if (!string.IsNullOrEmpty(user.ParentsNo))
            {
                var Total_question = test.Questions.Count();
                var correct_ans = Answer.Count(i => i.Correct);
                var message = string.Format("{0} has attempted today's chapter revision test with {1} correct response out of {2} questions. keep revising, keep improving.", user.FullName, correct_ans, Total_question);
                SmsProviderInstance.SendMessage(user.ParentsNo, message);
            }
            return Ok(1);
        }

        [HttpGet]
        [Route("api/DTReview/{id}")]
        public object Result(Guid Id)
        {
            var dc = _context;
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            var UserName = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == UserName);
            var result = dc.DTAppearences.Include(i => i.DaliyTest).FirstOrDefault(i => i.UserId == user.Id && i.DaliyTestId == Id);
            var attempts = dc.DTAnswereds.Include(i=>i.Answer)
                .Where(i => i.AppearedId == result.Id)
                .Select(i => new {
                    Question = new
                    {
                        Query = i.Answer.Question.Question,
                        Option = i.Answer.Question.Answers.
                        Select(p => new { p.Answer, Correct = p.IsCorrect, p.Id }),
                        i.Answer.Question.Solution,
                        ShowSolution = i.Answer.Question.ShowSloution
                    },
                    OptionId = i.AnswerId
                })
                .ToList();
            
            return new {  result,  attempts };
        }
        [HttpGet]
        [Route("api/LastTest/")]
        public object LastTest()
        {
            var dc = _context;          
            User user = this.GetUser();
            var result = dc
                .DTAppearences.Include(i => i.DaliyTest)
                .Where(i => i.UserId == user.Id).OrderByDescending(i=>i.On).FirstOrDefault();
            if (result != null)
            {

                var CorrectAns = dc.DTAnswereds.Count(i => i.Correct && i.UserId ==user.Id && i.DTQuestionSet.DaliyTestId == result.DaliyTestId);
                var TotalQuestion = dc.DTQuestionSet.Include(i => i.Questions).FirstOrDefault(i => i.Id == result.DTQuestionSetId).Questions.Count();
                return new { prev = true, result = new { CorrectAns, TotalQuestion }, test = new { result.DaliyTest.CheatCode, date = result.DaliyTest.StartsOn.ToString("dd/MM/yyyy") } };
            }
            else
            {

#if DEBUG
                return new { prev = true, result = new { CorrectAns = 0, TotalQuestion = 0 }, test =new { CheatCode = "ggg", date = DateTime.Now.ToString("dd/MM/yyyy") } };
#else
                return new { prev = false , result= new { CorrectAns=0,TotalQuestion=0 } };
#endif
            }

        }


    }
}
