﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Scoolfirst.Model.MCQ;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class MCQController : ApiController
    {
        private ScoolfirstContext db = new ScoolfirstContext();
        [Route("api/McqQuiz/")]
        [HttpGet]
       public IEnumerable<QuestionSet> SetQPapers(TYPEQuetionSet Type,string Class,int Level=0)
        {
            db.Configuration.LazyLoadingEnabled = false;
            db.Configuration.ProxyCreationEnabled = false;
            switch (Type)
            {
                case TYPEQuetionSet.NAO:
                    break;
            }
            return db.QuestionSets.Where(i => i.Type == Type && i.Class == Class && i.Level == Level).ToList();
        }

        [Route("api/McqQuiz/Set/{id}")]
        [HttpGet]
        public QuestionSet Set(Guid id)
        {
            return db.QuestionSets.Find(id);
        }


    }
}
