﻿using Scoolfirst.Api.Models;
using Scoolfirst.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Web.Http.Description;
using Newtonsoft.Json.Linq;
using Scoolfirst.Model.VirtualCompetetion;

namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class VirtualController : ApiController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();
        [Route("api/virtual/main")]
        [AllowAnonymous]
        public object GetMain()
        {
                dc.Configuration.LazyLoadingEnabled = false;
                dc.Configuration.ProxyCreationEnabled = false;
                var play = dc.VirtualCompetetionMainCategories.OrderByDescending(i => i.Id).ToList();
                return play;
        }


        [Route("api/virtual/Sub/{Id}")]
        [AllowAnonymous]
        public object GetSub(int Id)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var play = dc.VirtualCompetitonSubCategories
                .OrderByDescending(i => i.Id).Where(i=>i.CategoryId==Id)
                .ToList();
            return play;
        }


        
        [HttpGet]
        [Route("api/virtual/TestList/{Id}")]
        [AllowAnonymous]
        public object GetPlayQuestion(int Id)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            // var test = dc.PlayRoots.OrderByDescending(i => i.Id).Where(i => i.Subject == name).ToList();

            var test = dc.VirtualExams
                .OrderByDescending(i => i.Id)
                .Where(i => i.CategoryId == Id)
                .ToList();
            return test;
        }
        [HttpGet]
        [Route("api/Virtual/StartTest/{id}")]
        public object StartTest(long id)
        {
            var user = User.Identity.Name;
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var test = dc.VirtualExams.Include(i => i.QuestionsSet)
                .Include(i => i.QuestionsSet.Select(i_ => i_.Options))
                .FirstOrDefault(i => i.Id == id);

            var result = false;
            return new { test, appeared = result };
        }

        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("api/Virtual/SaveResult")]
        public IHttpActionResult SaveAnswers([FromBody]JObject data)//Guid[] AnswerId,int[] ReasoningId,Guid[] QuesionId)
        {

            Guid[] AnswerId;
            int[] ReasoningId;
            Guid[] QuesionId;
            var Iden = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == Iden);
            if (data["AnswerId"].Count() > 1)
            {
                AnswerId = data["AnswerId"].ToObject<Guid[]>();
                ReasoningId = data["ReasoningId"].ToObject<int[]>();
                QuesionId = data["QuesionId"].ToObject<Guid[]>();
            }
            else
            {
                AnswerId = new Guid[] { data["AnswerId"].ToObject<Guid>() };
                ReasoningId = new int[] { data["ReasoningId"].ToObject<int>() };
                QuesionId = new Guid[] { data["QuesionId"].ToObject<Guid>() };
            }
            var Attempt = new VirtualCompetetionAttempt
            {
                Id = Guid.NewGuid(),
                On = DateTime.UtcNow,
                ExamId = ReasoningId[0],
                UserId = user.Id
            };

            dc.VirtualCompetetionAttempts.Add(Attempt);
            dc.SaveChanges();
            VirtualCompetetionAnswers[] Answer = new VirtualCompetetionAnswers[AnswerId.Count()];
            for (int i = 0; i < AnswerId.Count(); i++)
            {
                Answer[i] = new VirtualCompetetionAnswers
                {
                    Id = Guid.NewGuid(),
                    AttemptId = Attempt.Id,
                    OptionId = AnswerId[i],
                    QuestionId = QuesionId[i],
                    ExamId = ReasoningId[i]
                };
            }
            dc.VirtualCompetetionAnswers.AddRange(Answer);
            dc.SaveChanges();



            return Ok(1);
        }

        [HttpGet]
        [Route("api/VirtualReview/{id}")]
        public object Result(int Id)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            var UserName = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == UserName);
            var result = dc.VirtualCompetetionAttempts
                .Include(i => i.Exam)
                .FirstOrDefault(i => i.UserId == user.Id && i.ExamId == Id);
            var attempts = dc.VirtualCompetetionAnswers
                .Include(i => i.Question)
                .Include(i => i.Question.Options)
                .Where(i => i.AttemptId == result.Id)
                .Select(i => new {
                    Question = new
                    {
                        Query = i.Question.Query,
                        Option = i.Question
                        .Options.Select(p => new { Answer = p.Answer, Correct = p.Correct, Id = p.Id }),
                        Solution = i.Question.Solution,
                        ShowSolution = i.Question.ShowSolution
                    },
                    OptionId = i.OptionId
                })
                .ToList();
            attempts.Debug();
            return new { result = result, attempts = attempts };
        }





    }
}
