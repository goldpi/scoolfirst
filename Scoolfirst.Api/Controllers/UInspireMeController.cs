﻿using Scoolfirst.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Scoolfirst.Model.Common;
using System.Web.Http.Cors;
using Scoolfirst.Api.Models;

namespace Scoolfirst.Api.Controllers
{
   
    
   
    public class UInspireMeController : ApiController
    {
        ScoolfirstContext dc = new ScoolfirstContext();
        [Route("api/uinspireme/")]
        [HttpGet]
        public IEnumerable<RoleModel> GetUinspireMeDataint(int page = 0, int size = 4,string Type="All")
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var d=DateTime.Now.AddHours(5);
            var data = dc.RoleModels.Where(i=>i.On<=d).AsQueryable();
            if(Type=="All")
            {
                data=data.OrderByDescending(i => i.On)
                .Skip((page) * size).Take(size);
            }
            else
            {
            data=data.Where(i => i.TypeOfRm == Type)
                    .OrderByDescending(i => i.On)
                    .Skip((page) * size).Take(size);
            }
            return data.ToList();
        }
    }
}
