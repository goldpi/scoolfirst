﻿using Scoolfirst.Model.Context;
using System.Web.Http;
using System.Data.Entity;
using System.Linq;
using System.Collections.Generic;
using Scoolfirst.Model.Common;

namespace Scoolfirst.Api.Controllers
{
   
    public class GeneralKnowledgeController : ApiController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();

        [Route("api/generalknowledgedata")]
        [HttpGet]
        public IEnumerable<GeneralKnowledge> GetGeneralKnowledgeData(int page = 0,int size = 4)
        {
            dc.Configuration.LazyLoadingEnabled = false; ;
            dc.Configuration.ProxyCreationEnabled = false;
            var gkdata = dc.GeneralKnowledges.Include(i => i.Class).OrderByDescending(i => i.On).Skip(page * size).Take(size).ToList();
            return gkdata.ToList();
        }
    }
}
