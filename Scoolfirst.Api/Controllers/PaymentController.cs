﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Payments;
using Scoolfirst.Model.Projects;
using Scoolfirst.Api.Models;

namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class PaymentController : ApiController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();
        
        [Route("api/Transaction/start")]
        [HttpPost]
        public object StartTransaction(TransactionModel model)
        {
            "Transaction".Debug();
            model.Debug();
            var userName = dc.Users.FirstOrDefault(i => i.UserName == User.Identity.Name);
            var trans = new Transaction
            {
                Amount = model.Amount,
                CartId=model.CartId,
                UserId=userName.Id,
                Description=model.Description,
                Type=model.type,
                Email=model.Email,
                Address=model.Address,
                Redeem=model.Points.HasValue
                
            };
            model.ProductId.HasValue.Debug();
            if (model.ProductId.HasValue)
            {
                trans.ProductId
                    = model.ProductId;
            }
            model.Points.HasValue.Debug();
            if (model.Points.HasValue)
            {
                userName.Points -= model.Points.Value;
                trans.Redeem = true;
                trans.Points = model.Points;
                trans.Amount = model.Points.Value;   
            }
            dc.Transactions.Add(trans);
            dc.SaveChanges();

            if (trans.Redeem)
            {
                "Reddem".Debug();
                var order = new Order
                {
                    UserId = trans.UserId,
                    Email = trans.Email,
                    Address = trans.Address,
                    ProductId = model.ProductId.Value,
                    TransactionId = trans.Id,
                    Name = userName.FullName,
                    OrderDate = DateTimeOffset.Now.DateTime,
                    Phone = userName.PhoneNumber,
                };
                dc.Orders.Add(order);
                dc.SaveChanges();
                using (var client = new WebClient())
                {
                    try
                    {
                        var responseString = client.DownloadString("http://upgradejr.com.strawberry.arvixe.com/payment/shootmail/" + order.Id);
                        responseString.Debug();
                        "Mail Fired".Debug();
                    }
                    catch
                    {
                        "Error Happend".Debug();
                    }
                    
                }
            }
            return trans;
        }
    }






    public class TransactionModel
    {
        public TransactionsType type { get; set; }
        public string Description { get; set; }
        public Double Amount { get; set; }
        public Guid? CartId { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int? ProductId { get; set; }
        public int? Points { get; set; }
    }
}
