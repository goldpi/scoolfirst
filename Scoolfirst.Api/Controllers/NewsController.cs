﻿using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Scoolfirst.Api.Controllers
{
    [Authorize]
    public class NewsController : ApiController
    {
        private ScoolfirstContext db = new ScoolfirstContext();
        [Route("api/News")]
        [HttpGet]
        public IEnumerable<NewsGlobal> Get(int page=0,int size = 4)
        {
            return db.News.Where(i => i.Type == NewsType.News).OrderByDescending(i => i.On).Skip(page * size).Take(size);
        }


        [Route("api/ComputerCreative")]
        [HttpGet]
        public IEnumerable<NewsGlobal> Comp(int page = 0, int size = 5)
        {
            return db.News.Where(i=>i.Type==NewsType.CC).OrderByDescending(i => i.On).Skip(page * size).Take(size);
        }

        [Route("api/Notice/Foundation")]
        [HttpGet]
        public object SchoolFoundationNotice(int school)
        {
            return new { Notice = db.Schools.Find(school).FoundationNotice };
        }
        [Route("api/Notice/Assement")]
        [HttpGet]
        public object SchoolAssementNotice(int school)
        {
            return new { Notice = db.Schools.Find(school).AssementNotice };
        }
    }
}
