﻿
app.controller('autoCompleteController',
    function ($scope, $timeout, autoCompleteFactory) {
        $scope.Term = "";
        $scope.Feed = [{}];
        $scope.KDC = [{}];
        $scope.Projects = [{}];
        $scope.Fetching = false;
        $scope.HasItem = false;
        $scope.ShowAtocomplete = false;
        $scope.TotalFeed = 0;
        $scope.TotalKDC = 0;
        $scope.TotalProjects = 0;
        $scope.Request = null;
        $scope.OnFocus = function () {
            $scope.ShowAtocomplete = true;
            console.log("true")
        }
        $scope.OnBlur = function () {
            $timeout(function () {
                $scope.ShowAtocomplete = false;
            }, 360)

            console.log("false")
        }
        $scope.OnChange = function () {
            if ($scope.Fetching) {
                // autoCompleteFactory.cancel("abort")
                console.log("Abort")
            }
            $scope.Fetching = true;
            autoCompleteFactory.getData($scope.Term, function (result) {
                $scope.Feed = result.Feed;
                $scope.KDC = result.KDC;
                $scope.Projects = result.Project;
                
                $scope.TotalFeed = result.TotalFeed;
                $scope.TotalKDC = result.TotalKDC;
                $scope.TotalProjects = result.TotalProjects;
                $scope.Fetching = false;
                $scope.HasItem = true;
                console.log(result)
            }, function () { });

        }
    });
window.app.factory('autoCompleteFactory', ["$http", function ($http) {
    return {
        getData: function (searchTerm, callback, errorCallback) {
            $http.get("/ajax/AutoSuggestion?s=" + searchTerm).success(callback).error(errorCallback);
        }
    }
}]);