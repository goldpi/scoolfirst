﻿'use strict';

app.controller('notificationController', ['$scope', 'backendHubProxy', '$timeout',
  function ($scope, backendHubProxy, $timeout) {
     // var qs = $cookies.get('token');
      $scope.notification = [];
     
      console.log('trying to connect to service')
      var notificationHub = backendHubProxy(backendHubProxy.defaultServer, 'notificationHub');
      console.log('connected to service')
      $scope.clear = function () {
          $scope.notification = [];
          notificationHub.invoke("MarkReadAll", "ee");
      }
      $scope.send = function () {
          var d = new Date();
          notificationHub.invoke("MyUnreadnotification", d.toUTCString());
      }
      $scope.getNotification = function () {
          var d = new Date();
          var last = $scope.notification.length;
          if (last > 0) {
              var findd = $scope.notification[last];
              notificationHub.invoke("MyUnreadnotification", findd.Date);
          } else {
              notificationHub.invoke("MyUnreadnotification", d.toUTCString());
          }
      }
      $timeout($scope.getNotification, 180000)
     
      notificationHub.on('notification', function (data) {
          
          angular.forEach(data, function (value, key) {
              $scope.notification.push(value)
          });

          console.log(data);
      });
      notificationHub.on('markedRead', function (data) {
          // sweetAlert("OOps..", data, "success");
      });
  }
]);