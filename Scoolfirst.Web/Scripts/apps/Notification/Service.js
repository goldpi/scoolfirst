﻿'use strict';

app.factory('backendHubProxy', ['$rootScope', 'backendServerUrl',
  function ($rootScope, backendServerUrl) {

      function backendFactory(serverUrl, hubName) {
        
         
          var connection = $.hubConnection(backendServerUrl);
          var proxy = connection.createHubProxy(hubName);

          connection.start().done(function () { });

          return {
              on: function (eventName, callback) {
                  proxy.on(eventName, function (result) {
                      try{
                          $rootScope.$apply(function () {
                              if (callback) {
                                  callback(result);
                              }
                          });
                      }
                      catch (ex) {
                          console.error(ex.message)
                      }
                  });
              },
              invoke: function (methodName,data, callback) {
                  proxy.invoke(methodName,data)
                  .done(function (result) {
                      try{

                     
                          $rootScope.$apply(function () {
                              if (callback) {
                                  callback(result);
                              }
                          });
                      }
                      catch (ex) {
                          console.error(ex.message)
                      }

                  });
              }
          };
      };

      return backendFactory;
  }]);