﻿app.controller("ReasoningRootController", ["$scope", "$http", function ($scope, $http) {
    $scope.PreviousHistory = false;
	$scope.IsTimed = Timed;
	$scope.timerRunning = false;
	TimerRelated = 0;
	$scope.Loaded = false;
	$scope.Time = mintExam;
	$scope.TestStarted = false;
	$scope.TestPaused = false;
	$scope.TestClosed = false;
	$scope.Index = 0;
	$scope.TimeTaken = 0;
	$scope.SelectedQuestion = null;
	$scope.SlecetQuestion = function (index) {
		console.log(index)
		if (index >= 0 && index < $scope.Test.Questions.length) {
			$scope.SelectedQuestion = $scope.Test.Questions[index];
			$scope.Index = index;

		}
	}
	$scope.ToChar = function (val) {
		return String.fromCharCode(65 + val);
	}

	$scope.Answer = function (data) {
		$scope.SelectedQuestion.Answer = data;
	}


	$scope.startTimer = function () {
		$scope.$broadcast('timer-start');
		$scope.timerRunning = true;
		$scope.TestStarted = true;
	};
	function stop() {
		$scope.TestClosed = true;



		// Correct Answers =
		var correct = 0;
		var notAnswred = 0;
		var wrong = 0;
		var Answers = []
		for (var i = 0; i < $scope.Test.Questions.length; i++) {
			if ($scope.Test.Questions[i].Answer != undefined) {
				for (var i2 = 0; i2 < $scope.Test.Questions[i].Option.length; i2++) {
					if ($scope.Test.Questions[i].Option[i2].Id == $scope.Test.Questions[i].Answer) {
						if ($scope.Test.Questions[i].Option[i2].Correct) {
							correct++;
						} else {
							wrong++;
						}
						Answers.push( { AnswerId: $scope.Test.Questions[i].Option[i2].Id, ReasoningId: $scope.Test.Id, QuesionId: $scope.Test.Questions[i].Id});
					}
				}


			} else {
				notAnswred++;
			}
		}

		if ($scope.PreviousHistory===false) {
		    $http.post("/api/ReasoningRoots/", Answers).success(function (save) {
		        alert("Data ")
		    })
		}
		
		$scope.Report = [];
		$scope.Report.push({ label: "Correct", value: correct });
		$scope.Report.push({ label: "Wrong", value: wrong });
		$scope.Report.push({ label: "Skipped", value: notAnswred });
		$scope.$apply();

	}
	$scope.stopTimer = function () {

		$scope.timerRunning = false;
		$scope.TestClosed = true;
		$scope.TimeTaken = TimerRelated.toString();
		TimerRelated = $scope.Time;
		$scope.$broadcast('timer-stop');
	};
	$scope.$on('timer-stopped', function (event, data) {
		if (TimerRelated == $scope.Time) {
			stop();
		}
	});
	$scope.$on('timer-tick', function (event, data) {
		TimerRelated++;
		if (TimerRelated == $scope.Time) {
			$scope.TimeTaken = TimerRelated.toString();
			$scope.$broadcast('timer-stop');
		}
	});
	$scope.ResumeTimer = function () {
		$scope.$broadcast('timer-resume');
		$scope.timerRunning = true;
	};

	$scope.Load = false;
	$scope.Sub = "P";
	$scope.change = function (sec) {
		$scope.Sub = sec;
	}
	$scope.TotalMin = function () {
		var p = getMin($scope.TimeTaken)
		return p;
	}
	$scope.Attempt = function () {
		var re = 0;
		if ($scope.Test.Questions != undefined)
			for (var i = 0; i < $scope.Test.Questions.length; i++) {
				if ($scope.Test.Questions[i].Answer != undefined) {
					re++;
				}
			}
		return re;
	}

	$scope.Redraw = function () {
		stop();
	}

	function getMin(v) {
		var m = parseInt(v);

		var min = parseInt(m / 60);
		var hour = parseInt(min / 60);
		min = min % 60;
		var sec = m % 60;
		return hour + " hours " + min + " minutes " + sec + " sec";
	}
	function getsec(v) {
		var m = parseInt(v);
		var min = (m / 60);
		var sec = m % 60;
		return m + " minutes " + sec + " sec";
	}
	$scope.GetId = function(Id) {
		console.log(Id)
		$scope.Load = true;
		$http.get("/api/ReasoningRoots/check/" + Id).success(function (result) {
		    $scope.PreviousHistory = result;
				var url = "/api/ReasoningRoots/" + Id;
				$http.get(url).success(function (data) {
				    $scope.Test = data;
				    $scope.Loaded = true;
				    $scope.SelectedQuestion = $scope.Test.Questions[0];
				    if (!$scope.IsTimed) {
				        $scope.$broadcast('timer-start');
				        $scope.timerRunning = true;
				        $scope.TestStarted = true;
				    }
				    if (result) {
				        $http.get("/api/ReasoningRoots/AnswersGiven/" + Id).success(function (ans) {
				            for (var index = 0; index < $scope.Test.Questions.length; index++) {
                                console.log(index)
				                for (var indexAns = 0; indexAns < ans.length; indexAns++) {
                                    console.log(indexAns)
				                    console.log('qid=' + ans[indexAns].QuestionId + ' \n tqi=' + $scope.Test.Questions[index].Id)
				                    if ($scope.Test.Questions[index].Id == ans[indexAns].QuestionId) {
                                        
				                        $scope.Test.Questions[index].Answer = ans[indexAns].OptionId;
				                    }
				                }
				            }
				            stop();
				        })
				    }
				})
			
		});
	}
}]);