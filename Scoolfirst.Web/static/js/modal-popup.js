﻿    $(function () {
        $.ajaxSetup({
            cache: false
        });
        $("[data-modal]").on("click", function (e) {
            e.preventDefault();

            $('#myModal').modal({
                keyboard: true
            }, 'show');

            $('#myModalContent').load(this.href, function () {
                console.clear();
                console.log(this);
                //bindForm(this);
            });
            return false;
        });


    });

    function bindForm(dialog) {
        $('form', dialog).submit(function () {
            $('#progress').show();
            $('#myModalContent').html('<div class="text-info pn centered " style="box-shadow:0px;"><i class="fa fa-spinner faa-spin animated" style="margin-top: 12%; font-size: 7.6em;"></i></div>');

            $.ajax({
                url: this.action,
                type: this.method,
                data: $(this).serialize(),
                success: function (result) {
                    console.log(result);
                    if (result.success) {
                        $('#myModal').modal('hide');
                        $('#progress').hide();
                        location.reload();
                    } else {
                        $('#progress').hide();
                        $('#myModalContent').html(result);
                        bindForm();
                    }
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr);
                    console.log(textStatus);
                    console.log(error);
                }
            });
            return false;
        });
    }

   