﻿/*
Expandable Floating Button
Author: Md Yusuf
Refrance: http://patrickdesjardins.com/blog/how-to-create-a-bottom-right-fixed-button-in-css-with-expanding-hover-animation
*/
$.fn.FloatExpandableButton = function () {
    //console.log(this);
    this.each(function () {
        $(this).hover(function () {
           
            $(this).find(".long-text").addClass("show-long-text");
        }, function () {
            $(this).find(".long-text").removeClass("show-long-text");
        });

    });

};