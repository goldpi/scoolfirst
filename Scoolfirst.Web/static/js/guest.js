﻿$().ready(function () {

    // Login Form Validation
    $('#loginform').bootstrapValidator({
        container: 'tooltip',
        //        trigger: 'blur',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },


        fields: {

            Email: {
                group: '.fg-line',
                message: 'The email id "or" mobile no. is not valid',
                validators: {
                    notEmpty: {
                        message: 'The email id "or" mobile no. is required and cannot be empty'
                    },

                    regexp: {
                        regexp: /\d{10}|^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        message: 'Please enter your valid E-Mail "or" Mobile No. only.'
                    }
                }
            },

            Password: {
                group: '.fg-line',
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    }
                }
            },

        }

    });


    //Registration Form Validation

    $('#registrationform').bootstrapValidator({
        //        live: 'disabled',
        container: 'tooltip',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            Name: {
                group: '.fg-line',
                validators: {
                    notEmpty: {
                        message: 'Your name is required and cannot be empty'
                    }
                }
            },
            PhoneNo: {
                group: '.fg-line',
                validators: {
                    notEmpty: {
                        message: 'Mobile No. is required and cannot be empty'
                    },
                    regexp: {
                        regexp: /\d{10}/,
                        message: 'Please enter your valid mobile no. only.'
                    }
                }
            },


            Email: {
                group: '.fg-line',
                validators: {
                    regexp: {
                        regexp: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        message: 'Please enter your valid e-mail only.'
                    }
                }
            },

            ClassId: {
                group: '.fg-line',
                validators: {
                    greaterThan: {
                        value: 0,
                        message: 'Please select a class'
                    }
                }
            },

            SchoolId: {
                group: '.fg-line',
                validators: {
                    greaterThan: {
                        value: 0,
                        message: 'Please select a school'
                    }
                }
            },


            WhoIam: {
                group: '.input-group',
                validators: {
                    notEmpty: {
                        message: 'are you Stuednt "or" Parent?'
                    }
                }
            }


        }
    });

    //Social Registration Form Validation

    $('#socialregistrationform').bootstrapValidator({
        //        live: 'disabled',
        container: 'tooltip',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            Name: {
                group: '.fg-line',
                validators: {
                    notEmpty: {
                        message: 'Your name is required and cannot be empty'
                    }
                }
            },
            PhoneNo: {
                group: '.fg-line',
                validators: {
                    notEmpty: {
                        message: 'Mobile No. is required and cannot be empty'
                    },
                    regexp: {
                        regexp: /\d{10}/,
                        message: 'Please enter your valid mobile no. only.'
                    }
                }
            },

            Email: {
                group: '.fg-line',
                validators: {
                    regexp: {
                        regexp: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, message: 'Please enter your valid e-mail only.'
                    }
                }
            },

            ClassId: {
                group: '.fg-line',
                validators: {
                    greaterThan: {
                        value: 0,
                        message: 'Please select a class'
                    }
                }
            },

            SchoolId: {
                group: '.fg-line',
                validators: {
                    greaterThan: {
                        value: 0,
                        message: 'Please select a school'
                    }
                }
            },


            WhoIam: {
                group: '.input-group',
                validators: {
                    notEmpty: {
                        message: 'are you Stuednt "or" Parent?'
                    }
                }
            }


        }
    });

    // ForgetPassword Form Validation
    $('#forgetpasswordform').bootstrapValidator({
        container: 'tooltip',
        //        trigger: 'blur',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },


        fields: {

            Email: {
                group: '.fg-line',
                message: 'The E-Mail "Or" Mobile No. is not valid',
                validators: {
                    notEmpty: {
                        message: 'The E-Mail "Or" Mobile No. is required and cannot be empty'
                    },

                    regexp: {
                        regexp: /\d{10}|^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        message: 'Please enter your valid E-Mail "or" Mobile No. only.'
                    }
                }
            },

        }

    });

    // Verification Form Validation
    $('#verifyform').bootstrapValidator({

        //        trigger: 'blur',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },


        fields: {

            verificationCode: {
                group: '.fg-line',
                message: 'The verification code is not valid',
                validators: {
                    notEmpty: {
                        message: 'The verification code is required and cannot be empty'
                    },

                    regexp: {
                        regexp: /\d{6}/,
                        message: 'Please enter 6 digit verification code. The code you have enter id invalid.'
                    }
                }
            },
        }

    });

});//Jquery End