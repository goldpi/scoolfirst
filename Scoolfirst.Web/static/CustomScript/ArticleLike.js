﻿var LikeUrl = "/mykdc/LikeArticle/";
var FavMe = "/mykdc/StarArticle/";

$(function () {
    Like();
    FaveMe()
})


function Like() {
    $(".LikeMe").click(function (e) {
        e.preventDefault();
        $(this).unbind("click");
        var PostId = $(this).data('id');
        var update = $(this).data('update');
        var url = LikeUrl + PostId;
        var th = this;
        $.get(url, function (data) {

            $(th).toggleClass('bgm-gray')
            $(th).toggleClass('bgm-indigo')
            $(update).html(data.Like)

        });
        Like();
    })
}

function FaveMe() {
    $(".FavMe").click(function (e) {
        e.preventDefault();
        $(this).unbind("click");
        var PostId = $(this).data('id');
        var url = FavMe + PostId;

        var th = this;
        $.get(url, function (data) {
            $(th).toggleClass('bgm-gray')
            $(th).toggleClass('bgm-pink')

        });
        FaveMe();
    })
}