﻿var CommentUrl = "/mykdc/ArticleComment/";
var CommentDelete = "/mykdc/DeleteArticleComment/";
var MoreComment = "/mykdc/LoadComments/"


$(function () {
    timeed();
    DeleteCommentRegister()
    loadComennts()
    CommentHandler();
})

//Delete Comment
function DeleteCommentRegister() {

    $(".deleteComment").unbind('click');
    $(".deleteComment").click(function (e) {
        e.preventDefault();
        var TargetDel = $(this).data('target');
        var commentCon = $(this).data('comment');
        //debugMsg(commentCon)
        var commentCount = parseInt($(commentCon).data('comment'));
        // $('#Comment_' + TargetDel).reomve();
        $.ajax({
            url: CommentDelete + TargetDel,
            type: 'post',
            success: function (data) {
                $('#Comment_' + TargetDel).remove();
                commentCount = commentCount - 1;
                //debugMsg(commentCount)
                $(commentCon).html(commentCount);
                $(commentCon).data('comment', commentCount);
            }

        })
    })
}
function CommentHandler() {
    $("form.Comment").unbind('submit');
    $("form.Comment").submit(function (e) {
        e.preventDefault();

        var Form = $(this);
        var Target = $(this).data('target');
        var comment = Form.data('comment');
        var commentCount = parseInt($(comment).data('comment'));
        //debugMsg(Target);
        $.ajax({
            url: CommentUrl,
            data: $(this).serialize(),
            success: function (data) {
                $(Target).append(data);
                DeleteCommentRegister();
                Form.trigger('reset');
                commentCount = commentCount + 1;
                //debugMsg(commentCount)
                $(comment).html(commentCount);
                $(comment).data('comment', commentCount);
                timeed();
            },
            dataType: 'html',
            type: 'Post',
            error: function () {
                Error("Some thing happend very bad!")
            }
        });
    })
}
function timeed() {
    $('small.CommentTime').each(function () {
        var dataT = $(this).data("time");
        //console.log(dataT);
        $(this).html(moment(dataT).startOf('hour').fromNow());
    });
}
function loadComennts() {
    $('.commentLoader').unbind('click');
    $('.commentLoader').click(function (e) {
        e.preventDefault();
        $(this).html("Loading..");
        var th = this;
        var PostId = $(this).data('id');
        var update = $(this).data('update');
        var page = parseInt($(this).data('page'));
        var date = $(this).data('date');
        var url = MoreComment + "?PostId=" + PostId + "&page=" + page + "&before=" + date;
        $.get(url, function (data) {
            $(update).prepend(data);
            $(th).html("More Comments");
            page++;
            $(th).data('page', page);
            timeed()
        })
    })
}