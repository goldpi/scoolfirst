﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Scoolfirst.Web.Models
{

    public class MenuItem
    {
        public MenuItem()
        {
            ParentId = 0;
            Menus = new List<MenuItem>();
        }

        public int MenuId { get; set; }
        public int ParentId { get; set; }
        public string Title { get; set; }
        public string ShortDisplay { get; set; }
        public int DisplayOrder { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }
        public string Urls { get; set; }
        public string[] Url => string.IsNullOrEmpty(Urls)? Urls.Contains(',')?Urls.ToLower().Split(','):new string[] { Urls.ToLower() } :new string[0];

        public bool IsActive(HttpContextBase HttpContextbase)
        {
            var controller = HttpContextbase.Request.RequestContext.RouteData.GetRequiredString("Controller").ToLower();
            var dissed = HttpContextbase
                .Request
                .Url
                .ToString()
                .ToLower()
                .Split('/').Where(i => !string.IsNullOrWhiteSpace(i));
            var rel = dissed.Any(i => Urls.Contains(i));
            var ret = Urls.ToLower().Contains(controller);
            var urls = Url.Any(o => o==controller);
            var dised = dissed.Any(o => Url.Any(p => p == o));
            return ret || rel || urls || dised;
        }
        public List<MenuItem> Menus { get; set; }

        //Level To manage menu depth
        public int Level { get; set; }
    }

    public class MenuItemManager
    {
        public List<MenuItem> Menus { get; set; }

        public List<MenuItem> Load(string location)
        {
            XElement menus = XElement.Load(location);

            Menus = LoadMenus(menus, 0);

            return Menus;
        }

        //TODO Multilavel menu prepration
        private List<MenuItem> LoadMenus(XElement menus, int ParentId)
        {
            List<MenuItem> nodes = new List<MenuItem>();

            nodes = (from node in menus.Elements("Menu")
                     where int.Parse(node.Element("ParentId").Value) == ParentId
                     orderby int.Parse(node.Element("DisplayOrder").Value)
                     select new MenuItem
                     {
                         Level =node.Element("Level").HasAttributes? int.Parse(node.Element("Level").Value) : -1,

                         MenuId = int.Parse(node.Element("MenuId").Value),
                         ParentId = int.Parse(node.Element("ParentId").Value),
                         Title = node.Element("Title").Value,
                         ShortDisplay = node.Element("ShortDisplay").Value,
                         DisplayOrder = int.Parse(node.Element("DisplayOrder").Value),
                         Action = node.Element("Action").Value,
                         Icon = node.Element("Icon").Value,
                         Urls = node.Element("Urls").Value,

                         Menus =
                             (ParentId != int.Parse(node.Element("MenuId").Value)
                                 ? this.LoadMenus(menus, int.Parse(node.Element("MenuId").Value))
                                 : new List<MenuItem>())
                     }
                ).ToList();



            return nodes;
        }
    }
}