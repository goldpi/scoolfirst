﻿using FizzWare.NBuilder;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Microsoft.AspNet.SignalR;
using Quartz;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.MongoDatabase;
using Scoolfirst.Model.Notifications;
using Scoolfirst.Model.Utility;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.Web.hubs;
using Scoolfirst.Web.Infra;
using System;
using System.Collections.Generic;
using System.IO;

namespace Scoolfirst.Web.Sheduler
{
    public class NotificationJob : IJob
    {
        Scoolfirst.Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();

        public void Execute(IJobExecutionContext context)
        {

            var Hubcontext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            var notificationtest = new NotificationManagerRepo().ListAllPer();


            foreach (var i in notificationtest)
            {
                var dat = DateTimeHelper.GetIST();
                var dat2 = DateTimeHelper.GetIST().Subtract(new TimeSpan(0, 0, 20));
                if (i.Date <= dat && i.Date >= dat2)
                {
                    var p = new List<Notification>();
                    p.Add(i);
                    Hubcontext.Clients.Group(i.Group).notification(p);
                }

            }
#if DEBUG
            RandomGenerator gen = new RandomGenerator();

            var chapters = Builder<Notification>.CreateListOfSize(1)
                   .All()
                       .With(o => o.Date = DateTime.Now)
                       .With(o => o.Group = "All")
                       .With(o => o.Id = Guid.NewGuid().ToString())
                       .With(o => o.Module = (Model.Notifications.ModType)gen.Next(0, 5))
                       .With(o => o.PostId = Guid.NewGuid().ToString())
                       .With(o => o.ImageUrl = "http://static.upgrde.net/source/imgres_1.jpg")
                       .With(o => o.Link = $"/post/{o.Module}/{o.PostId}")
                   .Build();
            Hubcontext.Clients.Group("All").notification(chapters);
#endif
        }
    }

    public class IndexingJob : IJob
    {
        ScoolfirstContext dc = new ScoolfirstContext();
        public void Execute(IJobExecutionContext context)
        {
            var p = new LuceneSearchProvider<Questions>();
            if (!Directory.Exists(p._luceneDir)) Directory.CreateDirectory(p._luceneDir);
            p.AddUpdateLuceneIndex(dc.Questions, (i, t) =>
            {
                var searchQuery = new TermQuery(new Term("Id", i.Id.ToString()));
                t.DeleteDocuments(searchQuery);
                var doc = new Document();
                doc.Add(new Field("Id", i.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                doc.Add(new Field("Refrence", i.Refrence ?? "", Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("Class", i.Class ?? "", Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("School", i.School ?? "", Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("Subject", i.Subject ?? "", Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("Title", i.Title ?? "", Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("Answer", i.Answer ?? "", Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("ChapterId", i.ChapterId.ToString(), Field.Store.YES, Field.Index.ANALYZED));
                return doc;
            });
            var feed = new LuceneSearchProvider<Model.Feed.Feeds>();
            if (!Directory.Exists(feed._luceneDir)) Directory.CreateDirectory(feed._luceneDir);
            feed.AddUpdateLuceneIndex(dc.Feeds, (i, t) =>
            {
                var searchQuery = new TermQuery(new Term("Id", i.Id.ToString()));
                t.DeleteDocuments(searchQuery);
                var doc = new Document();
                doc.Add(new Field("Id", i.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                doc.Add(new Field("Title", i.Title ?? "", Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("PostId", i.PostId.ToString() ?? "", Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("GroupId", i.GroupId.ToString() ?? "", Field.Store.YES, Field.Index.ANALYZED));

                return doc;
            });
            //ToDo: add indexing 
        }
    }
}