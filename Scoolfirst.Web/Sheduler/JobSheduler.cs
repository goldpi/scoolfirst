﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoolfirst.Web.Sheduler
{
    public class JobSheduler
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();
            IJobDetail job = JobBuilder.Create<NotificationJob>().Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger1", "group1")
                .StartNow()
                .WithSimpleSchedule(x => x
                .WithIntervalInSeconds(20)
                .RepeatForever())
                .Build();
            scheduler.ScheduleJob(job, trigger);
            //IJobDetail job2 = JobBuilder.Create<IndexingJob>().Build();
            //ITrigger trigger2 = TriggerBuilder.Create()
            //    .WithIdentity("trigger2", "group2")
            //    .StartNow()
            //    .WithSimpleSchedule(c => c
            //    .WithIntervalInHours(12)
            //    .RepeatForever())
            //    .Build();
            //scheduler.ScheduleJob(job2, trigger2);
        }


    }
}