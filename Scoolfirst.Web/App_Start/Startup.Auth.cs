﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Owin;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using Scoolfirst.Model.Provider;
using System;
using System.Threading.Tasks;

namespace Scoolfirst.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {


            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ScoolfirstContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            var cookie = new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),

                CookieDomain = $".{HostingServerInfo.Domain.Url()}",
                CookieName = "xauc",


                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, User>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            };

            app.Map("/signalr", map =>
            {
                // Setup the CORS middleware to run before SignalR.
                // By default this will allow all origins. You can 
                // configure the set of origins and/or http verbs by
                // providing a cors options with a different policy.
                //map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration
                {
                    EnableDetailedErrors = true,
                    // You can enable JSONP by uncommenting line below.
                    // JSONP requests are insecure but some older browsers (and some
                    // versions of IE) require JSONP to work cross domain
                    //EnableJSONP = true
                };
                map.UseCookieAuthentication(cookie);
                // Run the SignalR pipeline. We're not using MapSignalR
                // since this branch already runs under the "/signalr"
                // path.
                map.RunSignalR(hubConfiguration);
            });
            app.UseCookieAuthentication(cookie);
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            #region facebookConfig
            // FacebookAuthenticatedContext b = new FacebookAuthenticatedContext()


            var x = new FacebookAuthenticationOptions();
            x.Scope.Add("email");
            x.Scope.Add("user_birthday");
            x.Scope.Add("user_photos");
#if DEBUG
            x.AppId = "1705321053120024";
            x.AppSecret = "261ceb619cfe0b7554677922283d85a1";
#else
             x.AppId = "233610900318698";
            x.AppSecret = "5d79dc756049fa03ffa12fcfc19e8138";
#endif

            x.Provider = new FacebookAuthenticationProvider()
            {
                OnAuthenticated = (context) =>
                {
                    context.Identity.AddClaim(new System.Security.Claims.Claim("urn:facebook:access_token", context.AccessToken, "XmlSchemaString", "Facebook"));
                    // context.Identity.AddClaim(new System.Security.Claims.Claim("urn:facebook:email", context.Email, "XmlSchemaString", "Facebook"));
                    //context.Identity.AddClaim(new System.Security.Claims.Claim("urn:facebook:first_name", context.Name, "XmlSchemaString", "Facebook"));
                    context.Identity.AddClaim(new System.Security.Claims.Claim("urn:facebook:name", context.Name, "XmlSchemaString", "Facebook"));
                    //context.Identity.AddClaim(new System.Security.Claims.Claim("urn:facebook:birthday", context., "XmlSchemaString", "Facebook"));
                    //context.Identity.AddClaim(new System.Security.Claims.Claim("urn:facebook:hometown", context.Email, "XmlSchemaString", "Facebook"));

                    return Task.FromResult(0);
                }
            };
            x.SignInAsAuthenticationType = DefaultAuthenticationTypes.ExternalCookie;
            app.UseFacebookAuthentication(x);


            #endregion

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
    }
}