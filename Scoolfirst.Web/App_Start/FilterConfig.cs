﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Provider;
using Scoolfirst.Web.Infra;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new MenuService());
           
        }
    }
}
