﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.WorkBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Scoolfirst.Model.Common;
using Scoolfirst.ViewModel.WorkBook.TossTopic;

namespace Scoolfirst.Web.Controllers
{

    /// <summary>
    /// Derpreicated
    /// </summary>


    
    public class TossTopicController : AppAuthController
    {
        private ScoolfirstContext dc;
        public TossTopicController(ScoolfirstContext context)
        {
            dc = context;
        }
        // GET: TossTopic
        public ActionResult Index()
        {
            var ls = dc.Trending.Where(i => i.Display == true).ToList();
            return ControllerContext.IsChildAction||Request.IsAjaxRequest()?PartialView(ls) as ActionResult:View(ls);

        }

        public ActionResult Revision(string id,int page=0)
        {
            var user = dc.Users.FirstOrDefault(i => i.UserName == User.Identity.Name);
            var re=RevisionNote.RevisionsNotes(dc.Chapters.Include(i=>i.Notes),user.ClassId)
                .Where(i => i.Topic.Contains(id))
                .OrderBy(i => i.Topic)
                .Skip(page * 20)
                .Take(20)
                .ToList();

            return Request.IsAjaxRequest()? (ActionResult) PartialView(re):View(re);
        }
        public ActionResult Progressive(string id, int page = 0)
        {
            try
            {
                var user = dc.Users.FirstOrDefault(i => i.UserName == User.Identity.Name);
                var @class = dc.Classes.Find(user.ClassId);
                var re = ProgressiveQuestions(@class.NumericDisplay, dc.Classes, dc.Chapters, id).Distinct().OrderBy(i => i.ChapterId).Skip(page * 20).Take(20);
                return Request.IsAjaxRequest() ? (ActionResult)PartialView(re)
                    : View(re);
            }
            catch
            {
                return View("Error");
            }
        }
        private IEnumerable<Questions> ProgressiveQuestions(int classId,IQueryable<Classes> @Classes,IQueryable<Chapter> Chapteres,string st)
        {
            return ProgressiveLearing
                .ProgressiveQuestion(@Classes, Chapteres, classId)
                .Where(i => i.Refrence.Contains(st));
        }
        public ActionResult Hybrid(string id, int page = 0)
        {

            var user = dc.Users.FirstOrDefault(i => i.UserName == User.Identity.Name);
            var cl = user.ClassId;
            var pr = HybridLearning
                .HybridLearningQuestions(cl, dc.Chapters.Include(i => i.Questions)).Where(i => i.Refrence.Contains(id)).OrderBy(i => i.Title).Skip(page * 20).Take(20);
            return Request.IsAjaxRequest() ? (ActionResult)PartialView("Progressive",pr) : View("Progressive", pr);
        }
        private IEnumerable<Questions> TossTopicData(string term, int classId, int page = 0, int size = 30)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var bo = dc.Books.Include(i => i.Questions).Where(i => i.ClassesId == classId).Select(i => i.Questions).ToList();
            foreach (var item in bo)
            {
                foreach (var i in item.Where(i => i.Refrence.Contains(term)))
                {
                    yield return i;
                }
            }
        }
        
    }
}