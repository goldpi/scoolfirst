﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.NAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Web.Controllers
{

    /// <summary>
    /// Halted 
    /// </summary>
    
    public class NLOController : AppAuthController
    {
        private ScoolfirstContext dc;
       
        public NLOController(ScoolfirstContext Context)
        {
            dc =Context;
            
        }
        // GET: NLO
        public ActionResult Index(int page = 0)
        {

                var user = User.Identity.Name;
                var returnVal = dc.Olympiads
                    .OrderByDescending(i => i.AddedOn)
                    .Skip(page * 3)
                    .Take(3)
                    .ToList();
                if (Request.IsAjaxRequest())
                    return PartialView(returnVal);
                return View(returnVal);  

        }

        public ActionResult TestList(int page = 0)
        {
            
            var user = User.Identity.Name;

            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            var returnVal = dc.QuestionSets
                .Where(i => i.Type == Model.MCQ.TYPEQuetionSet.NAO)

                 .ToList();
            return PartialView(returnVal);
        }

        public ActionResult Test(Guid Id)
        {
            var data = dc.QuestionSets.FirstOrDefault(i=>i.Id==Id);

            return View("Exam",data);
        }

    }
}
