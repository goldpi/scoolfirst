﻿using Scoolfirst.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Web.Controllers
{

    
    public class YouInspireMeController : AppAuthController
    {
        public YouInspireMeController(ScoolfirstContext Context)
        {
            dc = Context;
        }
        private ScoolfirstContext dc;
        // GET: YouInspireMe
        public ActionResult Index(int page = 0, int size = 4)
        {
            var data = dc.RoleModels
                .OrderByDescending(i => i.On)
                .Skip((page) * size).Take(size).ToList();
            if (Request.IsAjaxRequest())
                return PartialView(data);
            return View(data);
        }


        public ActionResult Details(int Id)
        {
            var data = dc.RoleModels.Find(Id);
            return View(data);
        }

    }
}