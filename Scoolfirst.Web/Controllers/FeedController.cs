﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Feed;
using System.Data.Entity;
using Scoolfirst.ViewModel.Feed;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Web.Controllers
{
    
    public class FeedController : AppAuthController
    {
        public FeedController(ScoolfirstContext dataContext)
        {
            dc = dataContext;
        }
        ScoolfirstContext dc;
        FeedItems FeedHandler = new FeedItems();
        // GET: Feed
        public ActionResult Index(int page = 0, string Category = "MRT")
        {

            var Feeds = dc.Posts.Where(i => i.Type == PostType.MUST_REMEMBER_TRICKS)
                .OrderBy(i => i.OnDateTIme)
                .Skip(page * 3)
                .Take(3).ToList();
            if (Request.IsAjaxRequest())
                return PartialView("PartialFeed", Feeds);
            return View(Feeds);
            //FeedItems items = new FeedItems();
            //items.GetItems(dc, User.Identity.Name, page, 3, Category);
           
            //return View(items.Feed);
        }
        [HttpPost]
        public ActionResult FeedComment(Guid Id, string Comment)
        {
            FeedComment comment = FeedHandler.AddComment(dc, Comment, Id, User.Identity.Name);
            return PartialView(comment);
        }
        [HttpPost]
        public ActionResult DeleteComment(Guid Id)
        {
            var comment = dc.FeedComments.Find(Id);
            dc.FeedComments.Remove(comment);
            dc.SaveChanges();
            return Content("delete");
        }
        public ActionResult LoadComments(Guid PostId, DateTime before, int Page = 2)
        {
            return PartialView(dc.FeedComments.Where(i => i.FeedId == PostId && i.On < before).OrderByDescending(i => i.On).Skip(Page * 3).Take(3).ToList().OrderBy(i => i.On));
        }
        public ActionResult FeedLike(Guid Id)
        {
            var like = FeedHandler.AddRemoveFeedLike(dc, Id, User.Identity.Name);
            return Json(new { Like = like }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeedStar(Guid Id)
        {
            var like = FeedHandler.AddRemoveFeedStar(dc, Id, User.Identity.Name);
            return Json(new { Like = like }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(Guid Id)
        {
            var user = User.Identity.Name;
            var UserId = dc.Users.FirstOrDefault(i => i.UserName == user).Id;
            var l = dc.Feeds.Include(i => i.Post)
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Stared).FirstOrDefault(i => i.Id == Id);
            var feed = new FeedViewModel { Feed = l, FavByMe = l.Stared.Any(i => i.UserId == UserId), LikedByMe = l.Likes.Any(i => i.UserId == UserId) };
            return View(feed);
        }
        #region Removed

        public ActionResult ExpLearning(long Id)
        {
            var vid = dc.PostImages.Where(i => i.PostId == Id);
            return PartialView(vid);
        }

        public ActionResult BBF(long Id)
        {
            var vid = dc.PostVideo.FirstOrDefault(i => i.PostId == Id);
            return PartialView(vid);
        }


        //    public ActionResult UInspireMe(int page=0,int size=4)
        //    {
        //        var data = dc.RoleModels.OrderByDescending(i => i.On).Skip((page) * size).Take(size).ToList();
        //        if (Request.IsAjaxRequest())
        //            return PartialView(data);
        //        return View(data);
        //    }


        //    public ActionResult RandMTEst(int page = 0)
        //    {

        //        var user = User.Identity.Name;

        //        dc.Configuration.ProxyCreationEnabled = false;
        //        dc.Configuration.LazyLoadingEnabled = false;
        //        var returnVal = dc.QuestionSets.Where(i => i.Type == Model.MCQ.TYPEQuetionSet.RNM)

        //             .ToList();

        //        return PartialView(returnVal);
        //    }


        //    public ActionResult RandM(int page=0)
        //    {
        //        ViewBag.Page = string.Format("/feed/RANDM?page={0}", (page + 1));
        //        var user = User.Identity.Name;

        //        dc.Configuration.ProxyCreationEnabled = false;
        //        dc.Configuration.LazyLoadingEnabled = false;
        //        var returnVal =  dc.RnmPosts.OrderByDescending(i=>i.AddedOn).Skip(page*3).Take(3)
        //           // .Where(i => i.Class == @class)
        //            .ToList();
        //        if (Request.IsAjaxRequest())
        //            return PartialView("PartialRANDM", returnVal);
        //        return View(returnVal);
        //    }
        //    public ActionResult Exam(Guid Id)
        //    {
        //        var data = dc.QuestionSets.Find(Id);

        //        return View(data);
        //    }
        #endregion


    }
}