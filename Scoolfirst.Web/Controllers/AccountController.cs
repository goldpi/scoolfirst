﻿/// <summary>
/// 80 % code is genrated
/// Don't remove the cookie dependency
/// Signalr depedency
/// Authors : Md Yusuf and Imran Ali
/// </summary>
namespace Scoolfirst.Web.Controllers
{


    using Facebook;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Model.Common;
    using Model.Context;
    using Model.Identity;
    using Model.Provider;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;

    using Newtonsoft.Json;

    using Scoolfirst.ViewModel.Common;

    using ViewModel;

    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        /// <summary>
        /// Url For the Api Server to Request Token.
        /// </summary>
        string ApiUrl = HostingServerInfo.Api.Url();
        private ScoolfirstContext dc;

        public AccountController(ScoolfirstContext dataContext)
        {

            dc = dataContext;

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="signInManager"></param>
        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ScoolfirstContext dataContext)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            dc = dataContext;

        }




        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }




        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public async Task<ActionResult> AppLog(string token, string url = "/feed")
        {

            var res = await GetUserInfo(token);
            res = res.Remove(0, 1);
            res = res.Remove(res.Length - 1, 1);
            var user =
           HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().Users.FirstOrDefault(i => i.Id == res);
            if (user != null)
                HttpContext.GetOwinContext().Get<ApplicationSignInManager>().SignIn(user, true, true);
            else
            {
                return Content(res);
            }
            return RedirectToLocal(url);

        }


        private async Task<User> GetUser(string token)
        {
            var user = await GetUserInfo(token);
            var userInfo = dc.Users.Find(user);
            return userInfo;
        }


        private async Task<string> GetUserInfo(string token)
        {
            if (Request.IsSecureConnection)
                ApiUrl = "https://" + ApiUrl;
            else
                ApiUrl = "http://" + ApiUrl;
            var client = new HttpClient { BaseAddress = new Uri(ApiUrl) };
            client.DefaultRequestHeaders.Add("Authorization", "bearer " + token);
            var response = await client.GetStringAsync("api/account/UserEmail");
            return response;
        }

        private async Task<string> GetToken(string user, string pass)
        {
            if (Request.IsSecureConnection)
                ApiUrl = "https://" + ApiUrl;
            else
                ApiUrl = "http://" + ApiUrl;
            var client = new HttpClient { BaseAddress = new Uri(ApiUrl) };
            var tok = new StringContent("username=" + user + "&password=" + pass + "&grant_type=password");

            client.DefaultRequestHeaders.Add("Authorization", "bearer " + user);
            var response = await client.PostAsync("token", tok);
            var ressule = await response.Content.ReadAsStringAsync();
            return ressule;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.Ref = Request.UrlReferrer;
            return View(new LoginViewModel());
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken()]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl, string Ref)
        {
            Session.Clear();
            ViewBag.Ref = Ref;
            ViewBag.ReturnUrl = returnUrl;
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            if (Regex.IsMatch(model.Email, "\\d[10]"))
            {
                //TODO what if email id not exist

                var UserProfile = dc.Users.FirstOrDefault(i => i.PhoneNumber == model.Email);
                if (UserProfile != null)
                    model.Email = UserProfile.Email ?? UserProfile.PhoneNumber;

            }


            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {

                case SignInStatus.Success:
                    var userid = dc.Users.FirstOrDefault(i => i.UserName == model.Email);
                    //UserManager.IsInRole(userid, "School");
                    if (UserManager.IsInRole(userid.Id, "School"))
                        return RedirectToAction("Index", "Dashboard", new { area = "School" });
                    if (!userid.Subscribed)
                    {
                        return RedirectToAction("payment");
                    }
                    else
                        return RedirectToAction("TokenFromCookie", new { ret = returnUrl });
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);


            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        public void LoadRegistationViewData()
        {
            var Schools = dc.Schools.ToList();
            var Classes = dc.Classes.ToList();
            Classes.Insert(0, new Classes { Id = -1, RomanDisplay = "--Select Class--" });
            Schools.Insert(0, new School { Id = -1, SchoolName = "--Select School--" });
            Schools.Insert(Schools.Count, new School { Id = 0, SchoolName = "Other" });
            ViewBag.SchoolId = new SelectList(Schools, "Id", "SchoolName");
            ViewBag.ClassId = new SelectList(Classes, "Id", "RomanDisplay");

        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            LoadRegistationViewData();
            return View(new RegisterViewModel());
        }

        public ActionResult PartialRegister()
        {

            LoadRegistationViewData();
            return PartialView();
        }

        public ActionResult PartialForgetPassword()
        {

            return PartialView();
        }

        public ActionResult PartialLogin()
        {
            return PartialView();
        }
        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {

                string StudentAvtar = HostedApplicationConfig.DefaultStudentAvtar.Value();
                string ParentAvtar = HostedApplicationConfig.DefaultParentAvatar.Value();
                string code = string.Empty;
                code = NewCode(code);
                User user = model.NewUserInstance(model, StudentAvtar, ParentAvtar, code);
                var result = await UserManager.CreateAsync(user, "1qaz2wsx");
                if (result.Succeeded)
                {
                    RoleAssign(user);
                    //await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);

                    TempData["Resetcode"] = code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    //await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    TempData.Keep("Resetcode");
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link

                    var callbackUrl = Url.Action("ResetPassword", "Account", new { id = user.Id, code = code, CompleteProfile = true }, protocol: Request.Url.Scheme);
                    string message = StaticFileReader.EmailerContent(Server.MapPath("~/Templets/Welcome.html")).Replace("#candidatename#", model.Name).Replace("#varificationlink#", callbackUrl).Replace("#varificationCode#", TempData["code"].ToString()).Replace("#year#", DateTime.Now.Year.ToString());
                    await UserManager.SendEmailAsync(user.Id, "Registration-Welcome to Upgrde.com", message);
                    // string msg = string.Format("Welcome " + model.Name + "! You have successfully register with us. Your account verification code is {0}.", TempData["code"].ToString());
                    //load message

                    var expiry = DateTime.UtcNow.AddHours(3);
                    //res=res.GetMessage(MessageEvents.OnVerificationCodeSend, model.Name, TempData["code"].ToString());
                    await UserManager.SendSmsAsync(user.Id,
                        HttpUtility.UrlEncode(
                            MessageEvents.OnVerificationCodeSend.GetMessage(model.Name, TempData["code"].ToString(), expiry.ToShortDateString())));
                    return RedirectToAction("Verification", "Account", new { id = user.Id, cp = true });
                }
                AddErrors(result);
            }


            LoadRegistationViewData();
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private void RoleAssign(User user)
        {
            var store = new RoleStore<IdentityRole>(dc);
            var manager = new RoleManager<IdentityRole>(store);
            if (!manager.RoleExists("student"))
            {
                var role = new IdentityRole { Name = "student" };
                var res = manager.Create(role);
                if (res.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "student");
                }
            }
            else
            {
                UserManager.AddToRole(user.Id, "student");
            }
        }

        private string NewCode(string code)
        {
            if (TempData["code"] == null)
            {
                Random generator = new Random();
                code = generator.Next(0, 1000000).ToString("D6");
                TempData["code"] = code;
                TempData.Keep("Code");

            }

            return code;
        }


        private static User NewUserInstance(RegisterViewModel model, string StudentAvtar, string ParentAvtar, string code)
        {
            return new User
            {
                UserName = model.Email ?? model.PhoneNo,
                Email = model.Email ?? null,
                FullName = model.Name,
                Student_Parent_Admin = model.WhoIAm,
                PhoneNumber = model.PhoneNo,
                ClassId = model.ClassId,
                PictureUrl = model.WhoIAm == "Student" ? StudentAvtar : ParentAvtar,
                Address = "",
                AboutMe = "",
                Gender = "",
                SchoolId = model.SchoolId,
                RegisterdOn = DateTime.UtcNow,
                Expiry = DateTime.UtcNow.AddDays(15),
                Trial = true,
                Subscribed = false,
                ConnectionId = code,
                OtherSchool = model.OtherSchool
            };
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {

                User user;
                //check entred value is mobile no or email
                if (Regex.IsMatch(model.Email, "\\d[10]"))
                {
                    string UserId = dc.Users.FirstOrDefault(i => i.PhoneNumber.Equals(model.Email, StringComparison.InvariantCultureIgnoreCase)).Id;
                    user = await UserManager.FindByIdAsync(UserId);
                }

                else
                {
                    user = await UserManager.FindByEmailAsync(model.Email);
                }

                if (user == null)// || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // model.Email
                    ModelState.AddModelError("Email", "Sorry! We don't recognize you. Please enter your registred e-mail 'or' mobile no. and try again.");
                    // Don't reveal that the user does not exist or is not confirmed
                    return View(model);
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link

                if (TempData["code"] == null)
                {
                    Random generator = new Random();
                    TempData["code"] = generator
                        .Next(0, 1000000)
                        .ToString("D6");
                    TempData.Keep("Code");
                    user.ConnectionId = TempData["code"].ToString();
                    UserManager.Update(user);
                }
                //TODO Re-Format Communication Content
                TempData["Resetcode"] = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                TempData.Keep("Resetcode");
                var callbackUrl = Url.Action("ResetPassword", "Account", new { id = user.Id, code = TempData["Resetcode"] }, protocol: Request.Url.Scheme);
                string message = StaticFileReader
                    .EmailerContent(Server.MapPath("~/Templets/PasswordResetRequest.html"))
                    .Replace("#candidatename#", user.FullName)
                    .Replace("#varificationlink#", callbackUrl)
                    .Replace("#varificationCode#", TempData["code"].ToString())
                    .Replace("#year#", DateTime.Now.Year.ToString());
                await UserManager.SendEmailAsync(user.Id, "Password reset request-UpgradeJr.com", message);
                string msg = string.Format("Your " + Request.Url.Host + " account verification code is {0}.", user.ConnectionId.ToString());
                await UserManager.SendSmsAsync(user.Id, HttpUtility.UrlEncode(msg));
                return RedirectToAction("Verification", "Account", new { id = user.Id });
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Verification
        [AllowAnonymous]
        public ActionResult Verification(string id, bool cp = false)
        {

            if (id == null)
                return RedirectToAction("ForgotPassword", "Account");

            AccountVerificationViewModel model = new AccountVerificationViewModel();
            model.id = id;
            model.cp = cp;
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [ActionName("Verify")]
        public ActionResult Verification(AccountVerificationViewModel model)
        {

            if (string.IsNullOrEmpty(model.id))
                return RedirectToAction("ForgotPassword", "Account");

            var user = UserManager.FindById(model.id);
            if (string.Equals(user.ConnectionId, model.verificationCode, StringComparison.InvariantCultureIgnoreCase))
            {

                return RedirectToAction("ResetPassword", "Account", new { id = user.Id, Code = TempData["Resetcode"], CompleteProfile = model.cp });
            }

            ModelState.AddModelError("verificationCode", "Invalid varification code.");
            return View("Verification", model);
        }


        //TODO Resend verification code and Intergrate with UI.
        public async Task<ActionResult> ResendCode(string id)
        {
            //TODO Update the message sending to user throught SMS and mobile.
            var user = UserManager.FindById(id);
            string msg = string.Format("Your " + Request.Url.Host + " account verification code is {0}.", user.ConnectionId);
            await UserManager.SendSmsAsync(user.Id, HttpUtility.UrlEncode(msg));
            await UserManager.SendEmailAsync(user.Id, "Account Verification", msg);
            return Json(HttpStatusCode.OK, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string id, string Code, bool CompleteProfile = false)
        {
            ResetPasswordViewModel model = new ResetPasswordViewModel();
            var user = UserManager.FindById(id);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            model.id = id;
            model.Email = user.Email ?? user.PhoneNumber;
            model.Code = Code;
            model.CompleteProfile = CompleteProfile;
            return View(model);
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model, bool CompleteProfile)
        {
            ViewBag.CompleteProfile = CompleteProfile;
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            User UserDetail = null;

            if (Regex.IsMatch(model.Email, "\\d[10]"))
            {
                UserDetail = dc.Users.FirstOrDefault(i => i.PhoneNumber == model.Email);

            }
            else
                UserDetail = await UserManager.FindByNameAsync(model.Email);

            if (UserDetail == null)
            {
                //TODO Log to application if user dose not exist while setting the password.
                // Don't reveal that the user does not exist

            }
            var result = await UserManager.ResetPasswordAsync(UserDetail.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                // Todo communication with user after password change successfully.


                if (CompleteProfile)
                {
                    SignInManager.SignIn(UserDetail, true, true);

                    return RedirectToAction("Payment");

                }

                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        public ActionResult BrandAmbassador()
        {
            return View();
        }

        public ActionResult YesBrandAmbassador()
        {
            var user = User.Identity.Name;
            var data = dc.Users.FirstOrDefault(i => i.UserName == user || i.Email == user);
            data.RequestedforBrandAmbesdor = true;
            dc.SaveChanges();
            return RedirectToAction("index", "Profile", new { message = "Complete Your Profile", mes = true, error = true });
        }

        [AllowAnonymous]
        public ActionResult Payment()
        {
            return View(new Scoolfirst.Model.Common.SubscriptionError { Code = 0, Reason = "Please Pay And Proceed" });
        }


        [AllowAnonymous]
        public ActionResult ChooseSubscription()
        {

            string sURL = "http://upgradejr.com/umbraco/api/subscription/getall";
            WebRequest wrGETURL = WebRequest.Create(sURL);
            Stream objStream = wrGETURL.GetResponse().GetResponseStream();
            StreamReader objReader = new StreamReader(objStream);
            var value = objReader.ReadToEnd();
            var tmp = JsonConvert.DeserializeObject<List<Subscription>>(value);


            return View(tmp);
        }


        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    {
                        return RedirectToAction("TokenFromCookie", new { ret = returnUrl });
                    }
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    string FullName = string.Empty;
                    string ProfilePictureUrl = string.Empty;
                    string DOB;
                    if (loginInfo.Login.LoginProvider == "Facebook")
                    {
                        var identity = AuthenticationManager.GetExternalIdentity(DefaultAuthenticationTypes.ExternalCookie);
                        var access_token = identity.FindFirstValue("urn:facebook:access_token");
                        var fb = new FacebookClient(access_token);
                        dynamic myInfo = fb.Get("/me?fields=id,name,email,birthday,picture"); // specify the email field
                        loginInfo.Email = myInfo.email;
                        if (dc.Users.Any(i => i.Email == loginInfo.Email))
                        {
                            var user = UserManager.FindByEmail(loginInfo.Email);
                            var Loginresult = UserManager.AddLogin(user.Id, new UserLoginInfo(loginInfo.Login.LoginProvider, loginInfo.Login.ProviderKey));
                            if (Loginresult.Succeeded)
                            {
                                SignInManager.SignIn(user, true, true);
                                return RedirectToAction("TokenFromCookie", new { ret = returnUrl });
                            }
                            else
                            {
                                return View("Error");
                            }

                        }

                        FullName = myInfo.name;
                        ProfilePictureUrl = myInfo.picture.data.url;
                        DOB = myInfo.birthday;
                        LoadRegistationViewData();
                        return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email, Name = myInfo.name, ProfilePic = ProfilePictureUrl, DateofBirth = DOB });

                    }
                    else
                    {
                        LoadRegistationViewData();
                        return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email, Name = FullName });

                    }

            }
        }

        public ActionResult TokenFromCookie(string ret)
        {
            //var read = Request.Cookies["xauc"].Value;
            //var cookie = new HttpCookie("token", read);
            //Response.AppendCookie(cookie);
            return RedirectToLocal(ret);
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Feed");
            }
            LoadRegistationViewData();
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new User { UserName = model.Email, Email = model.Email, FullName = model.Name, DOB = string.IsNullOrWhiteSpace(model.DateofBirth) ? model.DateofBirth.ToString() : null, Student_Parent_Admin = model.WhoIAm, PhoneNumber = model.PhoneNo, ClassId = model.ClassId, PictureUrl = model.ProfilePic, Address = "", AboutMe = "About me", Gender = "", SchoolId = model.SchoolId, RegisterdOn = DateTime.UtcNow, Expiry = DateTime.UtcNow.AddDays(15), Trial = true, Subscribed = false };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToAction("TokenFromCookie", new { ret = returnUrl });
                    }
                }
                ModelState.AddModelError("Email", result.Errors.FirstOrDefault());
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        // [HttpPost]
        [AllowAnonymous]
        // [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Response.Cookies.Clear();
            Response.Cookies.Remove("xauc");
            Session.Clear();
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl, string Ref)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            if (!string.IsNullOrWhiteSpace(returnUrl) && !string.IsNullOrWhiteSpace(Ref))
                if (returnUrl.Contains("notification.scoolfirst.com") && Ref.Contains("notification.scoolfirst.com"))
                    return Redirect(returnUrl);
            return RedirectToAction("Index", "KnowledgeNutrition");
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "KnowledgeNutrition");
        }
        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}