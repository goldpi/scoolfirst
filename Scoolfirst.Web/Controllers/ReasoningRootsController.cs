﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.RNM;
using Scoolfirst.ViewModel.Reasoning;


namespace Scoolfirst.Web.Controllers
{
    [Authorize]
    public class ReasoningRootsController : ApiController
    {
        private ScoolfirstContext db = new ScoolfirstContext();
       
        [HttpGet]
        [Route("api/ReasoningRoots/check/{id}")]
        public bool CheckExamAppeared(long id)
        {
            var Iden = User.Identity.Name;
            var user = db.Users.FirstOrDefault(i => i.UserName == Iden);
            return db.ReasoningAnswers.Any(i => i.ReasoningId ==id && i.UserId == user.Id);
        }
        [HttpGet]
        [Route("api/ReasoningRoots/attemps/{id}")]
        public IEnumerable<ReasoningAttemp> Attemps(long id)
        {
            var Iden = User.Identity.Name;
            var user = db.Users.FirstOrDefault(i => i.UserName == Iden);
            return db.ReasoningAttemp.Where(i => i.ReasoningId == id && i.UserId == user.Id);
        }



        [HttpGet]
        [Route("api/ReasoningRoots/AnswersGiven/{id}")]
        public IEnumerable<ReasoningAnswers> AnswersGiven(long id)
        {
            var Iden = User.Identity.Name;
            var user = db.Users.FirstOrDefault(i => i.UserName == Iden);
            db.Configuration.LazyLoadingEnabled = false;
            db.Configuration.ProxyCreationEnabled = false;
            return db.ReasoningAnswers.Include(i=>i.Question).
                Include(i=>i.Question.Option).Where(i => i.ReasoningId == id && i.UserId == user.Id).ToList();
        }


        [HttpGet]
        [Route("api/ReasoningRoots/userAnswersGiven/{id}/{userid}")]
        public IEnumerable<ReasoningAnswers> AnswersGivenuser(long id,string userid)
        {
           
            return db.ReasoningAnswers.Include(i => i.Question).Where(i => i.ReasoningId == id && i.UserId == userid).ToList();
        }



        [HttpPost]
        [ResponseType(typeof(int))]
        public IHttpActionResult SaveAnswers(ReasnionAnswerBack[] Answer)
        {
            var Iden = User.Identity.Name;
            var user = db.Users.FirstOrDefault(i => i.UserName == Iden);
            var first = Answer[0];
            var Attempt = new ReasoningAttemp
            {
                Id = Guid.NewGuid(),
                On = DateTime.UtcNow,
                ReasoningId = first.ReasoningId,
                UserId = user.Id
            };


            db.ReasoningAttemp.Add(Attempt);
            db.SaveChanges();
            
            List<ReasoningAnswers> ans = new List<ReasoningAnswers>();
            //if (db.ReasoningAnswers.Any(i => i.ReasoningId==first.ReasoningId&& i.UserId == user.Id))
            //{
            //    return Ok(0);
            //}
            foreach(var i in Answer)
            {
                ans.Add(new ReasoningAnswers { Id = Guid.NewGuid(), OptionId = i.AnswerId, UserId = user.Id, QuestionId = i.QuesionId, ReasoningId = i.ReasoningId , AttempId=Attempt.Id});
            }
            db.ReasoningAnswers.AddRange(ans);
            db.SaveChanges();

            var reasoinig = db.ReasoningRoots.Find(first.ReasoningId);
            var tracking = db.ReasoningTracker.FirstOrDefault(i => i.UserId == user.Id && i.RnmPostId == reasoinig.Id);
            Attempt = db.ReasoningAttemp.Include(i => i.Ans).Include(i => i.Ans.Select(o => o.Option)).FirstOrDefault(o=>o.Id==Attempt.Id);
            


            if (Attempt.Ans.Count(i => i.Option.Correct == true) >= reasoinig.MinQuestionToPass)
            { tracking.TestPassed = true; db.SaveChanges(); }
            return Ok(1);
        }
        [ResponseType(typeof(ReasoningRoot))]
        public IHttpActionResult GetReasoningRoot(long id)
        {
            var Iden = User.Identity.Name;
            var user = db.Users.FirstOrDefault(i => i.UserName == Iden);
            //if (db.ReasoningAnswers.Any(i => i.ReasoningId == id && i.UserId == user.Id))
            //{
            //    return Ok(0);
            //}
            ReasoningRoot reasoningRoot = db.ReasoningRoots.Find(id);
            if (reasoningRoot == null)
            {
                return NotFound();
            }
            db.Entry(reasoningRoot).Collection(i => i.Questions).Load();
           // db.Entry(reasoningRoot).Collection(i=>)
            return Ok(reasoningRoot);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private bool ReasoningRootExists(long id)
        {
            return db.ReasoningRoots.Count(e => e.Id == id) > 0;
        }
    }
}