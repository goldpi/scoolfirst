﻿using Scoolfirst.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Scoolfirst.Model.Context;
using Scoolfirst.ViewModel.ConnectedSchool;
using Scoolfirst.Model.Identity;
using Scoolfirst.Model.WorkBook;

namespace Scoolfirst.Web.Controllers
{
    
    public class ConnectedSchoolController : AppAuthController
    {
        private ScoolfirstContext dc ;
        private int SchoolId = 0;
        private int ClassId = 0;
        private string SchoolName = "";
        private string Class = "";
       
        List<SchoolList> SchoolList;
        User currentUser = null;
        List<Subject> Subjects;

        public ConnectedSchoolController(ScoolfirstContext Context)
        {
            dc = Context;
            SchoolList = new List<SchoolList>();
            SchoolList.Add(new SchoolList { Id = 0, SchoolName = "ALL SCHOOL" });
            SchoolList.AddRange(dc.Schools.Select(i => new SchoolList { Id = i.Id, SchoolName = i.SchoolName }).ToList());
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            UserData(filterContext);
            SessionCheck();
            QueryStringCheck(filterContext);
            base.OnActionExecuting(filterContext);
        }

        private void QueryStringCheck(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.QueryString["SchoolId"] != null)
            {
                SchoolId = int.Parse(filterContext.HttpContext.Request.QueryString["SchoolId"].ToString());
                Session["SchoolId"] = SchoolId;
                if (SchoolId != 0)
                    SchoolName = dc.Schools.FirstOrDefault(i => i.Id == SchoolId).SchoolName;
                else
                    SchoolName = "All School";
                Session["SchoolName"] = SchoolName;
            }
            if (filterContext.HttpContext.Request.QueryString["ClassId"] != null)
            {
                ClassId = int.Parse(filterContext.HttpContext.Request.QueryString["ClassId"]);
                Session["ClassId"] = ClassId;
                Class = dc.Classes.FirstOrDefault(i => i.Id == ClassId).RomanDisplay??"Unknonw Class";
                Session["Class"] = Class;
            }
        }

        private void SessionCheck()
        {
            if (Session["SchoolId"] != null)
            {
                SchoolId = (int)Session["SchoolId"];
            }
            if (Session["ClassId"] != null)
            {
                ClassId = (int)Session["ClassId"];
            }
            if (Session["SchoolName"] != null)
            {
                SchoolName = Session["SchoolName"].ToString();
            }
            if (Session["Class"] != null)
            {
                Class = Session["Class"].ToString();
            }
        }

        private void UserData(ActionExecutingContext filterContext)
        {
            var user = User.Identity.Name;
            var userdata = dc.Users.FirstOrDefault(i => i.UserName == user || i.Email == user);
            if (userdata == null)
                filterContext.Result = new HttpNotFoundResult();
            dc.Entry(userdata).Reference(i => i.School).Load();
            dc.Entry(userdata).Reference(i => i.Class).Load();
            Subjects = dc.SchoolClassSubject
                .Where(i => i.ClassId == userdata.ClassId && i.SchoolId == userdata.SchoolId)
                .Select(i => i.Subject).ToList();
            currentUser = userdata;
            ClassId = currentUser.ClassId;
            ViewBag.Subjects = Subjects;
        }

        // GET: ConnectedSchool
        public ActionResult Index(int  SchoolId=0,int ClassId=-1,string Type="TT",bool IsSearch=false,string Topic="",int SubjectId=0)
        {
            if (SubjectId == 0)
            {
                SubjectId = Subjects.First().Id;
            }
            if (ClassId == -1 && (Type == "TT" || Type == "RN"))
            {
                ClassId = this.ClassId;
            }
            else
            {
                if (Type == "PL")
                    ClassId = currentUser.ClassId + 1;
                else
                    ClassId = this.ClassId;
            }
            if (Type == "PL" || Type == "RN")
            {
                SchoolId = currentUser.SchoolId??0;
            }
            if (!string.IsNullOrWhiteSpace(Topic))
            {
                IsSearch = true;
            }
            ViewBag.SchoolId = new SelectList(SchoolList, "Id", "SchoolName", SchoolId);
            ViewBag.ClassId = new SelectList(dc.Classes, "Id", "RomanDisplay", ClassId);
            ViewBag.SubjectId = SubjectId;
            var m = new ConnectedSchoolViewModel { IsSearch = IsSearch, Type = Type, Description = "asdasd",School=SchoolId.ToString(),Class=ClassId.ToString(), Topic=Topic, Subject=SubjectId.ToString() };
            m.Trending= GetTrending(ClassId, Type,SchoolId, SubjectId);
            if (IsSearch == true && Type == "TT")
            {
                m.SearchResult = TossTopicSearch(ClassId, SchoolId, SubjectId, Topic);
            }
               

            return View(m);
        }

        private List<ConnectedSchoolAggregate> GetTrending(int ClassId, string Type,int schoolId,int SubjectId)
        {
            List<ConnectedSchoolAggregate> list = new List<ConnectedSchoolAggregate>();
            switch (Type)
            {
                case "TT":
                  list=  TossTopic(ClassId, schoolId, SubjectId);

                    break;
                case "RN":
                    list = Revision(ClassId, schoolId, SubjectId);
                    
                    break;
                case "PL":
                    list = TossTopic(ClassId, schoolId, SubjectId);
                    break;


            }
            return list;
        }

        private List<ConnectedSchoolAggregate> Revision(int classId, int schoolId, int subjectId)
        {
            return dc.Database.SqlQuery<Note>($@"SELECT  Distinct  Notes.*
                                                            FROM SchoolClassSubjects INNER JOIN
                                                            Books ON SchoolClassSubjects.ClassId = Books.ClassesId And SchoolClassSubjects.SubjectId = Books.SubjectId INNER JOIN
                                                            ChapterBooks On Books.Id = ChapterBooks.Books_Id Inner Join
                                                            Chapters ON ChapterBooks.Chapter_Id = Chapters.Id inner join
                                                            Notes on Chapters.Id = Notes.ChapterId
                                                        Where(SchoolClassSubjects.SchoolId = {schoolId} AND SchoolClassSubjects.ClassId = {ClassId} AND SchoolClassSubjects.SubjectId = {subjectId} and Chapters.GlobalTrending = 1)")
                                                        .Select(cpr => new ConnectedSchoolAggregate { Id = cpr.Id.ToString(), Title = cpr.Topic }).ToList();

        }

        private List<ConnectedSchoolAggregate> TossTopic(int ClassId, int schoolId, int SubjectId)
        {
            if (schoolId == 0)
            {
               return dc.Database.SqlQuery<Chapter>($@"SELECT Distinct 
                                                        Chapters.*
                                                        FROM SchoolClassSubjects INNER JOIN
                                                        Books ON SchoolClassSubjects.ClassId = Books.ClassesId And SchoolClassSubjects.SubjectId = Books.SubjectId INNER JOIN
                                                        ChapterBooks On Books.Id = ChapterBooks.Books_Id Inner Join
                                                        Chapters ON ChapterBooks.Chapter_Id = Chapters.Id
                                                        WHERE (SchoolClassSubjects.ClassId = {ClassId} AND SchoolClassSubjects.SubjectId={SubjectId})")
                                                        .Where(i=>i.GlobalTrending==true)
                                                        .Select(cpr => new ConnectedSchoolAggregate { Id = cpr.Id.ToString(), Title = cpr.Name }).ToList();



                
            }
            else
            {
                return dc.Database.SqlQuery<Chapter>($@"SELECT Distinct 
                                                            Chapters.*
                                                                FROM SchoolClassSubjects INNER JOIN
                                                                Books ON SchoolClassSubjects.ClassId = Books.ClassesId 
                                                                      And 
                                                                        SchoolClassSubjects.SubjectId = Books.SubjectId INNER JOIN
                                                                ChapterBooks On Books.Id = ChapterBooks.Books_Id Inner Join
                                                                Chapters ON ChapterBooks.Chapter_Id = Chapters.Id
                                                            WHERE (SchoolClassSubjects.SchoolId={schoolId} AND SchoolClassSubjects.ClassId = {ClassId} AND SchoolClassSubjects.SubjectId={SubjectId})")
                                                        .Where(i => i.GlobalTrending == true)
                                                        .Select(cpr => new ConnectedSchoolAggregate { Id = cpr.Id.ToString(), Title = cpr.Name }).ToList();

            }
        }

        private List<ConnectedSchoolAggregate> TossTopicSearch(int ClassId, int schoolId, int SubjectId,string search)
        {
            if (schoolId == 0)
            {
                return dc.Database.SqlQuery<Chapter>($@"SELECT Distinct 
                                                        Chapters.*
                                                        FROM SchoolClassSubjects INNER JOIN
                                                        Books ON SchoolClassSubjects.ClassId = Books.ClassesId And SchoolClassSubjects.SubjectId = Books.SubjectId INNER JOIN
                                                        ChapterBooks On Books.Id = ChapterBooks.Books_Id Inner Join
                                                        Chapters ON ChapterBooks.Chapter_Id = Chapters.Id
                                                        WHERE (SchoolClassSubjects.ClassId = {ClassId} AND SchoolClassSubjects.SubjectId={SubjectId}) AND Chapters.Name Like '%{search}%'")
                                                         .Where(i => i.Trending == true)
                                                         .Select(cpr => new ConnectedSchoolAggregate { Id = cpr.Id.ToString(), Title = cpr.Name }).ToList();




            }
            else
            {
                return dc.Database
                    .SqlQuery<Chapter>($@"SELECT Distinct 
                                   Chapters.*
                                        FROM SchoolClassSubjects INNER JOIN
                                        Books ON SchoolClassSubjects.ClassId = Books.ClassesId 
                                                And 
                                                SchoolClassSubjects.SubjectId = Books.SubjectId INNER JOIN
                                        ChapterBooks On Books.Id = ChapterBooks.Books_Id Inner Join
                                        Chapters ON ChapterBooks.Chapter_Id = Chapters.Id
                                    WHERE (SchoolClassSubjects.SchoolId={schoolId} AND SchoolClassSubjects.ClassId = {ClassId} AND SchoolClassSubjects.SubjectId={SubjectId}) AND Chapters.Name Like '%{search}%'")
                                .Where(i => i.Trending == true)
                                .Select(cpr => new ConnectedSchoolAggregate { Id = cpr.Id.ToString(), Title = cpr.Name }).ToList();

            }
        }
        [Route("ConnectedSchool/topic/{Type}/{Id}")]
        public ActionResult Topic(string Type,string Id)
        {
            switch (Type.ToUpper())
            {
                case "TT"
                    : return RedirectToAction("TopicSelected", new { Id = Id });
                case "RN":
                    return RedirectToAction("Revision", new { Id = Id });
                case "PL": return RedirectToAction("ProgressiveLEarning", new { Id = Id });
            }
            return View();
        }


        public ActionResult TopicSelected(int Id)
        {
            var Unit = dc.Chapters.Include(i => i.Questions).FirstOrDefault(i => i.Id == Id);
            return View(Unit);
        }
        public ActionResult ProgressiveLEarning(int Id)
        {
            var Unit = dc.Chapters.Include(i => i.Questions).FirstOrDefault(i => i.Id == Id);
            return View("TopicSelected", Unit);
        }
        public ActionResult Revision(int Id)
        {
            var Unit = dc.Chapters.Include(i => i.Notes).FirstOrDefault(i => i.Id == Id);
            return View(Unit);
        }

        #region Deprecated
        //public ActionResult School(int SchoolId, int ClassId)
        //{

        //    return RedirectToAction("Subjects",
        //        new { SchoolId = SchoolId, ClassId = ClassId, School = SchoolName.Replace(' ', '-').Replace(',', '-'), Class = Class.Replace(' ', '-') });
        //}

        //[Route("ConnectedSchool/{school}/{class}/{Id}/{BookId}/{UnitName}")]
        //public ActionResult UnitWise(int Id, int Bookid)
        //{
        //    ViewBag.SchoolId = new SelectList(dc.Schools, "Id", "SchoolName", SchoolId);
        //    ViewBag.ClassId = new SelectList(dc.Classes, "Id", "RomanDisplay", ClassId);

        //    if (Id < 0)
        //    {
        //        return HttpNotFound();
        //    }
        //    var control = dc.Books.Include(i => i.Subject)
        //        .Include(i => i.Chapters)
        //        .FirstOrDefault(i => i.Id == Bookid);
        //    ViewBag.Books = control;
        //    var count = control.Chapters.Count();
        //    if (count < Id)
        //    {
        //        return HttpNotFound();
        //    }

        //    ViewBag.Next = (count > (Id+1));
        //    ViewBag.Previous = Id > 0;

        //    var unit = control
        //        .Chapters
        //        .OrderBy(i => i.Id)
        //        .Skip(Id * 1)
        //        .Take(1).FirstOrDefault();
        //    dc.Entry(unit).Collection(i => i.Questions).Load();
        //    dc.Entry(unit).Reference(i => i.Classes).Load();
        //    ViewBag.Count = unit.Questions.Count();
        //    ViewBag.Chapter = unit.Name;
        //    ViewBag.Subjects = dc.SchoolClassSubject
        //       .Include(i => i.Subject)
        //       .Include(i => i.Books)
        //       .Where(i => i.SchoolId ==SchoolId && i.ClassId == ClassId).ToList();
        //    var Questions = unit.Questions.OrderBy(i => i.Id).ToList();
        //    return View(Questions);
        //}

        //public ActionResult TestWorkbook()
        //{

        //    var Quiz = dc.Quizes
        //        .Where(i => i.School == SchoolId.ToString() && i.Class == ClassId.ToString())
        //        .ToList();
        //    return PartialView(Quiz);
        //}

        //public ActionResult Test(Guid Id)
        //{
        //    var Quiz = dc
        //        .Quizes
        //        .Include(i => i.Questions.GroupBy(io=>io.QuestionType))
        //        .FirstOrDefault(i => i.Id == Id);

        //    if (Quiz != null)
        //        return View(Quiz);
        //    else
        //        return HttpNotFound();

        //}

        ////public ActionResult Syllabus()
        ////{

        ////    var result = dc.SyllabusDates
        ////        //.Include(i => i.Quiz)
        ////        .Where(i => i.Class == ClassId && i.School==SchoolId)
        ////        .ToList();
        ////    return PartialView(result);
        ////}

        //public ActionResult Units(int Id)
        //{
        //    ViewBag.Id = Id;
        //    var Units = dc.Books.Find(Id).Chapters.ToList();
        //    return PartialView(Units);

        //}

        #endregion
    }
}