﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Scoolfirst.Model.Context;
using System.Data.Entity;
using Scoolfirst.Web.Controllers;

namespace Scoolfirst.Project.Controllers
{
    
    public class ProjectsController : AppAuthController
    {

        ScoolfirstContext dc = new ScoolfirstContext();
        
        //
        // GET: /Projects/
        public ActionResult Index(string search)
        {
            
            var t1 = dc.ProjectCategories.Include(i => i.Projects).ToList();
            return View(t1);
        
       }

      

        public ActionResult Detail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //var project = dc.Category.Include(i => i.Projects).First(i => i.Id == id).Projects;
            var project = dc.Projects.Find(id);
            dc.Entry(project).Reference(i => i.Category).Load();
            return View(project);
        }
	}
}