﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.MCQ;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.ViewModel.WorkBook.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Text.RegularExpressions;
using Scoolfirst.ViewModel.WorkBook.TossTopic;
using Scoolfirst.ViewModel.StringHelper;
using Scoolfirst.Web.Infra;
using Scoolfirst.Model.Feed;

namespace Scoolfirst.Web.Controllers
{
    [Authorize]
    public class AllApiController : ApiController
    {
        ScoolfirstContext dc;
       
        public AllApiController(ScoolfirstContext dcP)
        {
            dc = dcP;
        }
        [Route("api/workbook/book/{Id}")]
        [HttpGet]
        public QuizVM getByBook(int Id)
        {
            return QuizVM.BookQuestion(dc, Id);
        }


        [Route("api/workbook/bookUnit/{Id}")]
        [HttpGet]
        public IEnumerable<Object> BookUnit(int Id)
        {
            return dc.Books.Find(Id).Chapters.Select(i => new { ChapterName = i.Name, Id = i.Id });
        }


        [Route("api/workbook/Quiz/{Id}")]
        [HttpGet]
        public QuizVM getByQuiz(Guid Id)
        {
            return QuizVM.Quiz(dc, Id);
        }

        [Route("api/Book/")]
        [HttpGet]
        public IEnumerable<Books> getBbooks()
        {
            return dc.Books;
        }


        [Route("api/McqQuiz/Set/{id}")]
        [HttpGet]
        public QuestionSet Set(Guid id)
        {
            return dc.QuestionSets.Find(id);
        }
        [Route("api/AC")]
        [HttpGet]
        public Object AutoComplete(string St)
        {
            var user = dc.Users.FirstOrDefault(i => i.UserName == User.Identity.Name);
            var @classId = user.ClassId;
            var @class = dc.Classes.Find(@classId);
            string[] rn = RevisionNote.RevisonNotesTags(St,dc.Chapters, user.ClassId);
            string[] Ht = HybridLearning.HybridLearningTags(St, classId,dc.Chapters.Include(i=>i.Questions));
            string[] PL = ProgressiveLearing
                .ProgressiveLearningTags(St, dc.Classes.AsQueryable(), dc.Chapters.Include(i=>i.Questions).AsQueryable(), @class.NumericDisplay);
            return new { RN = rn, HL = Ht, PL = PL };
        }



        [Route("api/QuestionSearch/{serach}")]
        [HttpGet]
        public IEnumerable<Questions> questions(string serach)
        {
           LuceneSearchProvider<Questions> p = new LuceneSearchProvider<Questions>();
           return  p.Search(doc =>
            {
                return new Questions
                {
                    Id = Guid.Parse(doc.Get("Id")),
                    Refrence = doc.Get("Refrence"),
                    Answer = doc.Get("Answer"),
                    Title = doc.Get("Title"),
                    Subject = doc.Get("Subject"),
                    School = doc.Get("School"),
                    Class = doc.Get("Class"),
                    ChapterId = int.Parse(doc.Get("ChapterId"))
                };
            }, serach, new[] { "Id", "Refrence", "Answer", "Title", "Subject", "School", "Class", "ChapterId" });
        }
        [Route("api/FeedSearch/{serach}")]
        [HttpGet]
        public IEnumerable<Feeds> Feeds(string serach)
        {
            LuceneSearchProvider<Feeds> p = new LuceneSearchProvider<Feeds>();
            return p.Search(doc =>
            {
                return new Feeds
                {
                    Id = Guid.Parse(doc.Get("Id")),
                    Title = doc.Get("Title"),
                    PostId = long.Parse(doc.Get("PostId")),
                    GroupId = int.Parse(doc.Get("GroupId")),                  
                };
            }, serach, new[] { "Id", "Title", "PostId", "GroupId"});
        }


    }
}