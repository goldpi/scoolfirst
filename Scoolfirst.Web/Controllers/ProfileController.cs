﻿using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using Scoolfirst.ViewModel.User.ProfileUpdateWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Web.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        User currentUser = null;
        ScoolfirstContext dc ;
        public ProfileController(ScoolfirstContext Context)
        {
            dc = Context;
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userdata = new User();
            if (Session["UserDetails"] == null)
            {
                var user = User.Identity.Name;
                userdata = dc.Users.FirstOrDefault(i => i.UserName == user || i.Email == user);
                if (userdata == null)
                    filterContext.Result = new HttpNotFoundResult();
                dc.Entry(userdata).Reference(i => i.School).Load();
                dc.Entry(userdata).Reference(i => i.Class).Load();
                Session["UserDetails"] = userdata;
            }else
            {
                userdata = (User)Session["UserDetails"];
            }
            TempData["UserName"] = userdata.FullName;
            TempData["Points"] = userdata.Points;
            TempData["Class"] = userdata.Class.RomanDisplay;
            TempData["Subs"] = userdata.Subscribed ? userdata.Trial ? "Trial" : "Subscribed" : "Free";
            currentUser = userdata;
            base.OnActionExecuting(filterContext);
        }
       
        public ActionResult Pic(bool noProfile = true)
        {
            ViewBag.Noprofile = noProfile;
           
            return PartialView("Pic", currentUser.PictureUrl);
        }

        public ActionResult PPic(string Id,bool noProfile=false)
        {
            ViewBag.Noprofile = noProfile;
            var userdata = dc.Users.FirstOrDefault(i => i.Id==Id).PictureUrl;
            return PartialView("Pic", userdata);
        }

        // GET: Profile
        public ActionResult Index( string message="",bool mes=false,bool error=false)
        {
            if (mes)
            {
                if (error)
                    TempData["Error_message"] = message;
                else
                    TempData["message"] = message;

            }
            return View();
        }

        public ActionResult ContactInfoAction(bool SideBar=false)
        {
            ContactInformation cif = new ContactInformation
            {
                Address = currentUser.Address,
                Email = currentUser.Email,
                Id = currentUser.Id,
                PhoneNo = currentUser.PhoneNumber

            };
            ViewBag.SideBar = SideBar;
            return PartialView(cif);
        }
       [HttpPost]
        public ActionResult changeContactInfo(ContactInformation model)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("/profile?message=Data+entered+was+invalid!+No+Changes+Made!&error=true&mes=true");
            }
            currentUser.Address = model.Address;
            currentUser.PhoneNumber = model.PhoneNo;
           // dc.Entry(currentUser).State = System.Data.Entity.EntityState.Modified;
            dc.SaveChanges();
            return Redirect("/profile?message=Changes+Made+Successfully!&mes=true");
        }

        public ActionResult BasicInfo()
        {
            BasicInformation bif = new BasicInformation
            {
                Gender = currentUser.Gender,
                DateOfBirth = currentUser.DOB,
                Id = currentUser.Id,
                Name = currentUser.FullName
            };

            return PartialView(bif);
        }
        [HttpPost]
        public ActionResult changeBasicprofile(BasicInformation model)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("/profile?message=Data+entered+was+invalid!+No+Changes+Made!&error=true&mes=true");
            }
            currentUser.FullName = model.Name;
            currentUser.DOB = model.DateOfBirth;
            currentUser.Gender = model.Gender;
           // dc.Entry(currentUser).State = System.Data.Entity.EntityState.Modified;
            dc.SaveChanges();
            return Redirect("/profile?message=Changes+Made+Successfully!&mes=true");
        }

        public ActionResult AboutInfo()
        {
            var aif = new AboutInformation
            {
                About = currentUser.AboutMe,
                Id = currentUser.Id
            };
            return PartialView(aif);
        }

        [HttpPost]
        public ActionResult ChangeAboutInfo(AboutInformation model)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("/profile?message=Data+entered+was+invalid!+No+Changes+Made!&error=true&mes=true");
            }
            currentUser.AboutMe = model.About;
           
            //dc.Entry(currentUser).State = System.Data.Entity.EntityState.Modified;
            dc.SaveChanges();
            return Redirect("/profile?message=Changes+Made+Successfully!&mes=true");
        }


        public ActionResult AcademicInfo()
        {
            if (currentUser != null)
            {
                if (currentUser.Board == null)
                {
                    currentUser.Board = " ";
                }
                AcadamemicInformation aif = new AcadamemicInformation
                {
                    Board = currentUser.Board ?? "",
                    SchoolId = currentUser.SchoolId ?? 0,
                    Sponsered = currentUser.Sponsered,
                    ClassId = currentUser.ClassId,
                    School = currentUser.School.SchoolName ?? "",
                    Class = currentUser.Class.RomanDisplay ?? ""
                };
                ViewBag.ClassId = new SelectList(dc.Classes, "Id", "RomanDisplay", aif.ClassId);
                ViewBag.SchoolId = new SelectList(dc.Schools, "Id", "SchoolName", aif.SchoolId);
                return PartialView(aif);
            }
            else
            {
                ViewBag.ClassId = new SelectList(dc.Classes, "Id", "RomanDisplay");
                ViewBag.SchoolId = new SelectList(dc.Schools, "Id", "SchoolName");
               
                return PartialView();
            }
            
        }
        [HttpPost]
        public ActionResult changeAcademicInfo(AcadamemicInformation model)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("/profile?message=Data+entered+was+invalid!+No+Changes+Made!&error=true&mes=true");
            }
            currentUser.Board = model.Board;
            currentUser.SchoolId = model.SchoolId;
            currentUser.ClassId = model.ClassId;
            

            //dc.Entry(currentUser).State = System.Data.Entity.EntityState.Modified;
            dc.SaveChanges();
            return Redirect("/profile?message=Changes+Made+Successfully!&mes=true");
        }

        public ActionResult History()
        {
            var res = dc.PointsHistory.Where(i => i.UserId == currentUser.Id).OrderByDescending(i=>i.On).ToList();
            return View(res);
        }
    }
}