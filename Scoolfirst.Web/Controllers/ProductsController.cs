﻿using Scoolfirst.Model.Context;
using Scoolfirst.ViewModel.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Web.Controllers
{
    public class ProductsController : AppAuthController
    {
        private ScoolfirstContext dc;
        public ProductsController(ScoolfirstContext Context)
        {
            dc = Context;
        }
        // GET: Products
        public ActionResult Index(int page=0)
        {
            var Products = dc
                .Products
                .OrderByDescending(i => i.AddedOn)
                .Skip(page * 12)
                .Take(12).ToList();
            if (Request.IsAjaxRequest())
                return PartialView(Products);
            return View(Products);
        }

        public ActionResult SaleOff()
        {
            return View();
        }

        public ActionResult StoreClosed(string id)
        {
            StoreCloseVM model = new StoreCloseVM();
            model.ProductName = id;
            model.Name = "Md Yusuf Alam";
            model.Email = "mdyusufalam@gmail.com";
            model.PhoneNo = "9386699909";
            return View(model);
        }
    }
}