﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Web.Controllers
{
    public class AchievementController : AppAuthController
    {
        private ScoolfirstContext db = new ScoolfirstContext();
        // GET: Achievement
        public ActionResult Index()
        {
            var CurentYear = DateTime.Now;
            var List = db.Achievements.Where(i => i.OnDate.Year == CurentYear.Year).ToList();
            return View(List);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Achievement model)
        {
            
            if (Session["UserDetails"] == null)
                return Redirect("~/account/login/");
            
            MonthCalculation month = new MonthCalculation();
            var batchpre = month.PrevYear;
            var batchnext = month.NextYear;
            var userId = (Session["UserDetails"] as Model.Identity.User).Id;

            if (db.Achievements.Count(i => i.OnDate.Year >= batchpre && i.OnDate.Year <= batchnext && i.UserId == userId) > 3)
            {
                return RedirectToAction("Errors");
            }

            if(ModelState.IsValid)
            {                             
               
                model.Id = Guid.NewGuid();
                model.UserId = (Session["UserDetails"] as Model.Identity.User).Id;                
                db.Achievements.Add(model);
                db.SaveChanges();
                return RedirectToAction("Create");
            }
            return View(model);
        }
        public ActionResult Errors()
        {
            return View();
        }
    }
}