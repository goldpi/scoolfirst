﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using Scoolfirst.Model.RNM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Scoolfirst.Model.Provider;
using Scoolfirst.ViewModel.Reasoning;

namespace Scoolfirst.Web.Controllers
{

    public class ReasoningController : AppAuthController
    {
        private ScoolfirstContext dc;
        private User LoggedUser;     
        IShortMessageSender SmsSender;
        public ReasoningController(ScoolfirstContext Context,IShortMessageSender smsSender)
        {
            dc = Context;          
            SmsSender = smsSender;
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            string level = "Beginner";
            if (filterContext.HttpContext.Request.QueryString["level"] != null)
                level = filterContext.HttpContext.Request.QueryString["level"];

            if (Session["UserDetails"] != null)
                LoggedUser = Session["UserDetails"] as Model.Identity.User;
            else
            {
                var userId = User.Identity.Name;
                var UserS = dc.Users.FirstOrDefault(i => i.UserName == userId);
                if (UserS != null)
                    LoggedUser = UserS;
                else
                    filterContext.Result = Redirect("/Account/Login/");
            }
            if(Session["First"]==null){
                Session["First"] = false;
                
                filterContext.Result = RedirectToAction("Spalsh",new { Level=level});
            }
            base.OnActionExecuting(filterContext);
        }
        public ActionResult Spalsh(bool start=false,string  Level="Beginner")
        {
            Session["First"] = true;
            var Resoing = dc.ReasoningRoots
                .OrderBy(i => i.Id)
                .Where(i=>i.IsLessonOrExam!= ReasoningRootType.Single && i.Level==Level)
                .ToList();
            var count = dc.ReasoningTracker.Count(i => i.UserId == LoggedUser.Id && i.Level==Level );
            if (count == 0)
                ViewBag.Start = true;
            else
                ViewBag.Start = start;
            ViewBag.Id = count;
              
            return View(Resoing);
        }
        // GET: Reasoning
        public ActionResult Index(string Level,int page = 0,bool preivew=false)
        {
            ReasoningRoot model = null;
           // page = page + 1;
            var AllCount = dc.ReasoningRoots.Where(i=>i.IsLessonOrExam==ReasoningRootType.Lesson && i.Level == Level).Count();
            if (dc.ReasoningTracker.Count(i => i.UserId == LoggedUser.Id && i.Level==Level) > 0)
            {
                
                var tracker = dc.ReasoningTracker
                    .Where(i => i.UserId == LoggedUser.Id && i.Level==Level)
                    .OrderBy(i => i.OnDateTime)
                    .ToList();
                var last = tracker.LastOrDefault();
                ViewBag.Current = tracker.Count();
                ViewBag.Now=page==0? tracker.Count():page;
                if (last.Complete)
                    {
                    var nextPage = page + 1;
                    var next = dc.ReasoningRoots
                        .FirstOrDefault(i => i.Order == nextPage && i.Level==Level);
                    ViewBag.CountCompleted = tracker.Count();
                    ViewBag.Sum = tracker.Sum(i => i.Days);
                    ViewBag.TotlaLesson = AllCount;
                    if (next != null)
                    {
                        ViewBag.Next = next;
                    }
                    if (page != 0)
                    {
                        if (tracker.Count() >= page)
                        {
                            var modelId = tracker.Skip(page - 1).Take(1).FirstOrDefault().RnmPostId;
                            model = dc.ReasoningRoots.Find(modelId);
                          //          ViewBag.Next// if(dc.ReasoningAttemp.Where(i=>i.ReasoningId==model.Id))

                            return View(model);
                        }
                            
                    }

                    return View(next);
                }
                else
                {
                    ViewBag.CountCompleted = tracker.Count();
                    ViewBag.TotlaLesson=AllCount;
                    ViewBag.Sum = tracker.Sum(i => i.Days);
                    if (page != 0)
                    {
                        if (tracker.Count() >= page)
                        {
                            var modelId = tracker.Skip(page - 1).Take(1).FirstOrDefault().RnmPostId;
                             model = dc.ReasoningRoots.Find(modelId);
                           
                            return View(model);
                        }
                    }
                    model = dc.ReasoningRoots.Find(last.RnmPostId);
                    return View(model);
                    
                }

            }
            else
            {
                return RedirectToAction("Spalsh",new { start=true});
                
            }

        }
        public ActionResult Preview(int id)
        {
            ReasoningTracker temp = new ReasoningTracker
            {
                RnmPost = dc.ReasoningRoots.Find(id),
                RnmPostId = id,
                OnDateTime = DateTime.Now,
                UserId = LoggedUser.Id
            };
            ViewBag.PB = Percentage(4 - 1, 6);
            ViewBag.Sum =4;
            ViewBag.Current = 78;
            ViewBag.Now = 2;
            return View("index", temp);
        }
        [NonAction]
        private int Percentage(int value,int total)
        {
            return (int)((((float)value )/((float)total)) * 100.00);
        }
        public ActionResult Start(long id, string Level)
        {
            if (dc.ReasoningTracker.Count(i => i.UserId == LoggedUser.Id&& i.Level==Level) > 0)
            {
                var tracker = dc.ReasoningTracker
                    .Include(i => i.RnmPost)
                    .Where(i => i.UserId == LoggedUser.Id && i.Level == Level)
                    .OrderBy(i => i.OnDateTime)
                    .ToList();
                var last = tracker.LastOrDefault();
                if (last.Complete)
                    dc.ReasoningTracker.Add(new ReasoningTracker { OnDateTime = DateTime.UtcNow, RnmPostId = id, UserId = LoggedUser.Id, Level = Level });
            } else
            {
                var rmnid = dc.ReasoningRoots
                    .Where(i=>i.Level==Level && i.IsLessonOrExam!=ReasoningRootType.Single)
                    .OrderBy(i => i.Order)
                    .Skip((int)id)
                    .Take(1)
                    .FirstOrDefault()
                    .Id;
                dc.ReasoningTracker.Add(new ReasoningTracker { OnDateTime = DateTime.UtcNow, RnmPostId = rmnid, UserId = LoggedUser.Id, Level = Level });
            }
            dc.SaveChanges();
            return RedirectToAction("Index", new { Level=Level});
        }
        public ActionResult SingleTest(string Level = "Beginner")
        {
            var Resoing = dc.ReasoningRoots
                .OrderByDescending(i => i.Id)
                .Where(i => i.IsLessonOrExam == ReasoningRootType.Single && i.Level==Level)
                .ToList();
            return View( Resoing);
        }
        public ActionResult Test(long Id)
        {
            ViewBag.UserData = LoggedUser;
            var re = dc.ReasoningRoots.Find(Id);
            if (re == null)
            {
                return HttpNotFound();
            }
            switch (re.IsLessonOrExam)
            {
                case Model.RNM.ReasoningRootType.Lesson:
                case Model.RNM.ReasoningRootType.RevisionOfLesson:
                    return View("PracticeTest",re);

                case Model.RNM.ReasoningRootType.Exam:
                    return View("Test", re);
                case Model.RNM.ReasoningRootType.Single:
                    return View("Test", re);
                case Model.RNM.ReasoningRootType.ReviewOfExam:
                    return View("NoTest", re);

                default:
                    return View("NoTest", re);
            }

           
           
        }
        [HttpGet]
        public ActionResult Report(int id)
        {
            var Iden = User.Identity.Name;
            var user = dc.Users.FirstOrDefault(i => i.UserName == Iden);
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var current = dc.ReasoningRoots.FirstOrDefault(i => i.Id == id);
            var trc = dc.ReasoningTracker.FirstOrDefault(i => i.RnmPostId == current.Id && i.UserId == LoggedUser.Id);
            var ccInt = current.Order - 1;
            var previoue = dc.ReasoningRoots
                .Include(i=>i.Questions)
                .Include(i=>i.Questions.Select(o=>o.Option))
                .FirstOrDefault(i => i.Order == ccInt && i.Level == current.Level);

            var attemps = dc.ReasoningAttemp
                .Include(i=>i.Ans)
                .Where(i => i.ReasoningId == previoue.Id && i.UserId == user.Id)
                .OrderByDescending(i => i.On)
                .FirstOrDefault();



            ReportAttempeded attempsResult = new ReportAttempeded
            {
                Answers = attemps.Ans.ToList(),
                Question = previoue.Questions.ToList(),
                Root=trc
            };
            return View(attempsResult);

        }
        [HttpPost]
        public ActionResult Question(int id,int qId,string yesNo)
        {
            var tracker = dc.ReasoningTracker
                    .FirstOrDefault(i => i.UserId == LoggedUser.Id && i.RnmPostId==id);
            if (string.IsNullOrEmpty(tracker.Accepted))
                tracker.Accepted = "";
            tracker.Accepted += "|" + qId + "," + yesNo;
            dc.SaveChanges();
            return Content("");
        }
        [ChildActionOnly]
        public ActionResult Moudles(string Level,int Current,int Now)
        {
            ViewBag.Current = Current;
            ViewBag.Now = Now;
            var Resoing = dc.ReasoningRoots
                .Where(i=>i.IsLessonOrExam!=ReasoningRootType.Single && i.Level==Level)
                .OrderBy(i => i.Order)
                .ToList();
            return PartialView(Resoing);
            
        }
        public ActionResult Cheat(int id)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            return Json(dc.ReasoningRoots
                .Include(i => i.Questions)
                .Include(i => i.Questions.Select(io => io.Option))
                .FirstOrDefault(i => i.Id == id).Questions,JsonRequestBehavior.AllowGet);
        }
    }
}