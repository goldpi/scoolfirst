﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Web.Controllers
{
    
    public class OrderController : AppAuthController
    {
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Pass Order Id to generate Invoice</param>
        /// <returns></returns>
        public ActionResult Invoice(string id)
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Pass Order Id to Download Invoice</param>
        /// <returns></returns>
        public ActionResult DownloadInvoice(string id)
        {
            return View();
        }
    }
}