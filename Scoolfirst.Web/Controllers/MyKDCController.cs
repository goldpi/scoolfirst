﻿using Scoolfirst.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Scoolfirst.Model.KDC;
using Scoolfirst.ViewModel.KDC;
using Scoolfirst.Model.Identity;

namespace Scoolfirst.Web.Controllers
{
    /// <summary>
    /// Depricatred //For Clean Up
    /// </summary>

    
    public class MyKDCController : AppAuthController
    {
        User thisUser;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var uid = User.Identity.Name;
            thisUser = dc.Users.FirstOrDefault(i => i.UserName == uid || i.Email == uid);
           
            base.OnActionExecuting(filterContext);  
        }
        ScoolfirstContext dc = new ScoolfirstContext();
        //
        // GET: /MyKDC/
        ArticlesItems ArtilceHandler = new ArticlesItems();
        public ActionResult Index(int page=0,bool my=false)
        {
            
            if (!my)
                ViewBag.Page = string.Format("/mykdc/?page={0}", (page + 1));
            else
                ViewBag.Page = string.Format("/mykdc/?page={0}&my=", (page + 1), my);
            if (!my)
                ArtilceHandler.GetItems(dc, User.Identity.Name, page);
            else
                ArtilceHandler.GetMyItems(dc, User.Identity.Name, page);
            if (Request.IsAjaxRequest())
                return PartialView("Partial", ArtilceHandler.KDC);
            return View(ArtilceHandler.KDC);
            
        }

        public ActionResult Details(Guid Id)
        {
            var ff = dc.Articles
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Favs)
                .FirstOrDefault(i => i.ID == Id);
            var t =  new KdcVM { Article = ff, FavByMe = ff.Favs.Any(i => i.UserId == thisUser.Id), LikedByMe = ff.Likes.Any(i => i.UserId == thisUser.Id) };
            return View(t);
        }

        public ActionResult LikeArticle(Guid Id)
        {
            var like = ArtilceHandler.AddRemoveFeedLike(dc, Id, User.Identity.Name);


            return Json(new { Like = like }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult StarArticle(Guid Id)
        {
            var like = ArtilceHandler.AddRemoveFeedStar(dc, Id, User.Identity.Name);
            return Json(new { Like = like }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ArticleComment(Guid Id, string Comment)
        {

            ArticleComment comment = ArtilceHandler.AddComment(dc, Comment, Id, User.Identity.Name);

            return PartialView(comment);
        }
        [HttpPost]
        public ActionResult DeleteArticleComment(Guid Id)
        {
            var comment = dc.ArticleComments.Find(Id);
            if (comment.UserId == thisUser.Id)
            {
                dc.ArticleComments.Remove(comment);
                dc.SaveChanges();
                return Content("delete");
            }
            
            return Content("Not Your Comment");
        }

        public ActionResult LoadComment(Guid PostId, DateTime before, int Page = 2)
        {

            return PartialView(dc.ArticleComments.Where(i => i.ArticleId == PostId && i.On < before).OrderByDescending(i => i.On).Skip(Page * 3).Take(3).ToList().OrderBy(i => i.On));
        }
    }
}