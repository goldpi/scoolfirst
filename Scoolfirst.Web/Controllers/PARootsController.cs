﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.PeriodicAssesment;
using Scoolfirst.ViewModel.Assement;

namespace Scoolfirst.Web.Controllers
{
    public class PARootsController : ApiController
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        [HttpGet]
        [Route("api/PARoots/check/{id}")]
        public bool CheckExamAppeared(long id)
        {
            var Iden = User.Identity.Name;
            var user = db.Users.FirstOrDefault(i => i.UserName == Iden);
            return db.PAAnswers.Any(i => i.ReasoningId == id && i.UserId == user.Id);
        }
        [HttpGet]
        [Route("api/PARoots/attemps/{id}")]
        public IEnumerable<PAAttemp> Attemps(long id)
        {
            var Iden = User.Identity.Name;
            var user = db.Users.FirstOrDefault(i => i.UserName == Iden);
            return db
                .PAAttemp
                .Where(i => i.ReasoningId == id && i.UserId == user.Id);
        }



        [HttpGet]
        [Route("api/PARoots/AnswersGiven/{id}")]
        public IEnumerable<PAAnswers> AnswersGiven(long id)
        {
            var Iden = User.Identity.Name;
            var user = db.Users.FirstOrDefault(i => i.UserName == Iden);
            db.Configuration.LazyLoadingEnabled = false;
            db.Configuration.ProxyCreationEnabled = false;
            return db
                .PAAnswers
                .Include(i => i.Question)
                .Include(i => i.Question.Option)
                .Where(i => i.ReasoningId == id && i.UserId == user.Id)
                .ToList();
        }


        [HttpGet]
        [Route("api/PARoots/userAnswersGiven/{id}/{userid}")]
        public IEnumerable<PAAnswers> AnswersGivenuser(long id, string userid)
        {
            return db
                .PAAnswers
                .Include(i => i.Question)
                .Where(i => i.ReasoningId == id && i.UserId == userid)
                .ToList();
        }



        [HttpPost]
        [Route("api/PARoots/Answers")]
        [ResponseType(typeof(int))] 
        public IHttpActionResult SaveAnswers(AnswerGiven answer)
        {
            var Iden = User.Identity.Name;
            var user = db.Users.FirstOrDefault(i => i.UserName == Iden);
            var first = answer.Answer[0];
            var Attempt = new PAAttemp
            {
                Id = Guid.NewGuid(),
                On = DateTime.UtcNow,
                ReasoningId = first.ReasoningId,
                UserId = user.Id,
                WrongAns= answer.Wrong,
                RightAns= answer.Correct,
                SkippedAns= answer.Skiped
            };
            db.PAAttemp.Add(Attempt);
            db.SaveChanges();
            List<PAAnswers> ans = new List<PAAnswers>();
            foreach (var i in answer.Answer)
            {
                ans.Add(new PAAnswers { Id = Guid.NewGuid(), OptionId = i.AnswerId, UserId = user.Id, QuestionId = i.QuesionId, ReasoningId = i.ReasoningId, AttempId = Attempt.Id });
            }
            db.PAAnswers.AddRange(ans);
            db.SaveChanges();
            return Ok(1);
        }
        [ResponseType(typeof(PARoot))]
        public IHttpActionResult GetPARoot(long id)
        {
            var Iden = User.Identity.Name;
            var user = db.Users.FirstOrDefault(i => i.UserName == Iden);
            db.Configuration.LazyLoadingEnabled = false;
            db.Configuration.ProxyCreationEnabled = false;
            PARoot PARoot = db
                .PARoots
                .Include(i=>i.QuestionsSet)
                .Include(i=>i.QuestionsSet.Select(o=>o.Option))
                .First(i=>i.Id==id);
            if (PARoot == null)
            {
                return NotFound();
            }
            
            return Ok(PARoot);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PARootExists(long id)
        {
            return db.PARoots.Count(e => e.Id == id) > 0;
        }
    }
}