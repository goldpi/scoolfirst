﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Scoolfirst.Web.Controllers
{
    
    public class AuthController : AppAuthController
    {
        

        // GET: Auth
        public async Task<ActionResult> Index(string token)
        {
            var user =await GetUserInfo(token);
            return View("index",user);
        }

        private async Task<string> GetUserInfo(string token)
        {
            var client = new HttpClient { BaseAddress = new Uri("http://api.scoolfirst.com") };
            client.DefaultRequestHeaders.Add("Authorization", "bearer "+token);
            var response = await client.GetStringAsync("api/account/userInfo");
            return response;
        }
    }
}