﻿
using ActionMailerNext.Mvc5_2;
using MvcRazorToPdf;
using Scoolfirst.Model.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using ActionMailerNext;

namespace Scoolfirst.Web.Controllers
{
    public class MailController : MailerBase
    {
        public MailController()
        {
            //this.MailSender = new ActionMailerNext.SendGridSender.SendGridMailSender();
        }
        // GET: Mail
        public EmailResult Invoice(Order model,ControllerContext Context)
        {
            
            MailAttributes.To.Add(new MailAddress(model.Email));
            MailAttributes.From = new MailAddress("no-reply@upgradejr.com");
            MailAttributes.Subject = "Invoice";
            byte[] stream = Context.GeneratePdf(model: model, viewName:"Invoice",configureSettings:(w,d)=> { });

            MailAttributes.Attachments.Add("Invoice.pdf",stream);
            return Email("Invoice", model);
        }
    }
}