﻿using Scoolfirst.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.ViewModel.Feed;

namespace Scoolfirst.Web.Controllers
{
  
    public class AjaxController : AppAuthController
    {
        ScoolfirstContext dc;
        public AjaxController(ScoolfirstContext context)
        {
            dc = context;
        }

       
        // GET: Ajax
        public ActionResult AutoSuggestion(string s)
        {
            var vm = new GraphSerachViewModel(dc,s,User.Identity.Name,5,5,5);

            return Json(vm, JsonRequestBehavior.AllowGet);
        }


        public ActionResult FeedCal(DateTime start,DateTime end)
        {
            
            var dt = dc.Feeds.Where(i => i.OnDateTime>=start&&i.OnDateTime<=end);
            List<FeedCalander> fd = new List<FeedCalander>();
            foreach (var item in dt)
            {
                fd.Add(new FeedCalander().FromFeed(item));
            }
            return Json(fd, JsonRequestBehavior.AllowGet);
        }
    }
}