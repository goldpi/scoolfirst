﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Web.Controllers
{

    /// <summary>
    /// Dreprectiated // For Clean up
    /// </summary>
    [Authorize]
    public class MenuController : Controller
    {

        ScoolfirstContext dc = new ScoolfirstContext();

        // GET: Menu
        public ActionResult FeedCategory()
        {
            return PartialView(/*dc.FeedCategories.ToList()*/);
        }


        public ActionResult ProjectCategory()
        {
            return PartialView(dc.ProjectCategories.ToList());
        }
    }
}