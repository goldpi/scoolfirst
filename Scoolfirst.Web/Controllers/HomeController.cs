﻿using Scoolfirst.Model.Context;
using System.Web.Mvc;

namespace Scoolfirst.Web.Controllers
{


    public class HomeController : AppAuthController
    {
        public HomeController(ScoolfirstContext Context)
        {
            dc = Context;
        }

        ScoolfirstContext dc;

            
        public ActionResult Index()
        {

            return RedirectToAction("Index2");
        }
        [Route("Dashboard")]
        public ActionResult Index2()
        {
            return View("Index");
        }


        public ActionResult Calender()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Payment(string user)
        {
            return View("payment",null, user);
        }
        
        public ActionResult PaymentSuccess(string user)
        {
            return View("paymentSuccess",null, user);
        }
        public ActionResult PaymentFailed(string user)
        {
            return View("paymentSuccess", null, user);
        }


    }
}