﻿using Scoolfirst.Model.Context;
using Scoolfirst.ViewModel.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Scoolfirst.Web.Controllers
{
    
    public class KnowledgeNutritionController : AppAuthController
    {
        private ScoolfirstContext dc;
        public KnowledgeNutritionController(ScoolfirstContext Context)
        {
            dc = Context;// new ScoolfirstContext();
        }
        // GET: KNOWLEDGENUTRITION
        public ActionResult Index(int page=0)
        {
            if (User == null)
                return RedirectToAction("login", "account");
            FeedItems items = new FeedItems();
            items.GetItems(dc, User.Identity.Name, page, 3, "KN");
            if (Request.IsAjaxRequest())
                return PartialView(items.Feed);
            return View(items.Feed);
        }

        // GET: KNOWLEDGENUTRITION/Details/5
        [Route("KNOWLEDGENUTRITION/{Id}/{Title}")]
        public ActionResult Details(Guid Id)
        {
            var user = User.Identity.Name;
            var UserId = dc.Users
                .FirstOrDefault(i => i.UserName == user)
                .Id;
            var l = dc.Feeds
                .Include(i => i.Post)
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Stared)
                .FirstOrDefault(i => i.Id == Id);
            var feed = new FeedViewModel { Feed = l, FavByMe = l.Stared.Any(i => i.UserId == UserId), LikedByMe = l.Likes.Any(i => i.UserId == UserId) };
            return View(feed);
        }
        public ActionResult BBF(long Id)
        {
            var vid = dc.PostVideo.FirstOrDefault(i => i.PostId == Id);
            return PartialView(vid);
        }

    }
}
