﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using System.Text;
using System.Security.Cryptography;
using Scoolfirst.Model.Provider;
using System.Web.Script.Serialization;
using Scoolfirst.Model.Provider.PaymentGateway;
using Scoolfirst.Model.Payments;
using Scoolfirst.Model.Projects;
using Microsoft.AspNet.SignalR;
using Scoolfirst.Web.hubs;
using System.Data.Entity;
using System.Net;


namespace Scoolfirst.Web.Controllers
{
    public class PaymentController : Controller
    {

        public PaymentController(ScoolfirstContext Context,IGateWay Gateway)
        {
            db = Context;
            Payment = Gateway;
        }
        private IGateWay Payment;
        private ScoolfirstContext db ;

        public void Index(Guid slag)
        {
            

            // or
           
            var t = db.Transactions.Find(slag);
            var user = db.Users.Find(t.UserId);
            var result = Payment.MakePayment(t.Id.ToString(),
                t.Amount.ToString(), t.Type.ToString(), t.Email, user.PhoneNumber, user.FullName);
            System.Web.HttpContext.Current.Response.Write(result);
            debug(result);
            debug(Payment);
            return;
        }

        private  void debug( object result)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<DebugHub>();
            var debug = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            context.Clients.All.show(result);
        }

        public ActionResult Success(string slag)
        {
            var TaxId = Guid.Parse(Request.Form["txnid"].ToString());
            var tnx = db.Transactions.Find(TaxId);
            var form = Request.Form;
            Dictionary<string, string> d = new Dictionary<string, string>();
            foreach(string i in Request.Form.Keys)
            {
                d.Add(i, Request.Form[i]);
            }
            var save= new JavaScriptSerializer().Serialize(d);
            tnx.TransactionResult = save;
            db.SaveChanges();


            Product product = null;

            product = Product(tnx, product);

            var user = db.Users.Find(tnx.UserId);
            if (string.IsNullOrEmpty(user.Address))
            {
                user.Address = tnx.Address;
            }
            var order = new Order
            {
                UserId = tnx.UserId,
                Email = tnx.Email,
                Address = tnx.Address,
                ProductId = product.Id,
                TransactionId = tnx.Id,
                Name = user.FullName,
                OrderDate = DateTimeOffset.Now.DateTime,
                Phone = user.PhoneNumber,
               
            };
            db.Orders.Add(order);
            db.SaveChanges();
            // SendInvoice(order.Id);
            using (var client = new WebClient())
            {
                var responseString = client.DownloadString("http://upgradejr.com.strawberry.arvixe.com/payment/shootmail/"+order.Id);
                debug(responseString);
                //debug(order);
            }
            //send email
            return View(model:save);
        }
        public ActionResult Failed(string slag)
        {
            var TaxId = Guid.Parse(Request.Form["txnid"].ToString());
            var tnx = db.Transactions.Find(TaxId);

            
            var form = Request.Form;

            Dictionary<string, string> d = new Dictionary<string, string>();
            foreach (string i in Request.Form.Keys)
            {
                d.Add(i, Request.Form[i]);
            }
            var save = new JavaScriptSerializer().Serialize(d);

            tnx.TransactionResult = save;
            db.SaveChanges();
            return View(model: save);
        }

        private Product Product(Transaction tnx, Product product)
        {
            var user = db.Users.Find(tnx.UserId);
            if (tnx.Type == Model.Payments.TransactionsType.F4x ||tnx.Type==TransactionsType.F8x)
            {
                var Boos4x = new Booster
                {
                    ExpiryOn = DateTime.Now.AddDays(45),
                    Factor = tnx.Type== TransactionsType.F4x?4:8,
                    Id = Guid.NewGuid(),
                    UserId = tnx.UserId
                };
                db.Booster.Add(Boos4x);
                db.SaveChanges();
                user.Factor = Boos4x.Factor;
                string S = tnx.Type == TransactionsType.F4x ? "4X BOOSTER" : "8X BOOSTER";
                if (db.Products.Any(i => i.Title == S))
                {
                    product = db.Products.FirstOrDefault(i => i.Title == S);
                }
                else
                {
                    ProductCategory category = db.ProductCategories.FirstOrDefault(i => i.Title == "Play");
                    if (category == null)
                    {
                        category = new ProductCategory { Title = "Play", Description = "play", ShortDescription = "play", Image = "" };
                        db.ProductCategories.Add(category);
                        db.SaveChanges();
                    }
                    product = new Product { Title = S, Active = false, Price = 40, AddedOn = DateTime.Now, ShortDisc = "For play booster", ProductDisc = "For Play", Reedeem = Reedeem.OnlyMoney, CategoryId = category.Id, ShowMoney = "", ShowPoints = "" };
                    db.Products.Add(product);
                    db.SaveChanges();
                }
            }
            if (tnx.Type == TransactionsType.Subscription)
            {
                string sText = "Subscription of Rs." + tnx.Amount;
                if (db.Products.Any(i => i.Title == sText))
                {
                    product = db.Products.FirstOrDefault(i => i.Title == sText);
                }
                else
                {
                    ProductCategory category = db.ProductCategories.FirstOrDefault(i => i.Title == "Subcription");
                    if (category == null)
                    {
                        category = new ProductCategory { Title = "Subcription", Description = "Subcription", ShortDescription = "Subcription", Image = "" };
                        db.ProductCategories.Add(category);
                        db.SaveChanges();
                    }
                    product = new Product { Title = sText, Active = false, Price = (decimal)tnx.Amount , AddedOn = DateTime.Now, ShortDisc =sText, ProductDisc = sText, Reedeem = Reedeem.OnlyMoney, CategoryId = category.Id, ShowMoney = "", ShowPoints = "" };
                    db.Products.Add(product);
                    db.SaveChanges();
                }

                
                user.Expiry = DateTime.Now.AddYears(1);
                
            }
            if (tnx.Type == TransactionsType.Purchase && tnx.ProductId.HasValue)
            {
                db.Entry(tnx).Reference(i => i.Product).Load();
                product =tnx.Product ; //resovle this.
            }
            db.SaveChanges();
            return product;
        }


        public ActionResult PaymentOver()
        {
            return View();
        }


        public ActionResult PaymentFailed()
        {
            return View();
        }

        public ActionResult Result()
        {

            return View();
        }

        public ActionResult Shootmail(int Id )
        {
            SendInvoice(Id);
            return Content("Mail fired");
        }

        private void SendInvoice(int Id)
        {
            try
            {
                var order = db.Orders.Include(i => i.Product).Include(i=>i.Transaction).FirstOrDefault(i => i.Id == Id);
                //debug(order);
                MailController m = new MailController();
                m.Invoice(order, this.ControllerContext).Deliver();
                debug("Mail sent to" + order.Email);
            }
            catch(Exception ex)
            {
                debug(ex);
            }
        }
    }
}