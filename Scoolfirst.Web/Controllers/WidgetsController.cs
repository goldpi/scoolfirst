﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.ViewModel.Feed;

namespace Scoolfirst.Web.Controllers
{
    
    public class WidgetsController : AppAuthController
    {
        // GET: Widgets

        public WidgetsController(ScoolfirstContext Context)
        {
            dc = Context;
        }

       private ScoolfirstContext dc ;

        //[OutputCache(Duration =3600,VaryByParam ="Type")]
        [ChildActionOnly]
        public ActionResult TopFeeds(string Type)
        {
          
            return Data();

        }
        [OutputCache(Duration = 3600)]
        [ChildActionOnly]
        public ActionResult TopKDC()
        {
            var p = dc.Articles
                
                .Include(i => i.Likes)
                .OrderByDescending(i => i.Likes.Count()).Take(5);
            return PartialView(p);
            
        }
        [OutputCache(Duration = 3600)]
        [ChildActionOnly]
        public ActionResult TopProjects()
        {
            var p = dc.Projects.OrderBy(i => Guid.NewGuid()).Take(5);
            return PartialView(p);
        }

        public ActionResult TopQuestion()
        {
            return PartialView();
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            LoadRegistationViewData();
            return PartialView();
        }
        [AllowAnonymous]
        public ActionResult RegisterUI()
        {
            return PartialView();

        }

        public void LoadRegistationViewData()
        {
            var Schools = dc.Schools.ToList();
            var Classes = dc.Classes.ToList();
            Classes.Insert(0, new Classes { Id = -1, RomanDisplay = "--Select Class--" });
            Schools.Insert(0, new School { Id = -1, SchoolName = "--Select School--" });
            ViewBag.SchoolId = new SelectList(Schools, "Id", "SchoolName");
            ViewBag.ClassId = new SelectList(Classes, "Id", "RomanDisplay");

        }

        public PartialViewResult Data()
        {
            var Controller = ControllerContext
                .ParentActionViewContext
                .RouteData
                .Values["Controller"]
                .ToString()
                .ToUpper();
            switch (Controller)
            {
                case "UINSPIREME":
                case "YOUINSPIREME":
                    return UInspireMeResult();

                case "CREATIVECANVAS":
                case "CREATIVE_CANVAS":
                case "CC":
                    ViewBag.Title = "Trending canvas";
                    return CrativeCanvas();
                case "KNOWLEDGENUTRITION":
                case "KNOWLEDGE_NUTRITION":
                case "KN":
                    ViewBag.Title = "Trending nutrition";
                    return KnowledgeNutrition();
                case "MUSTREMEMBERTRICKS":
                case "FEED":
                case "MRT":
                case "MUST_REMEMBER_TRICKS":
                    ViewBag.Title = "Trending tricks";
                    return MustRememberTricks();
                
                default:
                    ViewBag.Title = "Trending nutrition";
                    return KnowledgeNutrition();

            }
        }
        
        public PartialViewResult MustRememberTricks()
        {
            var posts = dc.Posts
                .Where(i => i.Pinned == true)
                .OrderByDescending(i => i.OnDateTIme)
                .Take(20);
            return PartialView("MustRememberTricks", posts);
        }

        public PartialViewResult KnowledgeNutrition()
        {
            FeedItems fi = new FeedItems();
            fi.GetItemsPined(dc, User.Identity.Name, 0, 20, "KN");
            return PartialView("KnowledgeNutrition", fi.Feed);
        }

        public PartialViewResult CrativeCanvas()
        {
            //FeedItems fi = new FeedItems();
            //fi.GetItemsPined(dc, User.Identity.Name, 0, 20, "KN");
            var t1 = dc.ProjectCategories.Include(i => i.Projects).ToList();
            return PartialView("CrativeCanvas",t1);
            //return PartialView("CrativeCanvas",fi.Feed);
        }

        public PartialViewResult UInspireMeResult()
        {
            var data = dc.RoleModels
                .Where(i=>i.Pinned==true)
                .OrderByDescending(i=>i.On)
                .Take(20).ToList();
            return PartialView("UInspireMeResult", data);
        }
    }
}