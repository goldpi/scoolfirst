﻿using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.ViewModel.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Web.Controllers
{
    
    public class CreativeCanvasController : AppAuthController
    {
        private ProjectCategory category;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session["category"] == null)
            {
               category = dc.ProjectCategories.FirstOrDefault();
               Session["category"] = category;
            }
            else
            {
                category = (ProjectCategory)Session["category"];
            }
            

            base.OnActionExecuting(filterContext);
            
        }
        private ScoolfirstContext dc;
        public CreativeCanvasController(ScoolfirstContext Context )
        {
            dc = Context;
        }
        // GET: CreativeCanvas
        public ActionResult Index(int page = 0, int cateId = 0)
        {
            if (cateId > 0)
            {
                var d = dc.ProjectCategories.Find(cateId);
                if (d != null)
                {
                    Session["category"] = category;
                    category = d;
                }
            }
            var creativeCanvas = dc.Projects.Where(i => i.CategoryId == category.Id&& i.Active==true).OrderByDescending(i=>i.AddedOn).Skip(page*3).Take(3);
            if (Request.IsAjaxRequest())
                return PartialView(creativeCanvas);
            return View(creativeCanvas);
        }
    }
}