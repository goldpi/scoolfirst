﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using Scoolfirst.Model.PeriodicAssesment;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Scoolfirst.Web.Controllers
{
    public class PeriodicAssessmentController : AppAuthController
    {
        private ScoolfirstContext dc;
        private User UserData;
        public PeriodicAssessmentController()
        {
            dc = new ScoolfirstContext();
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session["UserDetails"] == null)
            {
                var user = filterContext.HttpContext.User.Identity.Name;
                var UserData1 = (from l in dc.Users
                                 where l.UserName == user || l.Email == user

                                 select new { p = l, School = l.School, @class = l.Class }).FirstOrDefault();
                UserData = UserData1.p;
                UserData.School = UserData1.School;
                UserData.Class = UserData1.@class;
                Session["UserDetails"] = UserData;
            }
            else
            {
                UserData = (User)Session["UserDetails"];
            }
            base.OnActionExecuting(filterContext);
        }
        // GET: PeriodicAssessment
        public ActionResult Index(PAType type = PAType.WorkBook, int page = 0, int size = 30)
        {
            var Resoing = dc.PARoots
                .Where(i => i.SchoolId == UserData.SchoolId && i.ClassId == UserData.ClassId && i.TypeOfAssement == type)
                .OrderByDescending(i => i.Id).ToList();
            //TODO  Shashid  Pagging

            return View(Resoing);

        }

        public ActionResult Test(long Id)
        {
            ViewBag.UserData = UserData;
            var p = dc.PARoots.Include(i => i.QuestionsSet).FirstOrDefault(i => i.Id == Id);
            return View(p);
        }
    }
}