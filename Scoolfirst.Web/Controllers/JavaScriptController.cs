﻿using Scoolfirst.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
namespace Scoolfirst.Web.Controllers
{
    public class JavaScriptController : AppAuthController
    {
        private ScoolfirstContext dc;
        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
            filterContext.HttpContext.Response.ContentType = "text/javascript";
        }

        public JavaScriptController(ScoolfirstContext DataContext)
        {
            try
            {
                dc = DataContext;
            }
            catch
            {

            }
        }
        // GET: JavaScript
        public PartialViewResult ReasoningTest(int Id)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var re = dc
                .ReasoningRoots.Include(l => l.Questions)
                .Include(i => i.Questions.Select(io => io.Option))
                .FirstOrDefault(p=>p.Id==Id);
            
            switch (re.IsLessonOrExam)
            {
                case Model.RNM.ReasoningRootType.Lesson:
                case Model.RNM.ReasoningRootType.RevisionOfLesson:
                    return PartialView("PracticeTest", re);

                case Model.RNM.ReasoningRootType.Exam:
                    var user = dc.Users.FirstOrDefault(i => i.UserName == User.Identity.Name);
                    ViewBag.Appeared = dc.ReasoningAnswers.Any(i => i.ReasoningId == Id && i.UserId == user.Id);
                    return PartialView("Test", re);
                case Model.RNM.ReasoningRootType.Single:
                    return PartialView("Test", re);
                case Model.RNM.ReasoningRootType.ReviewOfExam:
                    return PartialView("NoTest", re);
                default:
                    return PartialView("NoTest", re);
            }
            
        }


        public PartialViewResult PATest(int Id)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var re = dc
                .PARoots.Include(l => l.QuestionsSet)
                .Include(i => i.QuestionsSet.Select(io => io.Option))
                .FirstOrDefault(p => p.Id == Id);
            var user = dc.Users.FirstOrDefault(i => i.UserName == User.Identity.Name);
            ViewBag.Appeared = dc.PAAnswers.Any(i => i.ReasoningId == Id && i.UserId == user.Id);
            return PartialView(re);

        }


    }
}