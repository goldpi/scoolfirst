﻿using Scoolfirst.Model.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Web.Controllers
{
    public class PostController : AppAuthController
    {

        // GET: Post
        public ActionResult Index(string Id,ModType type)
        {
            switch (type)
            {
                case ModType.KN:
                    return Redirect(Url.Action("Details", "KnowledgeNutrition", new { Id = Id }));   
                case ModType.RR:
                    return Redirect(Url.Action("index", "Reasoning"));
                case ModType.WRKSHT:
                    return Redirect(Url.Action("MySubjects", "WorkBook"));                    
                case ModType.UIM:
                    return Redirect(Url.Action("Details", "youinspireme", new { Id = Id }));                   
                case ModType.CC:
                    return Redirect(Url.Action("index", "creativecanvas"));
                case ModType.KDC_APP:
                    return Redirect(Url.Action("Details", "MyKdc", new { Id = Id }));
                default:
                    return Redirect("/");
            }
            
        }
    }
}