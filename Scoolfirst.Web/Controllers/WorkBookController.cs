﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Threading;
using System.Web.Mvc;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Web.Controllers
{
   
    public class WorkBookController : AppAuthController
    {
        ScoolfirstContext dc ;
        public WorkBookController(ScoolfirstContext Context)
        {
            dc = Context;
        }

        User currentUser = null;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = User.Identity.Name;
            var userdata = dc.Users.FirstOrDefault(i => i.UserName == user || i.Email == user);
            if (userdata == null)
                filterContext.Result = new HttpNotFoundResult();
            dc.Entry(userdata).Reference(i => i.School).Load();
            dc.Entry(userdata).Reference(i => i.Class).Load();
            
            TempData["UserName"] = userdata.FullName;
            TempData["Points"] = userdata.Points;
            TempData["SchoolName"] = userdata.School.SchoolName;
            TempData["ClassName"] = userdata.Class.RomanDisplay;
            
            currentUser = userdata;
            base.OnActionExecuting(filterContext);
        }
        // GET: WorkBook
        [Route("WorkBook/")]
        public ActionResult MySubjects()
        {
            if (currentUser.School.UseMasterContent == false)
            {
                var hasSub = dc.SchoolClassSubject
                .Include(i => i.School)
                .Include(i => i.Subject)
                .Include(i => i.Books)
                .Where(i => i.SchoolId == currentUser.SchoolId && i.ClassId == currentUser.ClassId).ToList();
                return View(hasSub);

            }
            else
            {
                var master = dc.Schools.FirstOrDefault(i => i.IsMasterContent);
                var hasSub = dc.SchoolClassSubject
                .Include(i => i.School)
                .Include(i => i.Subject)
                .Include(i => i.Books)
                .Where(i => i.SchoolId == master.Id && i.ClassId == currentUser.ClassId).ToList();
                return View(hasSub);
            }
             
        }

        
        [ChildActionOnly]
        public ActionResult Units(int Id)
        {
            ViewBag.Id = Id;
            var Units = dc.Books
                .Find(Id)
                .Chapters.OrderBy(i=>i.Id)
                .ToList();
            return PartialView(Units);

        }
        [Route("WorkBook/{id}/{Bookid}/UnitWise/")]
        public ActionResult UnitWise(int Id,int Bookid)
        {
            if (Id < 0)
            {
                return HttpNotFound();
            }
            var control = dc.Books.Include(i=>i.Subject)
                .Include(i => i.Chapters)
                .FirstOrDefault(i => i.Id == Bookid);
            ViewBag.Books = control;
            var count = control.Chapters.Count();
            if (count < Id)
            {
                return HttpNotFound();
            }

            ViewBag.Next = (count > (Id+1));
            ViewBag.Previous = Id > 0;
            
            var unit = control
                .Chapters
                .OrderBy(i => i.Id)
                .Skip(Id * 1)
                .Take(1).FirstOrDefault();
            dc.Entry(unit).Collection(i => i.Questions).Load();
            dc.Entry(unit).Reference(i => i.Classes).Load();
            ViewBag.Count = unit.Questions.Count();
            ViewBag.Chapter = unit.Name;
            ViewBag.Subjects = dc.SchoolClassSubject
               .Include(i => i.Subject)
               .Include(i => i.Books)
               .Where(i => i.SchoolId == currentUser.SchoolId && i.ClassId == currentUser.ClassId).ToList();
            var Questions = unit.Questions.OrderBy(i=>i.Id).ToList();
            return View(Questions);
        }
        [ChildActionOnly]
        public ActionResult TestWorkbook()
        {
           var Quiz=  dc.Quizes
                .Where(i => i.School == currentUser.SchoolId.ToString() && i.Class ==currentUser.ClassId.ToString())
                .ToList();
           return PartialView(Quiz);
        }
        [Route("WorkBook/{id}/{Name}/Test")]
        public ActionResult WorkBook(Guid Id)
        {
            var Quiz = dc.Quizes.Include(i=>i.Questions)
                .FirstOrDefault(i=>i.Id==Id);
            if (Quiz != null) return View(Quiz);
            else
                return HttpNotFound();
           
        }
        [ChildActionOnly]
        public ActionResult Syllabus()
        {
            if (currentUser.School.UseMasterContent == false)
            {
                var result = dc.SyllabusDates
              //.Include(i => i.Quiz)
              .Where(i => i.Class == currentUser.ClassId && i.School == currentUser.SchoolId)
              .ToList();
                return PartialView(result);
            }
            else
            {
                var master = dc.Schools.FirstOrDefault(i => i.IsMasterContent);
                var result = dc.SyllabusDates
             //.Include(i => i.Quiz)
             .Where(i => i.Class == currentUser.ClassId && i.School == master.Id)
             .ToList();
                return PartialView(result);
            }
            
        }


        
    }
}