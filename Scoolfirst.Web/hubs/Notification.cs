﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Scoolfirst.Model.Identity;
using Scoolfirst.Model.MongoDatabase;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Web.hubs
{


    public class DebugHub : Hub
    {

    }


    [HubName("NotificationHub"),
        Authorize]
    public class NotificationHub : Hub
    {
        ScoolfirstContext dc;
        GroupManagerManogDB GM = new GroupManagerManogDB();
        OnlinerUserRepo OnlineManger = new OnlinerUserRepo();

        public NotificationHub(ScoolfirstContext dataContext)
        {
            dc = dataContext;
        }
        public override Task OnConnected()
        {
            if (Context.User != null)
            {
              
                var ass = Context.User.Identity.Name;
                var user = dc.Users.FirstOrDefault(i => i.UserName == ass);
                addUser(user);
                AddToGruop(user);

            }
            return base.OnConnected();
        }

        private void AddToGruop(Model.Identity.User user)
        {

            var Groups = dc.Classes.Find(user.ClassId).Groups.Select(i => new { Name = i.Name, Classes = i.Class, i.Schools });
            foreach (var item in Groups)
            {
                if (GM.GroupExists(item.Name))
                {
                    GM.AddToGroup(item.Name, Context.ConnectionId);
                }
                else
                {
                    GM.AddGroupList(item.Name,
                        item.Classes.Select(i => i.RomanDisplay).ToList(),
                        item.Schools.Select(i => i.SchoolName).ToList(),
                        Context.ConnectionId);
                }
                this.Groups.Add(this.Context.ConnectionId, item.Name);
            }

        }

        private void addUser(Model.Identity.User user)
        {


            OnlineManger.AddOnlineUser(new OnlineUser
            {
                ConnectionId = Context.ConnectionId,
                UserId = user.UserName,
                DeviceId = Context.Request.Headers["User-Agent"] ?? "Unkonw",
                ConnectedOn = DateTime.UtcNow.AddHours(5.5),
                LastConnection = DateTime.UtcNow.AddHours(5.5),
                Reconnected = 0
            });
        }

        public void ClearOldPhoneCon()
        {

            OnlineManger.LastActivity(Context.ConnectionId);
            var con = OnlineManger.OnlineUser(Context.User.Identity.Name).Where(i => i.ConnectionId != Context.ConnectionId);
            foreach (var item in con)
                GM.RemoveFromGropu(item.ConnectionId);

            OnlineManger.DeleteByDievice(Context.ConnectionId, Context.User.Identity.Name, Context.Request.Headers["User-Agent"] ?? "Unkonw");
        }


        public override Task OnReconnected()
        {
            if (Context.User.Identity.IsAuthenticated )
            {
                var ass = Context.User.Identity.Name;
                if (OnlineManger.OnlineUser(ass).Any(i => i.ConnectionId == Context.ConnectionId))
                {
                    OnlineManger.Reconnect(Context.ConnectionId);
                }
                else
                {
                    var user = dc.Users.FirstOrDefault(i => i.UserName == ass);
                    addUser(user);
                }
                ////var user = dc.Users.FirstOrDefault(i => i.UserName == ass);
            }
            return base.OnReconnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {

            OnlineManger.DeleteByConection(Context.ConnectionId);
            GM.RemoveFromGropu(Context.ConnectionId);
           
            return base.OnDisconnected(stopCalled);
        }
        public void MarkReadAll()
        {
            NotificationRecivedManager NRM = new NotificationRecivedManager();

            var ret = NRM.MarkAll(Context.User.Identity.Name).ToList();
            Clients.Caller.Read(ret, "A");
        }

        public void MarkRead(string Id)
        {
            NotificationRecivedManager NRM = new NotificationRecivedManager();
            var ls = new List<string>();
            ls.Add(NRM.MarkRead(Id, Context.User.Identity.Name));
            Clients.Caller.Read(ls, "R");
        }


        public void MarkDelete(string Id)
        {
            NotificationRecivedManager NRM = new NotificationRecivedManager();
            var ls = new List<string>();
            ls.Add(NRM.MarkDelete(Id, Context.User.Identity.Name));
            Clients.Caller.Read(ls, "D");
        }


        public void MyUnreadNotification()
        {
            try
            {
                NotificationRecivedManager NRM = new NotificationRecivedManager();
                var us = Context.User.Identity.Name;
                var user = dc.Users.FirstOrDefault(i => i.UserName == us);
                var Groups = dc.Classes.Find(user.ClassId).Groups.Select(i => new { Name = i.Name, Classes = i.Class, i.Schools });

                var list = NRM.UnreadNotification(Context.User.Identity.Name, Groups.Select(i => i.Name).ToList()).ToList();
              
                Clients.Caller.notification(list);
            }
            catch 
            {
              
            }
        }

        public void MyDiliveredNotification()
        {
            NotificationRecivedManager NRM = new NotificationRecivedManager();
            var dd = NRM.DeliveredNotification(Context.User.Identity.Name).ToList();
            Clients.Caller.notification(dd);
        }


        public void Received(string Id)
        {
            
            NotificationRecivedManager NRM = new NotificationRecivedManager();
            NRM.Add(Id, Context.User.Identity.Name);
           
        }
    }
}
