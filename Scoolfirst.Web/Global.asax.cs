﻿using Newtonsoft.Json;
using Scoolfirst.ViewModel.Common;
using Scoolfirst.Web.Sheduler;
using System.IO;
using System.Net;
using System.Threading;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static BrandingVM Logos;
        public static string Path;
        protected void Application_Start()
        {
            
            
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            //GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            Thread b = new Thread(() => Logos = GetLogo());
            b.Start();
            b.Join();
            JobSheduler.Start();

        }
        private BrandingVM GetLogo()
        {
            try
            {
                string sURL ="http://"+HostingServerInfo.Admin.Url()+"/home/getlogos";
                WebRequest wrGETURL= WebRequest.Create(sURL);
                Stream objStream= wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream);
                BrandingVM tmp = JsonConvert.DeserializeObject<BrandingVM>(objReader.ReadToEnd());
                string resonse = objReader.ReadToEnd();
                // JObject json = JObject.Parse(objReader.ReadToEnd());
                return tmp;
            }
            catch
            {
               
            }
            return null;
        }
    }
}
