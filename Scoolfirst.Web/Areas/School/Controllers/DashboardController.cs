﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Web.Areas.School.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        // GET: School/Dashboard
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Calender()
        {
            return View();
        }
    }
}