﻿using Scoolfirst.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Scoolfirst.Model.Identity;
using Scoolfirst.ViewModel.Assement;

namespace Scoolfirst.Web.Areas.School.Controllers
{
    [Authorize]
    public class ExamsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();
        private User currentuser;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = filterContext.HttpContext.User.Identity.Name;
            currentuser = db.Users.FirstOrDefault(i => i.UserName == user);

            base.OnActionExecuting(filterContext);
        }
        // GET: School/Exams
        public ActionResult Index(int cid=0)
        {
            if (cid == 0)
            {
                cid = db.Classes.First().Id;
            }
            ViewBag.cid = new SelectList(db.Classes, "Id", "RomanDisplay",cid);
            return View(db.PARoots.Where(i=>i.ClassId==cid).OrderByDescending(i=>i.DisplayAfterDate));
        }

        public ActionResult Liststudent(int id)
        {
            ViewBag.Id = id;
            var users = db.PAAttemp.Include(i=>i.User).Where(i => i.ReasoningId == id);

            return View(users);
        }

        public ActionResult StudentReport(Guid Id)
        {

            var result1 = db.PAAttemp
                .Include(i => i.Reasoning)
                .Include(i=>i.Ans)
                .FirstOrDefault(i => i.Id == Id);
            var Questions = db.PAQuestions.Include(i=>i.Option).Where(i => i.SetId == result1.ReasoningId).ToList();
            var result = new PAReport
            {
                Attemp = result1,
                Questions = Questions
            };
            return PartialView(result);
        }

        public ActionResult Report(int id, string userId)
        {
            db.Configuration.LazyLoadingEnabled = false;
            db.Configuration.ProxyCreationEnabled = false;
            var result = db.ReasoningAnswers.Include(i => i.Question).Include(i => i.Attemp).
                Include(i => i.Question.Option).Where(i => i.ReasoningId == id && i.UserId == userId).ToList();
            return View(result);

        }
    }
}