﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Web.Areas.School.Controllers
{
    [Authorize]
    public class StudentsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();
        private User currentuser;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = filterContext.HttpContext.User.Identity.Name;
            currentuser = db.Users.FirstOrDefault(i => i.UserName == user);
            
            base.OnActionExecuting(filterContext);
        }
        // GET: School/Students
        public async Task<ActionResult> Index(int? classId,int page = 1, int size = 10, string search = "")
        {
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay");
            IQueryable<User> List= db.Users.Include(u => u.Class).Include(u => u.School).Where(i=>i.SchoolId==currentuser.SchoolId );
            if (!string.IsNullOrEmpty(search))
                List = List.Where(i => i.FullName.Contains(search));
            if (classId != null)
            {
                List = List.Where(i => i.ClassId == classId);
            }
            
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
          return View(await List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToListAsync());
        }

        // GET: School/Students/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //User user = db.Users.FirstOrDefault();//.FindAsync(id);
            User user = await db.Users.FirstOrDefaultAsync(i=>i.Id==id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
