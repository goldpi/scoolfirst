﻿using Scoolfirst.Web.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Scoolfirst.Web.Infra
{
    public class MenuService : ActionFilterAttribute, IDisposable
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Cache["Menu"] != null)
            {
                filterContext.Controller.TempData["menu"] = filterContext.HttpContext.Cache["Menu"] as List<MenuItem>;
            }
            else
            {
                MenuItemManager menu = new MenuItemManager();
                menu.Load(filterContext.HttpContext.Server.MapPath("~/Config/Menu.xml"));
                filterContext.Controller.TempData["menu"] = menu.Menus;
                filterContext.HttpContext.Cache["Menu"] = menu.Menus;
            }

            base.OnActionExecuting(filterContext);
        }
        public void Dispose()
        {

        }
    }
}