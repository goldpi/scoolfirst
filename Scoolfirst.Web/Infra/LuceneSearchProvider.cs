﻿using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Scoolfirst.Model.WorkBook;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Scoolfirst.Web.Infra
{
    public  class LuceneSearchProvider<T> /* where T :class*/
    {
      
        public  string _luceneDir => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data/lucene_index/"+typeof(T).Name);
        private  FSDirectory _directoryTemp;
        private  FSDirectory _directory {
        get {
            if (_directoryTemp == null) _directoryTemp = FSDirectory.Open(new DirectoryInfo(_luceneDir));
            if (IndexWriter.IsLocked(_directoryTemp)) IndexWriter.Unlock(_directoryTemp);
            var lockFilePath = Path.Combine(_luceneDir, "write.lock");
            if (File.Exists(lockFilePath)) File.Delete(lockFilePath);
            return _directoryTemp;
            }
        }
        private  void _addToLuceneIndex(T sampleData, IndexWriter writer,Func<T,IndexWriter,Document> d)
        {
            // remove older index entry



            // add new index entry
            //var doc = new Document();

            //// add lucene fields mapped to db fields
            //doc.Add(new Field("Id", sampleData.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            //doc.Add(new Field("Refrence", sampleData.Refrence, Field.Store.YES, Field.Index.ANALYZED));
            //doc.Add(new Field("Class", sampleData.Class, Field.Store.YES, Field.Index.ANALYZED));
            //doc.Add(new Field("School", sampleData.School, Field.Store.YES, Field.Index.ANALYZED));
            //doc.Add(new Field("Subject", sampleData.Subject, Field.Store.YES, Field.Index.ANALYZED));
            //doc.Add(new Field("Title", sampleData.Title, Field.Store.YES, Field.Index.ANALYZED));
            //doc.Add(new Field("Answer", sampleData.Answer, Field.Store.YES, Field.Index.ANALYZED));
            //doc.Add(new Field("ChapterId", sampleData.ChapterId.ToString(), Field.Store.YES, Field.Index.ANALYZED));

            // add entry to index
            var res = d.Invoke(sampleData,writer);
            writer.AddDocument(res);
        }
        public  void AddUpdateLuceneIndex(IEnumerable<T> sampleDatas, Func<T,IndexWriter, Document> d)
        {
            // init lucene
            var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);
            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                // add data to lucene search index (replaces older entry if any)
                foreach (var sampleData in sampleDatas) _addToLuceneIndex(sampleData, writer,d);

                // close handles
                analyzer.Close();
                writer.Dispose();
            }
        }
        public  void AddUpdateLuceneIndex(T sampleData,Func<T,IndexWriter,Document> d)
        {
            AddUpdateLuceneIndex(new List<T> { sampleData },d);
        }
        public  void ClearLuceneIndexRecord(int record_id)
        {
            // init lucene
            var analyzer = new StandardAnalyzer( Lucene.Net.Util.Version.LUCENE_30);
            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                // remove older index entry
                var searchQuery = new TermQuery(new Term("Id", record_id.ToString()));
                writer.DeleteDocuments(searchQuery);

                // close handles
                analyzer.Close();
                writer.Dispose();
            }
        }
        public  bool ClearLuceneIndex()
        {
            try
            {
                var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);
                using (var writer = new IndexWriter(_directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    // remove older index entries
                    writer.DeleteAll();

                    // close handles
                    analyzer.Close();
                    writer.Dispose();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public  void Optimize()
        {
            var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);
            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                analyzer.Close();
                writer.Optimize();
                writer.Dispose();
            }
        }
        private  T _mapLuceneDocumentToData(Document doc,Func<Document,T> document)
        {
            return document.Invoke(doc);
           
        }
        private  Query parseQuery(string searchQuery, QueryParser parser)
        {
            Query query;
            try
            {
                query = parser.Parse(searchQuery.Trim());
            }
            catch (ParseException)
            {
                query = parser.Parse(QueryParser.Escape(searchQuery.Trim()));
            }
            return query;
        }
        private  IEnumerable<T> _search(Func<Document, T> d,string searchQuery, string[] arr,string searchField = "")
        {
            // validation
            if (string.IsNullOrEmpty(searchQuery.Replace("*", "").Replace("?", ""))) return new List<T>();

            // set up lucene searcher
            using (var searcher = new IndexSearcher(_directory, false))
            {
                var hits_limit = 1000;
                var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);

                if (!string.IsNullOrEmpty(searchField))
                {
                    var parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, searchField, analyzer);
                    var query = parseQuery(searchQuery, parser);
                    var hits = searcher.Search(query, hits_limit).ScoreDocs;
                    var results = _mapLuceneToDataList(hits, searcher,d);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
                // search by multiple fields (ordered by RELEVANCE)
                else
                {
                    var parser = new MultiFieldQueryParser
                        (Lucene.Net.Util.Version.LUCENE_30, arr, 
                            analyzer);
                    var query = parseQuery(searchQuery, parser);
                    var hits = searcher.Search
                    (query, null, hits_limit, Sort.RELEVANCE).ScoreDocs;
                    var results = _mapLuceneToDataList(hits, searcher,d);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
            }

        }
        public  IEnumerable<T> Search(Func<Document, T> d,string input,string []arr, string fieldName = "")
        {
            if (string.IsNullOrEmpty(input)) return new List<T>();

            var terms = input.Trim().Replace("-", " ").Split(' ')
                .Where(x => !string.IsNullOrEmpty(x)).Select(x => x.Trim() + "*");
            input = string.Join(" ", terms);

            return _search(d,input,arr, fieldName);
        }
        public  IEnumerable<T> SearchDefault(Func<Document, T> d,string input, string[] arr, string fieldName = "")
        {
            return string.IsNullOrEmpty(input) ? new List<T>() : _search(d,input,arr, fieldName);
        }
        public  IEnumerable<T> GetAllIndexRecords(Func<Document, T> d)
        {
            // validate search index
            if (!System.IO.Directory.EnumerateFiles(_luceneDir).Any()) return new List<T>();

            // set up lucene searcher
            var searcher = new IndexSearcher(_directory, false);
            var reader = IndexReader.Open(_directory, false);
            var docs = new List<Document>();
            var term = reader.TermDocs();
            while (term.Next()) docs.Add(searcher.Doc(term.Doc));
            reader.Dispose();
            searcher.Dispose();
            return _mapLuceneToDataList(docs,d);
        }
        private  IEnumerable<T> _mapLuceneToDataList(IEnumerable<Document> hits,Func<Document,T>  d)
        {
            return hits.Select(i=>_mapLuceneDocumentToData(i,d)).ToList();
        }
        private  IEnumerable<T> _mapLuceneToDataList(IEnumerable<ScoreDoc> hits,
            IndexSearcher searcher,Func<Document,T> d)
        {
            return hits.Select(hit => _mapLuceneDocumentToData(searcher.Doc(hit.Doc),d)).ToList();
        }
    }
}
