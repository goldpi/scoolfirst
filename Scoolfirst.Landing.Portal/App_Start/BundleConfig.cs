﻿using System.Web;
using System.Web.Optimization;

namespace Scoolfirst.Landing.Portal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Static/js/jquery.js",
                      "~/Static/js/jquery.easing.min.js",
                      "~/Static/js/scrolling-nav.js",
                      "~/Static/js/App.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

     

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Static/js/bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/Static/css").Include(
                      "~/Static/css/bootstrap.min.css",
                      "~/Static/css/Style.css"));
        }
    }
}
