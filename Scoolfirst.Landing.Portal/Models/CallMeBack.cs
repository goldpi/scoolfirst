﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoolfirst.Landing.Portal.Models
{
    public class CallMeBack
    {
        public string Mobile { get; set; }

        public string SName { get; set; }
    }
}