﻿//Document Ready Start
$().ready(function () {

    //Select List 
    $('.selectpicker').selectpicker();


    $('#CreditorId').on('changed.bs.select', function (e) {
        console.log($('#CreditorId').selectpicker('val'));
        var selectedvalue = $('#CreditorId').selectpicker('val');
        if (selectedvalue == -1) {
            window.location.assign("/Creditors/Create");
        }
    });

    $('#DebitorId').on('changed.bs.select', function (e) {
        console.log($('#DebitorId').selectpicker('val'));
        var selectedvalue = $('#DebitorId').selectpicker('val');
        if (selectedvalue == -1) {
            window.location.assign("/Debitors/Create");
        }
    });

});// document Ready End