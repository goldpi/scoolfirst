﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;
using Scoolfirst.ViewModel.Subscription;
using Umbraco.Core;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace Scoolfirst.Landing.Portal.Controllers
{
    public class SubscriptionController : UmbracoApiController
    {
        /// <summary>
        /// URL: http://upgradejr.com/Umbraco/Api/Subscription/getall
        /// </summary>
        /// <returns></returns>
        public List<SubscriptionItem> GetAll()
        {
            List<SubscriptionItem> Subscriptions=new  List<SubscriptionItem>();
            // Get the Umbraco Content Service
            var contentService = Services.ContentService;
           
            foreach (var item in contentService.GetChildren(1125))
            {
               
              
                Subscriptions.Add(new SubscriptionItem
                {
                    Id = item.Id,
                    Title = item.GetValue<string>("title"),
                    Decription = item.GetValue<string>("description"),
                    MRP = item.GetValue<string>("mRP"),
                    Price = item.GetValue<string>("price"),
                    OfferValidity = item.GetValue<DateTime?>("OfferValidity"),
                    BGColor = item.GetValue<string>("color"),
                    BtmColor = item.GetValue<string>("subscribeBtnColor"),
                    BillingCycle = item.GetValue<string>("billingCycle"),
                    Image =Umbraco.Media(item.GetValue<string>("image")).Url,
                    MostPopular = item.GetValue<bool>("mostPopular"),
                    SubscriptionFor= item.GetValue<bool>("subscriptionFor"),
                    Days = item.GetValue<int>("Days")
                });
            }

           // JsonSerializerSettings jsonSerializer=new JsonSerializerSettings();

           // System.Web.Http.Results.JsonResult< List < SubscriptionItem >> jsonResult =new JsonResult<List<SubscriptionItem>>(Subscriptions);
         
            return Subscriptions.Where(x=>x.SubscriptionFor==true).ToList();
        }

        // GET: api/Subscription/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Subscription
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Subscription/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Subscription/5
        public void Delete(int id)
        {
        }
    }
}
