﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Landing.Portal.Controllers
{
    using Helper;
    using Models;
    using Model.Provider;
    using System.Web.Http;
    using Umbraco.Web.WebApi;

    public class CallbackController : UmbracoApiController
    {
        /// <summary>
        /// URL: http://upgradejr.com/Umbraco/Api/Callback/me
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public string me([FromBody]CallMeBack callMeBack )
        {
            if (string.IsNullOrWhiteSpace(callMeBack.Mobile))
            {
                return "Please enter a valid mobile no.";
            }
            else
            {
                // umbraco.library.SendMail("callback@upgradejr.com", "info.upgradejr@gmail.com", "New callback request from :- " + callMeBack.Mobile + "", "Please call me back on "+ callMeBack.Mobile, true);
                //SendgridHelper.send(callMeBack.Mobile);

                var SMS = new ShortMessageSender();
              //  var result =  SMS.SendOTPAsync("Md Yusuf Alam","34564","9386699909");
                //SMS.SendMessageAsync("9386699909", "The wuick brown fox jumps right over the lazy dog", "POST");
                //var result = SMS.SendMessageAsync("9386699909", "New callback request from: -" + callMeBack.Mobile);
                var result = SMS.SendMessageAsync("8877211234", "New callback request from: -" + callMeBack.Mobile);

                return "Thanks for your request. We'll revert you shortly.";
            }
            
        }

     
    }
}