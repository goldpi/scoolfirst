[![Build status](https://ci.appveyor.com/api/projects/status/yma9npchir49u8bv?svg=true)](https://ci.appveyor.com/project/imran04/scoolfirst)

# Project description #

1. Scoolfirst.Admin -> to handle all the admin stuffs
2. Scoolfirst.Api -> Api acess for ajax and mobile apps
3. Scoolfirst.Landing.Portal -> external marketing website
4. Scoolfirst.Model -> DataModel,Context,Data operation
5. Scoolfirst.ViewModel -> ViewModel and data-operation
6. Scoolfirst.Notification -> Signalr stuffs
7. Scoolfirst.PictureManager -> user side picture managing app
8. Scoolfirst.Web -> main app
9. responsive file manger. to handle files of admin.


Major changes

1. Feed has two major feed module KN ans CC
2. Workbook and TestWork book uses same UI