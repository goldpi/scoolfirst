﻿using Scoolfirst.Model.KDC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.KDC.API
{
    public class KDCApiVM
    {
        public KDCApiVM()
        {
            Comments = new List<ArticleComment>();
        }

        public int CommentCount { get; set; }
        public int LikeCount { get; set; }
        public int FavCount { get; set; }
        public bool LikedByMe { get; set; }
        public bool FavByMe { get; set; }
        public string Title { get; set; }
        public string Solution { get; set; }
        public string ImageUrl { get; set; }
        public bool Approved { get; set; }
        public bool Deleted { get; set; }
        public DateTime On { get; set; }
        public Guid ID { get; set; }
        public string UserId { get; set; }
        public string UserDisplay { get; set; }
        public string UserImageUrl { get; set; }
        public bool MyArticle { get; set; }
        public List<ArticleComment> Comments { get; set; }
        public string Comment { get; set; }

        public static KDCApiVM fromArticle(Article art,string UserId)
        {
            //string comment = string.Empty;
            //foreach (var c in art.Comments)
            //{
            //    comment += c.Comment + ",";
            //}
            //comment = comment.TrimEnd(',');
            return new KDCApiVM
            {
                Approved = art.Approved,
                CommentCount = art.Comments.Count(),
                Deleted = art.Deleted,
                FavByMe = art.Favs.Any(i => i.UserId == UserId),
                FavCount = art.Favs.Count(),
                UserId = art.UserId,
                ID = art.ID,
                ImageUrl = art.ImageUrl,
                LikeCount = art.Likes.Count(),
                LikedByMe = art.Likes.Any(i => i.UserId == UserId),
                MyArticle = art.UserId == UserId,
                On = art.On,
                Solution = art.Solution,
                Title = art.Title,
                UserDisplay = art.User.FullName,
                UserImageUrl = art.User.PictureUrl,
                //Comments=art.Comments.ToList(),
           
            };
            
        }
    }
}
