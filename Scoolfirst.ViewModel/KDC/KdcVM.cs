﻿using Scoolfirst.Model.KDC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Scoolfirst.Model.Notifications;
using Scoolfirst.ViewModel.KDC.API;

namespace Scoolfirst.ViewModel.KDC
{
    public class KdcVM
    {
        public Article Article { get; set; }
        public bool LikedByMe { get; set; }
        public bool FavByMe { get; set; }
    }
    public class ArticlesItems
    {
        public IEnumerable<KdcVM> KDC { get; set; }
        public void GetSearchedItems(Scoolfirst.Model.Context.ScoolfirstContext Data, string User, string Search, int Page = 0, int size = 3)
        {
            var UserId = Data.Users.FirstOrDefault(i => i.UserName == User).Id;

            DateTime Date = DateTime.UtcNow;
            var ff = Data.Articles
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Favs)
                .Where(i => i.On<= Date&&i.Approved==true)
                .OrderByDescending(i => i.On)
                .Skip(size * Page)
                .Take(size).ToList();
            var t = from l in ff
                    select new KdcVM { Article = l, FavByMe = l.Favs.Any(i => i.UserId == UserId), LikedByMe = l.Likes.Any(i => i.UserId == UserId) };
            KDC = t.ToList();//Data.Feeds.Include(i => i.Comments.Take(3)).Include(i => i.Likes).Where(i => i.OnDateTime <= Date).OrderByDescending(i => i.OnDateTime).Skip(Page * 3).Take(3);
        }
        public void GetItems(Scoolfirst.Model.Context.ScoolfirstContext Data, string User, int Page = 0, int size = 3)
        {
            var UserId = Data.Users.FirstOrDefault(i => i.UserName == User).Id;

            DateTime Date = DateTime.UtcNow;
            var ff = Data.Articles
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Favs)
                .Where(i => i.On <= Date && i.Approved == true)
                .OrderByDescending(i => i.On)
                .Skip(size * Page)
                .Take(size).ToList();
            var t = from l in ff
                    select new KdcVM { Article = l, FavByMe = l.Favs.Any(i => i.UserId == UserId), LikedByMe = l.Likes.Any(i => i.UserId == UserId) };
            KDC = t.ToList();//Data.Feeds.Include(i => i.Comments.Take(3)).Include(i => i.Likes).Where(i => i.OnDateTime <= Date).OrderByDescending(i => i.OnDateTime).Skip(Page * 3).Take(3);
        }
        public void GetMyItems(Scoolfirst.Model.Context.ScoolfirstContext Data, string User, int Page = 0, int size = 3)
        {
            var UserId = Data.Users.FirstOrDefault(i => i.UserName == User);



            var Groups = Data.Classes.Find(UserId.ClassId).Groups.Select(i => i.Id);

            DateTime Date = DateTime.UtcNow;
            var ff = Data.Articles
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Favs)
                .Where(i => i.UserId==UserId.Id)
                .OrderByDescending(i => i.On)
                .Skip(size * Page)
                .Take(size).ToList();
            var t = from l in ff

                    select new KdcVM { Article = l, FavByMe = l.Favs.Any(i => i.UserId == UserId.Id), LikedByMe = l.Likes.Any(i => i.UserId == UserId.Id) };
            KDC = t.ToList();
        }
        public IEnumerable<KDCApiVM> GetSearchedAPIItems(Scoolfirst.Model.Context.ScoolfirstContext Data, string User, string Search, int Page = 0, int size = 3)
        {
            var UserId = Data.Users.FirstOrDefault(i => i.UserName == User).Id;

            DateTime Date = DateTime.UtcNow;
            var ff = Data.Articles
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Favs)
                .Where(i =>  i.Approved == true)
                .OrderByDescending(i => i.On)
                .Skip(size * Page)
                .Take(size).ToList();
            foreach(var i in ff)
            {
                yield return KDCApiVM.fromArticle(i, UserId);
               
            }
        }
        public IEnumerable<KDCApiVM> GetApiItems(Scoolfirst.Model.Context.ScoolfirstContext Data, string User, int Page = 0, int size = 3)
        {
            var UserId = Data.Users.FirstOrDefault(i => i.UserName == User).Id;

            DateTime Date = DateTime.UtcNow;
            var ff = Data.Articles
                .Include(i => i.Comments)
                //.Include(i => i.Likes)
                //.Include(i => i.Favs)
                .Where(i => i.Approved == true)
                .OrderByDescending(i => i.On)
                .Skip(size * Page)
                .Take(size).ToList();
            foreach (var item in ff)
            {
                yield return KDCApiVM.fromArticle(item, UserId);
            }
           }
        public IEnumerable<KDCApiVM> GetAPIMyItems(Scoolfirst.Model.Context.ScoolfirstContext Data, string User, int Page = 0, int size = 3)
        {
            var UserId = Data.Users.Include(i=>i.Class).Include(i=>i.School).FirstOrDefault(i => i.UserName == User);
                


            var Groups = Data.Classes.Find(UserId.ClassId).Groups.Select(i => i.Id);

            DateTime Date = DateTime.UtcNow;
            var ff = Data.Articles
                //.Include(i => i.Comments)
                //.Include(i => i.Classes == UserId.Class)
                .Where(i => i.UserId == UserId.Id)
                .OrderByDescending(i => i.On)
                .Skip(size * Page)
                .Take(size).ToList();
           
            foreach (var item in ff)
            {
                item.ClassesId = UserId.ClassId;
                
                yield return KDCApiVM.fromArticle(item, UserId.Id);
            }
        }
        public ArticleComment AddComment(Scoolfirst.Model.Context.ScoolfirstContext Context, string Comment, Guid PostId, string User)
        {
            var UserId = Context.Users.FirstOrDefault(i => i.UserName == User).Id;
            ArticleComment NewComment = new ArticleComment
            {
                Comment = Comment,
                ArticleId = PostId,
                UserId = UserId,
                On = DateTime.UtcNow,
                Id = Guid.NewGuid()
            };
            Context.ArticleComments.Add(NewComment);
            Context.SaveChanges();
            NotificationHandler handler = new NotificationHandler();
            var Article = Context.Articles.Include(i=>i.User).FirstOrDefault(i=>i.ID==PostId);

            //handler.RaiseNotification(Context, new Notification
            //{
            //    Image = "Comment.png",
            //    Link = "/myKdc/details/" + PostId,
            //    Source = "Comment",
            //    TargetUserId = Article.UserId,
            //    SourceUserId = UserId,
            //    Text = "There is a new Comment on your KDC."
            //});
            //handler.RaiseNotification(Context, new Notification
            //{
            //    Image = "Comment.png",
            //    Link = "/myKdc/details/" + PostId,
            //    Source = "Comment",
            //    TargetUserId = "Admin",
            //    SourceUserId = UserId,
            //    Text = "There is a new Comment KDC of" + Article.User.FullName
            //});
            return NewComment;
        }

        public int AddRemoveFeedLike(Scoolfirst.Model.Context.ScoolfirstContext Context, Guid PostId, string User)
        {
            var UserId = Context.Users.FirstOrDefault(i => i.UserName == User).Id;
            bool retu = Context.LikeArticles.Any(i => i.UserId == UserId && i.ArticleId == PostId);
            if (retu == false)
            {
               LikeArticle NewFeedLike = new LikeArticle
               {
                    Id = Guid.NewGuid(),
                    On = DateTime.UtcNow,
                    UserId = UserId,
                    ArticleId = PostId

                };
                Context.LikeArticles.Add(NewFeedLike);
                Context.SaveChanges();
            }
            else
            {
                var find = Context.LikeArticles.Where(i => i.UserId == UserId && i.ArticleId == PostId);
                Context.LikeArticles.RemoveRange(find);
                Context.SaveChanges();
            }

            return Context.LikeArticles.Count(i => i.ArticleId == PostId);
        }

        public bool AddRemoveFeedStar(Scoolfirst.Model.Context.ScoolfirstContext Context, Guid PostId, string User)
        {
            var UserId = Context.Users.FirstOrDefault(i => i.UserName == User).Id;
            bool retu = Context.FavArticles.Any(i => i.UserId == UserId && i.ArticleId == PostId);
            if (retu == false)
            {
                FavArticle NewFeedStar = new FavArticle
                {
                    Id = Guid.NewGuid(),
                    On = DateTime.UtcNow,
                    UserId = UserId,
                    ArticleId = PostId

                };
                Context.FavArticles.Add(NewFeedStar);
                Context.SaveChanges();
            }
            else
            {
                var find = Context.FavArticles.Where(i => i.UserId == UserId && i.ArticleId == PostId);
                Context.FavArticles.RemoveRange(find);
                Context.SaveChanges();
            }

            return !retu;
        }

        
    }
}
