﻿using Scoolfirst.Model.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Scoolfirst.ViewModel
{
    public class GraphSerachViewModel
    {
        public GraphSerachViewModel()
        {
            Feed = new List<FeedVm>();
            KDC = new List<KDCVm>();
            Project = new List<ProjectVm>();
        }

        public GraphSerachViewModel(Scoolfirst.Model.Context.ScoolfirstContext context,string s,string UserName,int fm,int kd,int pd)
        {
            var User = context.Users.FirstOrDefault(i => i.UserName == UserName);
            DateTime Date = DateTimeHelper.GetIST();
            Feed = new List<FeedVm>();
            KDC = new List<KDCVm>();
            Project = new List<ProjectVm>();
            var kK = context.Articles.Where(i => i.Title.Contains(s)&&i.Approved==true).OrderBy(i => i.Likes.Count()).Take(kd).ToList();
            foreach (var item in kK)
            {
                KDC.Add(new KDCVm { Id = item.ID, Title = item.Title });
            }
            var pp = context.Projects.Where(i => i.Name.Contains(s) && i.Active == true).OrderBy(i => i.AddedOn).Take(pd).ToList();
            foreach (var item in pp)
            {
                Project.Add(new ProjectVm { Id = item.Id, Title = item.Name });
            }
            if (User != null)
            {
                var ff = context.Feeds
                  .Where(i => i.OnDateTime <= Date  && i.Title.Contains(s))
                  .OrderByDescending(i => i.OnDateTime)
                  .Take(fm).ToList();
                TotalFeed = context.Feeds
                  .Count(i => i.OnDateTime <= Date  && i.Title.Contains(s));
                foreach (var i in ff)
                {
                    Feed.Add(new FeedVm
                    {
                        Id = i.Id,
                        Title = i.Title
                    });

                }
            }
            else
            {
               var Groups = context.Classes.Find(User.ClassId).Groups.Select(i => i.Id);
               var ff = context.Feeds
                    .Where(i => i.OnDateTime <= Date && Groups.Any(o => o == i.GroupId) && i.Title.Contains(s))
                    .OrderByDescending(i => i.OnDateTime)
                    .Take(fm).ToList();
                TotalFeed = context.Feeds
                    .Count(i => i.OnDateTime <= Date && Groups.Any(o => o == i.GroupId) && i.Title.Contains(s));
                foreach (var i in ff)
                {
                    Feed.Add(new FeedVm
                    {
                        Id = i.Id,
                        Title = i.Title
                    });
                }

            }
       
            
            TotalKDC =  context.Articles.Count(i => i.Title.Contains(s) && i.Approved == true);
            TotalProjects = context.Projects.Count(i => i.Name.Contains(s) && i.Active == true);
        }
        public List<FeedVm> Feed { get; set; }
        public List<KDCVm> KDC { get; set; }
        public List<ProjectVm> Project { get; set; }
        public int TotalFeed { get; set; }
        public int TotalKDC { get; set; }
        public int TotalProjects { get; set; }
    }

    public class FeedVm
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }

    public class KDCVm
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }

    public class ProjectVm
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
