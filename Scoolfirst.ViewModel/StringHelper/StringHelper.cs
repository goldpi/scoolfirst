﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.StringHelper
{
    public class StringHelper
    {
        public static IEnumerable<string> sTags(IEnumerable<string> matches)
        {
            string regExp = @"[^\w\d]";
            foreach (var item in matches)
            {
                if (item != null)
                {
                    var spilts = item.Replace('\'', ' ').Split(new[] { ',', ' ', '-' });
                    if (spilts.Count() == 0)
                    {
                        var ret = Regex.Replace(item, regExp, "");

                        if (string.IsNullOrEmpty(ret))
                        { continue; }
                        else
                             if (ListofSpam.Contains(ret))
                            continue;
                        yield return ret;
                    }

                    else
                    {
                        foreach (var i in spilts)
                        {
                            var ret = Regex.Replace(i, regExp, "");

                            if (string.IsNullOrEmpty(ret))
                                continue;
                            else
                                if (ListofSpam.Contains(ret))
                                continue;
                            yield return ret;
                        }

                    }

                }
            }
        }

        static string[] ListofSpam = new[] { "is", "a", "an", "the", "or", "and", "on", "like", "that", "no", "who", "why", "where", "when", "what", "not", "now", "non" };
    }
}
