﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Payments;
using Scoolfirst.Model.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Scoolfirst.ViewModel.Carting
{
    public  class CartViewModelHandler
    {
        private HttpContextBase HttpContext;
        private ScoolfirstContext dc;
        public CartViewModelHandler(ScoolfirstContext context,HttpContextBase httpContext)
        {
            dc = context;
            HttpContext = httpContext;
            if (!isCartOnSession)
            {
                if (!dc.Carts.Any(i => i.User.UserName == UserName&&i.CheckOut==false))
                {
                    dc.Carts.Add(CartFromSession);
                    dc.SaveChanges();
                }
                else
                {
                    ToSession(dc.Carts.FirstOrDefault(i => i.User.UserName == UserName && i.CheckOut == false));
                }
            }

        }
       // public Cart Cart { get; set; } => CartFromSession;
        public string UserName => HttpContext.User.Identity.Name;
        public Scoolfirst.Model.Identity.User User => dc.Users.First(i => i.UserName == UserName);
        public string UserId => User.Id;
        
        public bool isCartOnSession => HttpContext.Session["Cart"] != null;

        public Cart CartFromSession => isCartOnSession ? (Cart)HttpContext.Session["Cart"] : new Cart() {Id=Guid.NewGuid(),UserId=UserId,CheckOut=false };

        public IQueryable<CartItems> CartItems => CartFromSession.Items.AsQueryable();

        public void ToSession(Cart cart) => HttpContext.Session["Cart"] = cart;

        public bool IsItemOnCart(CartItems items) => CartItems.Any(i => i.ProductId == items.ProductId);
        public bool IsItemOnCartByProductId(int items) => CartItems.Any(i => i.ProductId == items);
        public int NoOfItems(CartItems Items) => IsItemOnCart(Items) ? CartItems.FirstOrDefault(i => i.ProductId == Items.ProductId).Quantity : 0;
        public int NoOfItemsByProductId(int Id)=> IsItemOnCartByProductId(Id) ? CartItems.FirstOrDefault(i => i.ProductId == Id).Quantity : 0;

        public CartItems GetItem(int Id) => NoOfItemsByProductId(0) > 0 ? CartItems.FirstOrDefault(i => i.ProductId == Id) : new CartItems { ProductId = Id, Quantity = 0, Id=Guid.NewGuid() };
        public void UpdateCart(CartItems Item)
        {
            
            if (NoOfItemsByProductId(Item.ProductId) > 0)
            {
              var item=  dc.CartItems.Find(Item.Id);

            }
        }
       
        public void AddToCart(int Id,int Count)
        {
            var CartItem = GetItem(Id);
            CartItem.Quantity = Count;
            var cart = CartFromSession;
           
            

        }
        //Todo: Add cart related stuffs over here.



    }
}
