﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Common
{
    [Obsolete("This class obsolute use ")]

    public class BrandingVM
    {
        [Obsolete("Mobile Logo pre loaded in apk. No need to Upload Here")]
        public string MobileLogo { get; set; }
        public string Weblogo { get; set; }
        public string Adminlogo { get; set; }
        public string Invoicelogo { get; set; }

        public string CompanyName { get; set; }
        public string WebSiteUrl { get; set; }
        public string SaleEmail { get; set; }
        public string SupportEmail { get; set; }
        public string BillingEmail { get; set; }
        public string SupportMobile { get; set; }
        public string SaleMobile { get; set; }
        public string BillingMobile { get; set; }
        public string BillingAddress { get; set; }
        public string GeneralAddress { get; set; }
    }

    
}
