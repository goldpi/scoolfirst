﻿using Newtonsoft.Json;
using Scoolfirst.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Common.Api
{

    public class Resource
    {
        public List<Schools> SchoolList { get; set; }
        public List<Class> Clasess { get; set; }
    }

    public class Schools
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public bool IsMaster { get; set; }
     

        public  static IEnumerable<Api.Schools> FromListSchool(IEnumerable<School> school)
        {
            foreach (var item in school.ToList())
            {
                yield return new Api.Schools { Id = item.Id, Name = item.SchoolName, IsMaster=item.IsMasterContent,City= item.Area.City.Name};
            }
        }

      
    }
    
    public class Class
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static IEnumerable<Class> FromListClasses(IEnumerable<Classes> Classes)
        {
            foreach (var item in Classes.ToList())
            {
                yield return new Class { Id = item.Id, Name = item.RomanDisplay };
            }
        }
    }
}
