﻿using Scoolfirst.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Common
{
    public class RoleModelVM
    {
        public int Id { get; set; }
              
        public string Title { get; set; }
        public string Content { get; set; }
        
        public string ImageUrl { get; set; }
        
        public string VideoUrl { get; set; }
        public DateTime On { get; set; }


        public static RoleModelVM FromRoleModel(RoleModel rm)
        {
            return new RoleModelVM
            {
                Content = rm.Content,
                Id = rm.Id,
                ImageUrl = rm.ImageUrl,
                On = rm.On,
                Title = rm.Title,
                VideoUrl = rm.VideoUrl
            };
        }
    }

    public static class RMVMHelper
    {
        public static RoleModelVM ToRoleModeViewModel(this RoleModel Rm)
        {
            return RoleModelVM.FromRoleModel(Rm);
        }

        public static IEnumerable<RoleModelVM> ToRoleModeViewModel(this IEnumerable<RoleModel> Rm)
        {
            foreach(var i in Rm)
            yield return RoleModelVM.FromRoleModel(i);
        }
    }
}
