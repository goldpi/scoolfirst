﻿using Scoolfirst.Model.PlayReasoning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Play
{
   public class PlayQuestion
    {
        public PlayQuestion()
        {
            Option = new List<PlayOptions>();
        }
        public Guid Id { get; set; }
        public string Query { get; set; }
        public long? SetId { get; set; }
        public int ShortOrder { get; set; }
        public string Solution { get; set; }
        public bool ShowSolution { get; set; }
        public virtual PlayRoot Set { get; set; }
        public List<PlayOptions> Option { get; set; }
        

        public static PlayQuestion VMQuestion(Scoolfirst.Model.PlayReasoning.PlayQuestion q)
        {
            var question = new PlayQuestion
            {
                Option = PlayOptions.ToVm(q.Option.ToList()).ToList(),
                Id = q.Id,
                SetId = q.SetId,
                Query = q.Query,
                Solution = q.Solution,
                ShowSolution = q.ShowSolution,
                ShortOrder = q.ShortOrder,


            };
            return question;
        }
    }
}
