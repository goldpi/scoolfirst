﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Play
{
   public class PlayOptions
    {
        public Guid Id { get; set; }

        public string Answer { get; set; }
        public bool Correct { get; set; }

        public static IEnumerable<PlayOptions> ToVm(IList<Scoolfirst.Model.PlayReasoning.PlayOptions> c)
        {
            foreach (var item in c)
            {
                yield return PlayOptions.ToVmSingle(item);
            }

        }

        public static PlayOptions ToVmSingle(Scoolfirst.Model.PlayReasoning.PlayOptions c)
        {
            return new PlayOptions
            {
                Answer = c.Answer,
                Id = c.Id,
                Correct = c.Correct
            };
        }
    }
}
