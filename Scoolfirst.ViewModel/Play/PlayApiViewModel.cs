﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Play
{
   public class PlayApiViewModel
    {
        public List<Scoolfirst.Model.PlayReasoning.PlayRoot> Data { get; set; }
        public string Level { get; set; }
        public int Current { get; set; }
       
    }
}
