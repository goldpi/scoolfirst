﻿using Scoolfirst.Model.PlayReasoning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Play
{
   public class PlayAnswerBack
    {
        public Guid QuesionId { get; set; }
        public Guid AnswerId { get; set; }
        public int ReasoningId { get; set; }
    }
    public class AnswerGiven
    {
        public List<PlayAnswerBack> Answer { get; set; }
        public int Correct { get; set; }
        public int Wrong { get; set; }
        public int Skiped { get; set; }
    }



    public class PlayReport
    {
        public PlayAttempt Attemp { get; set; }
        public ICollection<Model.PeriodicAssesment.PAQuestion> Questions { get; set; }
    }
}
