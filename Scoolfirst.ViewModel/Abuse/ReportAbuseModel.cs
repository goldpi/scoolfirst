﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Abuse
{
    public class ReportAbuseModel
    {
        public string Reason { get; set; }
        public string PostId { get; set; }
        public bool Other { get; set; }
        public string OtherReason { get; set; }
        public TypeSource Source { get; set; }
    }
}
