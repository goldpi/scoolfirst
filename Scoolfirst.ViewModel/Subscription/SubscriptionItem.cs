﻿namespace Scoolfirst.ViewModel.Subscription
{
    using System;

    public class SubscriptionItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Decription { get; set; }
        public string Image { get; set; }
        public string MRP { get; set; }
        public string Price { get; set; }
        public DateTime? OfferValidity { get; set; }
        public string BillingCycle { get; set; }
        public string BGColor { get; set; }
        public string BtmColor { get; set; }
        public bool MostPopular { get; set; }
        public bool SubscriptionFor { get; set; }
        public int Days { get; set; }
    }

}
