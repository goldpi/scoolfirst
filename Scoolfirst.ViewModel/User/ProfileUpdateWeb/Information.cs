﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.User.ProfileUpdateWeb
{
    public class AboutInformation
    {
        public string Id { get; set; }

        public string About { get; set; }
    }

    public class BasicInformation
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }

    }
    public class AcadamemicInformation
    {
        public string Id { get; set; }
        
        public int SchoolId { get; set; }

        public string School { get; set; }
        public string Class { get; set; }
        public int ClassId { get; set; }
        public string Board { get; set; }
        public bool Sponsered { get; set; }
    }
    public class ContactInformation
    {
        public string Id { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNo { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string Address { get; set; }
    }
}
