﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.User.ProfileUpdateWeb
{
   public class ProfileVM
    {
        // public Guid Id { get; set; }
        [JsonProperty]
        public string UserId { get; set; }
        [JsonProperty]
        public string Name { get; set; }

        public string Gender { get; set; }
        public string Dob { get; set; }
        public int SchoolId { get; set; }

        public string SchoolName { get; set; }
        public string ClassName { get; set; }
        public int ClassId { get; set; }
        public string Board { get; set; }
        public bool Sponsered { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string Address { get; set; }
        public string AboutMe { get; set; }
       
    }
}
