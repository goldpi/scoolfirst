﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.User
{
   public class AdminUserViewModel
    {
        
        public string Email { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public Scoolfirst.Model.Identity.User AdminUserInstance(AdminUserViewModel model, string StudentAvtar, string ParentAvtar)
        {
            return new Scoolfirst.Model.Identity.User
            {
                UserName = model.Email ?? null,
                
            };
        }

        public Model.Identity.User NewUserInstance(AdminUserViewModel model, string studentAvtar, string parentAvtar)
        {
            throw new NotImplementedException();
        }
    }
}
