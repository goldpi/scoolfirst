﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.User
{
    public class UserViewModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public bool Collapsed { get; set; }
        public string DpUrl { get; set; }
        public string Color { get; set; }
    }
}
