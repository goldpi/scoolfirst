﻿using Scoolfirst.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.User
{
    public class NewUserViewModel
    {
        public string FullName { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public ICollection<Classes> Class { get; set; }
        public ICollection<Board> Board { get; set; }
        public UserType WhoIAm { get; set; }
    }
}
