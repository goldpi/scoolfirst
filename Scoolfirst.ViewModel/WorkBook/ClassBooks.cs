﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.WorkBook
{
    public class ClassBooks
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public int scid { get; set; }
        public int clid { get; set; }
        public int sub { get; set; }
    }
}
