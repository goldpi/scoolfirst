﻿using System.Data.Entity;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.WorkBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.WorkBook.TossTopic
{
    public class ProgressiveLearing
    {
        /// <summary>
        /// Search Progresive Learning
        /// </summary>
        /// <param name="Classes"></param>
        /// <param name="Chapters"></param>
        /// <param name="class"></param>
        /// <returns>Questions of Progresive Learning According to the tag.</returns>
        public static IEnumerable<Questions> ProgressiveQuestion(IQueryable<Classes> Classes, IQueryable<Chapter> Chapters,int @class)
        {
            var pf = from l in Chapters.Include(i => i.Questions)
                     join classes in @Classes
                     on l.ClassesId equals classes.Id
                     where classes.NumericDisplay > @class
                     select l.Questions;
            foreach (var i in pf)
                foreach (var p in i)
                        yield return p;
                
        }
        /// <summary>
        /// Progressive learning Tags
        /// </summary>
        /// <param name="St"></param>
        /// <param name="Classes"></param>
        /// <param name="Chapters"></param>
        /// <param name="class"></param>
        /// <returns>Array of string For search Progresive Learning</returns>
        public static string[] ProgressiveLearningTags(string St, IQueryable<Classes> Classes, IQueryable<Chapter> Chapters, int @class)
        {
            return StringHelper
                .StringHelper
                .sTags(ProgressiveQuestion(Classes,Chapters,@class)
                .Select(i => i.Refrence)
                .Distinct())
                .ToArray();   
        }
    }

    public class HybridLearning
    {
        public static IEnumerable<Questions> HybridLearningQuestions(int classId, IQueryable<Chapter> Chapters)
        {
            var p = from l in Chapters
                    where l.ClassesId == classId
                    select l.Questions;
            foreach (var item in p)
            {
                if(item!=null&&item.Count()>0)
                    foreach (var questions in item)
                    {
                        if(questions.Refrence!=null)
                        yield return questions;
                    }
            }

        }
        public static string[] HybridLearningTags(string St, int classId,IQueryable<Chapter> Chapters)
        {
            var pr = HybridLearning.HybridLearningQuestions(classId, Chapters).Where(i => i.Refrence.Contains(St));
            return StringHelper.StringHelper.sTags(pr.Select(i => i.Title).Distinct()).ToArray();
           
        }
    }

    public class RevisionNote
    {
        public static IEnumerable<Note> RevisionsNotes(IQueryable<Chapter> Chapter,int @class)
        {

           foreach(var p in (from p in Chapter 
                                where p.ClassesId == @class
                                select p.Notes))
                foreach(var i in p)
                {
                    if(i!=null)
                    yield return i;
                }
        }

        public static string[] RevisonNotesTags(string St,IQueryable<Chapter> Chapter, int @class)
        {
            return StringHelper
                .StringHelper
                .sTags(RevisionsNotes(Chapter,@class)
                .Where(i=>i.Topic.Contains(St))
                            .Select(i => i.Topic)
                            .ToArray())
                            .Distinct()
                            .ToArray();
        }
    }
}
