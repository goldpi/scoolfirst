﻿using Scoolfirst.Model.WorkBook;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.WorkBook.Api
{
    public class QuizVM
    {
        public Guid Id { get; set; }
        [EnumDataType(typeof(QuizType))]
        public string Name { get; set; }
        public Guid SyllabusId { get; set; }
        public string Subject { get; set; }
        public string Details { get; set; }
        public int NoOfQuestion { get; set; }
       
        public string Class { get; set; }
        public string School { get; set; }
        public DateTime On { get; set; }
     
        public  List<QuestionVM> Questions { get; set; }
      
       

        public static IEnumerable<QuestionVM> ToQM(IEnumerable<Questions> q)
        {
            foreach (var item in q)
            {
                yield return QuestionVM.FromQuestionToVm(item);
            }
        }


        public static QuizVM BookQuestion(Scoolfirst.Model.Context.ScoolfirstContext dc,int BookId)
        {

            var Book = (from i in dc.Books
                       where i.Id == BookId
                       select new { NoofQuestion = i.Questions.Count(), schoolName = "", Questions = i.Questions, Class = i.Classes.RomanDisplay }).First();

            if (Book == null)
                return null;
           
            return new QuizVM
            {
               
                NoOfQuestion = Book.NoofQuestion,
                School = "",
                Questions =ToQM(Book.Questions).ToList() ,
                Class = Book.Class

            };

        }


        public static QuizVM Quiz(Scoolfirst.Model.Context.ScoolfirstContext dc, Guid Id)
        {
            
           
            var Quiz =(from l in dc.Quizes
                       where l.Id == Id
                        select new { Id = l.Id ,Questions= l.Questions, NoOfQuestion =l.NoOfQuestion,Class=l.Class}).FirstOrDefault()
                            ;
            if (Quiz == null)
                return null;
            
            return new QuizVM
            {
                Id = Quiz.Id,
               
                NoOfQuestion = Quiz.NoOfQuestion,
                Questions = ToQM(Quiz.Questions).ToList(),
                School = "",
                Class = Quiz.Class,
               
            };
        }
    }
}
