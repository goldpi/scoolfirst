﻿using Scoolfirst.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Scoolfirst.Model.WorkBook;

namespace Scoolfirst.ViewModel.WorkBook.Api
{
    public class WB
    {
        public int SchoolId { get; set; }
        public int ClassId { get; set; }
        public string SchoolName { get; set; }
        public string ClassName { get; set; }
        public List<Book> Books { get; set; }
        public WB()
        {
            Books = new List<Book>();
        }


        public static WB GetBooks(ScoolfirstContext dc,int SchoolId,int ClassId)
        {
            var Ret = new WB();
            Ret.ClassId = ClassId;
            Ret.SchoolId = SchoolId;

            Ret.SchoolName = dc.Schools.Find(SchoolId).SchoolName;
            Ret.ClassName = dc.Classes.Find(ClassId).NumericDisplay.ToString();

            Ret.Books = Ret.GetBookss(dc, SchoolId, ClassId).ToList();
            return Ret;
        }

        public static IEnumerable<QuestionVM> GetBooksBySubject(ScoolfirstContext dc, int SchoolId, int ClassId, string SubjectId)
        {
            var Ret = new WB
            {
                ClassId = ClassId,
                SchoolId = SchoolId,

                SchoolName = dc.Schools.Find(SchoolId).SchoolName,
                ClassName = dc.Classes.Find(ClassId).NumericDisplay.ToString()
            };

            var books = Ret.Question(dc, SchoolId, ClassId, SubjectId).ToList();
            foreach (var i in books)
                foreach (var ip in i)
                    yield return QuestionVM.FromQuestionToVm(ip);



        }


        public static IEnumerable<QuestionVM2> GetBooksBySubjectSQL(ScoolfirstContext dc, int SchoolId, int ClassId, string SubjectId)
        {
            try
            {
                var sd = dc.Subjects.FirstOrDefault(i => i.SubjectName == SubjectId);
                return dc.Database.SqlQuery<QuestionVM2>(
@"SELECT [t4].[Id], [t4].[Title], [t5].[Name] AS [Chapter], [t4].[Answer], [t4].[Refrence], [t6].[Title] AS [Book], [t5].[Id] AS [ChapterId]
FROM [SchoolClassSubjects] AS [t0]
INNER JOIN [SchoolClassSubjectBooks] AS [t1] ON [t0].[Id] = [t1].[SchoolClassSubject_Id]
INNER JOIN [ChapterBooks] AS [t2] ON ((
    SELECT [t3].[Id]
    FROM [Books] AS [t3]
    WHERE [t3].[Id] = [t1].[Books_Id]
    )) = [t2].[Books_Id]
INNER JOIN [Questions] AS [t4] ON [t2].[Chapter_Id] = [t4].[ChapterId]
INNER JOIN [Chapters] AS [t5] ON [t5].[Id] = [t2].[Chapter_Id]
INNER JOIN [Books] AS [t6] ON [t6].[Id] = [t2].[Books_Id]
WHERE ([t0].[ClassId] = @p0) AND ([t0].[SchoolId] = @p1) AND ([t0].[SubjectId] = @p2)", ClassId, SchoolId, sd.Id);


            }
            catch
            {
                throw new ArgumentException("Subject Not Found");
            }
            

        }

        public IEnumerable<Book> GetBookss(ScoolfirstContext dc, int SchoolId, int ClassId)
        {
            var result = dc.SchoolClassSubject.Include(i=>i.Subject).Include(i=>i.Books).Where(i => i.ClassId == ClassId && i.SchoolId == SchoolId).ToList();
            foreach (var item in result)
            {
                foreach (var item2 in item.Books)
                {
                    Book va = new Book();
                    va.BookId = item2.Id;
                    va.Name =  item2.Title;
                    va.Image = item2.ImageUrl;
                    va.SubjectName = item.Subject.SubjectName;
                    dc.Entry(item2).Collection(i => i.Chapters).Load();
                    foreach (var item3 in item2.Chapters)
                    {
                        va.Units.Add(new Uints { Id = item3.Id, UnitName = item3.Name });
                    }

                    yield return va;
                }
            }
        }


        public IEnumerable<IEnumerable<Questions>> Question(ScoolfirstContext dc, int SchoolId, int ClassId, string SubjectId)
        {
            var result = dc.SchoolClassSubject.Include(i=>i.Subject).Include(i => i.Books).Where(i => i.ClassId == ClassId && i.SchoolId == SchoolId&& i.Subject.SubjectName ==SubjectId).ToList();
            foreach (var item in result)
            {
                foreach (var item2 in item.Books)
                {
                    Book va = new Book();
                    va.BookId = item2.Id;
                    va.Name = item2.Title;
                    va.Image = item2.ImageUrl;
                    va.SubjectName = item.Subject.SubjectName;
                    dc.Entry(item2).Collection(i => i.Chapters).Load();
                    foreach (var item3 in item2.Chapters)
                    {
                       yield return dc.Questions.Where(i => i.ChapterId == item3.Id).AsEnumerable();
                    }

                  //  yield return va;
                }
            }
        }
        }

    public class Book
    {
        public string Name { get; set; }
        public int BookId { get; set; }
        public string Image { get; set; }
        public string SubjectName { get; set; }
        public List<Uints> Units { get; set; }

        public Book()
        {
            Units = new List<Uints>();
        }
    }
    public class Uints
    {
         public int Id { get; set; }
        public string UnitName { get; set; }
    }

    
}
