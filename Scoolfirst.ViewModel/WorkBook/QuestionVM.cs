﻿using Scoolfirst.Model.WorkBook;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Scoolfirst.ViewModel.WorkBook
{
   public  class QuestionVM
    {

        public Guid Id { get; set; }
        public string QuestionType { get; set; }
        [AllowHtml]
        public string Title { get; set; }
        [AllowHtml]
        public string Answer { get; set; }
        // [AllowHtml]
        //This field Requires Plain Text.
        public string Refrence { get; set; }
        public string[] School { get; set; }
        public string Class { get; set; }
        public string Subject { get; set; }
        public int Order { get; set; }
        public int TypeOrder { get; set; }
        public int ChapterId { get; set; }
        
        public Questions FromViewModel()
        {
            return new Questions
            {
                Id = this.Id,
                QuestionType=this.QuestionType,
                Answer = this.Answer,
                Class = this.Class,
                Refrence = this.Refrence,
                //School = this.school(),
                Subject = this.Subject,
                Title = this.Title,
                ChapterId=this.ChapterId,
                Order=this.Order,
                TypeOrder=this.TypeOrder
            };
        }
        
       public static QuestionVM FromQuestionToVm(Questions q)
        {
            var sch = q.School!=null?q.School.Split(',').ToArray():new string[0];
            if (sch.Count() <= 0)
                sch = new string[] { q.School??"" };
            return new QuestionVM
            {
                Title = q.Title,
                QuestionType=q.QuestionType,
                Answer = q.Answer,
                Class = q.Class,
                Id = q.Id,
                Refrence = q.Refrence,
                School = sch,
                Subject = q.Subject,
                ChapterId=q.ChapterId,
                Order = q.Order,
                TypeOrder=q.TypeOrder
            };
        }
        public static QuestionVM FromQuestionToVm(Questions q,string Chapter,string Book)
        {
            var sch = q.School != null ? q.School.Split(',').ToArray() : new string[0];
            if (sch.Count() <= 0)
                sch = new string[] { q.School ?? "" };
            return new QuestionVM
            {
                Title = q.Title,
                QuestionType = q.QuestionType,
                Answer = q.Answer,
                Class = q.Class,
                Id = q.Id,
                Refrence = q.Refrence,
                School = sch,
                Subject = q.Subject,
                ChapterId = q.ChapterId,
                Order = q.Order,
                TypeOrder=q.TypeOrder

            };
        }
    }


    public class QuestionVM2
    {

        public Guid Id { get; set; }
       
        public string Title { get; set; }
       
        public string Answer { get; set; }
        // [AllowHtml]
        //This field Requires Plain Text.
        public string Refrence { get; set; }
        public string Chapter { get; set; }
        public string Book { get; set; }
       
        public static QuestionVM2 FromQuestionToVm(Questions q, string Chapter, string Book)
        {
            var sch = q.School != null ? q.School.Split(',').ToArray() : new string[0];
            if (sch.Count() <= 0)
                sch = new string[] { q.School ?? "" };
            return new QuestionVM2
            {
                Title = q.Title,
                Answer = q.Answer,
                Id = q.Id,
                Refrence = q.Refrence,
                
                Chapter = Chapter,
                Book=Book,
            };
        }
    }
}
