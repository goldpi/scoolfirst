﻿using Scoolfirst.Model.VirtualCompetetion;
using System;
using System.Collections.Generic;

namespace Scoolfirst.ViewModel.MCQ
{

    public class Options
    {
        public Guid Id { get; set; }
       
        public string Answer { get; set; }
        public bool Correct { get; set; }

        public static IEnumerable<Options> ToVm(IList<Scoolfirst.Model.MCQ.Options> c)
        {
            foreach (var item in c)
            {
                yield return Options.ToVmSingle(item);
            }

        }

        public static Options ToVmSingle(Scoolfirst.Model.MCQ.Options c)
        {
            return new Options
            {
                Answer = c.Answer,
                Id = c.Id,
                Correct = c.Correct
            };
        }
    }

    public class CampOptions
    {
        public Guid Id { get; set; }

        public string Answer { get; set; }
        public bool Correct { get; set; }

        public static IEnumerable<CampOptions> ToVm(IList<Scoolfirst.Model.Camp.CampOptions> c)
        {
            foreach (var item in c)
            {
                yield return CampOptions.ToVmSingle(item);
            }

        }

        public static CampOptions ToVmSingle(Scoolfirst.Model.Camp.CampOptions c)
        {
            return new CampOptions
            {
                Answer = c.Answer,
                Id = c.Id,
                Correct = c.Correct
            };
        }
    }

    public class VirtualOptions
    {
        public Guid Id { get; set; }

        public string Answer { get; set; }
        public bool Correct { get; set; }

        public static IEnumerable<VirtualOptions> ToVm(IList<VirtualCompetetionOptions> c)
        {
            foreach (var item in c)
            {
                yield return ToVmSingle(item);
            }

        }

        public static VirtualOptions ToVmSingle(VirtualCompetetionOptions c)
        {
            return new VirtualOptions
            {
                Answer = c.Answer,
                Id = c.Id,
                Correct = c.Correct
            };
        }
    }
}