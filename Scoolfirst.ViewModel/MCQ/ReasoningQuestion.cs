﻿using Scoolfirst.Model.MCQ;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Scoolfirst.ViewModel.MCQ
{

    public class ReasoningQuestion
    {
        public ReasoningQuestion()
        {
            Option = new List<ReasoningOptions>();
        }
        public Guid Id { get; set; }
        public string Query { get; set; }
        public long? SetId { get; set; }
        public int ShortOrder { get; set; }
        public string Module { get; set; }
        public virtual QuestionSet Set { get; set; }
        public string Solution { get; set; }
        public List<ReasoningOptions> Option { get; set; }

        public static ReasoningQuestion VMQuestion(Scoolfirst.Model.RNM.ReasoningQuestion q)
        {
            var question = new ReasoningQuestion
            {
                Option = ReasoningOptions.ToVm(q.Option.ToList()).ToList(),
                Id = q.Id,
                SetId = q.SetId,
                Query = q.Query,
                Solution=q.Solution,
                ShortOrder=q.ShortOrder,
                Module=q.Module

            };
            return question;
        }
    }
}