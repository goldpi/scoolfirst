﻿using Scoolfirst.Model.Camp;
using Scoolfirst.Model.MCQ;
using Scoolfirst.Model.VirtualCompetetion;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Scoolfirst.ViewModel.MCQ
{
    public class McqQuestion
    {
        public McqQuestion()
        {
            Option = new List<Options>();
        }
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Unit { get; set; }
        public string Query { get; set; }
        public Guid SetId { get; set; }
        public virtual QuestionSet Set { get; set; }
        public List<Options> Option { get; set; }

        public static McqQuestion VMQuestion(Scoolfirst.Model.MCQ.McqQuestion q)
        {
            var question = new McqQuestion
            {
                Option = Options.ToVm(q.Option.ToList()).ToList(), 
                Id = q.Id,
                Unit=q.Unit,
                Subject=q.Subject,
                SetId=q.SetId,
                Query = q.Query

            };
            return question;
        }
    }

    public class CampQuestion
    {
        public CampQuestion()
        {
            Option = new List<CampOptions>();
        }
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Unit { get; set; }
        public string Query { get; set; }
        public long SetId { get; set; }
        public virtual QuestionSet Set { get; set; }
        public List<CampOptions> Option { get; set; }

        public static CampQuestion VMQuestion(Scoolfirst.Model.Camp.CampQuestion q)
        {
            var question = new CampQuestion
            {
                Option = CampOptions.ToVm(q.Option.ToList()).ToList(),
                Id = q.Id,
               
                SetId = q.SetId,
                Query = q.Query

            };
            return question;
        }
    }

    public class VirtualQuestion
    {
        public VirtualQuestion()
        {
            Options = new List<VirtualOptions>();
        }
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Unit { get; set; }
        public string Query { get; set; }
        public string QueryActual { get; set; }
        public long ExamId { get; set; }
        public virtual QuestionSet Set { get; set; }
        public List<VirtualOptions> Options { get; set; }

        public static VirtualQuestion VMQuestion(Scoolfirst.Model.VirtualCompetetion.VirtualCompetetionQuestion q)
        {
            var question = new VirtualQuestion
            {
                Options = VirtualOptions.ToVm(q.Options.ToList()).ToList(),
                Id = q.Id,
                QueryActual=q.QueryActual,
                ExamId = q.ExamId,
                Query = q.Query

            };
            return question;
        }
    }
}

