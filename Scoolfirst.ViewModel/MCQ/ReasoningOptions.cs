﻿using System;
using System.Collections.Generic;

namespace Scoolfirst.ViewModel.MCQ
{

    public class ReasoningOptions
    {
        public Guid Id { get; set; }

        public string Answer { get; set; }
        public bool Correct { get; set; }

        public static IEnumerable<ReasoningOptions> ToVm(IList<Scoolfirst.Model.RNM.ReasoningOptions> c)
        {
            foreach (var item in c)
            {
                yield return ReasoningOptions.ToVmSingle(item);
            }

        }

        public static ReasoningOptions ToVmSingle(Scoolfirst.Model.RNM.ReasoningOptions c)
        {
            return new ReasoningOptions
            {
                Answer = c.Answer,
                Id = c.Id,
                Correct = c.Correct
            };
        }
    }
}