﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Feed;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Scoolfirst.Model.Utility;

namespace Scoolfirst.ViewModel.Feed
{
    public class KnowledgeNutritionViewModel
    {
        public long Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        public string UserId { get; set; }
        public string Tags { get; set; }
       
        public string[] TagsToArray { get { return Tags.Split(','); } }
        [Required]
        public string VideoUrl { get; set; }


        private Post ToPost()
        {
            return new Post { Id = Id, Content = Content, Tags = Tags, Title = Title, Type = PostType.KNOWLEDGE_NUTRITION, UserId = UserId };
        }


        public static KnowledgeNutritionViewModel FromPost(Post Post)
        {
            string Vid = "";
            if (Post.Type == PostType.KNOWLEDGE_NUTRITION)
            { if (Post.Videos != null || Post.Videos.Count() > 0)
                    Vid = Post.Videos.First().VideoUrl;
                return new KnowledgeNutritionViewModel { Id = Post.Id, Content = Post.Content, Tags = Post.Tags, Title = Post.Title, UserId = Post.UserId , VideoUrl=Vid};
            }
                
            else
                throw new TypeNotSameException();
        }

        
        public bool AddOrSave(Scoolfirst.Model.Context.ScoolfirstContext Context,string Username)
        {
            bool ret = false;
            if (this.Id == 0)
            {
                var userid = Context.Users.FirstOrDefault(i => i.UserName == Username || i.Email == Username).Id;
                var post = this.ToPost();
                post.UserId = userid;
                post.OnDateTIme = DateTime.UtcNow;
                Context.Posts.Add(post);
                ret= Context.SaveChanges()==1?true:false;
                var Vid = new PostVideos
                {
                    Id=Guid.NewGuid(),
                    PostId = post.Id,
                    VideoUrl = VideoUrl
                };
                Context.PostVideo.Add(Vid);
                Context.SaveChanges();
            }
            else
            {
                var post = this.ToPost();
                post.OnDateTIme = System.DateTime.UtcNow;
                Context.Entry(post).State=EntityState.Modified;
                ret = Context.SaveChanges() == 1 ? true : false;
                Context.Entry(post).Collection(i => i.Videos).Load();
                var Vid = post.Videos.First();
                Vid.VideoUrl = VideoUrl;
                Context.Entry(Vid).State =EntityState.Modified;
                Context.SaveChanges();
            }
            return ret;
        }

    }

    public class TypeNotSameException:Exception
    {
        public TypeNotSameException():base("The type of Post is Not Same as excpected")
        {
            
        }
    }


    public static class PostTypeHelpers
    {
        public static KnowledgeNutritionViewModel BreakBrainFastFromId(this ScoolfirstContext Context, long Id)
        {
            return Context.Posts.Include(i => i.Videos).FirstOrDefault(i => i.Id == Id).ToBreakBrainFast();
        }
        public static KnowledgeNutritionViewModel ToBreakBrainFast(this Post Post)
        {
            return KnowledgeNutritionViewModel.FromPost(Post);
        }
        public static IEnumerable<KnowledgeNutritionViewModel> AsIEumnrable(this IEnumerable<Post> Posts)
        {
            foreach (var item in Posts)
            {
                var ret = new KnowledgeNutritionViewModel();
                try
                {
                    ret = KnowledgeNutritionViewModel.FromPost(item);
                }
                catch
                {
                    continue;
                }
                yield return ret;
            }
        }
        public static IEnumerable<KnowledgeNutritionViewModel> BreakBrainFasts(this ScoolfirstContext Context)
        {
            return Context.Posts.Include(i => i.Videos).Where(i => i.Type == PostType.KNOWLEDGE_NUTRITION).AsIEumnrable();
        }

        public static IEnumerable<KnowledgeNutritionViewModel> BreakBrainFastsForUser(this ScoolfirstContext Context, string User, int Page = 0, int size = 3)
        {
            var UserId = Context.Users.FirstOrDefault(i => i.UserName == User);
            var Groups = Context.Classes.Find(UserId.ClassId).Groups.Select(i => i.Id);
            DateTime Date = DateTimeHelper.GetIST();
            var ff = Context.Feeds.Include(i => i.Post)
                .Where(i => i.OnDateTime <= Date && Groups.Any(o => o == i.GroupId) && i.Post.Type == PostType.KNOWLEDGE_NUTRITION)
                .OrderByDescending(i => i.OnDateTime)
                .Skip(size * Page)
                .Take(size).Select(i => i.Post).Include(o=>o.Videos);
            return ff.AsEnumerable().AsIEumnrable();
        }

        public static IEnumerable<KnowledgeNutritionViewModel> BreakBrainFastsForAPI(this ScoolfirstContext Context,int Page = 0, int size = 3)
        {
 
            DateTime Date = DateTimeHelper.GetIST();
            var ff = Context.Feeds.Include(i => i.Post)
                .Where(i => i.OnDateTime <= Date && i.Post.Type == PostType.KNOWLEDGE_NUTRITION)
                .OrderByDescending(i => i.OnDateTime)
                .Skip(size * Page)
                .Take(size).Select(i => i.Post).Include(o => o.Videos);
            return ff.AsEnumerable().AsIEumnrable();
        }
    }
}
