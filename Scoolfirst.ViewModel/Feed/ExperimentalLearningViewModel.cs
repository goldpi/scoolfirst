﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Feed;
using Scoolfirst.Model.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Feed
{
    public class CreativeCanvasViewModel
    {
        public long Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        public string UserId { get; set; }
        public string Tags { get; set; }
        public DateTime OnDateTime { get; set; }
        public string[] TagsToArray { get { return Tags.Split(','); } }

        public List<Guid> ImageId { get; set; }
        public List<string> ImageUrl { get; set; }
        public List<string> ImageTitle { get; set; }
        public CreativeCanvasViewModel()
        {
            Id = 0;
            ImageId = new List<Guid>();
            ImageUrl = new List<string>();
            ImageTitle = new List<string>();
        }
        private Post ToPost()
        {
            return new Post { Id = Id, Content = Content, Tags = Tags, Title = Title, Type = PostType.CREATIVE_CANVAS,
                UserId = UserId ,OnDateTIme=OnDateTime};
        }
        public static CreativeCanvasViewModel FromPost(Post Post)
        {
            if (Post == null)
            {
                return null;
            }
            List<string> image = new List<string>();
            List<Guid> id = new List<Guid>();
            List<string> ImageTitle=new List<string>();
            if (Post.Type == PostType.CREATIVE_CANVAS)
            {
                if (Post.Images != null || Post.Images.Count() > 0)
                {
                    image = Post.Images.Select(i => i.ImageUrl).ToList();
                    id = Post.Images.Select(i => i.Id).ToList();
                   //if( Post.Images.Count(i=>i.ImageTitle!=null)>0)
                    ImageTitle = Post.Images.Select(i => i.ImageTitle).ToList()??new List<string>();
                }
                return new CreativeCanvasViewModel { Id = Post.Id, Content = Post.Content, Tags = Post.Tags, Title = Post.Title, UserId = Post.UserId, ImageId = id, ImageUrl = image,ImageTitle=ImageTitle,OnDateTime=Post.OnDateTIme };
            }

            else
                throw new TypeNotSameException();
        }


        public bool Save(Scoolfirst.Model.Context.ScoolfirstContext Context, string userName)
        {
            bool ret = false;
            var post = ToPost();
            if (post.Id == 0)
            {
                var userid = Context.Users.FirstOrDefault(i => i.UserName == userName || i.Email == userName).Id;
                post.UserId = userid;
                post.OnDateTIme = DateTime.Now;
                Context.Posts.Add(post);
                ret = Context.SaveChanges() >= 1;

            }
            else
            {

                Context.Entry(post).State = EntityState.Modified;
                Context.SaveChanges();
                Context.Entry(post).Collection(i => i.Images).Load();
                Context.PostImages.RemoveRange(post.Images);
                ret = Context.SaveChanges() >= 1;

            }

            for (int indexex = 0; indexex < this.ImageId.Count(); indexex++)
            {
                if (ImageId[indexex] == Guid.Empty)
                {
                    Context.PostImages.Add(new PostImages { Id = Guid.NewGuid(), ImageUrl = ImageUrl[indexex],ImageTitle=ImageTitle[indexex], PostId = post.Id });
                    Context.SaveChanges();
                }
                else
                {
                    Context.PostImages.Add(new PostImages { Id = ImageId[indexex], ImageUrl = ImageUrl[indexex],ImageTitle=ImageTitle[indexex], PostId = post.Id });
                    Context.SaveChanges();
                }

            }
            return ret;
        }


    }
    public static class ExpHelper
    {
        public static CreativeCanvasViewModel ToExperimentalLearningViewModel(this Post post)
        {
            return CreativeCanvasViewModel.FromPost(post);
        }
        public static IEnumerable<CreativeCanvasViewModel> ExperimentalLearningAsIEnumerable(this IEnumerable<Post> Posts)
        {
            foreach (var item in Posts)
            {
                CreativeCanvasViewModel expLrn = null;
                try
                {
                    expLrn = CreativeCanvasViewModel.FromPost(item);
                    if (expLrn == null)
                        continue;
                }
                catch
                {
                    continue;
                }
                yield return expLrn;
            }
        }

        public static IEnumerable<CreativeCanvasViewModel> CreativeCanvasViewModelgApi(this ScoolfirstContext Context, int Page = 0, int size = 3)
        {

            DateTime Date = DateTimeHelper.GetIST();
            var ff = Context.Feeds.Include(i => i.Post)               //#16 Bugfix
                .Where(i => i.OnDateTime <= Date && i.Post.Type == PostType.CREATIVE_CANVAS)
                .OrderByDescending(i => i.OnDateTime)
                .Skip(size * Page)
                .Take(size).Select(i => i.Post).Include(o => o.Images);
            return ff.AsEnumerable().ExperimentalLearningAsIEnumerable();
        }

        public static IEnumerable<CreativeCanvasViewModel> CreativeCanvasViewModelForUser(this ScoolfirstContext Context, string User, int Page = 0, int size = 3)
        {
            var UserId = Context.Users.FirstOrDefault(i => i.UserName == User);
            var Groups = Context.Classes.Find(UserId.ClassId).Groups.Select(i => i.Id);
            DateTime Date = DateTimeHelper.GetIST();
            var ff = Context.Feeds.Include(i => i.Post)
                .Where(i => i.OnDateTime <= Date && Groups.Any(o => o == i.GroupId) && i.Post.Type == PostType.CREATIVE_CANVAS)
                .OrderByDescending(i => i.OnDateTime)
                .Skip(size * Page)
                .Take(size).Select(i => i.Post).Include(o => o.Images);
            return ff.AsEnumerable().ExperimentalLearningAsIEnumerable();
        }

    }

}
