﻿using Scoolfirst.Model.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Scoolfirst.Model.Notifications;
using Scoolfirst.ViewModel.Feed.Api;
using Scoolfirst.Model.Utility;
using Scoolfirst.Model.Context;

namespace Scoolfirst.ViewModel.Feed
{

    public class FeedViewModel
    {
        public Feeds Feed { get; set; }
        public bool LikedByMe { get; set; }
        public bool FavByMe { get; set; }
        public PostType Type { get; set; }
    }

    public class FeedItems
    {
        public IEnumerable<FeedViewModel> Feed { get; set; }
        public void GetSearchedItems(Scoolfirst.Model.Context.ScoolfirstContext Data, string User,string Search, int Page = 0, int size = 3)
        {
            var UserId = Data.Users.FirstOrDefault(i => i.UserName == User).Id;

            DateTime Date = DateTimeHelper.GetIST();
            var ff = Data.Feeds.Include(i => i.Post)
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Stared)
                .Where(i => i.OnDateTime <= Date)
                .OrderByDescending(i => i.OnDateTime)
                .Skip(size * Page)
                .Take(size).ToList();
            var t = from l in ff
                    select new FeedViewModel { Feed = l,
                            Type=l.Post.Type,
                        FavByMe = l.Stared.Any(i => i.UserId == UserId),
                        LikedByMe = l.Likes.Any(i => i.UserId == UserId) };
            Feed = t.ToList();//Data.Feeds.Include(i => i.Comments.Take(3)).Include(i => i.Likes).Where(i => i.OnDateTime <= Date).OrderByDescending(i => i.OnDateTime).Skip(Page * 3).Take(3);
        }
        public void GetItems(Scoolfirst.Model.Context.ScoolfirstContext Data, string User, int Page = 0, int size = 3,string Cat="All")
        {
            var UserId = Data.Users.FirstOrDefault(i => i.UserName == User);
            bool all = false;
            var Catergory = GetCategory(Cat, out all);

            var Groups = Data.Classes.Find(UserId.ClassId).Groups.Select(i => i.Id);

            DateTime Date = DateTimeHelper.GetIST();
            List<Feeds> ff = Feeds(Data, Page, size, Groups, Date,Catergory,all);
            var t = from l in ff

                    select new FeedViewModel { Feed = l, Type = l.Post.Type, FavByMe = l.Stared.Any(i => i.UserId == UserId.Id), LikedByMe = l.Likes.Any(i => i.UserId == UserId.Id) };
            Feed = t.ToList();//Data.Feeds.Include(i => i.Comments.Take(3)).Include(i => i.Likes).Where(i => i.OnDateTime <= Date).OrderByDescending(i => i.OnDateTime).Skip(Page * 3).Take(3);
        }
        public void GetItemsPined(Scoolfirst.Model.Context.ScoolfirstContext Data, string User, int Page = 0, int size = 3, string Cat = "All")
        {
            var UserId = Data.Users.FirstOrDefault(i => i.UserName == User);
            bool all = false;
            var Catergory = GetCategory(Cat, out all);

            var Groups = Data.Classes.Find(UserId.ClassId).Groups.Select(i => i.Id);

            DateTime Date = DateTimeHelper.GetIST();
            List<Feeds> ff = FeedsPinned(Data, Page, size, Groups, Date, Catergory, all);
            var t = from l in ff

                    select new FeedViewModel { Feed = l, Type = l.Post.Type, FavByMe = l.Stared.Any(i => i.UserId == UserId.Id), LikedByMe = l.Likes.Any(i => i.UserId == UserId.Id) };
            Feed = t.ToList();//Data.Feeds.Include(i => i.Comments.Take(3)).Include(i => i.Likes).Where(i => i.OnDateTime <= Date).OrderByDescending(i => i.OnDateTime).Skip(Page * 3).Take(3);
        }
        private static List<Feeds> Feeds(Model.Context.ScoolfirstContext Data, int Page, int size, IEnumerable<int> Groups, DateTime Date,PostType cat,bool all)
        {
            return all? 
                Data.Feeds.Include(i => i.Post)
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Stared)
                .Where(i => i.OnDateTime <= Date && Groups.Any(o => o == i.GroupId))
                .OrderByDescending(i => i.OnDateTime)
                .Skip(size * Page)
                .Take(size).ToList() : Data.Feeds.Include(i => i.Post)
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Stared)
                .Where(i => i.OnDateTime <= Date && Groups.Any(o => o == i.GroupId)&& i.Post.Type==cat)
                .OrderByDescending(i => i.OnDateTime)
                .Skip(size * Page)
                .Take(size).ToList();
        }
        private static List<Feeds> FeedsPinned(Model.Context.ScoolfirstContext Data, int Page, int size, IEnumerable<int> Groups, DateTime Date, PostType cat, bool all)
        {
            return all ?
                Data.Feeds.Include(i => i.Post)
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Stared)
                .Where(i => i.OnDateTime <= Date && Groups.Any(o => o == i.GroupId)&& i.Pinned==true)
                .OrderByDescending(i => i.OnDateTime)
                .Skip(size * Page)
                .Take(size).ToList() : Data.Feeds.Include(i => i.Post)
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Stared)
                .Where(i => i.OnDateTime <= Date && Groups.Any(o => o == i.GroupId) && i.Post.Type == cat && i.Pinned==true)
                .OrderByDescending(i => i.OnDateTime)
                .Skip(size * Page)
                .Take(size).ToList();
        }

        private PostType GetCategory(string catergory, out bool all)
        {
            all = false;
            switch (catergory.ToUpper())
            {
                case "CREATIVE_CANVAS"  :
                case "CC":
                    return PostType.CREATIVE_CANVAS;
                case "KNOWLEDGE_NUTRITION":
                case "KN":    
                    return PostType.KNOWLEDGE_NUTRITION;
                case "FEED":
                case "MRT":
                case "MUST_REMEMBER_TRICKS":
                    return PostType.MUST_REMEMBER_TRICKS;
                default:
                    all = true;
                    return PostType.MUST_REMEMBER_TRICKS;
                  
            }
        }

        public IEnumerable<ApiFeedModel> GetApiItems(Scoolfirst.Model.Context.ScoolfirstContext Data, string User, int Page = 0, int size = 3)
        {
            var UserId = Data.Users.FirstOrDefault(i => i.UserName == User);



            var Groups = Data.Classes.Find(UserId.ClassId).Groups.Select(i => i.Id);
            DateTime Date = DateTimeHelper.GetIST();
            var ff = Data.Feeds.Include(i => i.Post)
               
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Stared)
                .Where(i => i.OnDateTime <= Date && Groups.Any(o => o == i.GroupId))
                .OrderByDescending(i => i.OnDateTime)
                .Skip(size * Page)
                .Take(size).ToList();
            var t = from l in ff
                    select new ApiFeedModel
                    {
                        Content = l.Post.Content,
                        Id = l.Id,
                        Tags = l.Post.Tags,
                        Title = l.Post.Title,
                        UserId = l.Post.UserId,
                        FavByMe = l.Stared.Any(i => i.UserId == UserId.Id),
                        LikedByMe = l.Likes.Any(i => i.UserId == UserId.Id),
                        LikesCount = l.Likes.Count,
                        FavCount = l.Stared.Count,
                        CommentCount=l.Comments.Count
                    };
            foreach (var item in t)
            {
                var d = Data.Users.FirstOrDefault(i => i.Id == item.UserId);
                item.UserDisplayName = d.FullName;
                item.UserPicUrl = d.PictureUrl;
                yield return item;
            }
        }

        public IEnumerable<ApiFeedModel> GetApiSummary(Scoolfirst.Model.Context.ScoolfirstContext Data, int MONTH,int CID,int SID)
        {
            var Groups = Data.Classes.Find(CID).Groups.Select(i => i.Id);
            var DateOnAbove = DateTimeHelper.GetIST().AddMonths(-1);
            var DateBelow = DateTimeHelper.GetIST().AddMonths(1);
            var ff = Data.Feeds.Include(i => i.Post)
                .Include(i => i.Comments)
                .Include(i => i.Likes)
                .Include(i => i.Stared)
                .Where(i => i.OnDateTime >= DateOnAbove && i.OnDateTime <= DateBelow && Groups.Any(o => o == i.GroupId))
                .OrderBy(i => i.OnDateTime);
            var t = from l in ff
                    select new ApiFeedModel
                    {
                        Content = l.Post.Content,
                        Id = l.Id,
                        Tags = l.Post.Tags,
                        Title = l.Post.Title,
                        UserId = l.Post.UserId,
                        LikesCount = l.Likes.Count,
                        FavCount = l.Stared.Count,
                        CommentCount = l.Comments.Count
                    };
            foreach (var item in t)
            {
                yield return item;
            }
        }
        public FeedComment AddComment(ScoolfirstContext Context, string Comment, Guid PostId, string User)
        {
            var UserId = Context.Users.FirstOrDefault(i => i.UserName == User).Id;
            FeedComment NewComment = new FeedComment
            {
                Comment = Comment,
                FeedId = PostId,
                UserId = UserId,
                On = DateTime.UtcNow,
                Id = Guid.NewGuid()
            };
            Context.FeedComments.Add(NewComment);
            Context.SaveChanges();
            Context.Entry(NewComment).Reference(i => i.User).Load();
            return NewComment;
        }
        public IEnumerable<ApiFeedComment> GetComments(ScoolfirstContext Context,string User,  Guid PostId,int page=0,int size = 3)
        {
            var UserId = Context.Users.FirstOrDefault(i => i.UserName == User).Id;
            var items = Context.FeedComments.Include(i => i.User).Where(i => i.FeedId == PostId).OrderByDescending(i => i.On).Skip(page * size).Take(size);
            foreach (var item in items)
            {
                yield return ApiFeedComment.FromComment(item, UserId);
            }
        }
        public ApiFeedComment AddApiComent(ScoolfirstContext Context, string Comment, Guid PostId, string User)
        {
            var res = this.AddComment(Context, Comment, PostId, User);
            return new ApiFeedComment
            {
                Comment = Comment,
                FeedId = PostId,
                MyComment = true,
                OnDateTime = res.On,
                UserId = res.UserId,
                UserPictureUrl=res.User.PictureUrl,
                Name=res.User.FullName
            };
        }
        public int AddRemoveFeedLike(ScoolfirstContext Context, Guid PostId, string User)
        {
            var UserId = Context.Users.FirstOrDefault(i => i.UserName == User).Id;
            bool retu = Context.FeedLike.Any(i => i.UserId == UserId && i.FeedId == PostId);
            if (retu == false)
            {
                FeedLike NewFeedLike = new FeedLike
                {
                    Id = Guid.NewGuid(),
                    On = DateTime.UtcNow,
                    UserId = UserId,
                    FeedId = PostId

                };
                Context.FeedLike.Add(NewFeedLike);
                Context.SaveChanges();
            }
            else
            {
                var find = Context.FeedLike.Where(i => i.UserId == UserId && i.FeedId == PostId);
                Context.FeedLike.RemoveRange(find);
                Context.SaveChanges();
            }

            return Context.FeedLike.Count(i=>i.FeedId==PostId);
        }
        public bool AddRemoveFeedStar(Scoolfirst.Model.Context.ScoolfirstContext Context, Guid PostId, string User)
        {
            var UserId = Context.Users.FirstOrDefault(i => i.UserName == User).Id;
            bool retu = Context.FeedStars.Any(i => i.UserId == UserId && i.FeedId == PostId);
            if (retu == false)
            {
                FeedStar NewFeedStar = new FeedStar
                {
                    Id = Guid.NewGuid(),
                    On = DateTime.UtcNow,
                    UserId = UserId,
                    FeedId = PostId

                };
                Context.FeedStars.Add(NewFeedStar);
                Context.SaveChanges();
            }
            else
            {
                var find = Context.FeedStars.Where(i => i.UserId == UserId && i.FeedId == PostId);
                Context.FeedStars.RemoveRange(find);
                Context.SaveChanges();
            }

            return !retu;
        }
        public bool RemoveComment(ScoolfirstContext Context, Guid Id, string User)
        {
            try
            {
                var UserId = Context.Users.FirstOrDefault(i => i.UserName == User).Id;
                var comment = Context.FeedComments.Find(Id);
                if (comment.UserId == UserId)
                {
                    Context.FeedComments.Remove(comment);
                    Context.SaveChanges();
                    return true;
                }
            }
            catch
            {
                return false;
            }
            return false;
        }
    }
}
