﻿using Scoolfirst.Model.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Feed
{
    public class FeedCalander
    {
        public string title { get; set; }
        public string start { get; set; }
        public bool allDay { get; set;}
        public string className { get; set; }

        public FeedCalander FromFeed(Feeds Feed)
        {
            this.title = Feed.Title;
            //Tue Mar 01 2016 00:00:00 GMT+0530 (India Standard Time)
            start = Feed.OnDateTime.ToString("ddd MMM dd yyyy HH:mm:ss 'GMT'K '(GMT Standard Time)'");
            allDay = false;
            className = "bgm-cyan";
            return this;
        }
    }
}
