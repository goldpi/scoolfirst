﻿using Scoolfirst.Model.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Feed.Api
{
    public class ApiFeedModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string UserId { get; set; }
        public string Tags { get; set; }
        public string UserDisplayName { get; set; }
        public string UserPicUrl { get; set; }
        public int CommentCount { get; set; }
        public int LikesCount { get; set; }
        public int FavCount { get; set; }
        public bool LikedByMe { get; set; }
        public bool FavByMe { get; set; }
    }
}
