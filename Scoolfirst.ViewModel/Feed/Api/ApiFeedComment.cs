﻿using Scoolfirst.Model.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Feed.Api
{
    public class ApiFeedComment
    {
        public Guid Id { get; set; }
        public Guid FeedId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string UserPictureUrl { get; set; }
        public bool MyComment { get; set; }
        public string Comment { get; set; }
        public DateTime OnDateTime { get; set; }

        public static ApiFeedComment FromComment(FeedComment res,string UserId)
        {
            return new ApiFeedComment
            {
                Id=res.Id,
                Comment = res.Comment,
                FeedId = res.FeedId,
                MyComment = UserId==res.UserId,
                OnDateTime = res.On,
                UserId = res.UserId,
                UserPictureUrl = res.User.PictureUrl,
                Name = res.User.FullName
            };
        }
    }
}
