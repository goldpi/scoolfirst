﻿using Scoolfirst.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel
{
    public class GroupViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<int> Class { get; set; }
        public List<int> School { get; set; }
        public GroupViewModel()
        {
            Class = new List<int>();
           
        }
        public GroupViewModel(Group Group)
        {
            if (Group == null)
                return;
            Id = Group.Id;
            Name = Group.Name;
            if(Group.Class!=null)
            Class = Group.Class.Select(i => i.Id).ToList();
            if (Group.Schools != null)
                School = Group.Schools.Select(i => i.Id).ToList();
            
        }
        
        public Group AsGroup()
        {
            return new
                Group
            {
                Id = this.Id,
                Name = this.Name
                
            };
        }

    }
}
