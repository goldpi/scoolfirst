﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Scoolfirst.Model.Identity;

namespace Scoolfirst.ViewModel
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public string Name { get; set; }

        [Display(Name = "Phone")]
        public string PhoneNo { get; set; }
        public int ClassId { get; set; }
        //public string Board { get; set; }
        public int SchoolId { get; set; }
        public string WhoIAm { get; set; }

        public string ProfilePic { get; set; }
        public string DateofBirth { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        //[EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

    }

    public class RegisterViewModel
    {
       
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Full Name")]
        public string Name { get; set; }
        [Required]

        [Display(Name = "Phone")]
        public string PhoneNo { get; set; }
        public int ClassId { get; set; }
        //public string Board { get; set; }
        public int SchoolId { get; set; }
        public string WhoIAm { get; set; }
        public string OtherSchool { get; set; }
        public string Gender { get; set; }
        //[Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        //[DataType(DataType.Password)]
        //[Display(Name = "Password")]
        //public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //public string ConfirmPassword { get; set; }


        public Scoolfirst.Model.Identity.User NewUserInstance(RegisterViewModel model, string StudentAvtar, string ParentAvtar, string code)
        {
            return new Scoolfirst.Model.Identity.User
            {
                UserName = model.Email ?? model.PhoneNo,
                Email = model.Email ?? null,
                FullName = model.Name,
                Student_Parent_Admin = model.WhoIAm,
                PhoneNumber = model.PhoneNo,
                ClassId = model.ClassId,
                PictureUrl = model.WhoIAm == "Student" ? StudentAvtar : ParentAvtar,
                Address = "",
                AboutMe = "",
                Gender = "",
                SchoolId = model.SchoolId,
                RegisterdOn = DateTime.UtcNow,
                Expiry = DateTime.UtcNow.AddDays(15),
                Trial = true,
                Subscribed = false,
                ConnectionId = code,
                OtherSchool = model.OtherSchool
            };
        }

        public Scoolfirst.Model.Identity.User AdminUserInstance(RegisterViewModel model, string StudentAvtar, string ParentAvtar)
        {
            return new Scoolfirst.Model.Identity.User
            {
                UserName = model.Email ?? model.PhoneNo,
                Email = model.Email ?? null,
                FullName = model.Name,
                Student_Parent_Admin = model.WhoIAm,
                PhoneNumber = model.PhoneNo,
                ClassId = model.ClassId,
                PictureUrl = model.WhoIAm == "Student" ? StudentAvtar : ParentAvtar,
                Address = "",
                AboutMe = "",
                Gender = "",
                SchoolId = model.SchoolId,
                RegisterdOn = DateTime.UtcNow,
                Expiry = DateTime.UtcNow.AddDays(15),             
                OtherSchool = model.OtherSchool
            };
        }

        public Model.Identity.User NewUserInstance(RegisterViewModel model, string studentAvtar, string parentAvtar)
        {
            throw new NotImplementedException();
        }
    }

    public class BulkRegisterViewModel
    {

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Full Name")]
        public string Name { get; set; }
        [Required]

        [Display(Name = "Phone")]
        public string PhoneNo { get; set; }
        public int ClassId { get; set; }
        //public string Board { get; set; }
        public int SchoolId { get; set; }
        public string WhoIAm { get; set; }
        public string OtherSchool { get; set; }
        public string Gender { get; set; }

        public bool hasError { get; set; }
        public string ErrorMessage { get; set; }
        //[Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        //[DataType(DataType.Password)]
        //[Display(Name = "Password")]
        //public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //public string ConfirmPassword { get; set; }
        

        public Scoolfirst.Model.Identity.User NewUserInstance( string StudentAvtar, string ParentAvtar, string code)
        {
            return new Scoolfirst.Model.Identity.User
            {
                UserName = this.Email ?? this.PhoneNo,
                Email = this.Email ?? null,
                FullName = this.Name,
                Student_Parent_Admin = this.WhoIAm,
                PhoneNumber = this.PhoneNo,
                ClassId = this.ClassId,
                PictureUrl = this.WhoIAm == "Student" ? StudentAvtar : ParentAvtar,
                Address = "",
                AboutMe = "",
                Gender = "",
                SchoolId = this.SchoolId,
                RegisterdOn = DateTime.UtcNow,
                Expiry = DateTime.UtcNow.AddDays(15),
                Trial = true,
                Subscribed = false,
                ConnectionId = code,
                OtherSchool = this.OtherSchool,
                
            };
        }

    }


    public class ResetPasswordViewModel
    {
        public ResetPasswordViewModel()
        {
            CompleteProfile = false;
        }
        [Required]
        //[EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        public string id { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
        public bool CompleteProfile { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Please enter your registred E-Mail 'or' Mobile No. to reset password of your account.")]
       // [EmailAddress]
        [Display(Name = "Email 'or Mobile No.")]
        public string Email { get; set; }
    }
}

