﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.ConnectedSchool
{
    public class ConnectedSchoolViewModel
    {
        public bool IsSearch { get; set; }
        public List<ConnectedSchoolAggregate> SearchResult { get; set; }
        public List<ConnectedSchoolAggregate> Trending { get; set; }
        public string Topic { get; set; }
        public string Class { get; set; }
        public string School { get; set; }
        public string Subject { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public ConnectedSchoolViewModel()
        {
            SearchResult = new List<ConnectedSchoolAggregate>();
            Trending = new List<ConnectedSchoolAggregate>();
        }
    }
}
