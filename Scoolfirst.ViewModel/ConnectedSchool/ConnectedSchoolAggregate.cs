﻿using Scoolfirst.Model.WorkBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.ConnectedSchool
{
    public class ConnectedSchoolAggregate
    {
        public string Title { get; set; }
        public string Id { get; set; }
        public ICollection<Books> Books { get; set; }
    }
}
