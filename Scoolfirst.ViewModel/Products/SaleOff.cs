﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Scoolfirst.ViewModel.Products
{
    public class StoreCloseVM
    {
        public int Id { get; set; }
        [ScaffoldColumn(false)]
        public string Name { get; set; }

        [ScaffoldColumn(false)]
        public string UserID { get; set; }

        public string Reason { get; set; }// Reasons

        [DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [ScaffoldColumn(false)]
        public string Email { get; set; }

        [ScaffoldColumn(false)]
        public string PhoneNo { get; set; }

        [ScaffoldColumn(false)]
        public string ProductName { get; set; }

    }
}
