﻿using Scoolfirst.Model.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Products
{
    public class ProductCategoryVM
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Image { get; set; }


        public static ProductCategoryVM FromProductCategory(ProductCategory category)
        {
            return new ProductCategoryVM
            {
                Description = category.Description,
                Id = category.Id,
                Image = category.Image,
                ShortDescription = category.ShortDescription,
                Title = category.Title
            };
        }

        
    }
    public static class ProductHelper
    {
        public static IEnumerable<ProductCategoryVM> FromProductCategories(this IEnumerable<ProductCategory> categories)
        {
            foreach (var item in categories)
            {
                yield return ProductCategoryVM.FromProductCategory(item);
            }
        }
    }
}

