﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scoolfirst.Model.PeriodicAssesment;

namespace Scoolfirst.ViewModel.Assement
{
    public class PAAnswerBack
    {
        public Guid QuesionId { get; set; }
        public Guid AnswerId { get; set; }
        public int ReasoningId { get; set; }
    }

    public class AnswerGiven
    {
        public List<PAAnswerBack> Answer { get; set; }
        public int Correct { get; set; }
        public int Wrong { get; set; }
        public int Skiped { get; set; }
    }



    public class PAReport
    {
        public PAAttemp Attemp { get; set; }
        public ICollection<Model.PeriodicAssesment.PAQuestion> Questions { get; set; }
    }

}
