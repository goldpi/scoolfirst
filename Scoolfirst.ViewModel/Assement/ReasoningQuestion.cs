﻿using Scoolfirst.Model.MCQ;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Scoolfirst.ViewModel.Assement
{

    public class PAQuestion
    {
        public PAQuestion()
        {
            Option = new List<PAOptions>();
        }
        public Guid Id { get; set; }
        public string Query { get; set; }
        public long? SetId { get; set; }
        public int ShortOrder { get; set; }
        public string Module { get; set; }
        public virtual QuestionSet Set { get; set; }
        public List<PAOptions> Option { get; set; }
        public string Solution { get; set; }

        public static PAQuestion VMQuestion(Scoolfirst.Model.PeriodicAssesment.PAQuestion q)
        {
            var question = new PAQuestion
            {
                Option = PAOptions.ToVm(q.Option.ToList()).ToList(),
                Id = q.Id,
                SetId = q.SetId,
                Query = q.Query,
                Solution=q.Solution,
                ShortOrder =q.ShortOrder,
                Module=q.Module

            };
            return question;
        }
    }
}