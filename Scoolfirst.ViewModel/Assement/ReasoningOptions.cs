﻿using System;
using System.Collections.Generic;

namespace Scoolfirst.ViewModel.Assement
{

    public class PAOptions
    {
        public Guid Id { get; set; }

        public string Answer { get; set; }
        public bool Correct { get; set; }

        public static IEnumerable<PAOptions> ToVm(IList<Scoolfirst.Model.PeriodicAssesment.PAOptions> c)
        {
            foreach (var item in c)
            {
                yield return PAOptions.ToVmSingle(item);
            }

        }

        public static PAOptions ToVmSingle(Scoolfirst.Model.PeriodicAssesment.PAOptions c)
        {
            return new PAOptions
            {
                Answer = c.Answer,
                Id = c.Id,
                Correct = c.Correct
            };
        }
    }
}