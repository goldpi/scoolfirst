﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Reasoning
{
    public class ReasoningApiViewModel
    {
        public List<Scoolfirst.Model.RNM.ReasoningRoot> Data { get; set; }
        public string Level { get; set; }
        public int Current { get; set; }
        public Model.RNM.ReasoningTracker Tracker { get; set; }
    }
}
