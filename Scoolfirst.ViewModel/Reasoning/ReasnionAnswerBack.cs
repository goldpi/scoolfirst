﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.Reasoning
{
    public class ReasnionAnswerBack
    {
        public Guid QuesionId { get; set; }
        public Guid AnswerId { get; set; }
        public int ReasoningId { get; set; }
    }
}

namespace Scoolfirst.ViewModel.Camp
{
    public class CampAnswerBack
    {
        public Guid QuesionId { get; set; }
        public Guid AnswerId { get; set; }
        public int ReasoningId { get; set; }
    }
}
