﻿using Scoolfirst.Model.RNM;
using System.Collections.Generic;
using System.Linq;
namespace Scoolfirst.ViewModel.Reasoning
{
    public class ReportAttempeded
    {
        public ReportAttempeded()
        {
            Question = new List<Model.RNM.ReasoningQuestion>();
            Answers = new List<Model.RNM.ReasoningAnswers>();
        }
        public List<Model.RNM.ReasoningQuestion> Question { get; set; }
        public List<Model.RNM.ReasoningAnswers> Answers { get; set; }
        public ReasoningTracker Root { get; set; }
        public string Title { get; set; }



        public bool CorrectAnswer(ReasoningQuestion q)
        {
           // var correct = q.Option.FirstOrDefault(i => i.Correct);
            var answer =Answers.Any(_ => _.QuestionId == q.Id)? Answers.FirstOrDefault(_ => _.QuestionId == q.Id):null;
            if (answer == null)
            {
                return false;
            }
            else
            {
                return answer.Option.Correct;
            }
        }

        public bool Attempted(ReasoningQuestion q)
        {
            //v
            var answer = Answers.Any(_ => _.QuestionId == q.Id) ? Answers.FirstOrDefault(_ => _.QuestionId == q.Id) : null;
            if (answer == null)
            {
                return false;
            }
            else
            {
                return q.Option.Any(i => i.Id == answer.OptionId);
            }
        }
    }
}
