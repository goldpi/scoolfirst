﻿using System;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace SellerCentral.Models
{
    public class Referral
    {
        [Key]
        public Guid Id { get; set; }

        public string UserId { get; set; }
        //public virtual ApplicationUser User { get; set; }
        public string ReferalCode { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public bool Active { get; set; }


    }
}