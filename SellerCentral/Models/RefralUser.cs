﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SellerCentral.Models
{
    public class RefralUser
    {
        public IList<ApplicationUser> Tier1 { get; set; }
        public IList<ApplicationUser> Tier2 { get; set; }
        public IList<ApplicationUser> Tier3 { get; set; }
        public IList<ApplicationUser> Tier4 { get; set; }
        public IList<ApplicationUser> Tier5 { get; set; }
    }
}