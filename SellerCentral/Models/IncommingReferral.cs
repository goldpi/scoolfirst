﻿using System;

namespace SellerCentral.Models
{
    public class IncommingReferral
    {
        public Guid Id  { get; set; }
        public string Code { get; set; }
        public string RequestHeaders { get; set; }
        public DateTime  EntryDate { get; set; }
    }
}