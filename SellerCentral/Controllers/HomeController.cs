﻿using SellerCentral.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SellerCentral.Controllers
{
   
    public class HomeController : Controller
    {
        ApplicationDbContext dc = new ApplicationDbContext();
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

       


        public ActionResult Reffer(string code)
        {
            var refer = new IncommingReferral
            {
                Code = code,
                EntryDate = DateTime.UtcNow.AddHours(5.5),
                Id = Guid.NewGuid(),
                RequestHeaders = Request.UserHostAddress,

            };
            dc.IncommingReferrals.Add(refer);
            dc.SaveChanges();
            return Redirect("https://play.google.com/store/apps/details?id=com.scoolfirst.upgradejr");
        }
    }
}