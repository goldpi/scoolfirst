﻿using SellerCentral.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SellerCentral.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        ApplicationDbContext dc = new ApplicationDbContext();
        private ApplicationUser user;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userName = filterContext.HttpContext.User.Identity.Name;
            user = dc.Users.FirstOrDefault(i => i.UserName == userName);
            base.OnActionExecuting(filterContext);
        }


        // GET: Dashboard
        public ActionResult Index()
        {
            ViewBag.RefferRegister = dc.Users.Count(i => i.Referrer == user.ReferCode);
            ViewBag.Code = user.ReferCode;
            return View(dc.IncommingReferrals.Where(i=>i.Code==user.ReferCode).OrderByDescending(i=>i.EntryDate));
        }



        public ActionResult Referal(string rc="")
        {
            RefralUser users = new RefralUser();
            if (string.IsNullOrEmpty(rc))
                rc = user.ReferCode;
            users.Tier1 = dc.Users.Where(i => i.Referrer == rc).ToList();
            users.Tier2 = dc.Database.SqlQuery<ApplicationUser>($"select * from AspNetUsers  where Referrer IN (select refercode from AspNetUsers b where b.referrer = '{rc}')").ToList();
            users.Tier3 = dc.Database.SqlQuery<ApplicationUser>($"select * from AspNetUsers  where Referrer IN (select refercode from AspNetUsers b where Referrer IN (select refercode from AspNetUsers b where b.referrer = '{user.ReferCode}'))").ToList();
            users.Tier4 = dc.Database.SqlQuery<ApplicationUser>($"select * from AspNetUsers  where Referrer IN (select refercode from AspNetUsers b where Referrer IN (select refercode from AspNetUsers b where Referrer IN (select refercode from AspNetUsers b where b.referrer = '{user.ReferCode}')))").ToList(); 
            users.Tier5 = dc.Database.SqlQuery<ApplicationUser>($"select * from AspNetUsers  where Referrer IN (select refercode from AspNetUsers b where Referrer IN(select refercode from AspNetUsers b where Referrer IN (select refercode from AspNetUsers b where Referrer IN (select refercode from AspNetUsers b where b.referrer = '{user.ReferCode}'))))").ToList();
            return View(users);
        } 
        
        public ActionResult AppRefral(string code)
        {
            return View(dc.IncommingReferrals.Where(i => i.Code == code).OrderByDescending(i => i.EntryDate));
        }
    }
}