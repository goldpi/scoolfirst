﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SellerCentral.Models;

namespace SellerCentral.Controllers
{
    public class IncommingReferralsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/IncommingReferrals
        public IQueryable<IncommingReferral> GetIncommingReferrals()
        {
            return db.IncommingReferrals;
        }

        // GET: api/IncommingReferrals/5
        [ResponseType(typeof(IncommingReferral))]
        public IHttpActionResult GetIncommingReferral(Guid id)
        {
            IncommingReferral incommingReferral = db.IncommingReferrals.Find(id);
            if (incommingReferral == null)
            {
                return NotFound();
            }

            return Ok(incommingReferral);
        }

        // PUT: api/IncommingReferrals/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutIncommingReferral(Guid id, IncommingReferral incommingReferral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != incommingReferral.Id)
            {
                return BadRequest();
            }
            incommingReferral.EntryDate = DateTime.UtcNow;
            db.Entry(incommingReferral).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IncommingReferralExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/IncommingReferrals
        [ResponseType(typeof(IncommingReferral))]
        public IHttpActionResult PostIncommingReferral(IncommingReferral incommingReferral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            incommingReferral.Id = Guid.NewGuid();
            incommingReferral.EntryDate=DateTime.UtcNow;
            db.IncommingReferrals.Add(incommingReferral);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (IncommingReferralExists(incommingReferral.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = incommingReferral.Id }, incommingReferral);
        }

        // DELETE: api/IncommingReferrals/5
        [ResponseType(typeof(IncommingReferral))]
        public IHttpActionResult DeleteIncommingReferral(Guid id)
        {
            IncommingReferral incommingReferral = db.IncommingReferrals.Find(id);
            if (incommingReferral == null)
            {
                return NotFound();
            }

            db.IncommingReferrals.Remove(incommingReferral);
            db.SaveChanges();

            return Ok(incommingReferral);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IncommingReferralExists(Guid id)
        {
            return db.IncommingReferrals.Count(e => e.Id == id) > 0;
        }
    }
}