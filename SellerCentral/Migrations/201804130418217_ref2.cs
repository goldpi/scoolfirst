namespace SellerCentral.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ref2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Referrals", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Referrals", new[] { "UserId" });
            AlterColumn("dbo.Referrals", "UserId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Referrals", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Referrals", "UserId");
            AddForeignKey("dbo.Referrals", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
