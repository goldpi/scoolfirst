namespace SellerCentral.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class users : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ReferCode", c => c.String());
            AddColumn("dbo.AspNetUsers", "Referrer", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Referrer");
            DropColumn("dbo.AspNetUsers", "ReferCode");
        }
    }
}
