namespace SellerCentral.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _ref : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Referrals",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        ReferalCode = c.String(),
                        ValidFrom = c.DateTime(nullable: false),
                        ValidTo = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Referrals", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Referrals", new[] { "UserId" });
            DropTable("dbo.Referrals");
        }
    }
}
