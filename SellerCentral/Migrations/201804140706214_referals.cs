namespace SellerCentral.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class referals : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IncommingReferrals", "RequestHeaders", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.IncommingReferrals", "RequestHeaders");
        }
    }
}
