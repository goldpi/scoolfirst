namespace SellerCentral.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReferralTableUpdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IncommingReferrals",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        Code = c.String(),
                        EntryDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Referrals",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        userid = c.String(),
                        Guidcode = c.String(),
                        Guidvalidfrom = c.String(),
                        validTo = c.Guid(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "DistributorLimit", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "UseLimit", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "UseLimit");
            DropColumn("dbo.AspNetUsers", "DistributorLimit");
            DropTable("dbo.Referrals");
            DropTable("dbo.IncommingReferrals");
        }
    }
}
