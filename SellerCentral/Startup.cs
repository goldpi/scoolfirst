﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SellerCentral.Startup))]
namespace SellerCentral
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
