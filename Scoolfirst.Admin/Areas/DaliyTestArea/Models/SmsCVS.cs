﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoolfirst.Admin.Areas.DaliyTestArea.Models
{
    public class SmsCVS
    {
        public string PhoneNumber { get; set; }
        public string StudentName { get; set; }
        public string StudentNumber { get; set; }
        public string UserID { get; set; }
        public string Message { get; set; }

    }
}