﻿using System.Web.Mvc;

namespace Scoolfirst.Admin.Areas.DaliyTestArea
{
    public class DaliyTestAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DaliyTestArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DaliyTest_default",
                "DaliyTest/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}