﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.DaliyTestNameSpace;

namespace Scoolfirst.Admin.Areas.DaliyTestArea.Controllers
{
    public class DTQuestionSetsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        Guid DTI;


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Request.QueryString["DTI"] == null)
                filterContext.Result = new HttpNotFoundResult();
            else
            {
                DTI = Guid.Parse(Request.QueryString["DTI"].ToString());
                ViewBag.DTI = DTI;
            }
            base.OnActionExecuting(filterContext);
        }

        // GET: DaliyTestArea/DTQuestionSets
        public ActionResult Index()
        {
            var dTQuestionSet = db.DTQuestionSet.Include(d => d.DaliyTest).Where(i=>i.DaliyTestId==DTI);
            return View(dTQuestionSet.ToList());
        }
   


        // GET: DaliyTestArea/DTQuestionSets/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DTQuestionSet dTQuestionSet = db.DTQuestionSet.Find(id);
            if (dTQuestionSet == null)
            {
                return HttpNotFound();
            }
            return View(dTQuestionSet);
        }

        // GET: DaliyTestArea/DTQuestionSets/Create
        public ActionResult Create()
        {
            ViewBag.DaliyTestId = new SelectList(db.DaliyTests, "Id", "Id",DTI);
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay");
            return View();
        }

        // POST: DaliyTestArea/DTQuestionSets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DaliyTestId,ClassId")] DTQuestionSet dTQuestionSet)
        {
            if (ModelState.IsValid)
            {
                dTQuestionSet.Id = Guid.NewGuid();
                db.DTQuestionSet.Add(dTQuestionSet);
                db.SaveChanges();
                return RedirectToAction("Index",new { DTI});
            }
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", dTQuestionSet.ClassId);
            ViewBag.DaliyTestId = new SelectList(db.DaliyTests, "Id", "Id", dTQuestionSet.DaliyTestId);
            return View(dTQuestionSet);
        }

        // GET: DaliyTestArea/DTQuestionSets/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DTQuestionSet dTQuestionSet = db.DTQuestionSet.Find(id);
            if (dTQuestionSet == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", dTQuestionSet.ClassId);
            ViewBag.DaliyTestId = new SelectList(db.DaliyTests, "Id", "Id", dTQuestionSet.DaliyTestId);
            return View(dTQuestionSet);
        }

        // POST: DaliyTestArea/DTQuestionSets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DaliyTestId,ClassId")] DTQuestionSet dTQuestionSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dTQuestionSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index",new { DTI});
            }
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", dTQuestionSet.ClassId);
            ViewBag.DaliyTestId = new SelectList(db.DaliyTests, "Id", "Id", dTQuestionSet.DaliyTestId);
            return View(dTQuestionSet);
        }

        // GET: DaliyTestArea/DTQuestionSets/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DTQuestionSet dTQuestionSet = db.DTQuestionSet.Find(id);
            if (dTQuestionSet == null)
            {
                return HttpNotFound();
            }
            return View(dTQuestionSet);
        }

        // POST: DaliyTestArea/DTQuestionSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            DTQuestionSet dTQuestionSet = db.DTQuestionSet.Find(id);
            db.DTQuestionSet.Remove(dTQuestionSet);
            db.SaveChanges();
            return RedirectToAction("Index",new {DTI });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
