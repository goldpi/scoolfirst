﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.DaliyTestNameSpace;

namespace Scoolfirst.Admin.Areas.DaliyTestArea.Controllers
{
    public class DTAnswersController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();


        Guid DTI;
        Guid QSI;
        Guid QI;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Request.QueryString["DTI"] == null)
                filterContext.Result = new HttpNotFoundResult();
            else
            {
                DTI = Guid.Parse(Request.QueryString["DTI"].ToString());
                ViewBag.DTI = DTI;
            }
            if (Request.QueryString["QSI"] == null)
                filterContext.Result = new HttpNotFoundResult();
            else
            {
                QSI = Guid.Parse(Request.QueryString["QSI"].ToString());
                ViewBag.QSI = QSI;
            }
            if (Request.QueryString["QI"] == null)
                filterContext.Result = new HttpNotFoundResult();
            else
            {
                QI = Guid.Parse(Request.QueryString["QI"].ToString());
                ViewBag.QI = QI;
            }
            base.OnActionExecuting(filterContext);
        }

        // GET: DaliyTestArea/DTAnswers
        public ActionResult Index()
        {
            var dTAnswers = db.DTAnswers.Include(d => d.Question).Where(i=>i.QuestionId==QI);
            return View(dTAnswers.ToList());
        }

        // GET: DaliyTestArea/DTAnswers/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DTAnswer dTAnswer = db.DTAnswers.Find(id);
            if (dTAnswer == null)
            {
                return HttpNotFound();
            }
            return View(dTAnswer);
        }

        // GET: DaliyTestArea/DTAnswers/Create
        public ActionResult Create()
        {
            ViewBag.QuestionId = new SelectList(db.DTQuestions, "Id", "Question",QI);
            return View();
        }

        // POST: DaliyTestArea/DTAnswers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,QuestionId,Answer,IsCorrect,Order")] DTAnswer dTAnswer)
        {
            if (ModelState.IsValid)
            {
                dTAnswer.Id = Guid.NewGuid();
                db.DTAnswers.Add(dTAnswer);
                db.SaveChanges();
                return RedirectToAction("Index" ,new {QI,DTI,QSI });
            }

            ViewBag.QuestionId = new SelectList(db.DTQuestions, "Id", "Question", dTAnswer.QuestionId);
            return View(dTAnswer);
        }

        // GET: DaliyTestArea/DTAnswers/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DTAnswer dTAnswer = db.DTAnswers.Find(id);
            if (dTAnswer == null)
            {
                return HttpNotFound();
            }
            ViewBag.QuestionId = new SelectList(db.DTQuestions, "Id", "Question", dTAnswer.QuestionId);
            return View(dTAnswer);
        }

        // POST: DaliyTestArea/DTAnswers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,QuestionId,Answer,IsCorrect,Order")] DTAnswer dTAnswer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dTAnswer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { QI, DTI, QSI });
            }
            ViewBag.QuestionId = new SelectList(db.DTQuestions, "Id", "Question", dTAnswer.QuestionId);
            return View(dTAnswer);
        }

        // GET: DaliyTestArea/DTAnswers/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DTAnswer dTAnswer = db.DTAnswers.Find(id);
            if (dTAnswer == null)
            {
                return HttpNotFound();
            }
            return View(dTAnswer);
        }

        // POST: DaliyTestArea/DTAnswers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            DTAnswer dTAnswer = db.DTAnswers.Find(id);
            db.DTAnswers.Remove(dTAnswer);
            db.SaveChanges();
            return RedirectToAction("Index", new { QI, DTI, QSI });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
