﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.DaliyTestNameSpace;

namespace Scoolfirst.Admin.Areas.DaliyTestArea.Controllers
{
    public class DaliyTestsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: DaliyTest/DaliyTests
        public ActionResult Index()
        {
            return View(db.DaliyTests.ToList());
        }

        // GET: DaliyTest/DaliyTests/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DaliyTest daliyTest = db.DaliyTests.Find(id);
            if (daliyTest == null)
            {
                return HttpNotFound();
            }
            return View(daliyTest);
        }

        // GET: DaliyTest/DaliyTests/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DaliyTest/DaliyTests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( DaliyTest daliyTest)
        {
            if (ModelState.IsValid)
            {
                daliyTest.Id = Guid.NewGuid();
                db.DaliyTests.Add(daliyTest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(daliyTest);
        }

        // GET: DaliyTest/DaliyTests/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DaliyTest daliyTest = db.DaliyTests.Find(id);
            if (daliyTest == null)
            {
                return HttpNotFound();
            }
            return View(daliyTest);
        }

        // POST: DaliyTest/DaliyTests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( DaliyTest daliyTest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(daliyTest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(daliyTest);
        }

        // GET: DaliyTest/DaliyTests/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DaliyTest daliyTest = db.DaliyTests.Find(id);
            if (daliyTest == null)
            {
                return HttpNotFound();
            }
            return View(daliyTest);
        }

        // POST: DaliyTest/DaliyTests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            DaliyTest daliyTest = db.DaliyTests.Find(id);
            db.DaliyTests.Remove(daliyTest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public void SendMessage(Guid Id)
        {
            var message = "{0} has missed today's chapter revision test. ensure daily revision for school performance and  for ultimate reward points.";
            var users = db.Database.SqlQuery<Scoolfirst.Model.Identity.User>("Select * from AspnetUsers where ID not In (select UserId from DTAppeareds Where Id = '" + Id.ToString() + "')").ToList()
               // .Where(i => !string.IsNullOrEmpty(i.ParentsNo))
                .Select(o => new Models.SmsCVS
                {
                    Message = string.Format(message,string.IsNullOrEmpty(o.FullName)?"": o.FullName.ToUpper()),
                    PhoneNumber = o.ParentsNo,
                    StudentName = string.IsNullOrEmpty(o.FullName) ? "" : o.FullName.ToUpper(),
                    StudentNumber = o.PhoneNumber,
                    UserID = o.Id
                });

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add($"{DateTime.Now.ToShortDateString()}");
            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;
            //Header of table  
            //  
            workSheet.Row(1).Height = 20;
            workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;
            workSheet.Cells[1, 1].Value = "S.No";
            workSheet.Cells[1, 2].Value = "Parent Phone";
            workSheet.Cells[1, 3].Value = "Student Number";
            workSheet.Cells[1, 4].Value = "Student Name";
            workSheet.Cells[1, 5].Value = "User Id";
            workSheet.Cells[1, 6].Value = "Message";
            //Body of table  
            //  
            int recordIndex = 2;
            foreach (var student in users)
            {
                workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
                workSheet.Cells[recordIndex, 2].Value = student.PhoneNumber;
                workSheet.Cells[recordIndex, 3].Value = student.StudentNumber;
                workSheet.Cells[recordIndex, 4].Value = student.StudentName;
                workSheet.Cells[recordIndex, 5].Value = student.UserID;
                workSheet.Cells[recordIndex, 6].Value = student.Message;
                recordIndex++;
            }
            workSheet.Column(1).AutoFit();
            workSheet.Column(2).AutoFit();
            workSheet.Column(3).AutoFit();
            workSheet.Column(4).AutoFit();
            string excelName = $"Daily-Test-Report-{DateTime.UtcNow.ToString("dd-MM-yyyy")}";
            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            return ;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
