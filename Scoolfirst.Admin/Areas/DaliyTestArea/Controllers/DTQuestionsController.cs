﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.DaliyTestNameSpace;

namespace Scoolfirst.Admin.Areas.DaliyTestArea.Controllers
{
    public class DTQuestionsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        Guid DTI;
        Guid QSI;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Request.QueryString["DTI"] == null)
                filterContext.Result = new HttpNotFoundResult();
            else
            {
                DTI = Guid.Parse(Request.QueryString["DTI"].ToString());
                ViewBag.DTI = DTI;
            }
            if (Request.QueryString["QSI"] == null)
                filterContext.Result = new HttpNotFoundResult();
            else
            {
                QSI = Guid.Parse(Request.QueryString["QSI"].ToString());
                ViewBag.QSI = QSI;
            }
            base.OnActionExecuting(filterContext);
        }

        // GET: DaliyTestArea/DTQuestions
        public ActionResult Index()
        {
            var dTQuestions = db.DTQuestions.Include(d => d.QuestionSet).Where(i=>i.QuestionSetId==QSI);
            return View(dTQuestions.ToList());
        }

        // GET: DaliyTestArea/DTQuestions/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DTQuestion dTQuestion = db.DTQuestions.Find(id);
            if (dTQuestion == null)
            {
                return HttpNotFound();
            }
            return View(dTQuestion);
        }

        // GET: DaliyTestArea/DTQuestions/Create
        public ActionResult Create()
        {
            ViewBag.QuestionSetId = new SelectList(db.DTQuestionSet, "Id", "Id",QSI);
            return View();
        }

        // POST: DaliyTestArea/DTQuestions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,QuestionSetId,Question,Solution,Concept,ShowSloution")] DTQuestion dTQuestion)
        {
            if (ModelState.IsValid)
            {
                dTQuestion.Id = Guid.NewGuid();
                db.DTQuestions.Add(dTQuestion);
                db.SaveChanges();
                return RedirectToAction("Index", new { DTI, QSI });
            }

            ViewBag.QuestionSetId = new SelectList(db.DTQuestionSet, "Id", "Id", dTQuestion.QuestionSetId);
            return View(dTQuestion);
        }

        // GET: DaliyTestArea/DTQuestions/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DTQuestion dTQuestion = db.DTQuestions.Find(id);
            if (dTQuestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.QuestionSetId = new SelectList(db.DTQuestionSet, "Id", "Id", dTQuestion.QuestionSetId);
            return View(dTQuestion);
        }

        // POST: DaliyTestArea/DTQuestions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,QuestionSetId,Question,Solution,Concept,ShowSloution")] DTQuestion dTQuestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dTQuestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { DTI, QSI });
            }
            ViewBag.QuestionSetId = new SelectList(db.DTQuestionSet, "Id", "Id", dTQuestion.QuestionSetId);
            return View(dTQuestion);
        }

        // GET: DaliyTestArea/DTQuestions/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DTQuestion dTQuestion = db.DTQuestions.Find(id);
            if (dTQuestion == null)
            {
                return HttpNotFound();
            }
            return View(dTQuestion);
        }

        // POST: DaliyTestArea/DTQuestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            DTQuestion dTQuestion = db.DTQuestions.Find(id);
            db.DTQuestions.Remove(dTQuestion);
            db.SaveChanges();
            return RedirectToAction("Index", new {DTI,QSI });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
