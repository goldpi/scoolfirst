﻿using Scoolfirst.Model.MongoDatabase;
using Scoolfirst.Model.Notifications;
using Scoolfirst.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Admin.Areas.NotificationManager.Controllers
{
    public class NotificationsController : Controller
    {

        public NotificationManagerRepo repo = new NotificationManagerRepo();

        // GET: NotificationManager/Notifications
        public ActionResult Index(int page=1,int size=30)
        {
            var I = repo.ListAllT(page, size);
            ViewBag.Pager = Pagers.Items(I.Item2).PerPage(size).Move(page).Segment(20).Center();
            return View(I);
        }


       


    public ActionResult add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult add(Notification model)
        {
            if (ModelState.IsValid)
            {
                repo.AddNotification(model);
                return RedirectToAction("index");
            }
            return View(model);
        }
    }
}