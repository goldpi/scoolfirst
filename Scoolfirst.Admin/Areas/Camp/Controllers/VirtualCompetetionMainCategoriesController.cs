﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.VirtualCompetetion;

namespace Scoolfirst.Admin.Areas.Camp.Controllers
{
    public class VirtualCompetetionMainCategoriesController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Camp/VirtualCompetetionMainCategories
        public ActionResult Index()
        {
            return View(db.VirtualCompetetionMainCategories.ToList());
        }

        // GET: Camp/VirtualCompetetionMainCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualCompetetionMainCategory virtualCompetetionMainCategory = db.VirtualCompetetionMainCategories.Find(id);
            if (virtualCompetetionMainCategory == null)
            {
                return HttpNotFound();
            }
            return View(virtualCompetetionMainCategory);
        }

        // GET: Camp/VirtualCompetetionMainCategories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Camp/VirtualCompetetionMainCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Details,Position,Active,AddedOn")] VirtualCompetetionMainCategory virtualCompetetionMainCategory)
        {
            if (ModelState.IsValid)
            {
                db.VirtualCompetetionMainCategories.Add(virtualCompetetionMainCategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(virtualCompetetionMainCategory);
        }

        // GET: Camp/VirtualCompetetionMainCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualCompetetionMainCategory virtualCompetetionMainCategory = db.VirtualCompetetionMainCategories.Find(id);
            if (virtualCompetetionMainCategory == null)
            {
                return HttpNotFound();
            }
            return View(virtualCompetetionMainCategory);
        }

        // POST: Camp/VirtualCompetetionMainCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Details,Position,Active,AddedOn")] VirtualCompetetionMainCategory virtualCompetetionMainCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(virtualCompetetionMainCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(virtualCompetetionMainCategory);
        }

        // GET: Camp/VirtualCompetetionMainCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualCompetetionMainCategory virtualCompetetionMainCategory = db.VirtualCompetetionMainCategories.Find(id);
            if (virtualCompetetionMainCategory == null)
            {
                return HttpNotFound();
            }
            return View(virtualCompetetionMainCategory);
        }

        // POST: Camp/VirtualCompetetionMainCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VirtualCompetetionMainCategory virtualCompetetionMainCategory = db.VirtualCompetetionMainCategories.Find(id);
            db.VirtualCompetetionMainCategories.Remove(virtualCompetetionMainCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
