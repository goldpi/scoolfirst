﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.VirtualCompetetion;

namespace Scoolfirst.Admin.Areas.Camp.Controllers
{
    public class VirtualCompetitonSubCategoriesController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Camp/VirtualCompetitonSubCategories
        public ActionResult Index(int CatId)
        {
            ViewBag.CatId = CatId;
            var virtualCompetitonSubCategories = db.VirtualCompetitonSubCategories.Include(v => v.Category).Where(i=>i.CategoryId==CatId);
            return View(virtualCompetitonSubCategories.ToList());
        }

        // GET: Camp/VirtualCompetitonSubCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualCompetitonSubCategory virtualCompetitonSubCategory = db.VirtualCompetitonSubCategories.Find(id);
            if (virtualCompetitonSubCategory == null)
            {
                return HttpNotFound();
            }
            return View(virtualCompetitonSubCategory);
        }

        // GET: Camp/VirtualCompetitonSubCategories/Create
        public ActionResult Create(int CatId)
        {
            
            return View(new VirtualCompetitonSubCategory {CategoryId=CatId });
        }

        // POST: Camp/VirtualCompetitonSubCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Details,VideoUrl,Active,AddedOn,CategoryId")] VirtualCompetitonSubCategory virtualCompetitonSubCategory)
        {
            if (ModelState.IsValid)
            {
                db.VirtualCompetitonSubCategories.Add(virtualCompetitonSubCategory);
                db.SaveChanges();
                return RedirectToAction("Index", new { catid = virtualCompetitonSubCategory.CategoryId });
            }

            ViewBag.CategoryId = new SelectList(db.VirtualCompetetionMainCategories, "Id", "Title", virtualCompetitonSubCategory.CategoryId);
            return View(virtualCompetitonSubCategory);
        }

        // GET: Camp/VirtualCompetitonSubCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualCompetitonSubCategory virtualCompetitonSubCategory = db.VirtualCompetitonSubCategories.Find(id);
            if (virtualCompetitonSubCategory == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.VirtualCompetetionMainCategories, "Id", "Title", virtualCompetitonSubCategory.CategoryId);
            return View(virtualCompetitonSubCategory);
        }

        // POST: Camp/VirtualCompetitonSubCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Details,VideoUrl,Active,AddedOn,CategoryId")] VirtualCompetitonSubCategory virtualCompetitonSubCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(virtualCompetitonSubCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index",new {catid=virtualCompetitonSubCategory.CategoryId });
            }
            ViewBag.CategoryId = new SelectList(db.VirtualCompetetionMainCategories, "Id", "Title", virtualCompetitonSubCategory.CategoryId);
            return View(virtualCompetitonSubCategory);
        }

        // GET: Camp/VirtualCompetitonSubCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualCompetitonSubCategory virtualCompetitonSubCategory = db.VirtualCompetitonSubCategories.Find(id);
            if (virtualCompetitonSubCategory == null)
            {
                return HttpNotFound();
            }
            return View(virtualCompetitonSubCategory);
        }

        // POST: Camp/VirtualCompetitonSubCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VirtualCompetitonSubCategory virtualCompetitonSubCategory = db.VirtualCompetitonSubCategories.Find(id);
            db.VirtualCompetitonSubCategories.Remove(virtualCompetitonSubCategory);
            db.SaveChanges();
            return RedirectToAction("Index", new { catid = virtualCompetitonSubCategory.CategoryId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
