﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Camp;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Admin.Areas.Camp.Controllers
{
    public class CampPostsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Camp/CampPosts
        public ActionResult Index()
        {
            return View(db.CampPosts.ToList());
        }

        // GET: Camp/CampPosts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampPost campPost = db.CampPosts.Find(id);
            if (campPost == null)
            {
                return HttpNotFound();
            }
            return View(campPost);
        }

        // GET: Camp/CampPosts/Create
        public ActionResult Create()
        {
            ViewBag.CampCategoryId = new SelectList(db.CampCategories, "Id", "DisplayName");
            return View();
        }

        // POST: Camp/CampPosts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Id,CampCategoryId,Title,Active,Page,VideoUrl")] CampPost campPost)
        {
            if (ModelState.IsValid)
            {
                db.CampPosts.Add(campPost);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CampCategoryId = new SelectList(db.CampCategories, "Id", "DisplayName",campPost.CampCategoryId);

            return View(campPost);
        }

        // GET: Camp/CampPosts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampPost campPost = db.CampPosts.Find(id);
            if (campPost == null)
            {
                return HttpNotFound();
            }
            ViewBag.CampCategoryId = new SelectList(db.CampCategories, "Id", "DisplayName", campPost.CampCategoryId);

            return View(campPost);
        }

        // POST: Camp/CampPosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken,ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,CampCategoryId,Title,Active,Page,VideoUrl")] CampPost campPost)
        {
            if (ModelState.IsValid)
            {
                db.Entry(campPost).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CampCategoryId = new SelectList(db.CampCategories, "Id", "DisplayName", campPost.CampCategoryId);

            return View(campPost);
        }

        // GET: Camp/CampPosts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampPost campPost = db.CampPosts.Find(id);
            if (campPost == null)
            {
                return HttpNotFound();
            }
            return View(campPost);
        }

        // POST: Camp/CampPosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CampPost campPost = db.CampPosts.Find(id);
            db.CampPosts.Remove(campPost);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
