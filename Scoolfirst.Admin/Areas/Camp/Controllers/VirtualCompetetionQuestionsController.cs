﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.VirtualCompetetion;

namespace Scoolfirst.Admin.Areas.Camp.Controllers
{
    public class VirtualCompetetionQuestionsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Camp/VirtualCompetetionQuestions
        public ActionResult Index(int id)
        {
            ViewBag.ExamId = id;
            var virtualCompetetionQuestions = db.VirtualCompetetionQuestions.Include(v => v.Exam).Where(i=>i.ExamId==id).ToList();
            return View(virtualCompetetionQuestions.ToList());
        }

        // GET: Camp/VirtualCompetetionQuestions/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualCompetetionQuestion virtualCompetetionQuestion = db.VirtualCompetetionQuestions.Find(id);
            if (virtualCompetetionQuestion == null)
            {
                return HttpNotFound();
            }
            return View(virtualCompetetionQuestion);
        }

        // GET: Camp/VirtualCompetetionQuestions/Create
        public ActionResult Create(int ExamId)
        {
            
            return View(new VirtualCompetetionQuestion{ ExamId=ExamId });
        }

        // POST: Camp/VirtualCompetetionQuestions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Query,Solution,ShowSolution,ExamId,ShortOrder")] VirtualCompetetionQuestion virtualCompetetionQuestion)
        {
            if (ModelState.IsValid)
            {
                virtualCompetetionQuestion.Id = Guid.NewGuid();
                db.VirtualCompetetionQuestions.Add(virtualCompetetionQuestion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ExamId = new SelectList(db.VirtualExams, "Id", "Title", virtualCompetetionQuestion.ExamId);
            return View(virtualCompetetionQuestion);
        }

        // GET: Camp/VirtualCompetetionQuestions/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualCompetetionQuestion virtualCompetetionQuestion = db.VirtualCompetetionQuestions.Find(id);
            if (virtualCompetetionQuestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.ExamId = new SelectList(db.VirtualExams, "Id", "Title", virtualCompetetionQuestion.ExamId);
            return View(virtualCompetetionQuestion);
        }

        // POST: Camp/VirtualCompetetionQuestions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Query,Solution,ShowSolution,ExamId,ShortOrder")] VirtualCompetetionQuestion virtualCompetetionQuestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(virtualCompetetionQuestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ExamId = new SelectList(db.VirtualExams, "Id", "Title", virtualCompetetionQuestion.ExamId);
            return View(virtualCompetetionQuestion);
        }

        // GET: Camp/VirtualCompetetionQuestions/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualCompetetionQuestion virtualCompetetionQuestion = db.VirtualCompetetionQuestions.Find(id);
            if (virtualCompetetionQuestion == null)
            {
                return HttpNotFound();
            }
            return View(virtualCompetetionQuestion);
        }

        // POST: Camp/VirtualCompetetionQuestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            VirtualCompetetionQuestion virtualCompetetionQuestion = db.VirtualCompetetionQuestions.Find(id);
            db.VirtualCompetetionQuestions.Remove(virtualCompetetionQuestion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
