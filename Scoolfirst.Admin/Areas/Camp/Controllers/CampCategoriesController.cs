﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Camp;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Admin.Areas.Camp.Controllers
{
    public class CampCategoriesController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Camp/CampCategories
        public ActionResult Index()
        {
            return View(db.CampCategories.ToList());
        }

        // GET: Camp/CampCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampCategory campCategory = db.CampCategories.Find(id);
            if (campCategory == null)
            {
                return HttpNotFound();
            }
            return View(campCategory);
        }

        // GET: Camp/CampCategories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Camp/CampCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DisplayName,Active")] CampCategory campCategory)
        {
            if (ModelState.IsValid)
            {
                db.CampCategories.Add(campCategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(campCategory);
        }

        // GET: Camp/CampCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampCategory campCategory = db.CampCategories.Find(id);
            if (campCategory == null)
            {
                return HttpNotFound();
            }
            return View(campCategory);
        }

        // POST: Camp/CampCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DisplayName,Active")] CampCategory campCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(campCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(campCategory);
        }

        // GET: Camp/CampCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampCategory campCategory = db.CampCategories.Find(id);
            if (campCategory == null)
            {
                return HttpNotFound();
            }
            return View(campCategory);
        }

        // POST: Camp/CampCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CampCategory campCategory = db.CampCategories.Find(id);
            db.CampCategories.Remove(campCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
