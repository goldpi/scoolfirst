﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.VirtualCompetetion;

namespace Scoolfirst.Admin.Areas.Camp.Controllers
{
    public class VirtualExamsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Camp/VirtualExams
        public ActionResult Index(int catId)
        {
            ViewBag.CatId = catId;
            var virtualExams = db.VirtualExams.Include(v => v.Category).Where(i=>i.CategoryId==catId);
            return View(virtualExams.ToList());
        }

        // GET: Camp/VirtualExams/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualExam virtualExam = db.VirtualExams.Find(id);
            if (virtualExam == null)
            {
                return HttpNotFound();
            }
            return View(virtualExam);
        }

        // GET: Camp/VirtualExams/Create
        public ActionResult Create(int catId)
        {
            
            return View(new VirtualExam { CategoryId = catId });
        }

        // POST: Camp/VirtualExams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Content,TimeInMinutes,AddedOn,PointsPerQuestion,TestBrief,Order,Level,Color,TitleColor,CategoryId,Subject,Active")] VirtualExam virtualExam)
        {
            if (ModelState.IsValid)
            {
                db.VirtualExams.Add(virtualExam);
                db.SaveChanges();
                return RedirectToAction("Index",new {catId=virtualExam.CategoryId });
            }

            ViewBag.CategoryId = new SelectList(db.VirtualCompetitonSubCategories, "Id", "Title", virtualExam.CategoryId);
            return View(virtualExam);
        }

        // GET: Camp/VirtualExams/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualExam virtualExam = db.VirtualExams.Find(id);
            if (virtualExam == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.VirtualCompetitonSubCategories, "Id", "Title", virtualExam.CategoryId);
            return View(virtualExam);
        }

        // POST: Camp/VirtualExams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Content,TimeInMinutes,AddedOn,PointsPerQuestion,TestBrief,Order,Level,Color,TitleColor,CategoryId,Subject,Active")] VirtualExam virtualExam)
        {
            if (ModelState.IsValid)
            {
                db.Entry(virtualExam).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { catId = virtualExam.CategoryId });
            }
            ViewBag.CategoryId = new SelectList(db.VirtualCompetitonSubCategories, "Id", "Title", virtualExam.CategoryId);
            return View(virtualExam);
        }

        // GET: Camp/VirtualExams/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VirtualExam virtualExam = db.VirtualExams.Find(id);
            if (virtualExam == null)
            {
                return HttpNotFound();
            }
            return View(virtualExam);
        }

        // POST: Camp/VirtualExams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            VirtualExam virtualExam = db.VirtualExams.Find(id);
            db.VirtualExams.Remove(virtualExam);
            db.SaveChanges();
            return  RedirectToAction("Index", new { catId = virtualExam.CategoryId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
