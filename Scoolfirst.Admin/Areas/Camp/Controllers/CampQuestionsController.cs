﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Camp;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Admin.Areas.Camp.Controllers
{
    public class CampQuestionsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Camp/CampQuestions
        public ActionResult Index(int PostId)
        {
            return View(db.CampQuestions.Where(i=>i.SetId==PostId).ToList());
        }

        // GET: Camp/CampQuestions/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampQuestion campQuestion = db.CampQuestions.Find(id);
            if (campQuestion == null)
            {
                return HttpNotFound();
            }
            return View(campQuestion);
        }

        public ActionResult Create(int setId)
        {
            // ViewBag.SetId = new SelectList(db.PlayRoots, "Id", "Title");
            return View(new CampQuestion { SetId=setId});
        }

        // POST: Camp/CampQuestions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Query,Solution,ShowSolution,SetId,ShortOrder")] CampQuestion campQuestion)
        {
            if (ModelState.IsValid)
            {
                campQuestion.Id = Guid.NewGuid();
                db.CampQuestions.Add(campQuestion);
                db.SaveChanges();
                return RedirectToAction("Index",new { postid=campQuestion.SetId});
            }

            return View(campQuestion);
        }

        // GET: Camp/CampQuestions/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampQuestion campQuestion = db.CampQuestions.Find(id);
            if (campQuestion == null)
            {
                return HttpNotFound();
            }
            return View(campQuestion);
        }

        // POST: Camp/CampQuestions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Query,Solution,ShowSolution,SetId,ShortOrder")] CampQuestion campQuestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(campQuestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index",new {postid=campQuestion.SetId });
            }
            return View(campQuestion);
        }

        // GET: Camp/CampQuestions/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampQuestion campQuestion = db.CampQuestions.Find(id);
            if (campQuestion == null)
            {
                return HttpNotFound();
            }
            return View(campQuestion);
        }

        // POST: Camp/CampQuestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            CampQuestion campQuestion = db.CampQuestions.Find(id);
            db.CampQuestions.Remove(campQuestion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
