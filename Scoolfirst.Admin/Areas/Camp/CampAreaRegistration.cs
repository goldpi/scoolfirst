﻿using System.Web.Mvc;

namespace Scoolfirst.Admin.Areas.Camp
{
    public class CampAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Camp";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Camp_default",
                "Camp/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}