﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Admin.Areas.ModuleManagment.Controllers
{
    public class ModuleDatasController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: ModuleManagment/ModuleDatas
        public ActionResult Index()
        {
            return View(db.Modules.ToList());
        }

        // GET: ModuleManagment/ModuleDatas/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModuleData moduleData = db.Modules.Find(id);
            if (moduleData == null)
            {
                return HttpNotFound();
            }
            return View(moduleData);
        }

        // GET: ModuleManagment/ModuleDatas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ModuleManagment/ModuleDatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( ModuleData moduleData)
        {
            if (ModelState.IsValid)
            {
                db.Modules.Add(moduleData);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(moduleData);
        }

        // GET: ModuleManagment/ModuleDatas/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModuleData moduleData = db.Modules.Find(id);
            if (moduleData == null)
            {
                return HttpNotFound();
            }
            return View(moduleData);
        }

        // POST: ModuleManagment/ModuleDatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ModuleData moduleData)
        {
            if (ModelState.IsValid)
            {
                db.Entry(moduleData).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(moduleData);
        }

        // GET: ModuleManagment/ModuleDatas/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModuleData moduleData = db.Modules.Find(id);
            if (moduleData == null)
            {
                return HttpNotFound();
            }
            return View(moduleData);
        }

        // POST: ModuleManagment/ModuleDatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ModuleData moduleData = db.Modules.Find(id);
            db.Modules.Remove(moduleData);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
