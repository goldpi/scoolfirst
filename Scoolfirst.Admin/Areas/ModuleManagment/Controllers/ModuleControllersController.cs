﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Admin.Areas.ModuleManagment.Controllers
{
    public class ModuleControllersController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: ModuleManagment/ModuleControllers
        public ActionResult Index()
        {
            return View(db.ControllerAction.OrderBy(i=>i.Controller).ToList());
        }

        // GET: ModuleManagment/ModuleControllers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModuleControllers moduleControllers = db.ControllerAction.Find(id);
            if (moduleControllers == null)
            {
                return HttpNotFound();
            }
            return View(moduleControllers);
        }

        // GET: ModuleManagment/ModuleControllers/Create
        public ActionResult Create()
        {
            //ViewBag.ModuleId = new SelectList(db.Modules, "ModuleName", "ModuleName");
            return View();
        }

        // POST: ModuleManagment/ModuleControllers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Controller,Action,ModuleId")] ModuleControllers moduleControllers)
        //{
        //    moduleControllers.Id = Guid.NewGuid().ToString();
        //    if (ModelState.IsValid)
        //    {
        //        db.ControllerAction.Add(moduleControllers);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ModuleId = new SelectList(db.Modules, "ModuleName", "ModuleName",moduleControllers.ModuleId);
        //    return View(moduleControllers);
        //}

        // GET: ModuleManagment/ModuleControllers/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModuleControllers moduleControllers = db.ControllerAction.Find(id);
            if (moduleControllers == null)
            {
                return HttpNotFound();
            }
            ViewBag.ModuleId = new SelectList(db.Modules, "ModuleName", "ModuleName", moduleControllers.ModuleId);
            return View(moduleControllers);
        }

        // POST: ModuleManagment/ModuleControllers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ModuleId,Controller,Action")] ModuleControllers moduleControllers)
        {
            if (ModelState.IsValid)
            {
                
                db.Entry(moduleControllers).State = EntityState.Modified;
                db.Entry(moduleControllers).Property(i => i.Action).IsModified = true;
                db.Entry(moduleControllers).Property(i => i.Controller).IsModified = true;
                //db.Entry()
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ModuleId = new SelectList(db.Modules, "ModuleName", "ModuleName", moduleControllers.ModuleId);
            return View(moduleControllers);
        }

        // GET: ModuleManagment/ModuleControllers/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModuleControllers moduleControllers = db.ControllerAction.Find(id);
            if (moduleControllers == null)
            {
                return HttpNotFound();
            }
            return View(moduleControllers);
        }

        // POST: ModuleManagment/ModuleControllers/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(string id)
        //{
        //    ModuleControllers moduleControllers = db.ControllerAction.Find(id);
        //    db.ControllerAction.Remove(moduleControllers);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
