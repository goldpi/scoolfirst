﻿using System.Web.Mvc;

namespace Scoolfirst.Admin.Areas.ModuleManagment
{
    public class ModuleManagmentAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ModuleManagment";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ModuleManagment_default",
                "ModuleManagment/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}