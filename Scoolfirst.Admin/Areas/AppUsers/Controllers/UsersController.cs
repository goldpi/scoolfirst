﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using System.IO;
using Excel;
using Scoolfirst.ViewModel;
using Scoolfirst.Model.Provider;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Scoolfirst.ViewModel.User;
using System.Text;
using System.Threading.Tasks;
using Scoolfirst.Model.Common;
using System.Data.SqlClient;

namespace Scoolfirst.Admin.Areas.AppUsers.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        /// <summary>
        /// Url For the Api Server to Request Token.
        /// </summary>
        string ApiUrl = HostingServerInfo.Api.Url();

        string StudentAvtar = HostedApplicationConfig.DefaultStudentAvtar.Value();
        string ParentAvtar = HostedApplicationConfig.DefaultParentAvatar.Value();
        private ScoolfirstContext db = new ScoolfirstContext();



        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        public ActionResult Index(int school = 0, int classid = 0, int page = 1, int size = 30, string type = "school", bool Last24Hrs = false, bool desending = false)
        {
            var role= db.Roles.FirstOrDefault(i => i.Name == "student").Id;
            ViewBag.Role = role;
            var rr = db.Roles.FirstOrDefault(i => i.Name == type).Id;
            var List = db.Users.Include(i=>i.Roles).Where(i => i.Roles.Any(io=>io.RoleId==rr));                                   
            if (Last24Hrs)
            {
                var date = DateTime.Now.AddDays(-1);
                List = List.Where(i => i.RegisterdOn > date);
            }
            if (type != "school" && classid != 0)
            {
                ViewBag.classid = new SelectList(db.Classes, "Id", "RomanDisplay", classid).Concat(new SelectListItem[] { new SelectListItem { Text = "Select A Class" } });
                List = List.Where(i => i.ClassId == classid);
            }
            else
                ViewBag.classid = new SelectList(db.Classes, "Id", "RomanDisplay").Concat(new SelectListItem[] { new SelectListItem { Text = "Select A class", Selected = true, Value = "0" } });
            if (school != 0)
            {
                ViewBag.school = new SelectList(db.Schools, "Id", "SchoolName", school).Concat(new SelectListItem[] { new SelectListItem { Text = "Select A School" } });
                List = List.Where(i => i.SchoolId == school);
            }
            else
                ViewBag.school = new SelectList(db.Schools, "Id", "SchoolName").Concat(new SelectListItem[] { new SelectListItem { Text = "Select A School", Selected = true, Value = "0" } });

            if (Request.QueryString["search"] != null)
            {
                var sear = Request.QueryString["search"];
                List = List.Where(i => i.UserName.Contains(sear) || i.FullName.Contains(sear) || i.PhoneNumber.Contains(sear));
            }

            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            if (desending)
            {
                List= List.OrderByDescending(i => i.Points).Skip((page - 1) * size).Take(size);
            }
            else
            {
                List = List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size);
            }
            return View(List.ToList());
            
        }

        

        public ActionResult ListAllUser(int school = 0, int classid = 0, int page = 1, int size = 30)
        {
            ViewBag.Role = db.Roles.FirstOrDefault(i => i.Name == "student").Id;
            IQueryable<User> List = db.Users;
            
            if (school != 0)
            {
                ViewBag.school = new SelectList(db.Schools, "Id", "SchoolName", school).Concat(new SelectListItem[] { new SelectListItem { Text = "Select A School" } });
                List = List.Where(i => i.SchoolId == school);
            }
            else
                ViewBag.school = new SelectList(db.Schools, "Id", "SchoolName").Concat(new SelectListItem[] { new SelectListItem { Text = "Select A School", Selected = true, Value = "0" } });
            if (classid != 0)
            {
                ViewBag.classid = new SelectList(db.Classes, "Id", "RomanDisplay", classid).Concat(new SelectListItem[] { new SelectListItem { Text = "Select A Class" } });
                List = List.Where(i => i.ClassId == classid);
            }
            else
                ViewBag.classid = new SelectList(db.Classes, "Id", "RomanDisplay").Concat(new SelectListItem[] { new SelectListItem { Text = "Select A class", Selected = true, Value = "0" } });

            if (Request.QueryString["search"] != null)
            {
                var sear = Request.QueryString["search"];
                List = List.Where(i => i.UserName.Contains(sear) || i.FullName.Contains(sear));
            }

            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
        }

        [HttpPost]
        public ActionResult Modules(string uid,int schoolid,string[] ModulesSubscribed,bool? subscribed,DateTime? Expiry)
        {
            var u = db.Users.Find(uid);
            u.ModulesSubscribed = ModulesSubscribed;
            if (subscribed!=null)
            {
                u.Subscribed = subscribed.Value;
                db.Database.ExecuteSqlCommand("update dbo.AspNetUsers set dbo.AspNetUsers.Subscribed = @p1 where dbo.AspNetUsers.SchoolId =@p2 ", new SqlParameter("p1", (subscribed.Value ? 1 : 0)), new SqlParameter("p2", schoolid));
            }
            if (Expiry != null)
            {
                u.Expiry = Expiry.Value;
                db.Database.ExecuteSqlCommand("update dbo.AspNetUsers set dbo.AspNetUsers.Expiry = @p1 where dbo.AspNetUsers.SchoolId =@p2 ", new SqlParameter("p1", u.Expiry), new SqlParameter("p2", schoolid));
            }
            db.SaveChanges();
            
            db.Database.ExecuteSqlCommand("update dbo.AspNetUsers set dbo.AspNetUsers.Modules = @p1 where dbo.AspNetUsers.SchoolId =@p2 ", new SqlParameter("p1", u.Modules),new SqlParameter("p2",schoolid));
            return RedirectToAction("details", new { id = uid });
        }

        [HttpPost]
        public ActionResult Singlemodules(string uid, int schoolid, string[] ModulesSubscribed, bool? subscribed, DateTime? Expiry,string ParentsNo="")
        {
            var u = db.Users.Find(uid);
            u.ModulesSubscribed = ModulesSubscribed;
            if (subscribed != null)
            {
                u.Subscribed = subscribed.Value;
            }
            if (Expiry != null)
            {
                u.Expiry = Expiry.Value;
            }
            u.ParentsNo = ParentsNo;
            db.SaveChanges();
            return RedirectToAction("details", new { id = uid });
        }




        // GET: AppUsers/Users/Details/5
        public ActionResult Details(string id)
        {
            var roles = db.Roles.ToList();
            var sid = roles.First(o => o.Name == "student").Id;
            var scoolid = roles.First(o => o.Name.ToLower() == "school").Id;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Include(e=>e.Roles).FirstOrDefault(i=>i.Id==id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.ModulesSubscribed = new MultiSelectList(db.Modules, "ModuleName", "ModuleName", user.ModulesSubscribed);
            if (user.Roles.Any(i=>i.RoleId== sid))
            {
                return View("StudentDetails", user);
            }
            if (user.Roles.Any(i => i.RoleId == scoolid))
            {
               
                return View("SchoolDetails", user);
            }
            return View(user);
        }

        public ActionResult ListRoles()
        {
            var data = db.Roles.OrderBy(i => i.Name).ToList();

            return View(data);
        }


        //public ActionResult Last24Hrs()
        //{
           
        //    var Last24HrsUsers = db.Users.Where(i => i.RegisterdOn > date).ToList();

        //}


        // GET: AppUsers/Users/Create
        public ActionResult Create()
        {
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay");
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName");
            return View();
        }

        //POST: AppUsers/Users/Create
        //To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FullName,Student_Parent_Admin,ClassId,Board,PictureUrl,Points,AboutMe,DOB,Address,SchoolId,Trial,Subscribed,Expiry,RegisterdOn,Sponsered,RegisterAs,RequestedforBrandAmbesdor,Gender,ConnectionId,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] User user)
        {
            if (ModelState.IsValid)
            {
                //db.Users.Add(user);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", user.ClassId);
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName", user.SchoolId);
            return View(user);
        }

        
        // GET: AppUsers/Users/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            
            return View(user);
        }

        // POST: AppUsers/Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            User user = db.Users.Find(id);

            //clean Up
            var achivments = db.Achievements.Where(i => i.UserId == id);
            db.Achievements.RemoveRange(achivments);

            var Answereds = db.Answereds.Where(i => i.UserId == id);
            db.Answereds.RemoveRange(Answereds);

            var AnswerGiven =db.AnswerGiven.Where(i => i.UserId == id);
            db.AnswerGiven.RemoveRange(AnswerGiven);

            var ArticleComments = db.ArticleComments.Where(i => i.UserId == id);
            db.ArticleComments.RemoveRange(ArticleComments);

            var FavArticles = db.FavArticles.Where(i => i.UserId == id);
            db.FavArticles.RemoveRange(FavArticles);


            var FeedComments = db.FeedComments.Include(i=>i.Replys).Where(i => i.UserId == id);
            
            db.FeedComments.RemoveRange(FeedComments);

         

            user.Roles.Clear();
            
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Excel

        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {

                    var stream = upload.InputStream;
                    IExcelDataReader reader = null;
                    if (upload.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (upload.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        return View();
                    }
                    reader.IsFirstRowAsColumnNames = true;
                    var result = reader.AsDataSet();
                    reader.Close();
                    var dt = result.Tables[0];
                    var Columns = dt.Columns;
                    var Rows = dt.Rows;
                    var BulkUserRegistration = new List<BulkRegisterViewModel>();
                    foreach (DataRow row in dt.Rows)
                    {
                        StringBuilder sb = new StringBuilder();
                        var obj = new BulkRegisterViewModel
                        {
                            WhoIAm = row[0].ToString(),
                            Name = row[1].ToString(),
                            PhoneNo = row[2].ToString(),
                            ClassId = int.Parse(row[3].ToString()),
                            SchoolId = int.Parse(row[4].ToString()),
                            Email = row[6].ToString(),
                            Gender = row[5].ToString(),
                        };
                        try
                        {
                         var results= UserManager.Create(obj.NewUserInstance(StudentAvtar, ParentAvtar, "0000"), "welcome123");
                            if (!results.Succeeded)
                            {
                                obj.ErrorMessage = "";
                                foreach(var i in results.Errors)
                                {
                                    obj.ErrorMessage  +=i+" , ";
                                }
                                BulkUserRegistration.Add(obj);
                            }
                        }
                        catch(Exception Ex)
                        {
                            obj.ErrorMessage = Ex.Message;
                            BulkUserRegistration.Add(obj);
                        }
                    }
                    return View(BulkUserRegistration);
                }
                ModelState.AddModelError("upload", "Please Upload Your file");
            }
            return View();
        }

        #endregion


        private void RoleAssign(User user,string Role)
        {
            var store = new RoleStore<IdentityRole>(db);
            var manager = new RoleManager<IdentityRole>(store);
            if (!manager.RoleExists(Role))
            {
                var role = new IdentityRole { Name = Role };
                var res = manager.Create(role);
                if (res.Succeeded)
                {
                    UserManager.AddToRole(user.Id, Role);
                }
            }
            else
            {
                UserManager.AddToRole(user.Id, Role);
            }
        }


        [HttpGet]
        public ActionResult AddStudentParent()
        {
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay");
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName");
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddStudentParent(RegisterViewModel model,string Password)
        {
            if (ModelState.IsValid)
            {

                string StudentAvtar = HostedApplicationConfig.DefaultStudentAvtar.Value();
                string ParentAvtar = HostedApplicationConfig.DefaultParentAvatar.Value();
               
               
                User user = model.AdminUserInstance(model, StudentAvtar, ParentAvtar);
                user.Student_Parent_Admin = "Student";
                var result = await UserManager.CreateAsync(user, Password);
                if (result.Succeeded)
                {
                    RoleAssign(user,"student");
                    return RedirectToAction("Index");
                }
               
            }

            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay",model.ClassId);
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName",model.SchoolId);
            
            return View(model);
        }


        public ActionResult MakeStudent(string Uid,string role)
        {
            var us = db.Users.Find(Uid);
            if(us!=null)
            RoleAssign(us, role);
            if (Request.UrlReferrer != null)
                return Redirect(Request.UrlReferrer.ToString());
            return RedirectToAction("ListAllUser");
        }

        [HttpGet]
        public ActionResult AddAdminUsers()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddAdminUsers(AdminUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                string StudentAvtar = HostedApplicationConfig.DefaultStudentAvtar.Value();
                string ParentAvtar = HostedApplicationConfig.DefaultParentAvatar.Value();


                User user = model.AdminUserInstance(model, StudentAvtar, ParentAvtar);
                user.Student_Parent_Admin = "Admin";
                user.PhoneNumber = model.Email;
                
                user.RegisterdOn = DateTime.UtcNow;
                user.Expiry = DateTime.UtcNow;
                user.ClassId = db.Classes.FirstOrDefault().Id;
                user.SchoolId = db.Schools.FirstOrDefault().Id; 
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    RoleAssign(user, "Admin");
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }


        [HttpGet]
        public ActionResult AddSchools()
        {
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName");

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddSchools(SchoolRepresentativeProfile model)
        {
            if (ModelState.IsValid)
            {

                string StudentAvtar = HostedApplicationConfig.DefaultStudentAvtar.Value();
                string ParentAvtar = HostedApplicationConfig.DefaultParentAvatar.Value();


                User user = model.AdminSchoolUserInstance(model, StudentAvtar, ParentAvtar);
                user.Student_Parent_Admin = "School";
                user.ClassId = db.Classes.FirstOrDefault().Id;
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    RoleAssign(user, "School");
                    return RedirectToAction("Index");
                }

            }

            
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName", model.SchoolId);

            return View(model);
        }


        public string UserActivityLast10Days(string username,int days = 10)
        {
            var da = DateTimeOffset.Now.AddDays(days * -1);
            var today = DateTimeOffset.Now;

            var result = db.ActivityLogger.Count(i => i.UserName == username && i.OnDateOffset >= da && i.OnDateOffset <= today);
            var total = db.ActivityLogger.Count(i => i.UserName == username);
            return $"LT- {result}, O-{total}";
        }


        public JsonResult UserLog(string username,int days=30)
        {
            var da = DateTimeOffset.Now.AddDays(days * -1);
            var today = DateTimeOffset.Now;

            var result = db.ActivityLogger.Where(i => i.UserName == username && i.OnDateOffset >= da && i.OnDateOffset <= today).OrderBy(i=>i.OnDateOffset).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReasoningRoots(string UserId, string level)
        {
            var res = db.ReasoningTracker.OrderByDescending(i => i.UserId == UserId && i.Level == level).First();
            if (res == null)
            {
                return Content("N/A");
            }
            else return Content(res.RnmPost.Title);
        }

       
    }
}

