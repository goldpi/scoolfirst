﻿using Scoolfirst.Model.WorkBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoolfirst.Admin.Models
{
     /* :null,
        : 0,
        ClassId: 0,
        Subject:"--",
        Questions: [],
        Title: "",
        Details:"" */

    public class QuizEntryVm
    {
        public Guid SyllabusId { get; set; }
        public int SchoolId { get; set; }
        public int ClassId { get; set; }
        public string Subject { get; set; }
        public ICollection<Questions> Questions { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
    }
}