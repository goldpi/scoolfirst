﻿using Microsoft.Owin;
using Owin;
using System.Configuration;

[assembly: OwinStartupAttribute(typeof(Scoolfirst.Admin.Startup))]
namespace Scoolfirst.Admin
{
    //ToDo : flash messages [yusuf task]

    public partial class Startup: ConfigurationSection
    {
        public void Configuration(IAppBuilder app)
        {
         
            ConfigureAuth(app);

        }

        public override bool IsReadOnly()
        {
            return false;
        }
    }
}
