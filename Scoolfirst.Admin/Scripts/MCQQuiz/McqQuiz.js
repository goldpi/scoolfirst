﻿
app.controller("QuizControl", ["$scope",  "$http", function ($scope,$http) {
    $scope.id = '00000000-0000-0000-0000-000000000000';
    $scope.Exec = function (val,val2) {
        $scope.id = val;
        $scope.SId = val2;
        console.log(val);
        QuestionById(val,val2);
    }
    $scope.Model = {
        McqQuestion: {
            Id: '00000000-0000-0000-0000-000000000000',
            Subject:"English",
            Unit: "One",
            Query:"Query Here",
            SetId:'00000000-0000-0000-0000-000000000000',
            Option: [{
                Id: '00000000-0000-0000-0000-000000000000',
                Correct: false,
                Answer: "Option"
            }],
       
        }
      
    };

    /// Sending data to server
    $scope.sendData = function () {

        $http.post('/api/SaveMcq', $scope.Model.McqQuestion)
           .success(function (data, status) {
               $scope.PostDataResponse = data;
               console.log(data);
           })
           
    }

    /// Adding more choice option
    $scope.addOption = function () {
        $scope.Model.McqQuestion.Option.push({
            Id: 0,
            Correct: false,
            Answer: "This is an Answer"
        })
    }
    /// Removing choice option
    $scope.removeOption = function (index) {
        $scope.Model.McqQuestion.Option.splice(index, 1);
    }



   // QuestionById($scope.id)
    function QuestionById(id,sid) {
        var uri = "/api/McqQuest/?id=" + id +"&sid="+sid;
        $http.get(uri)
            .success(function (data) {
                console.log(data);
                $scope.Model.McqQuestion = data;
            })
            
    }
}]);
