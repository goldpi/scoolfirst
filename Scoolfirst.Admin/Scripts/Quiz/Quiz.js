﻿app.controller("QuicCntrl", ["$scope", "$http", function ($scope, $http) {
    $scope.Schools = [];
    $scope.Classes = [];
    $scope.Subjects = [];
    $scope.Questions = [];
    $scope.Selected = [];

    $scope.QuizModel = {
        SyllabusId:null,
        SchoolId: 0,
        ClassId: 0,
        Subject:"--",
        Questions: [],
        Title: "",
        Details:""
    }

    $scope.School = 0;
    $scope.Class = 0;
    $scope.Subject = "--";
    function GetVariables() {
        $http.get("/api/schools/").then(function (data, status, headers, config) {
            $scope.Schools = data.data;

        })
        $http.get("/api/Class/").then(function (data, status, headers, config) {
            $scope.Classes = data.data;

        })
        $http.get("/api/Sub/").then(function (data, status, headers, config) {
            $scope.Subjects = data.data;

        })

    }
    GetVariables()
    $scope.changequestion = function () {
        //  alert($scope.School);

        if ($scope.QuizModel.SchoolId === 0 || $scope.QuizModel.ClassId === 0 || $scope.QuizModel.Subject === "--")
            return;
        $http.get("/api/WB/GetBooksOfClass?sid=" + $scope.QuizModel.SchoolId + "&cid=" + $scope.QuizModel.ClassId + "&subjectname=" + $scope.QuizModel.Subject).then(function (data, status, headers, config) {
            $scope.Questions = data.data;

        })
    }

    $scope.Add = function (q,Index) {
        $scope.QuizModel.Questions.push(q);
        $scope.Questions.splice(Index, 1);
    }
    $scope.Remove = function (q, index) {
        $scope.Questions.push(q);
        $scope.QuizModel.Questions.splice(index, 1);
    }
    
    $scope.Save = function () {
        $http.post("/SyllabusDates/CreateQuiz", $scope.QuizModel).then(function (data, status, headers, config) {
            alert(data.Measage);
            $scope.QuizModel = data.data;

        });
    }


}])