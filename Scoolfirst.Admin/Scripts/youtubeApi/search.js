// After the API loads, call a function to enable the search box.
function handleAPILoaded() {
    $('#search-button').attr('disabled', false);
    $("#login-container").remove();
}

// Search for a specified string.
function search() {
    var q = $('#query-field').val();
    console.log(q)
  var request = gapi.client.youtube.search.list({
    q: q,
    part: 'snippet',
      maxResults:50
  });

  request.execute(function(response) {
      var result = response.items;
      var it = "";
      for (var i = 0; i < result.length; i++)
      {

         //ToDo : pagination
          if(result[i].id.kind==="youtube#video")
          it += "<div class='media ' style='cursor:pointer;' onclick=add('https://youtu.be/" + result[i].id.videoId + "')><div class='media-left'><a href='#'><img class='media-object' src='" + result[i].snippet.thumbnails.default.url + "' alt='" + result[i].snippet.title + "'></a></div><div class='media-body waves-effect' style='display:table-cell!important;'><h4 class='media-heading'>" + result[i].snippet.title + "</h4><P>" + result[i].snippet.description + "</P></div></div>";

          //it += "<div class='card' onclick=add('https://youtu.be/" + result[i].id.videoId + "') ><h4>" + result[i].snippet.title + "</h4><p> <img src='" + result[i].snippet.thumbnails.default.url + "'  class='pull-left m-r-10'/>" + result[i].snippet.description + "</p><div class='clearfix'></div></div>";
      } 
      $('#results').html(it);
  });

}
function Toggle() {
    $('#results').toggle();
}
function add(url) {
    $("input#VideoUrl").val(url);
}