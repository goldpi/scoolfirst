﻿'use strict';

app.controller('messageController', ['$scope', 'backendHubProxy',
  function ($scope, backendHubProxy) {
      $scope.message = [];
      console.log('trying to connect to service')
      var MessageHub = backendHubProxy(backendHubProxy.defaultServer, 'MessageHub');
      console.log('connected to service')
      $scope.clear = function () {
          $scope.message = [];
          MessageHub.invoke("MarkReadAll", "ee");
      }
      $scope.brodcastMessage = "";
      $scope.sendToAll = function () {
          MessageHub.invoke("sendToAll", $scope.brodcastMessage);
      }

      MessageHub.on('messages', function (data) {
          angular.forEach(data, function (value, key) {
              $scope.message.push(value)
          });

          console.log(data);
      });
      MessageHub.on('markedRead', function (data) {
          // sweetAlert("OOps..", data, "success");
      });
  }
]);