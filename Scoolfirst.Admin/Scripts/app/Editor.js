﻿"use strict";

app.controller("EditorController", ['$scope', function ($scope) {

    $scope.Heading = "This Is Heading";
    $scope.editHeading = false;
    $scope.HeadingEdit = function () {
        $scope.editHeading = !$scope.editHeading;
    }
    $scope.P = [{ p: "helloe", show: true }, { p: "helloe", show: true }];
    $scope.images = [{img:"http://picture.scoolfirst.com/content/default.gif",show:false}];
    $scope.video = [];
    $scope.EditPragraph = function (index) {
        $scope.P[index].show = !$scope.P[index].show;
    }
    $scope.RemoveP = function (index) {
        $scope.P.splice(index, 1);
    }
    $scope.RemoveImg = function (index) {
        $scope.images.splice(index, 1);
    }
    $scope.AddImg = function () {
        $scope.images.push({ img: "http://picture.scoolfirst.com/content/default.gif", show: false });
    }
    $scope.AddPra = function () {
        $scope.P.push({ p: "New Para", show: true });
        
    }
}])