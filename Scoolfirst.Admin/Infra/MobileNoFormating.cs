﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoolfirst.Admin.Infra
{
    using System.Text;
    using System.Web.Mvc;

    public static class MobileNo
    {
        public static MvcHtmlString Format(string MobileNo)
        {
            StringBuilder sb=new StringBuilder();
            sb.Append("<a href='tel:" + MobileNo + "'>" + MobileNo + "</a>");
            MvcHtmlString htmlString=new MvcHtmlString(sb.ToString());
            return htmlString;
        }
    }
}