﻿using System.Web;
using System.Web.Optimization;

namespace Scoolfirst.Admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/static/vendors/bower_components/jquery/dist/jquery.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/static/vendors/bower_components/jquery/dist/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/static/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/static/css").Include(
                      "~/static/css/app.min.1.css",
                      "~/static/css/app.min.2.css"));
        }
    }
}
