[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Scoolfirst.Admin.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Scoolfirst.Admin.App_Start.NinjectWebCommon), "Stop")]

namespace Scoolfirst.Admin.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Model.Context;

    using Model.Common;
    using Model.Provider;

    public static class NinjectWebCommon 
    {
        
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ScoolfirstContext>().ToSelf().InRequestScope();
            // kernel.Bind<IRepository<Classes>>().To<ProductRepository>().InRequestScope();
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            kernel.Bind<IShortMessageSender>().ToConstructor(i => new ShortMessageSender($"{baseDirectory}/Config/SmsProvider.xml"));
            kernel.Bind<IShortMessageContentProvider>().ToConstructor(i => new ShortMessaeContentProvider($"{baseDirectory}/Config/Message.xml"));
            kernel.Bind<IServerConfig>().ToConstructor(i => new ServerConfig($"{baseDirectory}/Config/ServerConfig.xml"));
            kernel.Bind<IApplicationConfig>().ToConstructor(i => new ApplicationConfig($"{baseDirectory}/Config/ApplicationConfig.xml"));
            HostingServerInfoHelper.Init(kernel);
            ApplicationConfigHelper.Init(kernel);
        }        
    }
}
