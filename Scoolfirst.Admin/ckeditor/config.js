/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
if (fburl === undefined) {
  var  fburl= 'http://static.scoolfirst.com/filemanager/dialog.php?type=3&editor=ckeditor&crossdomain=1';
}
if (fuurl === undefined) {
   var fuurl='http://static.scoolfirst.com/filemanager/dialog.php?type=3&editor=ckeditor&crossdomain=1';
}
if(fbiurl===undefined){
    var fbiurl='http://static.scoolfirst.com/filemanager/dialog.php?type=0&editor=ckeditor&crossdomain=1';
}
CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.filebrowserBrowseUrl =fburl;
    config.filebrowserUploadUrl = fuurl;
    config.filebrowserImageBrowseUrl = fbiurl;
};
