﻿function alertinfo(title, message) {
    toastr.info(message, title)

}
function alerterror(title,message) {
    toastr.error(message, title)
    console.log(message);
}
function alertwarning(title, message) {
    toastr.warning(message, title)

}
function alertsuccess(title, message) {
    toastr.success(message, title)
}

function reload(title, message) {
    toastr.success(message, title)
    location.reload();
}



$(function () {
    $.ajaxSetup({ cache: false });


        $("[data-modal]").on("click", function (e) {
            e.preventDefault();

            $('#myModal').modal({
                keyboard: true
            }, 'show');

            $('#myModalContent').load(this.href, function () {
                console.clear();
                console.log(this);
                
                bindForm(this);
            });
            return false;
        });


    });

    function bindForm(dialog) {
        $('form', dialog).submit(function () {
            $('#progress').show();
            $('#myModalContent').html('<div class="text-info pn centered " style="box-shadow:0px;"><i class="fa fa-spinner faa-spin animated" style="margin-top: 12%; font-size: 7.6em;"></i></div>');

            $.ajax({
                url: this.action,
                type: this.method,
                data: $(this).serialize(),
                success: function (result) {
                    if (result.callbacks != undefined) {

                        getFunctions(result.callbacks)
                        $('#myModal').modal('hide');
                    }
                   // console.log(result);
                    if (result.success) {
                        Cookies.set('Message', result.Message);
                        toastr.success(Cookies.get('yusuf'));
                        
                       
                        $('#progress').hide();
                        //location.reload();

                    } else {
                        $('#progress').hide();
                        $('#myModalContent').html(result);
                        bindForm();
                    }
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr);
                    console.log(textStatus);
                    console.log(error);
                }
            });
            return false;
        });

       

       

    }
    function getFunctions(object) {
        //console.log("get callbacks");
        if (object === undefined) {
            console.log("No Callbacks");
            return;
        }
        if (Object.prototype.toString.call(object) === '[object Array]') {
            $.each(object, function (i, ele) {
                //console.log("loops");
                //console.log(ele.function);
                var func = window[ele.function];
                if (isFunction(func)) {
                    console.log("function");
                    if (ele.parameters != undefined) {
                        func.apply(this, ele.parameters)
                    }

                    if (ele.parameters === undefined)
                        func();
                } else {
                    alert("function does not exist. please write a javascript function. named " + ele.function + "");
                }
            })
        }
    }
    function isFunction(funcobject) {
        //console.log(funcobject);
        var re = Object.prototype.toString.call(funcobject) === '[object Function]';
        //console.log(re);
        return re;
    }