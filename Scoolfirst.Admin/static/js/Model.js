﻿// plug-ins
(function ($) {
    $.fn.popup = function (title, url, callback, func) {
        this.click(function (evt) {
            evt.preventDefault();
            var id = $(this).attr("id") + "-modal";
            var modal = $("#" + id);
            if (modal.length < 1) {
                modal = $('<div class="modal hide"><div class="modal-header"><a class="close" data-dismiss="modal">&times;</a><h3 class="modal-title"></h3></div><div class="modal-body"></div><div class="modal-footer"><a href="#" class="btn btn-primary">Add</a><a href="#" class="btn dlClose">Close</a></div></div>')
                    .attr("id", id);
                $(".modal-title", modal).text(title);
                var args = (func != undefined) ? func() : {};
                $.get(url, args, function (form) {
                    $(".modal-body", modal).html(form);
                    modal.modal("show");
                    $(".dlClose", modal).click(function () { modal.modal('hide'); });
                    var frm = $("form", modal);
                    $(".btn-primary", modal).click(function () { frm.submit(); });
                    frm.validate({
                        submitHandler: function () {
                            var data = frm.serialize();
                            $.post(frm.attr("action"), data, function (data) {
                                callback(data);
                                modal.modal('hide');
                                modal.remove();
                            });
                        }
                    });
                    modal.modal();
                });
            }
            else {
                modal.modal();
            }
        });
    };
})(jQuery);