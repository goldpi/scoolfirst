﻿function getFunctions(object) {
    //console.log("get callbacks");
    if (object === undefined) {
        console.log("No Callbacks");
        return;
    }
    if (Object.prototype.toString.call(object) === '[object Array]') {
        $.each(object, function (i, ele) {
            //console.log("loops");
            //console.log(ele.function);
            var func = window[ele.function];
            if (isFunction(func)) {
                console.log("function");
                if (ele.parameters != undefined) {
                    func.apply(this, ele.parameters)
                }

                if (ele.parameters === undefined)
                    func();
            } else {
                alert("function does not exist. please write a javascript function. named " + ele.function + "");
            }
        })
    }
}
function isFunction(funcobject) {
    //console.log(funcobject);
    var re = Object.prototype.toString.call(funcobject) === '[object Function]';
    //console.log(re);
    return re;
}