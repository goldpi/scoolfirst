﻿/// <reference path="plugins/responsivefilemanager/plugin.min.js" />
/// <reference path="plugins/responsivefilemanager/plugin.min.js" />
tinymce.init({
    height: '350px',
    mode: "textareas",
    theme: "modern",
    element_format : "html",
    forced_root_block : '',
    allow_html_in_named_anchor: true,
   
    //extended_valid_elements:"*[*]",
    extended_valid_elements: "span[class|src|border|margin|padding|title|style|width|height|align|onmouseover|onmouseout|name]",
    filemanager_crossdomain: true,
    external_filemanager_path: fburl,
    external_plugins: { "filemanager": fpurl },
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste  youtube responsivefilemanager"
    ],
    content_css: "css/content.css",
    //responsivefilemanager
    toolbar: "responsivefilemanager | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image youtube | print preview media fullpage | forecolor backcolor emoticons | code",
    image_advtab: true,
   
    filemanager_title: "Responsive File Manager",
    
    style_formats: [
         { title: 'Bold text', inline: 'b' },
         { title: 'Red text', inline: 'span', styles: { color: '#ff0000' } },
         { title: 'Red header', block: 'h1', styles: { color: '#ff0000' } },
         { title: 'Example 1', inline: 'span', classes: 'example1' },
         { title: 'Example 2', inline: 'span', classes: 'example2' },
         { title: 'Table styles' },
         { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }]
});