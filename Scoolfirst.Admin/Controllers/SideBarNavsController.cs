﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Admin.Controllers
{
    public class SideBarNavsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: SideBarNavs
        public ActionResult Index()
        {
            return View(db.SideBarNavigation.ToList());
        }

        // GET: SideBarNavs/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SideBarNav sideBarNav = db.SideBarNavigation.Find(id);
            if (sideBarNav == null)
            {
                return HttpNotFound();
            }
            return View(sideBarNav);
        }

        // GET: SideBarNavs/Create
        public ActionResult Create()
        {
            ViewBag.ModuleDataId = new SelectList(db.Modules, "ModuleName", "ModuleName");
            return View();
        }

        // POST: SideBarNavs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title,Order,SubMenu,ModuleDataId")] SideBarNav sideBarNav)
        {
            if (ModelState.IsValid)
            {
                db.SideBarNavigation.Add(sideBarNav);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ModuleDataId = new SelectList(db.Modules, "ModuleName", "ModuleName", sideBarNav.ModuleDataId);
            return View(sideBarNav);
        }

        // GET: SideBarNavs/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SideBarNav sideBarNav = db.SideBarNavigation.Find(id);
            if (sideBarNav == null)
            {
                return HttpNotFound();
            }
            ViewBag.ModuleDataId = new SelectList(db.Modules, "ModuleName", "ModuleName",sideBarNav.ModuleDataId);
            return View(sideBarNav);
        }

        // POST: SideBarNavs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Title,Order,SubMenu,ModuleDataId")] SideBarNav sideBarNav)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sideBarNav).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ModuleDataId = new SelectList(db.Modules, "ModuleName", "ModuleName", sideBarNav.ModuleDataId);
            return View(sideBarNav);
        }

        // GET: SideBarNavs/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SideBarNav sideBarNav = db.SideBarNavigation.Find(id);
            if (sideBarNav == null)
            {
                return HttpNotFound();
            }
            return View(sideBarNav);
        }

        // POST: SideBarNavs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            SideBarNav sideBarNav = db.SideBarNavigation.Find(id);
            db.SideBarNavigation.Remove(sideBarNav);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
