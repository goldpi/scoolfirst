﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.RNM;
using Scoolfirst.Model.PeriodicAssesment;
using Scoolfirst.Model.PlayReasoning;

namespace Scoolfirst.Admin.Controllers
{
    public class RnmQuestionController : ApiController
    {
        private ScoolfirstContext db = new ScoolfirstContext();
        [Route("api/RnmQuest/")]
        public Scoolfirst.ViewModel.MCQ.ReasoningQuestion Get(Guid? Id, long? sid)
        {
            if (Id == Guid.Empty)
            {

                ReasoningQuestion q = new ReasoningQuestion()
                {
                    Id = Guid.Empty,
                    Query = "Query here",
                    Solution="Enter Solution",
                    SetId = sid,
                    Module="Enter Module",
                    ShortOrder=1,
                    Option = new List<ReasoningOptions>(),

                };
                for (int w = 1; w < 5; w++)
                {
                    q.Option.Add(new ReasoningOptions { Answer = $"Option {w}", Correct = false });
                }
                return Scoolfirst.ViewModel.MCQ.ReasoningQuestion.VMQuestion(q);
            }

            else
            {
                {
                    var res = db.ReasoningQuestions.Find(Id);
                    var x = db.ReasoningOptions.Where(i => i.QuestionId == Id).ToList();

                    //res.Choices = new List<Choice>();
                    foreach (var item in x)
                    {
                        res.Option.Add(item);
                    }
                    //db.Entry(res).Collection(i => i.Choices).Load();
                    return Scoolfirst.ViewModel.MCQ.ReasoningQuestion.VMQuestion(res);
                }

            }
        }

        [Route("api/SaveRnmQuest")]
        [HttpPost]
        public IHttpActionResult Post(ReasoningQuestion question)
        {
            if (question.Id == Guid.Empty)
            {
                question.Id = Guid.NewGuid();

                foreach (var option in question.Option)
                {
                    option.Id = Guid.NewGuid();
                }
                db.ReasoningQuestions.Add(question);


                db.SaveChanges();
            }
            else
            {
                #region QuestionModified
                var qq = new ReasoningQuestion
                {
                    Id = question.Id,
                    Query = question.Query,
                    SetId = question.SetId,
                    Module = question.Module,
                    ShortOrder=question.ShortOrder,
                    Solution=question.Solution,
                    Option = new List<ReasoningOptions>()
                };

                db.Entry(qq).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                #endregion
                var optionlist = question.Option.ToList();
                var toDel = new List<Guid>().ToList();
                var Option = db.ReasoningOptions.Where(i => i.QuestionId == question.Id).ToList();

                foreach (var item in Option)
                {
                    #region OptionsModified
                    if (optionlist.Any(i => i.Id == item.Id))
                    {
                        var ss = optionlist.First(i => i.Id == item.Id);
                        item.Answer = ss.Answer;
                        item.Correct = ss.Correct;
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    #endregion

                    else
                    #region Remove
                    {
                        toDel.Add(item.Id);
                    }
                    #endregion
                }
                foreach (var items in optionlist)
                {
                    if (items.Id == Guid.Empty)
                    {
                        items.QuestionId = qq.Id;
                        items.Id = Guid.NewGuid();
                        db.ReasoningOptions.Add(items);
                        db.SaveChanges();
                    }
                }

                #region RemovingItem
                foreach (Guid id in toDel)
                {
                    var Id = db.ReasoningOptions.Find(id);
                    db.ReasoningOptions.Remove(Id);
                    db.SaveChanges();
                }
                #endregion
            }
            //return question.Query;
            return Json(new { data = Scoolfirst.ViewModel.MCQ.ReasoningQuestion.VMQuestion(question), Message = "Data Saved" });
        }

        [Route("api/PAQuest/")]
        public Scoolfirst.ViewModel.Assement.PAQuestion GetPA(Guid? Id, long? sid)
        {
            if (Id == Guid.Empty)
            {

                PAQuestion q = new PAQuestion()
                {
                    Id = Guid.Empty,
                    Query = "Query here",
                    Solution="Enter Solution",
                    SetId = sid??0,
                    Module = "Enter Module",
                    ShortOrder = 1,
                    Option = new List<PAOptions>(),

                };
                for (int w = 1; w < 5; w++)
                {
                    q.Option.Add(new PAOptions { Answer = $"Option {w}", Correct = false });
                }
                return Scoolfirst.ViewModel.Assement.PAQuestion.VMQuestion(q);
            }

            else
            {
                {
                    var res = db.PAQuestions.Find(Id);
                    var x = db.PAOptions.Where(i => i.QuestionId == Id).ToList();

                    //res.Choices = new List<Choice>();
                    foreach (var item in x)
                    {
                        res.Option.Add(item);
                    }
                    //db.Entry(res).Collection(i => i.Choices).Load();
                    return Scoolfirst.ViewModel.Assement.PAQuestion.VMQuestion(res);
                }

            }
        }

        [Route("api/SavePAQuest")]
        [HttpPost]
        public IHttpActionResult PAPost(PAQuestion question)
        {
            if (question.Id == Guid.Empty)
            {
                question.Id = Guid.NewGuid();

                foreach (var option in question.Option)
                {
                    option.Id = Guid.NewGuid();
                }
                db.PAQuestions.Add(question);


                db.SaveChanges();
            }
            else
            {
                #region QuestionModified
                var qq = new PAQuestion
                {
                    Id = question.Id,
                    Query = question.Query,
                    SetId = question.SetId,
                    Module = question.Module,
                    Solution=question.Solution,
                    ShortOrder = question.ShortOrder,
                    Option = new List<PAOptions>()
                };

                db.Entry(qq).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                #endregion
                var optionlist = question.Option.ToList();
                var toDel = new List<Guid>().ToList();
                var Option = db.PAOptions.Where(i => i.QuestionId == question.Id).ToList();

                foreach (var item in Option)
                {
                    #region OptionsModified
                    if (optionlist.Any(i => i.Id == item.Id))
                    {
                        var ss = optionlist.First(i => i.Id == item.Id);
                        item.Answer = ss.Answer;
                        item.Correct = ss.Correct;
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    #endregion

                    else
                    #region Remove
                    {
                        toDel.Add(item.Id);
                    }
                    #endregion
                }
                foreach (var items in optionlist)
                {
                    if (items.Id == Guid.Empty)
                    {
                        items.QuestionId = qq.Id;
                        items.Id = Guid.NewGuid();
                        db.PAOptions.Add(items);
                        db.SaveChanges();
                    }
                }

                #region RemovingItem
                foreach (Guid id in toDel)
                {
                    var Id = db.PAOptions.Find(id);
                    db.PAOptions.Remove(Id);
                    db.SaveChanges();
                }
                #endregion
            }
            //return question.Query;
            return Json(new { data = Scoolfirst.ViewModel.Assement.PAQuestion.VMQuestion(question), Message = "Data Saved" });
        }


        [Route("api/PlayQuest/")]
        public Scoolfirst.ViewModel.Play.PlayQuestion GetPLay(Guid? Id, long? sid)
        {
            if (Id == Guid.Empty)
            {

                PlayQuestion q = new PlayQuestion()
                {
                    Id = Guid.Empty,
                    Query = "Query here",
                    ShowSolution=false,
                    Solution="",
                    SetId = sid ?? 0,
                   
                    ShortOrder = 1,
                    Option = new List<PlayOptions>(),

                };
                for (int w = 1; w < 5; w++)
                {
                    q.Option.Add(new PlayOptions { Answer = $"Option {w}", Correct = false });
                }
                return Scoolfirst.ViewModel.Play.PlayQuestion.VMQuestion(q);
            }

            else
            {
                {
                    var res = db.PlayQuestions.Find(Id);
                    var x = db.PlayOptions.Where(i => i.QuestionId == Id).ToList();

                    //res.Choices = new List<Choice>();
                    foreach (var item in x)
                    {
                        res.Option.Add(item);
                    }
                    //db.Entry(res).Collection(i => i.Choices).Load();
                    return Scoolfirst.ViewModel.Play.PlayQuestion.VMQuestion(res);
                }

            }
        }

        [Route("api/SavePlayQuest")]
        [HttpPost]
        public IHttpActionResult PlayPost(PlayQuestion question)
        {
            if (question.Id == Guid.Empty)
            {
                question.Id = Guid.NewGuid();

                foreach (var option in question.Option)
                {
                    option.Id = Guid.NewGuid();
                }
                db.PlayQuestions.Add(question);


                db.SaveChanges();
            }
            else
            {
                #region QuestionModified
                var qq = new PlayQuestion
                {
                    Id = question.Id,
                    Query = question.Query,
                    SetId = question.SetId,
                    Solution=question.Solution,
                    ShowSolution=question.ShowSolution,
                    ShortOrder = question.ShortOrder,
                    Option = new List<PlayOptions>()
                };

                db.Entry(qq).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                #endregion
                var optionlist = question.Option.ToList();
                var toDel = new List<Guid>().ToList();
                var Option = db.PlayOptions.Where(i => i.QuestionId == question.Id).ToList();

                foreach (var item in Option)
                {
                    #region OptionsModified
                    if (optionlist.Any(i => i.Id == item.Id))
                    {
                        var ss = optionlist.First(i => i.Id == item.Id);
                        item.Answer = ss.Answer;
                        item.Correct = ss.Correct;
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    #endregion

                    else
                    #region Remove
                    {
                        toDel.Add(item.Id);
                    }
                    #endregion
                }
                foreach (var items in optionlist)
                {
                    if (items.Id == Guid.Empty)
                    {
                        items.QuestionId = qq.Id;
                        items.Id = Guid.NewGuid();
                        db.PlayOptions.Add(items);
                        db.SaveChanges();
                    }
                }

                #region RemovingItem
                foreach (Guid id in toDel)
                {
                    var Id = db.PlayOptions.Find(id);
                    db.PlayOptions.Remove(Id);
                    db.SaveChanges();
                }
                #endregion
            }
            //return question.Query;
            return Json(new { data = Scoolfirst.ViewModel.Play.PlayQuestion.VMQuestion(question), Message = "Data Saved" });
        }
    }
}
