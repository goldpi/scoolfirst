﻿using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Geo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.ViewModel.WorkBook;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
   [Authorize]
    public class SchoolsController : Controller
    {
        private ScoolfirstContext dc = new ScoolfirstContext();

        // GET: Countries
        public ActionResult Index( int page = 1, int size = 10,string search="")
        {
            IQueryable<School> List;
            if (!string.IsNullOrEmpty(search))
            {
                List = dc.Schools.Where(i => i.SchoolName.Contains(search));
            }
            else
                List = dc.Schools;
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());

        }
        // GET: Countries/Edit/5
        public ActionResult Edit(int id = 0)
        {
            School m = new School { Id = 0 };
            if (id != 0)
            {
                m = dc.Schools.Find(id);
                ViewBag.AreaId = new SelectList(dc.Areas, "Id", "Name",m.AreaId );
                ViewBag.BoardId = new SelectList(dc.Boards, "Id", "Name", m.BoardId);
          
            }
            else
            {
                ViewBag.AreaId = new SelectList(dc.Areas, "Id", "Name");
                ViewBag.BoardId = new SelectList(dc.Boards, "Id", "Name");
            }

            if (Request.IsAjaxRequest())
                return PartialView(m);


            return View(m);
        }

        // POST: Countries/Edit/5
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(School model)
        {

            
            if (ModelState.IsValid)
            {
                if (model.Id != 0)
                    dc.Entry(model).State = System.Data.Entity.EntityState.Modified;
                else
                dc.Schools.Add(model);
                dc.SaveChanges();
                if (Request.IsAjaxRequest())
                    return Json(new { success = 1, msg = "Saved Changes" }, JsonRequestBehavior.AllowGet);
                else
                {
                    TempData["Sucess"] = "Saved Changes";
                    //return Redirect(Request.UrlReferrer.ToString());
                    return RedirectToAction("Details","Schools", new { id=model.Id});
                }

            }
            ViewBag.AreaId = new SelectList(dc.Areas, "Id", "Name", model.AreaId);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            return View(model);
        }

        // GET: Countries/Delete/5
        public ActionResult Delete(int? id)
        {

            School value = null;
            if (id == null)
                return new HttpStatusCodeResult(404, "Not Found");
            if ((value = dc.Schools.Find(id)) != null)
            {
                dc.Entry(value).Collection(i => i.Book).Load();
                
                value.Book.Clear();
                var scs=dc.SchoolClassSubject.Where(i => i.SchoolId == id);
                dc.SchoolClassSubject.RemoveRange(scs);
                dc.Schools.Remove(value);
                dc.SaveChanges();
                return Json(new { success = 1, msg = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return new HttpStatusCodeResult(404, "Not Found");
        }

        public ActionResult Details(int Id)
        {
           var m = dc.Schools.Find(Id);
           return View(m);
        }

        public ActionResult Classes(int Id)
        {
            var Classes = dc.Classes.ToList();
            ViewBag.Id = Id;
            return PartialView(Classes);
        }

       

        public ActionResult Subjects(int sid,int cid)
        {
            var sub = dc.SchoolClassSubject
                .Include(i=>i.Subject)
                .Include(i=>i.Books)
                .Where(i => i.SchoolId == sid && i.ClassId == cid);
            return PartialView(sub);

        }

        public ActionResult AddSubject(int sid, int cid)
        {
            ViewBag.School = dc.Schools.Find(sid).SchoolName;
            ViewBag.Class = dc.Classes.Find(cid).NumericDisplay;

            ViewBag.SubjectId = new SelectList(GetSubject(sid, cid), "Id", "SubjectName");
            if(Request.IsAjaxRequest())
            return PartialView(new SchoolClassSubject { SchoolId = sid, ClassId = cid });
            return View(new SchoolClassSubject { SchoolId = sid, ClassId = cid });

        }
        [HttpPost]
        public ActionResult AddSubject(SchoolClassSubject model)
        {
            
            if (ModelState.IsValid)
            {
                dc.SchoolClassSubject.Add(model);
                dc.SaveChanges();
                return RedirectToAction("details", new { id = model.SchoolId });
            }
            ViewBag.School = dc.Schools.Find(model.SchoolId).SchoolName;
            ViewBag.Class = dc.Classes.Find(model.ClassId).NumericDisplay;
            ViewBag.SubjectId = new SelectList(GetSubject(model.SchoolId, model.ClassId), "Id", "SubjectName",model.SubjectId);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            return View( model);

        }

        public ActionResult AddBook(int subId,int ScId,int clid,int sub)
        {
          
            ViewBag.BookId = new SelectList(GetBooks(subId,clid,sub), "Id", "Title");
            return View(new ClassBooks { Id = subId,scid=ScId,clid=clid ,sub=sub});
        }
        [HttpPost]
        public ActionResult AddBook(ClassBooks model)
        {
            if (ModelState.IsValid)
            {
                var sc = dc.SchoolClassSubject.Find(model.Id);
                dc.Entry(sc).Collection(i => i.Books).Load();
                if (!sc.Books.Any(i => i.Id == model.BookId))
                {
                    var book=dc.Books.Find(model.BookId);
                    sc.Books.Add(book);
                    dc.Entry(sc).State = EntityState.Modified;
                    dc.SaveChanges();
                }
                return RedirectToAction("Details", new { id = model.scid });
            }
            
            ViewBag.BookId = new SelectList(GetBooks(model.Id,model.clid,model.sub), "Id", "Title");
            return View(model);
        }
        private IEnumerable<Books> GetBooks(int subId,int clid,int sub)
        {
            var ps = new List<Books>();
            if(dc.SchoolClassSubject.Any(i => i.Id == subId))
           ps.AddRange(dc.SchoolClassSubject.FirstOrDefault(i => i.Id == subId).Books);
            var x = dc.Books.Where(i=>i.ClassesId==clid&& i.SubjectId==sub);
            foreach (var item in x)
            {
                if (ps.Any(i => i.Id == item.Id)) continue;
                else
                    yield return item;
            }
        }

        private IEnumerable<Subject> GetSubject(int sid, int cid)
        {
            var hasSub = dc.SchoolClassSubject.Where(i => i.SchoolId == sid && i.ClassId == cid).Select(i => i.Subject);
            var DDl = dc.Subjects.Except(hasSub);
            return DDl;
        }

        public ActionResult RemoveSubject(int id)
        {
            var sub = dc.SchoolClassSubject.Find(id);
            dc.SchoolClassSubject.Remove(sub);
            dc.SaveChanges();
            if (Request.IsAjaxRequest())
            {
                return Json(new { Success = "record deleted" });
            }
            return Redirect(Request.UrlReferrer.ToString());
        }
        
        public ActionResult RemoveBook(int Id,int SubId)
        {
            var sub = dc.SchoolClassSubject.Include(i => i.Books).FirstOrDefault(o => o.Id == SubId);
            var book = sub.Books.First(i => i.Id == Id);
            sub.Books.Remove(book);
            dc.SaveChanges();
            if (Request.IsAjaxRequest())
            {
                return Json(new { Success = "record deleted" });
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dc.Dispose();
            }
            base.Dispose(disposing);
        }



        public ActionResult Syllabus(int SchoolId,int ClassId,int SubjectId)
        {
            //ViewBag.Chapters =
            var schools = dc.SchoolClassSubject.Where(i=>i.ClassId==ClassId&&i.SchoolId==SchoolId&&i.SubjectId==SubjectId)
                .SelectMany(o=>o.Books)
                //.Select(o => new { o = o, op = o.Schools.Any(i => i.Id == SchoolId) }).Where(i => i.op == true)
                .SelectMany(i => i.Chapters)
                .Select(i => new { Id = i.Id, Name = i.Name, BookName = i.Books.Select(p => new { BookName = p.Title }) })
                .ToList();
            ViewBag.Chapters = schools;
            if (dc.BookClassSchoolSyllabus.Any(i => i.SchoolId == SchoolId && i.ClassId == ClassId &&   i.SubjectId == SubjectId))
            {
                var re = dc.BookClassSchoolSyllabus.FirstOrDefault(i => i.SchoolId == SchoolId && i.ClassId == ClassId && i.SubjectId==SubjectId);
                return View(re);
            }
            else
            {
                var n = new BookClassSchoolSyllabus
                {
                   
                    ClassId = ClassId,
                    SubjectId=SubjectId,
                    SchoolId = SchoolId,
                    Map = "[]"
                };
                dc.BookClassSchoolSyllabus.Add(n);
                dc.SaveChanges();
            return View(n);
            }
        }
        [HttpPost]
        public ActionResult Syllabus(BookClassSchoolSyllabus model)
        {
            dc.Entry(model).State = EntityState.Modified;
            dc.SaveChanges();
            return RedirectToAction("Details", new { id = model.SchoolId });
        }
    }
}