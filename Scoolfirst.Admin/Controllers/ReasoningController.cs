﻿using Scoolfirst.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.RNM;
using Scoolfirst.Model.Provider;
using System.Net;
using System.Data.Entity;
using Scoolfirst.Model.MongoDatabase;
using Scoolfirst.Model.Notifications;

namespace Scoolfirst.Admin.Controllers
{
    public class ReasoningController : Controller
    {
        private ScoolfirstContext dc;
        public ReasoningController(ScoolfirstContext Context)
        {
            dc = Context;
        }
        // GET: Reasoning
        public ActionResult Index(int page=1, int size=10,string search="",string Level= "Beginner",string type="Series")
        {
            
            

            var List= dc.ReasoningRoots.Where(i=>i.Level==Level );
            if (type == "Series")
                List = List.Where(i => i.IsLessonOrExam != ReasoningRootType.Single);
            else
                List = List.Where(i => i.IsLessonOrExam == ReasoningRootType.Single);

            if (!string.IsNullOrEmpty(search))
                List = List.Where(i => i.Title.Contains(search));
            
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            ViewBag.Level = new SelectList(ListItem(), "Text", "Value", Level);
            return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
        }

        
        public ActionResult Create()
        {
            ViewBag.PreviousPostId = new SelectList(dc.ReasoningRoots,"Id", "Title");
            ViewBag.Level = new SelectList(ListItem(), "Text", "Value");
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ReasoningRoot model)
        {
            if (ModelState.IsValid)
            {
                dc.ReasoningRoots.Add(model);
                dc.SaveChanges();
                AddNotification(model);
                return RedirectToAction("Index");
            }
            ViewBag.PreviousPostId = new SelectList(dc.ReasoningRoots, "PreviousPostId");
            ViewBag.Level = new SelectList(ListItem(), "Text", "Value",model.Level);
            return View(model);
        }

        private static void AddNotification(ReasoningRoot model)
        {
            NotificationManagerRepo repo = new NotificationManagerRepo();
            var typeFeed = Model.Notifications.ModType.RR;

            var Nf = new Notification
            {
                Id = Guid.NewGuid().ToString(),
                Date = DateTime.UtcNow.AddDays(10),
                Group = "All",
                Message = $"New Module for reasioning Root has been added",
                Title = "Reasoning Root",
                PostId = model.Id.ToString(),
                Module = typeFeed,
                ImageUrl = "/static/img/feedIcon/ic_RnM_Foundation.png",
                Size = true,
                Link = $"/POST/{typeFeed}/{model.Id}"

            };
            //db.Notification.Add(Nf);
            repo.AddNotification(Nf);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReasoningRoot reasoningroot = dc.ReasoningRoots.Find(id);
            if (reasoningroot == null)
            {
                return HttpNotFound();
            }
            ViewBag.PreviousPostId = new SelectList(dc.ReasoningRoots, "Id", "Title", reasoningroot.Id);
            ViewBag.Level = new SelectList(ListItem(), "Text", "Value",reasoningroot.Level);
            return View(reasoningroot);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(ReasoningRoot model)
        {
            if (ModelState.IsValid)
            {
                dc.Entry(model).State= EntityState.Modified;
                dc.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PreviousPostId = new SelectList(dc.ReasoningRoots, "PreviousPostId");
            ViewBag.Level = new SelectList(ListItem(),"Text", "Value", model.Level);
            return View(model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReasoningRoot reasoningroot = dc.ReasoningRoots.Find(id);
            if (reasoningroot == null)
            {
                return HttpNotFound();
            }
            return View(reasoningroot);
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int? id)
        {
            //ReasoningRoot model = dc.ReasoningRoots.Find(id);
            //ReasoningRoot model = dc.ReasoningRoots.Include(i => i.PreviousPost).FirstOrDefault(i => i.Id == id);
            ReasoningRoot model = dc.ReasoningRoots.Include(i => i.Questions).FirstOrDefault(i => i.Id == id);
            dc.ReasoningRoots.Remove(model);
            dc.SaveChanges();
            return RedirectToAction("Index");
            
        }
        

        private List<SelectListItem> ListItem()
        {
           List<SelectListItem> List = new List<SelectListItem>{
                new SelectListItem{Text ="Beginner", Value="Beginner"},
                new SelectListItem{Text ="Learner", Value="Learner"},
                new SelectListItem{Text ="Advanced", Value="Advanced"},
            };
           
            return List;
        }
    }
}