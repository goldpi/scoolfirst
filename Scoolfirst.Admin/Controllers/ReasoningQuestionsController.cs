﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.RNM;

namespace Scoolfirst.Admin.Controllers
{
    public class ReasoningQuestionsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: ReasoningQuestions
        public ActionResult Index(long? id)
        {
            ViewBag.SetId = id;
            if (id == null)
                return View(db.ReasoningQuestions.ToList());
            //var reasoningQuestions = db.ReasoningQuestions.Include(r => r.Set);
            return View(db.ReasoningQuestions.Where(i => i.SetId==id).ToList());
        }

        // GET: ReasoningQuestions/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReasoningQuestion reasoningQuestion = db.ReasoningQuestions.Find(id);
            if (reasoningQuestion == null)
            {
                return HttpNotFound();
            }
            return View(reasoningQuestion);
        }

        // GET: ReasoningQuestions/Create
        public ActionResult Create()
        {
            ViewBag.SetId = new SelectList(db.ReasoningRoots, "Id", "Class");
            return View();
        }

        // POST: ReasoningQuestions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ReasoningQuestion reasoningQuestion)
        {
            if (ModelState.IsValid)
            {
                reasoningQuestion.Id = Guid.NewGuid();
                db.ReasoningQuestions.Add(reasoningQuestion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SetId = new SelectList(db.ReasoningRoots, "Id", "Class", reasoningQuestion.SetId);
            return View(reasoningQuestion);
        }

        // GET: ReasoningQuestions/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReasoningQuestion reasoningQuestion = db.ReasoningQuestions.Find(id);
            if (reasoningQuestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.SetId = new SelectList(db.ReasoningRoots, "Id", "Class", reasoningQuestion.SetId);
            return View(reasoningQuestion);
        }

        // POST: ReasoningQuestions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ReasoningQuestion reasoningQuestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reasoningQuestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SetId = new SelectList(db.ReasoningRoots, "Id", "Class", reasoningQuestion.SetId);
            return View(reasoningQuestion);
        }

        // GET: ReasoningQuestions/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReasoningQuestion reasoningQuestion = db.ReasoningQuestions.Find(id);
            if (reasoningQuestion == null)
            {
                return HttpNotFound();
            }
            return View(reasoningQuestion);
        }

        // POST: ReasoningQuestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ReasoningQuestion reasoningQuestion = db.ReasoningQuestions.Include(i=>i.Option).FirstOrDefault(i=>i.Id==id);
            db.ReasoningQuestions.Remove(reasoningQuestion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult CreateRnmQuestion(Guid? Id, long? sid)
        {
            ReasoningQuestion question = new ReasoningQuestion();
            ViewBag.SetId = sid;
            ViewBag.Id = Id ?? Guid.Empty;
            return View();
        }
    }
}
