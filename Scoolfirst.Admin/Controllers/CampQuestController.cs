﻿using Scoolfirst.Model.Camp;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.MCQ;
using Scoolfirst.Model.VirtualCompetetion;
using Scoolfirst.ViewModel.MCQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Scoolfirst.Admin.Controllers
{
    public class CampQuestController : ApiController
    {
        private ScoolfirstContext db = new ScoolfirstContext();
        [Route("api/CampQuest/")]
        public Scoolfirst.ViewModel.MCQ.CampQuestion Get(Guid? Id, int? sid)
        {
            if (Id == Guid.Empty)
            {

                Model.Camp.CampQuestion q = new Model.Camp.CampQuestion()
                {
                    Id = Guid.Empty,
                   
                    Query = "Query here",
                    SetId = sid??0 ,
                    Option = new List<Model.Camp.CampOptions>(),

                };
                for (int w = 1; w < 5; w++)
                {
                    q.Option.Add(new Model.Camp.CampOptions { Answer = $"Option {w}", Correct = false });
                }
                return Scoolfirst.ViewModel.MCQ.CampQuestion.VMQuestion(q);
            }

            else
            {
                {
                    var res = db.CampQuestions.Find(Id);
                    var x = db.CampOptions.Where(i => i.QuestionId == Id).ToList();

                    //res.Choices = new List<Choice>();
                    foreach (var item in x)
                    {
                        res.Option.Add(item);
                    }
                    //db.Entry(res).Collection(i => i.Choices).Load();
                    return Scoolfirst.ViewModel.MCQ.CampQuestion.VMQuestion(res);
                }

            }
        }

        [Route("api/SaveCampQuest")]
        [HttpPost]
        public IHttpActionResult Post(Model.Camp.CampQuestion question)
        {
            if (question.Id == Guid.Empty)
            {
                question.Id = Guid.NewGuid();

                foreach (var option in question.Option)
                {
                    option.Id = Guid.NewGuid();
                }
                db.CampQuestions.Add(question);


                db.SaveChanges();
            }
            else
            {
                #region QuestionModified
                var qq = new Model.Camp.CampQuestion
                {
                    Id = question.Id,
                  
                    Query = question.Query,
                    SetId = question.SetId,
                    Option = new List<Model.Camp.CampOptions>()
                };

                db.Entry(qq).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                #endregion
                var optionlist = question.Option.ToList();
                var toDel = new List<Guid>().ToList();
                var Option = db.Options.Where(i => i.McqQuestionId == question.Id).ToList();

                foreach (var item in Option)
                {
                    #region OptionsModified
                    if (optionlist.Any(i => i.Id == item.Id))
                    {
                        var ss = optionlist.First(i => i.Id == item.Id);
                        item.Answer = ss.Answer;
                        item.Correct = ss.Correct;
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    #endregion

                    else
                    #region Remove
                    {
                        toDel.Add(item.Id);
                    }
                    #endregion
                }
                foreach (var items in optionlist)
                {
                    if (items.Id == Guid.Empty)
                    {
                        items.QuestionId = qq.Id;
                        items.Id = Guid.NewGuid();
                        db.CampOptions.Add(items);
                        db.SaveChanges();
                    }
                }

                #region RemovingItem
                foreach (Guid id in toDel)
                {
                    var Id = db.Options.Find(id);
                    db.Options.Remove(Id);
                    db.SaveChanges();
                }
                #endregion
            }
            //return question.Query;
            return Json(new { data = question, Message = "Data Saved" });
        }
    }

    public class VirtualQuestController : ApiController
    {
        private ScoolfirstContext db = new ScoolfirstContext();
        [Route("api/VirtualCompetitonQuest/")]
        public Scoolfirst.ViewModel.MCQ.VirtualQuestion Get(Guid? Id, int? sid)
        {
            if (Id == Guid.Empty)
            {

                VirtualCompetetionQuestion q = new VirtualCompetetionQuestion()
                {
                    Id = Guid.Empty,
                    QueryActual="Actual Query",
                    Query = "Query here",
                    ExamId = sid ?? 0,
                    Options = new List<VirtualCompetetionOptions>(),

                };
                for (int w = 1; w < 5; w++)
                {
                    q.Options.Add(new VirtualCompetetionOptions { Answer = $"Option {w}", Correct = false });
                }
                return Scoolfirst.ViewModel.MCQ.VirtualQuestion.VMQuestion(q);
            }

            else
            {
                {
                    var res = db.VirtualCompetetionQuestions.Find(Id);
                    var x = db.VirtualCompetetionOptions.Where(i => i.QuestionId == Id).ToList();

                    //res.Choices = new List<Choice>();
                    foreach (var item in x)
                    {
                        res.Options.Add(item);
                    }
                    //db.Entry(res).Collection(i => i.Choices).Load();
                    return Scoolfirst.ViewModel.MCQ.VirtualQuestion.VMQuestion(res);
                }

            }
        }

        [Route("api/SaveVirtualCompeQuest")]
        [HttpPost]
        public IHttpActionResult Post(VirtualCompetetionQuestion question)
        {
            if (question.Id == Guid.Empty)
            {
                question.Id = Guid.NewGuid();

                foreach (var option in question.Options)
                {
                    option.Id = Guid.NewGuid();
                }
                db.VirtualCompetetionQuestions.Add(question);


                db.SaveChanges();
            }
            else
            {
                #region QuestionModified
                var qq = new VirtualCompetetionQuestion
                {
                    Id = question.Id,
                    QueryActual = question.QueryActual,
                    Query = question.Query,
                    ExamId = question.ExamId,
                    Options = new List<VirtualCompetetionOptions>(),
                    ShowSolution = question.ShowSolution,
                    ShortOrder = question.ShortOrder,
                    Solution = question.Solution

                };

                db.Entry(qq).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                #endregion
                var optionlist = question.Options.ToList();
                var toDel = new List<Guid>().ToList();


                var Option = db.VirtualCompetetionOptions.Where(i => i.QuestionId == question.Id).ToList();

                foreach (var item in Option)
                {
                    #region OptionsModified
                    if (optionlist.Any(i => i.Id == item.Id))
                    {
                        var ss = optionlist.First(i => i.Id == item.Id);
                        item.Answer = ss.Answer;
                        item.Correct = ss.Correct;
                        
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    #endregion

                    else
                    #region Remove
                    {
                        toDel.Add(item.Id);
                    }
                    #endregion
                }
                foreach (var items in optionlist)
                {
                    if (items.Id == Guid.Empty)
                    {
                        items.QuestionId = qq.Id;
                        items.Id = Guid.NewGuid();
                        db.VirtualCompetetionOptions.Add(items);
                        db.SaveChanges();
                    }
                }

                #region RemovingItem
                foreach (Guid id in toDel)
                {
                    var Id = db.Options.Find(id);
                    db.Options.Remove(Id);
                    db.SaveChanges();
                }
                #endregion
            }
            //return question.Query;
            return Json(new { data = Scoolfirst.ViewModel.MCQ.VirtualQuestion.VMQuestion(question), Message = "Data Saved" });
        }
    }
}
