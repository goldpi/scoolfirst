﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.Admin.Models;
using Scoolfirst.Model.Provider;
using Scoolfirst.Model.MongoDatabase;
using Scoolfirst.Model.Notifications;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class SyllabusDatesController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: SyllabusDates
        public ActionResult Index(int page=1,int size=10,string search="")
        {
            IQueryable<SyllabusDate> List;
            if (!string.IsNullOrEmpty(search))
                List = db.SyllabusDates.Where(i => i.Title.Contains(search));
            else
                List = db.SyllabusDates;
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderBy(i=>i.Id).Skip((page-1)*size).Take(size).ToList());
        }

        // GET: SyllabusDates/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SyllabusDate syllabusDate = db.SyllabusDates.Find(id);
            if (syllabusDate == null)
            {
                return HttpNotFound();
            }
            return View(syllabusDate);
        }

        // GET: SyllabusDates/Create
        public ActionResult Create()
        {
            ViewBag.Class = new SelectList(db.Classes, "Id", "RomanDisplay");
            ViewBag.School = new SelectList(db.Schools, "Id", "SchoolName");
            return View();
        }

        // POST: SyllabusDates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
       [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Id,Title,Class,School,From,To,Content")] SyllabusDate syllabusDate)
        {
            if (ModelState.IsValid)
            {
                syllabusDate.Id = Guid.NewGuid();
                
                db.SyllabusDates.Add(syllabusDate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Class = new SelectList(db.Classes, "Id", "RomanDisplay", syllabusDate.Class);
            ViewBag.School = new SelectList(db.Schools, "Id", "SchoolName", syllabusDate.School);
            return View(syllabusDate);
        }

        // GET: SyllabusDates/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SyllabusDate syllabusDate = db.SyllabusDates.Find(id);
            if (syllabusDate == null)
            {
                return HttpNotFound();
            }
            ViewBag.Class = new SelectList(db.Classes, "Id", "RomanDisplay",syllabusDate.Class);
            ViewBag.School = new SelectList(db.Schools, "Id", "SchoolName",syllabusDate.School);
            return View(syllabusDate);
        }

        // POST: SyllabusDates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,Title,Class,School,From,To,Content")] SyllabusDate syllabusDate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(syllabusDate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Class = new SelectList(db.Classes, "Id", "RomanDisplay", syllabusDate.Class);
            ViewBag.School = new SelectList(db.Schools, "Id", "SchoolName", syllabusDate.School);
            return View(syllabusDate);
        }
        //private IEnumerable<Object> ClassList()
        //{
        //    foreach (var item in db.Classes)
        //    {
        //        yield return new { Value = item.Id.ToString(), Name = item.RomanDisplay };
        //    }
        //}
        // GET: SyllabusDates/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SyllabusDate syllabusDate = db.SyllabusDates.Find(id);
            if (syllabusDate == null)
            {
                return HttpNotFound();
            }
            return View(syllabusDate);
        }

        // POST: SyllabusDates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            //SyllabusDate syllabusDate = db.SyllabusDates.Find(id);
            var quizs = db.Quizes.Where(i => i.SyllabusId == id).ToList();
            foreach(var i in quizs)
            {
                i.Questions.Clear();
            }
            db.Quizes.RemoveRange(quizs);
            db.SaveChanges();
            SyllabusDate syllabusDate = db.SyllabusDates.Find(id);
            db.SyllabusDates.Remove(syllabusDate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult CreateQuiz(Guid? Id)
        {
            ViewBag.Id = Id ?? Guid.Empty;
            return View();
        }

        public ActionResult Quiz(Guid? sid,int page=1,int size=10,string search="")
        {
            ViewBag.SyllabusId = sid;
            IQueryable<Quiz> List;
            if (!string.IsNullOrEmpty(search))
                List = db.Quizes.Where(i => i.Name.Contains(search) || i.Syllabus.Title.Contains(search));
            else
                List = db.Quizes.Where(i => i.SyllabusId == sid);
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderBy(i=>i.Id).Skip((page-1)*size).Take(size).ToList());
        }

        [HttpPost]
        public ActionResult CreateQuiz(QuizEntryVm Question,int? id)
        {
            ViewBag.Quizid = id;
            var p = new Quiz
            {
                Class = Question.ClassId.ToString(),
                Subject = Question.Subject,
                Details = Question.Details,
                Id = Guid.NewGuid(),
                Questions = new List<Questions>(),
                Name = Question.Title,
                NoOfQuestion = Question.Questions.Count(),
                On = DateTime.UtcNow,
                School = Question.SchoolId.ToString()
            };
            if (Question.SyllabusId != Guid.Empty)
            {
                p.SyllabusId = Question.SyllabusId;
            }

            
            db.Quizes.Add(p);
            db.SaveChanges();
            foreach(var i in Question.Questions)
            {
                var ip = db.Questions.Find(i.Id);
                p.Questions.Add(ip);
                //ip.Quiz.Add(p);
                db.Entry(p).State = EntityState.Modified;
                db.SaveChanges();
            }
            NotificationManagerRepo repo = new NotificationManagerRepo();
            var typeFeed = Model.Notifications.ModType.WRKSHT;

            var Nf = new Notification
            {
                Id = Guid.NewGuid().ToString(),
                Date = DateTime.UtcNow.AddDays(10),
                Group = p.Class,
                Message = $"New Work Sheet has been added",
                Title = "Work Book",
                PostId = p.Id.ToString(),
                Module = typeFeed,
                ImageUrl = "/static/img/feedIcon/ic_my_workbook.png",
                Size = true,
                Link = $"/POST/{typeFeed}/{p.Id}"

            };
            //db.Notification.Add(Nf);
            repo.AddNotification(Nf);
            return Json(new { data = Question, Measage = "Data Saved" });

        }
        public ActionResult DeleteQuiz(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quiz quiz = db.Quizes.Find(id);
            if (quiz == null)
            {
                return HttpNotFound();
            }
            return View(quiz);
        }

        // POST: SyllabusDates/Delete/5
        [HttpPost, ActionName("DeleteQuiz")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteQuizConfirmed(Guid id)
        {

            Quiz quiz = db.Quizes.Include(i => i.Questions).FirstOrDefault(i => i.Id == id);
            db.Quizes.Remove(quiz);
            db.SaveChanges();
            return RedirectToAction("Quiz", new { sid=quiz.SyllabusId});
        }

    }
}
