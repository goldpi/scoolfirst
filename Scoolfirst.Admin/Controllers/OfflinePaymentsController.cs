﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Payments;
using Scoolfirst.Model.Provider;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class OfflinePaymentsController : Controller
    {

        private ScoolfirstContext dc = new ScoolfirstContext();


        // GET: OfflinePayments
        public ActionResult Index(int page=1,int size=20,bool paid=false)
        {
            var data = dc.OfflinePayments.Include(i => i.User).Where(u => u.Paid == paid);
            ViewBag.Pager = Pagers.Items(data.Count()).PerPage(size).Move(page).Segment(20).Center();
            data = data.OrderByDescending(i => i.RequestDate).Skip((page-1) * size).Take(size);
            return View(data.ToList());
        }

        // GET: OfflinePayments/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: OfflinePayments/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: OfflinePayments/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: OfflinePayments/Edit/5
        public ActionResult Edit(Guid id)
        {
            var item = dc.OfflinePayments.Find(id);
            return View(item);
        }

        // POST: OfflinePayments/Edit/5
        [HttpPost]
        public ActionResult Edit(OfflinePayment payment)
        {
            try
            {
                dc.Entry(payment).State = EntityState.Modified;
                dc.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: OfflinePayments/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: OfflinePayments/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
