﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class SummariesController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Summaries
        public ActionResult Index()
        {
            return View(db.Summary.ToList());
        }



        // GET: Summaries/Create
        public ActionResult CreateWorld()
        {
            ViewBag.Month = new SelectList(DDLMonth(), "Text", "Value");
            ViewBag.Year = new SelectList(DDLYear(), "Text", "Value");
            return View(new Summary { Type = SummaryType.WORLD_AROUND, School = 0, Class = 0 });
        }

        // POST: Summaries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateWorld([Bind(Include = "Id,Type,Summarizer,Visualizer,Class,Month,Year,School")] Summary summary)
        {
            if (ModelState.IsValid)
            {
                Save(summary);
                return RedirectToAction("Index");
            }
            ViewBag.Month = new SelectList(DDLMonth(), "Text", "Value",summary.Month);
            ViewBag.Year = new SelectList(DDLYear(), "Text", "Value",summary.Year);
            return View(summary);
        }



        // GET: Summaries/Create
        public ActionResult CreateSchool()
        {
            return View(new Summary { Type = SummaryType.SCHOOL, School = 0, Class = 0 });
        }

        // POST: Summaries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatSchool([Bind(Include = "Id,Type,Summarizer,Visualizer,Month,Year,Class,School")] Summary summary)
        {
            if (ModelState.IsValid)
            {
                Save(summary);
                return RedirectToAction("Index");
            }

            return View(summary);
        }

        public ActionResult CreateUpgrade()
        {
            return View(new Summary { Type = SummaryType.UPGRADE, School = 0, Class = 0 });
        }

        // POST: Summaries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUpgrade([Bind(Include = "Id,Type,Summarizer,Visualizer,Month,Year,Class,School")] Summary summary)
        {
            if (ModelState.IsValid)
            {
                Save(summary);
                return RedirectToAction("Index");
            }

            return View(summary);
        }


        private void Save(Summary summary)
        {
            if (summary.Id == Guid.Empty)
            {
                summary.Id = Guid.NewGuid();
                db.Summary.Add(summary);
                db.SaveChanges();
            }
            else
            {
                db.Entry(summary).State = EntityState.Modified;
                db.SaveChanges();
            }
        }






        // GET: Summaries/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Summary summary = db.Summary.Find(id);
            if (summary == null)
            {
                return HttpNotFound();
            }
            return View(summary);
        }

        // GET: Summaries/Create
        public ActionResult Create()
        {
            ViewBag.Month = new SelectList(DDLMonth(), "Text", "Value");
            ViewBag.Year = new SelectList(DDLYear(), "Text", "Value");
            return View();
        }

        // POST: Summaries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Type,Summarizer,Visualizer,Month,Year,Class,School")] Summary summary)
        {
            if (ModelState.IsValid)
            {
                Save(summary);
                return RedirectToAction("Index");
            }
            ViewBag.Month = new SelectList(DDLMonth(), "Text", "Value",summary.Month);
            ViewBag.Year = new SelectList(DDLYear(), "Text", "Value",summary.Year);
            return View(summary);
        }

        // GET: Summaries/Edit/5
        public ActionResult Edit(Guid? id)
        {
           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Summary summary = db.Summary.Find(id);
            if (summary == null)
            {
                return HttpNotFound();
            }
            ViewBag.Month = new SelectList(DDLMonth(), "Text", "Value",summary.Month);
            ViewBag.Year = new SelectList(DDLYear(), "Text", "Value",summary.Year);
            return View(summary);
        }

        // POST: Summaries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Type,Summarizer,Visualizer,Month,Year,Class,School")] Summary summary)
        {
            if (ModelState.IsValid)
            {
                db.Entry(summary).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Month = new SelectList(DDLMonth(), "Text", "Value",summary.Month);
            ViewBag.Year = new SelectList(DDLYear(), "Text", "Value",summary.Year);
            return View(summary);
        }

        // GET: Summaries/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Summary summary = db.Summary.Find(id);
            if (summary == null)
            {
                return HttpNotFound();
            }
            return View(summary);
        }

        // POST: Summaries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Summary summary = db.Summary.Find(id);
            db.Summary.Remove(summary);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public IEnumerable<object> DDLMonth()
        {
            for(int i=1;i<=12;i++)
            {
                yield return new { value = i, Text = i };
            }
        }
        public IEnumerable<object> DDLYear()
        {
            int currentyear = (DateTime.Now.Year-2);
            int endyear = currentyear + 4;
            for(int i=currentyear;i<=endyear;i++)
            {
                yield return new  { Text = i, Value = i };
            }
        }
    }
}
