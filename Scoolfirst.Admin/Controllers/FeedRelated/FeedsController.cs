﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Feed;
using Scoolfirst.Model.MongoDatabase;
using Scoolfirst.Model.Provider;
using Scoolfirst.Model.Notifications;

namespace Scoolfirst.Admin.Controllers
{
    
    public class FeedsController : BaseController
    {
        private ScoolfirstContext db = new ScoolfirstContext();
        public static TimeZoneInfo INDIAN_ZONE =
        TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        // GET: Feeds
        public ActionResult Index(int page = 1, int size = 20,string search="")
        {
            IQueryable<Feeds> List;
            if (!string.IsNullOrEmpty(search))
                List = db.Feeds.Where(i => i.Title.Contains(search));
            else
             List = db.Feeds.Include(f => f.Group).Include(f => f.Post);
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderByDescending(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
        }

        // GET: Feeds/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Feeds feeds = db.Feeds.Find(id);
            if (feeds == null)
            {
                return HttpNotFound();
            }
            return View(feeds);
        }

        // GET: Feeds/Create
        public ActionResult Create()
        {
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name");
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title");
            return View();
        }

        // POST: Feeds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Feeds feeds,string ret="")
        {
            if (ModelState.IsValid)
            {
                feeds.Id = Guid.NewGuid();
                db.Feeds.Add(feeds);
                db.SaveChanges();
                if (ret == "")
                    return RedirectToAction("Index");
                else Redirect(ret);
            }

            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", feeds.GroupId);
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", feeds.PostId);
            ViewBag.ret = ret;
            return View(feeds);
        }

        // GET: Feeds/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Feeds feeds = db.Feeds.Find(id);
            if (feeds == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", feeds.GroupId);
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", feeds.PostId);
            return View(feeds);
        }

        // POST: Feeds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Feeds feeds)
        {
            if (ModelState.IsValid)
            {
                db.Entry(feeds).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", feeds.GroupId);
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", feeds.PostId);
            return View(feeds);
        }

        // GET: Feeds/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Feeds feeds = db.Feeds.Find(id);
            if (feeds == null)
            {
                return HttpNotFound();
            }
            return View(feeds);
        }

        // POST: Feeds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Feeds feeds = db.Feeds.Find(id);
            db.Feeds.Remove(feeds);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Here Id is a post ID
        /// </summary>
        /// <param name="id"></param>
        public ActionResult Schedule(int id)
        {
            ViewBag.ret = Request.UrlReferrer.ToString();
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name",id);
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title");
            if (Request.IsAjaxRequest())
                return PartialView("create");
            else
                return View("create");
            //return View();
        }

        [HttpPost]
        public ActionResult AddtoFeed(string Id,string type,string title,string body,string imageurl, string Group ,string date, string time,bool Pinned=false)
        {
            try
            {

                Feeds feed = new Feeds
                {
                    GroupId = int.Parse(Group),
                    Id = Guid.NewGuid(),
                    Pinned = Pinned,
                    OnDateTime = DateTime.Parse(date + " " + time),
                    PostId = long.Parse(Id),
                    Title = title,

                };
                db.Feeds.Add(feed);
                db.SaveChanges();
                db.Entry(feed).Reference(i => i.Group).Load();
                db.Entry(feed).Reference(I => I.Post).Load();
                NotificationManagerRepo repo = new NotificationManagerRepo();
                var typeFeed = feed.Post.Type == PostType.CREATIVE_CANVAS ?
                    Model.Notifications.ModType.CC : feed.Post.Type == PostType.KNOWLEDGE_NUTRITION ? Model.Notifications.ModType.KN : Model.Notifications.ModType.CC;
                var Nf = new Notification
                {
                    Id=Guid.NewGuid().ToString(),
                    Date = feed.OnDateTime,
                    Group = feed.Group.Name,
                    Message = body,
                    Title = title,
                    PostId = Id,
                    Module =typeFeed,
                    ImageUrl = imageurl,
                    Size = !(string.IsNullOrEmpty(imageurl) && string.IsNullOrWhiteSpace(imageurl)),
                    Link=$"/POST/{typeFeed}/{Id}"

                };
                //db.Notification.Add(Nf);
                repo.AddNotification(Nf);
            return Json(new { message = "Added to feed, will appear on : "+Nf.Date, Class = "alert-success" });
            }
            catch(Exception ex)
            {
                return Json(new { message = ex.Message, Class = "alert-danger" });
            }
            
            
           
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
