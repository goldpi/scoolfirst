﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Feed;
using Scoolfirst.ViewModel.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    
    public class PostController : BaseController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();
        // GET: Post
        public ActionResult Index(PostType? Type, int page = 1, int size = 10, string search = "")
        {

            IQueryable<Post> Listt;
            if (Type.HasValue)
                Listt = dc.Posts.Where(I => I.Type == Type);
            else
                Listt = dc.Posts;
            if (string.IsNullOrEmpty(search) && string.IsNullOrWhiteSpace(search))
                Listt = from l in Listt
                        select l;
            else
                Listt = from l in Listt
                        where l.Title.Contains(search)
                        select l;
            //  var List = Type.HasValue  ?  Listt.Where(i => i.Type == Type):Listt;
            ViewBag.Group = new SelectList(dc.Groups, "Id", "Name", 1);
            ViewBag.Pager = Pagers.Items(Listt.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(Listt.OrderByDescending(i => i.Id).Skip((page - 1) * size).Take(size).ToList());

        }

        public ActionResult Edit(long Id, PostType Type = PostType.MUST_REMEMBER_TRICKS)
        {
            Post m = new Post { Id = 0, Type = Type, OnDateTIme = DateTime.Now };
            if (Id != 0)
                m = dc.Posts.Find(Id);
            if (Request.IsAjaxRequest())
                return PartialView(m);

            return View(m);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(Post model)
        {
            var req = Request.IsAjaxRequest();
            var user = User.Identity.Name;
            var uid = dc.Users.FirstOrDefault(i => i.UserName == user);
            model.UserId = uid.Id;
            bool p;
            if (ModelState.IsValid)
            {
                if (model.Id != 0)
                    dc.Entry(model).State = System.Data.Entity.EntityState.Modified;
                else
                {
                    model.OnDateTIme = DateTime.UtcNow;
                    dc.Posts.Add(model);
                }
                dc.SaveChanges();
                if ((p = Request.IsAjaxRequest()))
                    return Json(new { success = 1, msg = "Saved Changes" }, JsonRequestBehavior.AllowGet);
                else
                {
                    TempData["Sucess"] = "Saved Changes";
                    //return Redirect(Request.UrlReferrer.ToString());
                    return RedirectToAction("Index" ,new { type = model.Type });
                }

            }

            if (Request.IsAjaxRequest())
                return PartialView(model);
            return View(model);
        }

        public ActionResult Delete(long? id)
        {
            Post value = null;
            if (id == null)
                return new HttpStatusCodeResult(404, "Not Found");
            if ((value = dc.Posts.Find(id)) != null)
            {
                dc.Entry(value).Collection(i => i.Images).Load();
                dc.Entry(value).Collection(i => i.Videos).Load();
                var p = dc.Feeds.Include(i => i.Stared).Include(i => i.Likes).Include(i => i.Comments).Where(i => i.PostId == id);
                dc.Feeds.RemoveRange(p);
                dc.PostVideo.RemoveRange(value.Videos);
                dc.PostImages.RemoveRange(value.Images);
                dc.Posts.Remove(value);

                dc.SaveChanges();

                if (Request.IsAjaxRequest())
                    return Json(new { success = 1, msg = "" }, JsonRequestBehavior.AllowGet);
                else
                {
                    TempData["Sucess"] = "Deleted";
                    return Redirect(Request.UrlReferrer.ToString());
                }
            }
            else
                return new HttpStatusCodeResult(404, "Not Found");

        }



        public ActionResult CreateBreakBrainFast()
        {
            return View(new KnowledgeNutritionViewModel
            { Id = 0 });
        }


        public ActionResult EditBreakBrainFast(int Id)
        {
            try
            {
                var post = dc.BreakBrainFastFromId(Id);
                return View("CreateBreakBrainFast", post);
            }
            catch
            {
                return new HttpStatusCodeResult(404, "Not Found");
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateBreakBrainFast(KnowledgeNutritionViewModel post)
        {
            if (ModelState.IsValid)
            {
                if (post.AddOrSave(dc, User.Identity.Name))
                {
                    
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("Title", "Post Was not saved !");
                return View(post);
            }
            return View(post);
        }

        public ActionResult CreativeCanvasCreate(int id = 0)
        {
            if (id == 0)
                return View(new CreativeCanvasViewModel() { OnDateTime = DateTime.UtcNow });
            else
            {
                var p = dc.Posts.Include(i => i.Images).FirstOrDefault(i => i.Id == id).ToExperimentalLearningViewModel();
                if (p != null)
                    return View(p);
                else
                    return HttpNotFound();
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreativeCanvasCreate(CreativeCanvasViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Save(dc, User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(model);
        }



    }
}