﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Admin.Controllers
{
    public class TinyMceController : Controller
    {
        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
            filterContext.HttpContext.Response.ContentType = "text/javascript";
        }
        // GET: TinyMce
        public ActionResult settings()
        {
            
            return PartialView();
        }
        //[Route("RNMQuiz")]
        public ActionResult RNMAngular()
        {
            return PartialView();
        }

        public ActionResult PAAngular()
        {
            return PartialView();
        }

        public ActionResult PlayAngular()
        {
            return PartialView();
        }

        public ActionResult CampAngular(int page = 0)
        {
            if (page == 0)
                ViewBag.Page = "";
            else
            {
                using (var data = new Scoolfirst.Model.Context.ScoolfirstContext())
                {
                    var p = data.CampPosts.Find(page);
                    ViewBag.Page = p.Page;
                }
            }
            return PartialView();
        }

        public ActionResult CampQuestionAngular()
        {
            return PartialView();
        }

        public ActionResult VirtualCompetionAngular(int page=0)
        {
            
            return PartialView();
        }
    }
}