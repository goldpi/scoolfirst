﻿using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.ViewModel.Common.Api;
using Scoolfirst.ViewModel.WorkBook.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Scoolfirst.Admin.Controllers
{
    public class QuizAppController : ApiController
    {
        ScoolfirstContext dc = new ScoolfirstContext();
        [Route("api/Schools")]
        [HttpGet]
        public IEnumerable<Schools> GetSchools()
        {
            var list = dc.Schools;
            return Schools.FromListSchool(list);

        }
        [Route("api/Sub")]
        [HttpGet]
        public IEnumerable<Subject> sub()
        {
            dc.Configuration.ProxyCreationEnabled = false;
            dc.Configuration.LazyLoadingEnabled = false;
            return dc.Subjects.ToList();
        }
        [Route("api/Class")]
        [HttpGet]
        public IEnumerable<Class> GetClass()
        {
            var list = dc.Classes;
            return Class.FromListClasses(list);


        }
        [Route("api/WB/GetBooksOfClass/")]
        [HttpGet]
        public object GetBookQuestion(int sid, int cid, string SubjectName)
        {
            return WB.GetBooksBySubjectSQL(dc, sid, cid, SubjectName);
        }

    }
}