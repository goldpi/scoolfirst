﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.PeriodicAssesment;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class PAQuestionsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: PAQuestions
        public ActionResult Index(int Id,int page=1,int size=30)
        {
            var pAQuestions = db.PAQuestions.Include(p => p.Set).Where(i=>i.SetId ==Id);
            if (Request.QueryString["search"] != null)
            {
                var search = Request.QueryString["search"];
                pAQuestions = pAQuestions.Where(i => i.Query.Contains(search));
            }
            ViewBag.Pager = Pagers.Items(pAQuestions.Count()).PerPage(size).Move(page).Segment(20).Center();

            return View(pAQuestions.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
            
        }

        // GET: PAQuestions/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PAQuestion pAQuestion = db.PAQuestions.Find(id);
            if (pAQuestion == null)
            {
                return HttpNotFound();
            }
            return View(pAQuestion);
        }

        // GET: PAQuestions/Create
        public ActionResult Create(long setId)
        {
            
            return View();
        }

        // POST: PAQuestions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Query,SetId,ShortOrder,Module")] PAQuestion pAQuestion)
        {
            if (ModelState.IsValid)
            {
                pAQuestion.Id = Guid.NewGuid();
                db.PAQuestions.Add(pAQuestion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SetId = new SelectList(db.PARoots, "Id", "Title", pAQuestion.SetId);
            return View(pAQuestion);
        }

        // GET: PAQuestions/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PAQuestion pAQuestion = db.PAQuestions.Find(id);
            if (pAQuestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.SetId = new SelectList(db.PARoots, "Id", "Title", pAQuestion.SetId);
            return View(pAQuestion);
        }

        // POST: PAQuestions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Query,SetId,ShortOrder,Module")] PAQuestion pAQuestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pAQuestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SetId = new SelectList(db.PARoots, "Id", "Title", pAQuestion.SetId);
            return View(pAQuestion);
        }

        // GET: PAQuestions/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PAQuestion pAQuestion = db.PAQuestions.Find(id);
            if (pAQuestion == null)
            {
                return HttpNotFound();
            }
            return View(pAQuestion);
        }

        // POST: PAQuestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            PAQuestion pAQuestion = db.PAQuestions.Find(id);
            db.PAQuestions.Remove(pAQuestion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
