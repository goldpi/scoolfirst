﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class GeneralKnowledgeController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: GeneralKnowledge
        public ActionResult Index(int page = 1, int size = 10, string search = "")
        {
            IQueryable<GeneralKnowledge> List;
            if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search))
                List = db.GeneralKnowledges.Include(r => r.Class).Where(i => i.Title.Contains(search));

            else
                List = db.GeneralKnowledges.Include(r => r.Class);
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            var res = List.OrderByDescending(i => i.Id).Skip((page - 1) * size).Take(size).ToList();
            return View(res.Select(i => {
                return new GeneralKnowledge
                {
                    Class = i.Class,
                    ClassId = i.ClassId,
                    Content = i.Content,
                    Id = i.Id,
                    Title = i.Title ?? "",
                    ImageUrl = i.ImageUrl ?? "",
                    On = i.On,
                    VideoUrl = i.VideoUrl ?? "",
                    Subject=i.Subject
                };
            })
            );
            
        }

        // GET: GeneralKnowledge/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneralKnowledge generalKnowledge = db.GeneralKnowledges.Find(id);
            if (generalKnowledge == null)
            {
                return HttpNotFound();
            }
            return View(generalKnowledge);
        }

        // GET: GeneralKnowledge/Create
        public ActionResult Create()
        {
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay");
            return View();
        }

        // POST: GeneralKnowledge/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Id,ClassId,Title,Content,ImageUrl,VideoUrl,Pinned,On,Subject")] GeneralKnowledge generalKnowledge)
        {
            if (ModelState.IsValid)
            {
                db.GeneralKnowledges.Add(generalKnowledge);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", generalKnowledge.ClassId);
            return View(generalKnowledge);
        }

        // GET: GeneralKnowledge/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneralKnowledge generalKnowledge = db.GeneralKnowledges.Find(id);
            if (generalKnowledge == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", generalKnowledge.ClassId);
            return View(generalKnowledge);
        }

        // POST: GeneralKnowledge/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,ClassId,Title,Content,ImageUrl,VideoUrl,Pinned,On,Subject")] GeneralKnowledge generalKnowledge)
        {
            if (ModelState.IsValid)
            {
                db.Entry(generalKnowledge).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", generalKnowledge.ClassId);
            return View(generalKnowledge);
        }

        // GET: GeneralKnowledge/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneralKnowledge generalKnowledge = db.GeneralKnowledges.Find(id);
            if (generalKnowledge == null)
            {
                return HttpNotFound();
            }
            return View(generalKnowledge);
        }

        // POST: GeneralKnowledge/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GeneralKnowledge generalKnowledge = db.GeneralKnowledges.Find(id);
            db.GeneralKnowledges.Remove(generalKnowledge);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
