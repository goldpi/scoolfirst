﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Slides;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class SliderSliceController : Controller
    {
        ScoolfirstContext db = new ScoolfirstContext();

        // GET: /FrontStore/SliderSlice/
        public ActionResult Index(int? id)
        {
            if (id == null)
                return RedirectToAction("Index", "slider");
            ViewBag.sliderid = id;
            List<SelectListItem> Target = new List<SelectListItem>();
            Target.Add(new SelectListItem { Text = "_self", Value = "_self" });
            Target.Add(new SelectListItem { Text = "_blank", Value = "_blank" });
            ViewBag.Target = new SelectList(Target, "Value", "Text");
          


            var item = db.SliderSlices.Include(i => i.Slider
                ).Where(i => i.Slider.Id == id).OrderBy(i=>i.slidingOrder).ToList();
            
            return View(item.ToList());
        }

        // POST: /FrontStore/SliderSlice/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Id,Title,Description,Link,Image,UpdateOn")] SliderSlice sliderslice)
        {
            if (ModelState.IsValid)
            {
                SliderSlice NewSlice = new SliderSlice();
                NewSlice.Image = sliderslice.Image;
                NewSlice.Title = sliderslice.Title;
                NewSlice.Description = sliderslice.Description;
                NewSlice.Link = sliderslice.Link;
                NewSlice.Target = sliderslice.Target;
                NewSlice.CreatedOn = DateTime.UtcNow;
                NewSlice.UpdateOn = DateTime.UtcNow;

                var data = db.Sliders.First(i => i.Id == sliderslice.Id);
                data.Slices.Add(NewSlice);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        // GET: /FrontStore/SliderSlice/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SliderSlice sliderslice =db.SliderSlices.Find(id);
            if (sliderslice == null)
            {
                return HttpNotFound();
            }
            return View(sliderslice);
        }

        // GET: /FrontStore/SliderSlice/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: /FrontStore/SliderSlice/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Description,Link,Image,UpdateOn")] SliderSlice sliderslice)
        {
            if (ModelState.IsValid)
            {
                db.SliderSlices.Add(sliderslice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return PartialView(sliderslice);
        }

        // GET: /FrontStore/SliderSlice/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SliderSlice sliderslice = db.SliderSlices.Find(id);
            if (sliderslice == null)
            {
                return HttpNotFound();
            }
            return View(sliderslice);
        }

        // POST: /FrontStore/SliderSlice/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]

        public ActionResult Edit(SliderSlice sliderslice)
        {
            if (ModelState.IsValid)
            {
                var NewSlice = db.SliderSlices.FirstOrDefault(i => i.Id == sliderslice.Id);
                if (NewSlice != null)
                {
                    NewSlice.Image = sliderslice.Image;
                    NewSlice.Title = sliderslice.Title;
                    NewSlice.Description = sliderslice.Description;
                    NewSlice.Link = sliderslice.Link;
                    NewSlice.Target = sliderslice.Target;
                    NewSlice.UpdateOn = DateTime.UtcNow;

                    //db.Entry(NewSlice).State = EntityState.Modified;
                    db.SaveChanges();

                }



                return RedirectToAction("Index", new { id = sliderslice.Slider.Id });
            }
            return RedirectToAction("Index", new { id = sliderslice.Slider.Id });
        }

        // GET: /FrontStore/SliderSlice/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SliderSlice sliderslice = db.SliderSlices.Find(id);
            if (sliderslice == null)
            {
                return HttpNotFound();
            }
            return View(sliderslice);
        }

        // POST: /FrontStore/SliderSlice/Delete/5
        [HttpPost, ActionName("Delete")]

        public ActionResult DeleteConfirmed(int Id)
        {
            int Sliderid = 0;

            SliderSlice sliderslice = db.SliderSlices.Include(i => i.Slider).FirstOrDefault(i => i.Id == Id);
            Sliderid = sliderslice.Slider.Id;
            db.SliderSlices.Remove(sliderslice);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = Sliderid });
        }

        public ActionResult ImagePicker()
        {
            return PartialView();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}