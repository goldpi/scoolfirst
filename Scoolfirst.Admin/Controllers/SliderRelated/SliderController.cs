﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Provider;
using Scoolfirst.Model.Slides;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class SliderController : Controller
        {
            ScoolfirstContext db = new ScoolfirstContext();

            // GET: /FrontStore/Slider/
            public ActionResult Index()
            {
                return View( db.Sliders.ToList());
            }

            // GET: /FrontStore/Slider/Details/5
            public ActionResult Details(int? id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Slider slider =  db.Sliders.Find(id);
                if (slider == null)
                {
                    return HttpNotFound();
                }
                return View(slider);
            }

            // GET: /FrontStore/Slider/Create
            public ActionResult Create()
            {
                if (Request.IsAjaxRequest())
                    return PartialView();
                else return View();
            }

            // POST: /FrontStore/Slider/Create
            // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
            // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult Create([Bind(Include = "Id,SliderName,Status,UpdateOn")] Slider slider)
            {
                if (ModelState.IsValid)
                {
                    slider.UpdateOn = DateTime.UtcNow;
                    db.Sliders.Add(slider);

                     db.SaveChanges();
                    if (Request.IsAjaxRequest())
                    {
                        return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
                    }

                    return RedirectToAction("Index");
                }

                if (Request.IsAjaxRequest())
                    return PartialView(slider);
                else return View(slider);

            }

        [HttpPost]
        public ActionResult SaveOrder(int Id,string Order)
        {
            var arr = Split(Order);
            int count = 0;
            foreach(var i in arr)
            {
                var pp = db.SliderSlices.Find(i);
                pp.slidingOrder = count++;
                db.SaveChanges();
            }

            TempData["Saved"] = "Saved Succesfully";
            return RedirectToAction("Index", "SliderSlice", new { Id = Id });
        }

        private IEnumerable<int> Split(string input)
        {
            var arr = input.Split('|');
            foreach (var item in arr)
            {
                int val;
                try {
                    val= int.Parse(item);
                }
                catch
                {
                    continue;
                }
                yield return val;
            }
        } 

            // GET: /FrontStore/Slider/Edit/5
            public ActionResult Edit(int? id)
            {
            
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Slider slider =  db.Sliders.Find(id);
                if (slider == null)
                {
                    return HttpNotFound();
                }
                return PartialView(slider);
            }

            // POST: /FrontStore/Slider/Edit/5
            // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
            // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult Edit([Bind(Include = "Id,SliderName,Status,UpdateOn")] Slider slider)
            {
                if (ModelState.IsValid)
                {
                slider.UpdateOn = DateTime.UtcNow;
                
                    db.Entry(slider).State = EntityState.Modified;
                     db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(slider);
            }

            // GET: /FrontStore/Slider/Delete/5
            public ActionResult Delete(int? id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Slider slider =  db.Sliders.Find(id);
                if (slider == null)
                {
                    return HttpNotFound();
                }
                return PartialView(slider);
            }

            // POST: /FrontStore/Slider/Delete/5
            [HttpPost, ActionName("Delete")]
            [ValidateAntiForgeryToken]
            public ActionResult DeleteConfirmed(int id)
            {
                Slider slider =  db.Sliders.Find(id);
            if(slider.IsSytemDefine)
            {
                if (Request.IsAjaxRequest())
                {
                    ServerSideCallback<Slider> callback = new ServerSideCallback<Slider>();
                    this.Warning("Slider", "Slider is system define and can't be deleted.");
                    callback.callbacks.Add(new Function { function = "reload", parameters = new[] { "Slider", "Slider is system define and can't be deleted." } });
                    return Json(callback, JsonRequestBehavior.AllowGet);
                }
                return RedirectToAction("Index");
            
             }
                db.Entry(slider).Collection(i => i.Slices).Load();
                db.SliderSlices.RemoveRange(slider.Slices);
     
           
                db.Sliders.Remove(slider);
                db.SaveChanges();

            if (Request.IsAjaxRequest())
            {
                ServerSideCallback<Slider> callback = new ServerSideCallback<Slider>();
                this.Success("Slider", "Slider deleted.");
                callback.callbacks.Add(new Function { function = "reload", parameters = new[] { "Slider", "Slider deleted." } });

                return Json(callback, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index");
            }

            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                base.Dispose(disposing);
            }
        }
   
}