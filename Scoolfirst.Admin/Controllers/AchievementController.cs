﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using System.Net;

using Scoolfirst.Model.Common;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class AchievementController : Controller
    {
        private ScoolfirstContext db;
        public AchievementController(ScoolfirstContext Context)
        {
            db = Context;
        }
        // GET: Achievement
        public ActionResult Index()
        {
            return View(db.Achievements.ToList());
        }
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Achievement model = db.Achievements.Find(id);
           
            if (model==null)
            {
                return HttpNotFound();
            }
          
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(Achievement model)
        {
            if(ModelState.IsValid)
            {
                
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }
        public ActionResult Delete(Guid? id)
        {
            if(id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Achievement model = db.Achievements.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Achievement model = db.Achievements.Find(id);
            db.Achievements.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}