﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{

    public class BooksController : BaseController
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Books
        public ActionResult Index(int page=1,int size=10,string Search="")
        {
            IQueryable<Books> List;
            if (!string.IsNullOrEmpty(Search))
            {
                List = db.Books.Where(i => i.Title.Contains(Search));
            }
            else
                List = db.Books;
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
        }
        // ToDo Shahid : Review required
        // GET: Books/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Books books = db.Books.Find(id);
            if (books == null)
            {
                return HttpNotFound();
            }
            return View(books);
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            ViewBag.ClassesId = new SelectList(db.Classes, "Id", "RomanDisplay");
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "DisplayName");
            ViewBag.BoardId = new SelectList(db.Boards, "Id", "Name");
            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Books books)
        {
            if (ModelState.IsValid)
            {
                db.Books.Add(books);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClassesId = new SelectList(db.Classes, "Id", "RomanDisplay", books.ClassesId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "DisplayName", books.SubjectId);
            ViewBag.BoardId = new SelectList(db.Boards, "Id", "Name",books.BoardId);
            return View(books);
        }

        // GET: Books/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Books books = db.Books.Find(id);
            if (books == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassesId = new SelectList(db.Classes, "Id", "RomanDisplay", books.ClassesId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "SubjectName", books.SubjectId);
            ViewBag.BoardId = new SelectList(db.Boards, "Id", "Name", books.BoardId);
            return View(books);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Books books)
        {
            if (ModelState.IsValid)
            {
                db.Entry(books).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClassesId = new SelectList(db.Classes, "Id", "RomanDisplay", books.ClassesId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "SubjectName", books.SubjectId);
            ViewBag.BoardId = new SelectList(db.Boards, "Id", "Name", books.BoardId);
            return View(books);
        }

        // GET: Books/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Books books = db.Books.Find(id);
            if (books == null)
            {
                return HttpNotFound();
            }
            return View(books);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Books books = db.Books.Find(id);
            db.Entry(books).Collection(i => i.SchoolClassBooks).Load();
            db.Entry(books).Collection(i => i.Chapters).Load();
            db.Entry(books).Collection(i => i.Questions).Load();
            db.Entry(books).Collection(i => i.Schools).Load(); 
            var Question = new List<Questions>();
            Question.AddRange(books.Questions);
            foreach (var item in books.Chapters)
            {
                Question
                    .AddRange(db.Questions.Where(i => i.ChapterId == item.Id));
                
            }
            foreach(var i in Question)
            {
                db.Entry(i).Collection(ip => ip.Quiz).Load();
                i.Quiz.Clear();
                db.SaveChanges();
            }
            db.Questions.RemoveRange(Question);
            db.Chapters.RemoveRange(books.Chapters);
            // Delete all chapters
            // db.Chapters.Where(i=>i.)
            books.SchoolClassBooks.Clear();
            books.Chapters.Clear();
            books.Schools.Clear();
            books.Questions.Clear();
            db.SaveChanges();
            // delete All Questions
            //books.Chapters.
            db.Books.Remove(books);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
