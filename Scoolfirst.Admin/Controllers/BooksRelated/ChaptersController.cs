﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    
    public class ChaptersController : BaseController
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Chapters
        public ActionResult Index(string search ="",int page=1,int size=20)
        {
            IQueryable<Chapter> List;
            if (!string.IsNullOrEmpty(search))
           
                List = db.Chapters.Where(i => i.Name.Contains(search));
         
            else
         
                List = db.Chapters;
                ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
                return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());    
        }

     
        public ActionResult BooksChapter(int BookId)
        {
            ViewBag.BookName = db.Books.Find(BookId).Title;
            ViewBag.BookId = BookId;
            var chapters = db.Chapters.Where(i => i.Books.Any(o => o.Id == BookId));
            if (ControllerContext.IsChildAction)
                return PartialView(chapters);
            return View(chapters);
        }

        public ActionResult CreateChapter(int BookId)
        {
            var book = db.Books.Find(BookId);
            ViewBag.Book = book;
            return View(new Chapter { ClassesId = book.ClassesId });
        }

        [HttpPost]
        public ActionResult CreateChapter(Chapter model,int BookId)
        {
           
            var book = db.Books.Find(BookId);
            if (ModelState.IsValid)
            {
               
                string[] strSplit = { "\r\n", "," };


                string[] chapters = model.Name.Split(strSplit, StringSplitOptions.None);

                if (chapters.Length > 0)
                {
                    List<string> ChapterExist = new List<string>();
                    foreach (string item in chapters)
                    {
                        if (db.Chapters.Any(i => i.Books.Any(o => o.Id == BookId) && i.Name == item))
                        {
                            ChapterExist.Add(item);
                        }
                        else
                        {
                            Chapter NewChapter = new Chapter();
                            NewChapter.Name = item;
                            
                            NewChapter.ClassesId = model.ClassesId;
                            NewChapter.Month = model.Month;
                            db.Chapters.Add(NewChapter);
                            db.SaveChanges();
                            db.Entry(NewChapter).Collection(i => i.Books).Load();
                            NewChapter.Books.Add(book);
                            db.SaveChanges();
                        }
                    }


                    //db.SaveChanges();
                    //db.Entry(model).Collection(i => i.Books).Load();
                    //model.Books.Add(book);
                    //db.SaveChanges();

                    if(ChapterExist.Count>0)
                    TempData["ChapterExist"] = ChapterExist;
                    return RedirectToAction("details", "Books", new { Id = BookId });
                }
            }
           
            ViewBag.Book = book;
            return View(model);
        }
        // GET: Chapters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chapter chapter = db.Chapters.Find(id);
            if (chapter == null)
            {
                return HttpNotFound();
            }
            return View(chapter);
        }

        // GET: Chapters/Create
        public ActionResult Create()
        {
            ViewBag.ClassesId = new SelectList(db.Classes, "Id", "RomanDisplay");
            return View();
        }

        // POST: Chapters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Chapter chapter)
        {
            if (ModelState.IsValid)
            {
                db.Chapters.Add(chapter);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClassesId = new SelectList(db.Classes, "Id", "RomanDisplay", chapter.ClassesId);
            return View(chapter);
        }

        // GET: Chapters/Edit/5
        public ActionResult Edit(int? id, int Bookid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var book = db.Books.Find(Bookid);
            ViewBag.BookId = book;
            Chapter chapter = db.Chapters.Find(id);
            if (chapter == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassesId = new SelectList(db.Classes, "Id", "RomanDisplay", chapter.ClassesId);
            return View(chapter);
        }

        // POST: Chapters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Chapter chapter,int Bookid)
        {
           
            if (ModelState.IsValid)
            {

                var book = db.Books.Find(Bookid).Id;
                ViewBag.BookId = book;
                db.Entry(chapter).State = EntityState.Modified;
               
                db.SaveChanges();
                //return Redirect(Request.UrlReferrer.ToString());
                return RedirectToAction("BooksChapter", "Chapters",new { bookid = book});
            }
            ViewBag.ClassesId = new SelectList(db.Classes, "Id", "RomanDisplay", chapter.ClassesId);
            return View(chapter);
        }

        // GET: Chapters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chapter chapter = db.Chapters.Find(id);
            if (chapter == null)
            {
                return HttpNotFound();
            }
            return View(chapter);
        }

        // POST: Chapters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Chapter chapter = db.Chapters.Find(id);
            db.Entry(chapter).Collection(i => i.Books).Load();
            db.Entry(chapter).Collection(i => i.Questions).Load();
            db.Entry(chapter).Collection(i => i.Notes).Load();
            db.Questions.RemoveRange(chapter.Questions);
            db.Notes.RemoveRange(chapter.Notes);
            chapter.Books.Clear();
            db.Chapters.Remove(chapter);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
