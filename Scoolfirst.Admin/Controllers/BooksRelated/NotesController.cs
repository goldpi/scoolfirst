﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers.BooksRelated
{
    public class NotesController : BaseController
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Notes
        public ActionResult Index(int BookId,int ChapterId,int page=1,int size=10,string search="")
        {
            ViewBag.Book = BookId;
            ViewBag.ChapterId = ChapterId;
            IQueryable<Note> List;
            if (!string.IsNullOrEmpty(search))
                List = db.Notes.Include(n => n.Chapter).Where(i => i.Topic.Contains(search));
            else
                List = db.Notes.Include(n => n.Chapter).Where(i=>i.ChapterId==ChapterId);
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(10).Center();
            return View(List.OrderBy(i=>i.Id).Skip((page-1)*size).Take(size).ToList());
        }

        // GET: Notes/Details/5
        public ActionResult Details(Guid? id,int BookId)
        {
            ViewBag.Book = BookId;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // GET: Notes/Create
        public ActionResult Create(int ChapterId,int BookId)
        {
            ViewBag.Book = BookId;
            ViewBag.Chapter = ChapterId;
            //ViewBag.ChapterId = new SelectList(db.Chapters, "Id", "Name");
            return View();
        }

        // POST: Notes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [ValidateInput(false)]
        [HttpPost]       
        public ActionResult Create( Note note,int BookId)
        {
           
            if (ModelState.IsValid)
            {
                note.Id = Guid.NewGuid();
                db.Notes.Add(note);
                db.SaveChanges();
                return RedirectToAction("Index",new { ChapterId=note.ChapterId,BookId=BookId});
            }

            //ViewBag.ChapterId = new SelectList(db.Chapters, "Id", "Name", note.ChapterId);
            return View(note);
        }

        // GET: Notes/Edit/5
        public ActionResult Edit(Guid? id,int BookId)
        {
            ViewBag.Book = BookId;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChapterId = new SelectList(db.Chapters, "Id", "Name", note.ChapterId);
            return View(note);
        }

        // POST: Notes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [ValidateInput(false)]
        [HttpPost]
       
        public ActionResult Edit(Note note,int BookId)
        {
            if (ModelState.IsValid)
            {
                db.Entry(note).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { ChapterId=note.ChapterId,BookId=BookId});
            }
            ViewBag.ChapterId = new SelectList(db.Chapters, "Id", "Name", note.ChapterId);
            return View(note);
        }

        // GET: Notes/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Note note = db.Notes.Include(i=>i.Chapter).FirstOrDefault(i=>i.Id==id);
            db.Notes.Remove(note);
            db.SaveChanges();
            return RedirectToAction("Index", new { ChapterId=note.ChapterId});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
