﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.PeriodicAssesment;
using Scoolfirst.Model.Provider;
using Scoolfirst.ViewModel.Play;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class PARootsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: PARoots
        public ActionResult Index(int page=1,int size=30,int schoolId=0,string type="WorkBook",int Class=0)
        {
            ViewBag.type = new SelectList(ListItem(), "Text", "Value", type);
            ViewBag.Class = new SelectList(db.Classes, "Id", "RomanDisplay", Class);

            var pARoots = db.PARoots.Include(i => i.Class)
               .Include(i => i.School);
            if (type == "WorkBook")
            {
                pARoots = pARoots.Where(i => i.TypeOfAssement == PAType.WorkBook);
            }
            if (type == "SiteRelated")
            {
                pARoots = pARoots.Where(i => i.TypeOfAssement == PAType.SiteRealted);
            }
            if (type == "SingleTest")
            {
                pARoots = pARoots.Where(i => i.TypeOfAssement == PAType.SingleTest);
            }
            if (schoolId > 0)
            {
               pARoots= pARoots.Where(i => i.SchoolId == schoolId);
            }
            if (Class > 0)
            {
                pARoots = pARoots.Where(i => i.ClassId == Class);
            }
            if(Request.QueryString["search"] != null)
            {
                string search = Request.QueryString["search"];
                pARoots = pARoots.Where(i => i.Title.Contains(search)  || i.School.SchoolName.Contains(search) );
            }
            ViewBag.Pager = Pagers.Items(pARoots.Count()).PerPage(size).Move(page).Segment(20).Center();

            return View(pARoots.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
        }

        // GET: PARoots/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PARoot pARoot = db.PARoots.Find(id);
            if (pARoot == null)
            {
                return HttpNotFound();
            }
            return View(pARoot);
        }

        // GET: PARoots/Create
        [ValidateInput(false)]
        public ActionResult Create()
        {
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay");
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName");
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "DisplayName");
            return View();
        }

        // POST: PARoots/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create( PARoot pARoot)
        {
            if (ModelState.IsValid)
            {
                db.PARoots.Add(pARoot);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", pARoot.ClassId);
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName", pARoot.SchoolId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "DisplayName",pARoot.SubjectId);
            return View(pARoot);
        }

        // GET: PARoots/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PARoot pARoot = db.PARoots.Find(id);
            if (pARoot == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", pARoot.ClassId);
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName", pARoot.SchoolId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "DisplayName", pARoot.SubjectId);
            return View(pARoot);
        }

        // POST: PARoots/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit( PARoot pARoot)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pARoot).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", pARoot.ClassId);
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName", pARoot.SchoolId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "DisplayName", pARoot.SubjectId);
            return View(pARoot);
        }

        // GET: PARoots/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PARoot pARoot = db.PARoots.Find(id);
            if (pARoot == null)
            {
                return HttpNotFound();
            }
            return View(pARoot);
        }

        // POST: PARoots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            PARoot pARoot = db.PARoots.Find(id);
            db.PARoots.Remove(pARoot);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Copy(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PARoot pARoot = db.PARoots.Find(id);
            if (pARoot == null)
            {
                return HttpNotFound();
            }
           
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName", pARoot.SchoolId);
           
            return PartialView(new PARootsCopy { CopyId=id.Value, SchoolId=pARoot.SchoolId });
        }

        [HttpPost]
        public ActionResult Copy(PARootsCopy copy)
        {
            if (copy.CopyId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PARoot p = db.PARoots.FirstOrDefault(i=>i.Id==copy.CopyId);


            
            if (p == null)
            {
                return HttpNotFound();
            }

            var n = new PARoot
            {
                AddedOn = DateTime.Now,
                ClassId = p.ClassId,
                Color = p.Color,
                Content = p.Content,
                DisplayAfterDate = p.DisplayAfterDate,
                IsPassable = p.IsPassable,
                Level = p.Level,
                MinQuestionToPass = p.MinQuestionToPass,
                Month = p.Month,
                Order = p.Order,
                SchoolId = copy.SchoolId,
                ShortDiscription = p.ShortDiscription,
                Solution = p.Solution,
                SubjectId = p.SubjectId,
                TestBrief = p.TestBrief,
                TimeInMinutes = p.TimeInMinutes,
                Tips = p.Tips,
                Title = p.Title,
                TitleColor = p.TitleColor,
                TypeOfAssement = p.TypeOfAssement,
                QuestionsSet= new List<PAQuestion>()
            };

            db.PARoots.Add(n);           
            db.SaveChanges();

            db.Entry(p).Collection(i => i.QuestionsSet).Load();
            foreach(var i in p.QuestionsSet)
            {
                var options = db.PAOptions.Where(io => io.QuestionId == i.Id).ToList();
                var pr = new PAQuestion
                {
                    Id = Guid.NewGuid(),
                    Module = i.Module,
                    Query = i.Query,
                    SetId = n.Id,
                    ShortOrder = i.ShortOrder,
                    Solution = i.Solution,


                };
                db.PAQuestions.Add(pr);
                db.SaveChanges();
                foreach (var item in options)
                {
                    var neOp = new PAOptions
                    {
                        Answer = item.Answer,
                        Correct = item.Correct,
                        Id = Guid.NewGuid(),
                        QuestionId = pr.Id,
                        ShortOrder = pr.ShortOrder
                    };
                    db.PAOptions.Add(neOp);
                    db.SaveChanges();
                }
            }
            return Content("Saved changes");
        }





        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public List<SelectListItem> ListItem()
        {
            List<SelectListItem> item = new List<SelectListItem>()
            {
                new SelectListItem{Value="WorkBook", Text="WorkBook"},
                new SelectListItem{Value="SiteRelated",Text="SiteRelated"},
                new SelectListItem{Value="SingleTest",Text="SingleTest"},
            };
            return item;
        }
    }
}
