﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using System.Data.Entity;
using System.Configuration;
using Scoolfirst.ViewModel.KDC;
using Scoolfirst.Model.KDC;
using System.Net;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class KDCController : Controller
    {
        ScoolfirstContext dc = new ScoolfirstContext();

        // GET: KDC
        public ActionResult Index(string type, int page=0,int size=20, string search = "")
        {
            //page = page < 0 ? 0 : page;
            //ViewBag.NoPage = dc.Articles.Count(i => i.Approved == false && i.Deleted == false);
            ViewBag.type = new SelectList(ListItem(), "Text", "Value", type);
            //var unblished = dc.Articles.Include(i=>i.User).Include(i=>i.Classes).Where(i => i.Approved == false && i.Deleted == false).OrderBy(i=>i.On).Skip(page* size).Take(size);
            // var user = User.Identity.Name;
          
            var unblished = dc.Articles.Include(i => i.User).Include(i => i.Classes).ToList();
            if (type == "Answered")
            {
                unblished = unblished.Where(i => i.Answered == true).OrderByDescending(i => i.On).ToList();
            }
          if(type == "UnAnswered")
            {
                unblished = unblished.Where(i => i.Answered == false).OrderByDescending(i => i.On).ToList();
            }
          if(type == "All")
            {
                unblished = unblished.OrderByDescending(i => i.On).ToList();
            }

            if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search) && type == "All")
                
                unblished = dc.Articles.Include(i => i.User).Where(i => i.Title.Contains(search)).ToList();
            else
               
                ViewBag.Pager = Pagers.Items(unblished.Count()).PerPage(size).Move(page).Segment(20).Center();

            return View(unblished.OrderByDescending(i => i.On).Skip((page - 1) * size).Take(size).ToList());
        }

        public List<SelectListItem> ListItem()
        {
            List<SelectListItem> item = new List<SelectListItem>()
            {
                 new SelectListItem{Value="All",Text="All"},
                new SelectListItem{Value="Answered", Text="Answered"},
                new SelectListItem{Value="UnAnswered",Text="UnAnswered"},
               
            };
            return item;
        }

        public ActionResult All(int page = 0, int size = 30)
        {
            page = page < 0 ? 0 : page;
            ViewBag.NoPage = dc.Articles.Count();
            var unblished = dc.Articles.Include(i => i.User).OrderByDescending(i => i.On).Skip(page * size).Take(size);
            ViewBag.Page = page;
            ViewBag.Size = size;

            return View("Index",unblished);
        }

        public ActionResult Rejected(int page = 0, int size = 30)
        {
            page = page < 0 ? 0 : page;
            ViewBag.NoPage = dc.Articles.Count(i => i.Approved == false && i.Deleted == true);
            var unblished = dc.Articles.Include(i => i.User).Where(i => i.Approved == false && i.Deleted == true).OrderByDescending(i => i.On).Skip(page * size).Take(size);
            ViewBag.Page = page;
            ViewBag.Size = size;

            return View("Index", unblished);
        }
        public ActionResult Details(Guid? Id)
        {
            ViewBag.ID = Id;
            if (Id == null)
                return HttpNotFound();
            
            var c = dc.Articles.Include(i=>i.User).FirstOrDefault(i=>i.ID==Id);
            return View(c);
        }
        public ActionResult Approve(Guid id,int Points=20 )
        {
            ViewBag.Id = id;
            ViewBag.Points = Points;
            return PartialView();
        }

        [HttpPost]
        public ActionResult ApproveIt(Guid id, int Points = 20)
        {
            var at = dc.Articles.Find(id);
            dc.Entry(at).Reference(i => i.User).Load();
            at.Approved =true;
            at.User.Points += Points;
            dc.SaveChanges();
            //dc.Notification.Add(new Model.Notifications.Notification
            //{
            //    Id = Guid.NewGuid(),
            //    Image = ConfigurationManager.AppSettings["image"] + "/content/Points.png",
            //    Link = "/MyKdc/Details/" + at.ID,
            //    RaisedOn = DateTime.UtcNow,
            //    Read = false,
            //    Source = "Admin",
            //    TargetUserId = at.UserId,
            //    Text = "Congrats your KDC has been approved, You have been awared " + Points + "points."
            //});
            dc.PointsHistory.Add(new Model.Common.PointsHistory
            {
                RelatedId= id.ToString(),
                RelatedType="Article",
                ShortDescription= "Congrats your KDC has been approved, You have been awared " + Points + "points.",
                Id = Guid.NewGuid(),
                On = DateTime.UtcNow,
                Points = Points,
                UserId = at.UserId
                //ToDo: Add reason
            });
            dc.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DisApproveIt(Guid id)
        {
            var at = dc.Articles.Find(id);
          
            at.Approved = false;
           
            dc.SaveChanges();
            //dc.Notification.Add(new Model.Notifications.Notification
            //{
            //    Id = Guid.NewGuid(),
            //    Image = ConfigurationManager.AppSettings["image"] + "/content/Points.png",
            //    Link = "/MyKdc/Details/" + at.ID,
            //    RaisedOn = DateTime.UtcNow,
            //    Read = false,
            //    Source = "Admin",
            //    TargetUserId = at.UserId,
            //    Text = "Your KDC has bee Disapproved! Thank you for your efforts."
            //});
            dc.SaveChanges();
            return RedirectToAction("Index");
        }

       public ActionResult My(int page=0)
        {
            //ToDo : Add myaction
            ArticlesItems ArtilceHandler = new ArticlesItems();
            ArtilceHandler.GetItems(dc, User.Identity.Name, page);
            var re = ArtilceHandler.KDC.Select(i => i.Article);
            return View("index",re);
        }

        public ActionResult Articles()
        {
            //var user = User.Identity.Name;
            //var userId = dc.Users.FirstOrDefault(i=>i.UserName==user);
            var article = dc.Articles.Include(i=>i.Classes).ToList();
            return View(article);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ArticleComments(ArticleComment model)
        {
            if (ModelState.IsValid)
            {
                var user = User.Identity.Name;
                var userid = dc.Users.FirstOrDefault(i => i.UserName == user);
                var article = dc.Articles.FirstOrDefault(i => i.ID == model.ArticleId);
                article.Answered = true;
                
                model.UserId = userid.Id;
                
                model.Id = Guid.NewGuid();
                model.On = System.DateTime.UtcNow;
         
                dc.ArticleComments.Add(model);
                dc.SaveChanges();
                
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Edit(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = dc.Articles.Include(i=>i.User).FirstOrDefault(i=>i.ID==id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }
        [HttpPost]
        public ActionResult Edit(Article model)
        {
            if (ModelState.IsValid)
            {
                var user = User.Identity.Name;
                var userid = dc.Users.FirstOrDefault(i => i.UserName == user).Id;
                model.UserId = userid;
                model.On = System.DateTime.Now;
                dc.Entry(model).State = EntityState.Modified;
                dc.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}