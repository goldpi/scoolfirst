﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.ViewModel.WorkBook;
using Scoolfirst.Model.Provider;
using HtmlAgilityPack;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class QuestionsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Questions
        public ActionResult Index(int page=1,int size=20,string search="")
        {
            IQueryable<Questions> List;
            if (!string.IsNullOrEmpty(search))
                List = db.Questions.Where(i => i.Title.Contains(search));
            else
                List = db.Questions;
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
        }
        //ToDo: Don't be so lazy. Searching code is not that Tough
        public ActionResult ChaptresQuestion(int ChapterId,int? BookId,int page=1,int size=10,string search="")
        {
            var Chapters= db.Chapters.Find(ChapterId);
            ViewBag.Book = BookId != null ? db.Books.Find(BookId) : Chapters.Books.First();
            ViewBag.Chapter = Chapters;
            IQueryable<Questions> list;
            if (!string.IsNullOrEmpty(search))
            {
                list= from l in db.Questions
                      where l.Title.Contains(search) && l.ChapterId==ChapterId
                      select l;
            }
            else
            {
                list = from l in db.Questions
                       where  l.ChapterId == ChapterId
                       select l;
            }
            ViewBag.Pager = Pagers.Items(list.Count()).PerPage(size).Move(page).Segment(20).Center();
            if (ControllerContext.IsChildAction || Request.IsAjaxRequest())
                return PartialView(list.ToList());
            return View(list.OrderByDescending(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
            
        }
        public ActionResult CreateQuestion(int ChapterId,int? BookId) 
        {
            if(BookId!=null)
            ViewBag.BookName = db.Books.Find(BookId);
            ViewBag.Chapter = db.Chapters.Find(ChapterId);
            return View(new Questions { ChapterId = ChapterId });
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateQuestion(Questions ques, int? BookId)
        {
            Books book = null;
            if (BookId != null)
                 book = db.Books.Find(BookId);

            if (ModelState.IsValid)
            {
                ques.Id = Guid.NewGuid();
               
                db.Questions.Add(ques);
                db.SaveChanges();
                db.Entry(ques).Collection(i => i.Books).Load();
                if(book!=null)
                ques.Books.Add(book);
                db.SaveChanges();
                if (BookId != null)
                    return RedirectToAction("ChaptresQuestion", new { BookId = BookId, ChapterId = ques.ChapterId });
                else
                    return RedirectToAction("Details", "Chapters", new { Id = ques.ChapterId });
            }
            if(book!=null)
            ViewBag.BookName = book;
            ViewBag.Chapter = db.Chapters.Find(ques.ChapterId);
           
            return View(ques);
        }
        // GET: Questions/Details/5
        public ActionResult Details(Guid? id)
        {
           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Questions questions = db.Questions.Find(id);
            if (questions == null)
            {
                return HttpNotFound();
            }
          
            return View(questions);
        }
        [ValidateInput(false)]
        // GET: Questions/Create
        public ActionResult Create()//int? BooksId)
        {

            ViewData["School"] = new MultiSelectList(db.Schools, "SchoolName", "SchoolName");
            ViewBag.Class = new SelectList(db.Classes, "RomanDisplay", "RomanDisplay");
            //if(BooksId!=null)
            //ViewBag.ChapterId = new SelectList(db.Chapters.Where(i=>i.Books.Any(o=>o.Id==BooksId)), "Id", "Name" );
            //else
                ViewBag.ChapterId = new SelectList(db.Chapters, "Id", "Name");
            ViewBag.Subject = new SelectList(db.Subjects, "SubjectName", "SubjectName");
            return View();
        }

        // POST: Questions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create( QuestionVM q)
        {
            if (ModelState.IsValid)
            {
                var questions = q.FromViewModel();
                questions.Id = Guid.NewGuid();
                db.Questions.Add(questions);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["School"] = new MultiSelectList(db.Schools, "SchoolName", "SchoolName", q.School);
            ViewBag.ChapterId = new SelectList(db.Chapters, "Id", "Name", q.ChapterId);
            ViewBag.Class = new SelectList(db.Classes, "RomanDisplay", "RomanDisplay", q.Class);
            ViewBag.Subject = new SelectList(db.Subjects, "SubjectName", "SubjectName", q.Subject);
            return View(q);
        }

        // GET: Questions/Edit/5
        public ActionResult Edit(Guid? id,int? BookId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Questions questions = db.Questions.Find(id);
            if (questions == null)
            {
                return HttpNotFound();
            }
            var q = QuestionVM.FromQuestionToVm(questions);
            
            return View(q);
        }

        // POST: Questions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(QuestionVM q,int? BookId,string QuestionType)
        {
            if (ModelState.IsValid)
            {
                var questions = q.FromViewModel();
                
                
                db.Entry(questions).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ChaptresQuestion", "Questions", new { ChapterId = q.ChapterId,BookId=BookId });
                //return RedirectToAction("Index");

            }
            
           
            return View(q);
        }

        // GET: Questions/Delete/5
        public ActionResult Delete(Guid? id,int? ChapterId,int? BookId)
        {
            if (BookId != null)
                ViewBag.Book = db.Books.Find(BookId);
            ViewBag.ChapterId = db.Chapters.Find(ChapterId);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Questions questions = db.Questions.Find(id);
            
            if (questions == null)
            {
                return HttpNotFound();
            }
            return View(questions);
        }

        // POST: Questions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id,int? BookId)
        {
           
            Questions questions = db.Questions
                .FirstOrDefault(i=>i.Id==id);
            db.Entry(questions).Collection(i => i.Books).Load();
            db.Entry(questions).Collection(i => i.Quiz).Load();
            questions.Books.Clear();
            questions.Quiz.Clear();
            db.Questions.Remove(questions);
            db.SaveChanges();
            return RedirectToAction("ChaptresQuestion", "Questions", new { ChapterId = questions.ChapterId, BookId = BookId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
