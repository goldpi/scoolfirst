﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.PlayReasoning;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class PlayQuestionController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: PlayQuestion
        public ActionResult Index(int Id,int page=1,int size=30)
        {
            var PlayQuestions = db.PlayQuestions.Include(p => p.Set).Where(i => i.SetId == Id);
            if (Request.QueryString["search"] != null)
            {
                var search = Request.QueryString["search"];
                PlayQuestions = PlayQuestions.Where(i => i.Query.Contains(search));
            }
            ViewBag.Pager = Pagers.Items(PlayQuestions.Count()).PerPage(size).Move(page).Segment(20).Center();

            return View(PlayQuestions.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
        }

        // GET: PlayQuestion/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayQuestion playQuestion = db.PlayQuestions.Find(id);
            if (playQuestion == null)
            {
                return HttpNotFound();
            }
            return View(playQuestion);
        }

        // GET: PlayQuestion/Create
        public ActionResult Create(long setId)
        {
           // ViewBag.SetId = new SelectList(db.PlayRoots, "Id", "Title");
            return View();
        }

        // POST: PlayQuestion/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( PlayQuestion playQuestion)
        {
            if (ModelState.IsValid)
            {
                playQuestion.Id = Guid.NewGuid();
                db.PlayQuestions.Add(playQuestion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SetId = new SelectList(db.PlayRoots, "Id", "Title", playQuestion.SetId);
            return View(playQuestion);
        }

        // GET: PlayQuestion/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayQuestion playQuestion = db.PlayQuestions.Find(id);
            if (playQuestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.SetId = new SelectList(db.PlayRoots, "Id", "Title", playQuestion.SetId);
            return View(playQuestion);
        }

        // POST: PlayQuestion/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( PlayQuestion playQuestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(playQuestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SetId = new SelectList(db.PlayRoots, "Id", "Title", playQuestion.SetId);
            return View(playQuestion);
        }

        // GET: PlayQuestion/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayQuestion playQuestion = db.PlayQuestions.Find(id);
            if (playQuestion == null)
            {
                return HttpNotFound();
            }
            return View(playQuestion);
        }

        // POST: PlayQuestion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            PlayQuestion playQuestion = db.PlayQuestions.Find(id);
            db.PlayQuestions.Remove(playQuestion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
