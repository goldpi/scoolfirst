﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.NAO;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class OlympiadController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Olympiad
        public ActionResult Index(int page=1,int size=20,string search="")
        {
            IQueryable<Olympiad> List;
            if(!string.IsNullOrEmpty(search))
            List = db.Olympiads.Where(i => i.Subject.Contains(search) || i.Title.Contains(search));
            else
           
                List = db.Olympiads;
                ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
                return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
           
        }

        // GET: Olympiad/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Olympiad olympiad = db.Olympiads.Find(id);
            if (olympiad == null)
            {
                return HttpNotFound();
            }
            return View(olympiad);
        }

        // GET: Olympiad/Create
        public ActionResult Create()
        {
            ViewBag.Level = new SelectList(DDl(), "Value", "Name");
            ViewBag.Class = new SelectList(ClassList(), "Value", "Name");
            return View();
        }

        private IEnumerable<Object> ClassList()
        {
            foreach (var item in db.Classes)
            {
                yield return new { Value = item.Id.ToString(), Name = item.RomanDisplay };
            }
        }

        // POST: Olympiad/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Id,Level,Subject,Class,Content,Title,AddedOn")] Olympiad olympiad)
        {
            if (ModelState.IsValid)
            {
                
                db.Olympiads.Add(olympiad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Class = new SelectList(ClassList(), "Value", "Name", olympiad.Class);
            ViewBag.Label = new SelectList(DDl(), "Value", "Name", olympiad.Level);
            return View(olympiad);
        }

        // GET: Olympiad/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Olympiad olympiad = db.Olympiads.Find(id);
            if (olympiad == null)
            {
                return HttpNotFound();
            }
            ViewBag.Class = new SelectList(ClassList(), "Value", "Name", olympiad.Class);
            ViewBag.Level = new SelectList(DDl(), "Value", "Name",olympiad.Level);
            return View(olympiad);
        }

        // POST: Olympiad/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,Level,Subject,Class,Content,Title,AddedOn")] Olympiad olympiad)
        {
            if (ModelState.IsValid)
            {
                db.Entry(olympiad).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Class = new SelectList(ClassList(), "Value", "Name", olympiad.Class);
            ViewBag.Level = new SelectList(DDl(), "Value", "Name", olympiad.Level);
            return View(olympiad);
        }

        // GET: Olympiad/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Olympiad olympiad = db.Olympiads.Find(id);
            if (olympiad == null)
            {
                return HttpNotFound();
            }
            return View(olympiad);
        }

        // POST: Olympiad/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Olympiad olympiad = db.Olympiads.Find(id);
            db.Olympiads.Remove(olympiad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public IEnumerable<object> DDl()
        {
            for (int i = 1; i < 7; i++)
                yield return new { Value = i, Name = "Level " + i };
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
