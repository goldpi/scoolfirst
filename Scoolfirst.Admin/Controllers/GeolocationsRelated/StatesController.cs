﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Geo;
using Scoolfirst.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Admin.Controllers
{
    
    public class StatesController : BaseController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();

        // GET: Countries
        public ActionResult Index(int page=1,int size=20,string search="")
        {
            IQueryable<State> List;
            if (!string.IsNullOrEmpty(search))
                List = dc.States.Where(i => i.Name.Contains(search));
            else
                List = dc.States;            
                ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
                return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
            
        }



        // GET: Countries/Edit/5
        public ActionResult Edit(int id=0)
        {
           
            State m = new State { Id = 0 };
           
            if (id!= 0)
            { 
                m = dc.States.Find(id);
                ViewBag.CountryId = new SelectList(dc.Countries, "Id", "Name",m.CountryId);
            }
            else
                ViewBag.CountryId = new SelectList(dc.Countries, "Id", "Name");

            if (Request.IsAjaxRequest())
                return PartialView(m);

           
            return View(m);
        }

        // POST: Countries/Edit/5
        [HttpPost]
        public ActionResult Edit(State model)
        {
           
            if (ModelState.IsValid)
            {
                if (model.Id != 0)
                    dc.Entry(model).State = System.Data.Entity.EntityState.Modified;
                else
                    dc.States.Add(model);
                    dc.SaveChanges();
                if (Request.IsAjaxRequest())
                    return Json(new { success = 1, msg = "Saved Changes" }, JsonRequestBehavior.AllowGet);
                else
                {
                    TempData["Sucess"] = "Saved Changes";
                    //return Redirect(Request.UrlReferrer.ToString());
                    return RedirectToAction("Index");
                }

            }
            ViewBag.CountryId = new SelectList(dc.Countries, "Id", "Name", model.CountryId);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            return View(model);
        }

        // GET: Countries/Delete/5
        public ActionResult Delete(int? id)
        {

            State value = null;
            if (id == null)
                return new HttpStatusCodeResult(404, "Not Found");
            if ((value = dc.States.Find(id)) != null)
            {
                dc.States.Remove(value);
                dc.SaveChanges();
                return Json(new { success = 1, msg = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return new HttpStatusCodeResult(404, "Not Found");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dc.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}