﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Geo;
using Scoolfirst.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Admin.Controllers
{
    
    public class AreaController : BaseController
    {
        private ScoolfirstContext dc ;
        public AreaController(ScoolfirstContext Context)
        {
            dc = Context;
        }

        // GET: Countries
        public ActionResult Index(int page=1,int size=20,string search="")
        {
            IQueryable<Area> List;
            if (!string.IsNullOrEmpty(search))
                List = dc.Areas.Where(i => i.Name.Contains(search));
            else
                List = dc.Areas;          
                ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
                return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
            
           
        }



        // GET: Countries/Edit/5
        public ActionResult Edit(int id = 0)
        {

            Area m = new Area { Id = 0 };

            if (id != 0)
            {
                m = dc.Areas.Find(id);
                ViewBag.CityId = new SelectList(dc.Cities, "Id", "Name", m.CityId);
            }
            else
                ViewBag.CityId = new SelectList(dc.Cities, "Id", "Name");

            if (Request.IsAjaxRequest())
                return PartialView(m);


            return View(m);
        }

        // POST: Countries/Edit/5
        [HttpPost]
        public ActionResult Edit(Area model)
        {
          
            
            if (ModelState.IsValid)
            {
                if (model.Id != 0)
                    dc.Entry(model).State = System.Data.Entity.EntityState.Modified;
                else
                    dc.Areas.Add(model);
                dc.SaveChanges();
                if (Request.IsAjaxRequest())
                    return Json(new { success = 1, msg = "Saved Changes" }, JsonRequestBehavior.AllowGet);
                else
                {
                    TempData["Sucess"] = "Saved Changes";
                    //return Redirect(Request.UrlReferrer.ToString());
                    return RedirectToAction("Index");
                }

            }
            ViewBag.CityId = new SelectList(dc.Cities, "Id", "Name", model.CityId);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            return View(model);
        }

        // GET: Countries/Delete/5
        public ActionResult Delete(int? id)
        {

            Area value = null;
            if (id == null)
                return new HttpStatusCodeResult(404, "Not Found");
            if ((value = dc.Areas.Find(id)) != null)
            {
                dc.Areas.Remove(value);
                dc.SaveChanges();
                if(Request.IsAjaxRequest())
                return 
                    Json(new { success = 1, msg = "" }, JsonRequestBehavior.AllowGet);
                TempData["Succes"] = "Removed Area";
                return RedirectToAction("Index");
            }
            else
                return new HttpStatusCodeResult(404, "Not Found");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dc.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}