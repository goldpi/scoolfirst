﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.MCQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Scoolfirst.Admin.Controllers
{
    public class McQuizController : ApiController
    {
        private ScoolfirstContext db = new ScoolfirstContext();
        [Route("api/McqQuest/")]
        public Scoolfirst.ViewModel.MCQ.McqQuestion Get(Guid? Id,Guid? sid)
        {
            if (Id == Guid.Empty)
            {

                McqQuestion q = new McqQuestion()
                {
                    Id = Guid.Empty,
                    Subject = $"Enter your Subject here  {0}",
                    Unit = "one",
                    Query = "Query here",
                    SetId=sid??Guid.Empty,
                    Option = new List<Options>(),
                    
                };
                for (int w = 1; w < 5; w++)
                {
                    q.Option.Add(new Options { Answer = $"Option {w}", Correct = false });
                }
                return Scoolfirst.ViewModel.MCQ.McqQuestion.VMQuestion(q);
            }

            else
            {
                {
                    var res = db.McqQuestions.Find(Id);
                    var x = db.Options.Where(i => i.McqQuestionId == Id).ToList();

                    //res.Choices = new List<Choice>();
                    foreach (var item in x)
                    {
                        res.Option.Add(item);
                    }
                    //db.Entry(res).Collection(i => i.Choices).Load();
                    return Scoolfirst.ViewModel.MCQ.McqQuestion.VMQuestion(res);
                }

            }
        }

        [Route("api/SaveMcq")]
        [HttpPost]
        public IHttpActionResult Post(McqQuestion question)
        {
            if (question.Id == Guid.Empty)
            {
                question.Id = Guid.NewGuid();
             
                foreach (var option in question.Option)
                {
                    option.Id = Guid.NewGuid();
                }
                db.McqQuestions.Add(question);
                

                db.SaveChanges();
            }
            else
            {
                #region QuestionModified
                var qq = new McqQuestion
                {
                    Id = question.Id,
                    Subject = question.Subject,
                    Unit = question.Unit,
                    Query=question.Query,
                    SetId=question.SetId,
                    Option = new List<Options>()
                };

                db.Entry(qq).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                #endregion
                var optionlist = question.Option.ToList();
                var toDel = new List<Guid>().ToList();
                var Option = db.Options.Where(i => i.McqQuestionId == question.Id).ToList();

                foreach (var item in Option)
                {
                    #region OptionsModified
                    if (optionlist.Any(i => i.Id == item.Id))
                    {
                        var ss = optionlist.First(i => i.Id == item.Id);
                        item.Answer = ss.Answer;
                        item.Correct = ss.Correct;
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    #endregion

                    else
                    #region Remove
                    {
                        toDel.Add(item.Id);
                    }
                    #endregion
                }
                foreach (var items in optionlist)
                {
                    if (items.Id == Guid.Empty)
                    {
                        items.McqQuestionId = qq.Id;
                        items.Id = Guid.NewGuid();
                        db.Options.Add(items);
                        db.SaveChanges();
                    }
                }

                #region RemovingItem
                foreach (Guid id in toDel)
                {
                    var Id = db.Options.Find(id);
                    db.Options.Remove(Id);
                    db.SaveChanges();
                }
                #endregion
            }
            //return question.Query;
            return Json(new { data = question, Message = "Data Saved" });
        }
    }
}
