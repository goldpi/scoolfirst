﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.MCQ;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class McqQuestionsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: McqQuestions
        public ActionResult Index(Guid? id)
        {

            ViewBag.SetId = id;
            if (id == null)
            {
                return View (db.McqQuestions.ToList());
            }
                else
            return View(db.McqQuestions.Where(i=>i.SetId==id).ToList());
        }

        // GET: McqQuestions/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            McqQuestion mcqQuestion = db.McqQuestions.Find(id);
            if (mcqQuestion == null)
            {
                return HttpNotFound();
            }
            return View(mcqQuestion);
        }

        // GET: McqQuestions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: McqQuestions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Subject,Unit,Query")] McqQuestion mcqQuestion)
        {
            if (ModelState.IsValid)
            {
                mcqQuestion.Id = Guid.NewGuid();
                db.McqQuestions.Add(mcqQuestion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mcqQuestion);
        }

        // GET: McqQuestions/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            McqQuestion mcqQuestion = db.McqQuestions.Find(id);
            if (mcqQuestion == null)
            {
                return HttpNotFound();
            }
            return View(mcqQuestion);
        }

        // POST: McqQuestions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Subject,Unit,Query")] McqQuestion mcqQuestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mcqQuestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mcqQuestion);
        }

        // GET: McqQuestions/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            McqQuestion mcqQuestion = db.McqQuestions.Find(id);
           
            if (mcqQuestion == null)
            {
                return HttpNotFound();
            }
            return View(mcqQuestion);
        }

        // POST: McqQuestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {

            McqQuestion mcqQuestion = db.McqQuestions.Include(i => i.Option).FirstOrDefault(i => i.Id == id);           
            db.McqQuestions.Remove(mcqQuestion);            
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult CreateQuestion(Guid? Id,Guid? sid)
        {
            McqQuestion question = new McqQuestion();
            ViewBag.SetId = sid;
            ViewBag.Id = Id??Guid.Empty;
            return View();
        }
        


    }
}
