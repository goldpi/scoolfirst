﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.MCQ;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class OptionsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Options
        public ActionResult Index()
        {
            var options = db.Options.Include(o => o.Question);
            return View(options.ToList());
        }

        // GET: Options/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Options options = db.Options.Find(id);
            if (options == null)
            {
                return HttpNotFound();
            }
            return View(options);
        }

        // GET: Options/Create
        public ActionResult Create()
        {
            ViewBag.McqQuestionId = new SelectList(db.McqQuestions, "Id", "Subject");
            return View();
        }

        // POST: Options/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,McqQuestionId,Answer,Correct")] Options options)
        {
            if (ModelState.IsValid)
            {
                options.Id = Guid.NewGuid();
                db.Options.Add(options);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.McqQuestionId = new SelectList(db.McqQuestions, "Id", "Subject", options.McqQuestionId);
            return View(options);
        }

        // GET: Options/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Options options = db.Options.Find(id);
            if (options == null)
            {
                return HttpNotFound();
            }
            ViewBag.McqQuestionId = new SelectList(db.McqQuestions, "Id", "Subject", options.McqQuestionId);
            return View(options);
        }

        // POST: Options/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,McqQuestionId,Answer,Correct")] Options options)
        {
            if (ModelState.IsValid)
            {
                db.Entry(options).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.McqQuestionId = new SelectList(db.McqQuestions, "Id", "Subject", options.McqQuestionId);
            return View(options);
        }

        // GET: Options/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Options options = db.Options.Find(id);
            if (options == null)
            {
                return HttpNotFound();
            }
            return View(options);
        }

        // POST: Options/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Options options = db.Options.Find(id);
            db.Options.Remove(options);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
