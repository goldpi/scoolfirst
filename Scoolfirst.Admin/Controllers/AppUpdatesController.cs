﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;

namespace Scoolfirst.Admin.Controllers
{
    public class AppUpdatesController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: AppUpdates
        public ActionResult Index()
        {
            return View(db.AppUpdates.ToList());
        }

        // GET: AppUpdates/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppUpdate appUpdate = db.AppUpdates.Find(id);
            if (appUpdate == null)
            {
                return HttpNotFound();
            }
            return View(appUpdate);
        }

        // GET: AppUpdates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AppUpdates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,UpdateTitle1,Update1,ShowUpdate1,UpdateTitle2,Update2,ShowUpdate2,UpdateTitle3,Update3,ShowUpdate3,UpdateTitle4,Update4,ShowUpdate4,AddedOn")] AppUpdate appUpdate)
        {
            if (ModelState.IsValid)
            {
                appUpdate.Id = Guid.NewGuid();
                db.AppUpdates.Add(appUpdate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(appUpdate);
        }

        // GET: AppUpdates/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppUpdate appUpdate = db.AppUpdates.Find(id);
            if (appUpdate == null)
            {
                return HttpNotFound();
            }
            return View(appUpdate);
        }

        // POST: AppUpdates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,UpdateTitle1,Update1,ShowUpdate1,UpdateTitle2,Update2,ShowUpdate2,UpdateTitle3,Update3,ShowUpdate3,UpdateTitle4,Update4,ShowUpdate4,AddedOn")] AppUpdate appUpdate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(appUpdate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(appUpdate);
        }

        // GET: AppUpdates/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppUpdate appUpdate = db.AppUpdates.Find(id);
            if (appUpdate == null)
            {
                return HttpNotFound();
            }
            return View(appUpdate);
        }

        // POST: AppUpdates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            AppUpdate appUpdate = db.AppUpdates.Find(id);
            db.AppUpdates.Remove(appUpdate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
