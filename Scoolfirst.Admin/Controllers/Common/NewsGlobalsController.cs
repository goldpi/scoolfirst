﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    
    public class NewsGlobalsController : BaseController
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: NewsGlobals
        public ActionResult Index(NewsType Type=NewsType.News,int page=1,int size=20,string search="")
        {
            ViewBag.Type = Type;
            IQueryable<NewsGlobal> List;
            if (!string.IsNullOrEmpty(search))
                List = db.News.Where(i => i.Title.Contains(search));
            else
                List = db.News;
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
        }

        // GET: NewsGlobals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsGlobal newsGlobal = db.News.Find(id);
            if (newsGlobal == null)
            {
                return HttpNotFound();
            }
            return View(newsGlobal);
        }

        // GET: NewsGlobals/Create
        public ActionResult Create(NewsType Type=NewsType.News)
        {
            return View(new NewsGlobal { Type = Type });
        }

        // POST: NewsGlobals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create( NewsGlobal newsGlobal)
        {
            if (ModelState.IsValid)
            {
                db.News.Add(newsGlobal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(newsGlobal);
        }

        // GET: NewsGlobals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsGlobal newsGlobal = db.News.Find(id);
            if (newsGlobal == null)
            {
                return HttpNotFound();
            }
            return View(newsGlobal);
        }

        // POST: NewsGlobals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(NewsGlobal newsGlobal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(newsGlobal).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(newsGlobal);
        }

        // GET: NewsGlobals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsGlobal newsGlobal = db.News.Find(id);
            if (newsGlobal == null)
            {
                return HttpNotFound();
            }
            return View(newsGlobal);
        }

        // POST: NewsGlobals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NewsGlobal newsGlobal = db.News.Find(id);
            db.News.Remove(newsGlobal);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
