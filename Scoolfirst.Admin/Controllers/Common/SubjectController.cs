﻿using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Admin.Controllers
{
    
    public class SubjectController : BaseController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();
        // GET: Subject
        public ActionResult Index(int page=1,int size=20,string search="")
        {
            IQueryable<Subject> List;
             
            if (!string.IsNullOrEmpty(search))
                List = dc.Subjects.Where(i => i.SubjectName.Contains(search) || i.DisplayName.Contains(search));
            else
                List = from l in dc.Subjects
                       select l;
                ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
                return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
           
        }

        public ActionResult Edit(int Id)
        {
            Subject m = new Subject { Id = 0 };
            if(Id!=0)
            m = dc.Subjects.Find(Id);
            if (Request.IsAjaxRequest())
                return PartialView(m);

            return View(m);
        }

        [HttpPost]
        public ActionResult Edit(Model.Common.Subject model)
        {
            var req = Request.IsAjaxRequest();
            bool p;
            if (ModelState.IsValid)
            {
                if (model.Id != 0)
                    dc.Entry(model).State = System.Data.Entity.EntityState.Modified;
                else
                    dc.Subjects.Add(model);
                dc.SaveChanges();
                if ((p=Request.IsAjaxRequest()))
                    return Json(new { success = 1, msg = "Saved Changes" }, JsonRequestBehavior.AllowGet);
                else
                {
                    TempData["Sucess"] = "Saved Changes";
                    //return Redirect(Request.UrlReferrer.ToString());
                    return RedirectToAction("Index");
                }
                    
            }

            if (Request.IsAjaxRequest())
                return PartialView(model);
            return View(model);
        }

        public ActionResult Delete(int? id)
        {

            Subject value = null;




           //var Books = (from s in dc.Books.Include("Subject")
           //            where s.SubjectId ==id
           //            select s).FirstOrDefault(i=>i.Id==id);
           // Subject sub = dc.Subjects.FirstOrDefault<Subject>();
           
            if (id == null)
                return new HttpStatusCodeResult(404, "Not Found");
            if ((value = dc.Subjects.Find(id)) != null)
            {
                dc.Entry(value).Collection(i => i.Books).Load();
                foreach(var i in value.Books)
                {
                    dc.Entry(i).Collection(io => io.Chapters).Load();
                    dc.Entry(i).Collection(io => io.Questions).Load();
                    dc.Entry(i).Collection(io => io.SchoolClassBooks).Load();
                    dc.Entry(i).Collection(io => io.Schools).Load();
                    //dc.Entry(i).Collection(io => io.)
                    i.Chapters.Clear();
                    i.Questions.Clear();
                    i.SchoolClassBooks.Clear();
                    i.Schools.Clear();
                    dc.SaveChanges();

                }
                dc.Books.RemoveRange(value.Books);
                var scs = dc.SchoolClassSubject.Where(i => i.SubjectId == id).ToList();
                foreach (var item in scs)
                {
                    dc.Entry(item).Collection(i => i.Books).Load();
                    
                    item.Books.Clear();
                    dc.SaveChanges();
                }
                dc.SchoolClassSubject.RemoveRange(scs);
                dc.SaveChanges();




                value.Books.Clear();
                dc.Subjects.Remove(value);

                dc.SaveChanges();
                return Json(new { success = 1, msg = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return new HttpStatusCodeResult(404, "Not Found");

        }

        

    }
}