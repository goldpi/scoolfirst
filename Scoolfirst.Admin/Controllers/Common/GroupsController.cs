﻿using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Scoolfirst.ViewModel;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    
    public class GroupsController : BaseController
    {
        private ScoolfirstContext dc = new ScoolfirstContext();
        // GET: Group
        public ActionResult Index(int page = 1, int size =20,string search="")
        {
            IQueryable<Group> List;
            if (!string.IsNullOrEmpty(search))
                List = dc.Groups.Where(i => i.Name.Contains(search));
            else
        
                List = from l in dc.Groups.Include(i => i.Schools)
                           .Include(i => i.Class)
                           select l;
                ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
                return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
         
        }

        public ActionResult Edit(int Id=0)
        {

            Group m = new Group { Id = 0 };
            GroupViewModel vm = new GroupViewModel(m);
            if (Id != 0)
            {
                m = dc.Groups.Include(i=>i.Class)
                    .Include(i=>i.Schools)
                    .FirstOrDefault(i=>i.Id==Id);
                vm = new GroupViewModel(m);
                ViewData["Class"] = new MultiSelectList(dc.Classes,"Id","NumericDisplay", m.Class.Select(i=>i.Id));
                ViewData["School"] = new MultiSelectList(dc.Schools, "Id", "SchoolName", m.Schools.Select(i => i.Id));
            }
            else
            {
                ViewData["Class"] = new MultiSelectList(dc.Classes, "Id", "NumericDisplay");
                ViewData["School"] = new MultiSelectList(dc.Schools, "Id", "SchoolName");
            }
            if (Request.IsAjaxRequest())
                return PartialView(vm);

            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(GroupViewModel model)
        {
            var mod = model.AsGroup();

            Classes ite = null;
            School school = null;
            if (ModelState.IsValid)
            {
                if (model.Id != 0)
                {
                    mod = dc.Groups.Find(model.Id);
                    mod.Name = model.AsGroup().Name;
                    dc.Entry(mod).Collection(i => i.Class).Load();
                    dc.Entry(mod).Collection(i => i.Schools).Load();
                    mod.Class.Clear();
                    mod.Schools.Clear();
                    dc.Entry(mod).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {

                    dc.Groups.Add(mod);
                }

                dc.SaveChanges();
                if (mod.Class == null)
                    mod.Class = new List<Classes>();
                if (mod.Schools == null)
                    mod.Schools = new List<School>();

                foreach (var item in model.Class)
                {
                    if (mod.Class.Any(i => i.Id == item))
                        continue;
                    if ((ite = dc.Classes.Find(item)) != null)
                    {
                        mod.Class.Add(ite);
                    }
                }
                foreach (var item in model.School)
                {
                    if (mod.Schools.Any(i => i.Id == item))
                        continue;
                    if ((school = dc.Schools.Find(item)) != null)
                    {
                        mod.Schools.Add(school);
                    }
                }
                dc.SaveChanges();
                if ((Request.IsAjaxRequest()))
                    return Json(new { success = 1, msg = "Saved Changes" }, JsonRequestBehavior.AllowGet);
                else
                {
                    TempData["Sucess"] = "Saved Changes";
                    //return Redirect(Request.UrlReferrer.ToString());
                    return RedirectToAction("Index");
                }

            }
            if (mod.Id != 0)
            {
                ViewData["Class"] = new MultiSelectList(dc.Classes, "Id", "NumericDisplay", mod.Class.Select(i => i.Id));
                ViewData["School"] = new MultiSelectList(dc.Schools, "Id", "SchoolName", mod.Schools.Select(i => i.Id));
            }

            else
            {
                ViewData["Class"] = new MultiSelectList(dc.Classes, "Id", "NumericDisplay");
                ViewData["School"] = new MultiSelectList(dc.Schools, "Id", "SchoolName");
            }


            if (Request.IsAjaxRequest())
                return PartialView(mod);
            return View(mod);
        }

        public ActionResult Delete(int? id)
        {
            Group value = null;
            if (id == null)
                return new HttpStatusCodeResult(404, "Not Found");
            if ((value = dc.Groups.Find(id)) != null)
            {
                dc.Groups.Remove(value);
                dc.SaveChanges();
                return Json(new { success = 1, msg = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return new HttpStatusCodeResult(404, "Not Found");
            // return Json(new { success = 1, msg = "" }, JsonRequestBehavior.AllowGet);
        }
    }
}