﻿using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class ClassesController : Controller
    {
        private ScoolfirstContext dc;

        public ClassesController(ScoolfirstContext Context)
        {
            dc = Context;
        }

        // GET: Classes
        public ActionResult Index(int page=1,int size=20,string search="")
        {
           
            IQueryable<Classes> List;
            //List = from l in dc.Classes
            //           select l;
            if (!string.IsNullOrEmpty(search))
                List = dc.Classes.Where(i => i.RomanDisplay.Contains(search));

            else
                List = dc.Classes;
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderBy(i=>i.Id).Skip((page - 1) * size).Take(size).ToList());
        }

        public ActionResult Edit(int Id=0)
        {
            Classes m = new Classes { Id = 0 };
            if (Id != 0)
                m = dc.Classes.FirstOrDefault(i=>i.Id==Id);
            if (m == null)
                return HttpNotFound();
            if (Request.IsAjaxRequest())
                return PartialView(m);

            return View(m);
        }

        [HttpPost]
        public ActionResult Edit(Model.Common.Classes model)
        {
            var req = Request.IsAjaxRequest();
            bool p;
            if (ModelState.IsValid)
            {
                if (model.Id != 0)
                    dc.Entry(model).State = System.Data.Entity.EntityState.Modified;
                else
                    dc.Classes.Add(model);
                dc.SaveChanges();
                if ((p = Request.IsAjaxRequest()))
                    return Json(new { success = 1, msg = "Saved Changes" }, JsonRequestBehavior.AllowGet);
                else
                {
                    TempData["Sucess"] = "Saved Changes";
                    return RedirectToAction("Index","Classes");
                }

            }

            if (Request.IsAjaxRequest())
                return PartialView(model);
            return View(model);
        }

        public ActionResult Delete(int? id)
        {
            Classes value = null;
            if (id == null)
                return HttpNotFound();
            if ((value = dc.Classes.Find(id)) != null)
            {
                dc.Classes.Remove(value);
                dc.SaveChanges();
                return Json(new { success = 1, msg = "" }, JsonRequestBehavior.AllowGet);

            }
            else
                return HttpNotFound();
            // return Json(new { success = 1, msg = "" }, JsonRequestBehavior.AllowGet);
        }

    }
}