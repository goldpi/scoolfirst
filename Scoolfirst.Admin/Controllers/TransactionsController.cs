﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Payments;

namespace Scoolfirst.Admin.Controllers
{
    public class TransactionsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Transactions
        public ActionResult Index()
        {
            var transactions = db.Transactions.Include(t => t.Cart).Include(t => t.Product).Include(t => t.Subcription).OrderByDescending(i=>i.On);
            return View(transactions.ToList());
        }

        // GET: Transactions/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // GET: Transactions/Create
        public ActionResult Create()
        {
            ViewBag.CartId = new SelectList(db.Carts, "Id", "UserId");
            ViewBag.ProductId = new SelectList(db.Products, "Id", "Title");
            ViewBag.SubcriptionId = new SelectList(db.Subcriptions, "Id", "UserId");
            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Type,Amount,Description,CartId,UserId,SubcriptionId,TransactionResult,ProductId,Points,Redeem,On,Email,Address")] Transaction transaction)
        {
            if (ModelState.IsValid)
            {
                transaction.Id = Guid.NewGuid();
                db.Transactions.Add(transaction);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CartId = new SelectList(db.Carts, "Id", "UserId", transaction.CartId);
            ViewBag.ProductId = new SelectList(db.Products, "Id", "Title", transaction.ProductId);
            ViewBag.SubcriptionId = new SelectList(db.Subcriptions, "Id", "UserId", transaction.SubcriptionId);
            return View(transaction);
        }

        // GET: Transactions/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            ViewBag.CartId = new SelectList(db.Carts, "Id", "UserId", transaction.CartId);
            ViewBag.ProductId = new SelectList(db.Products, "Id", "Title", transaction.ProductId);
            ViewBag.SubcriptionId = new SelectList(db.Subcriptions, "Id", "UserId", transaction.SubcriptionId);
            return View(transaction);
        }

        // POST: Transactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Type,Amount,Description,CartId,UserId,SubcriptionId,TransactionResult,ProductId,Points,Redeem,On,Email,Address")] Transaction transaction)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transaction).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CartId = new SelectList(db.Carts, "Id", "UserId", transaction.CartId);
            ViewBag.ProductId = new SelectList(db.Products, "Id", "Title", transaction.ProductId);
            ViewBag.SubcriptionId = new SelectList(db.Subcriptions, "Id", "UserId", transaction.SubcriptionId);
            return View(transaction);
        }

        // GET: Transactions/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // POST: Transactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Transaction transaction = db.Transactions.Find(id);
            db.Transactions.Remove(transaction);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
