﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
   
    public class RoleModelsController : Controller
    {
        //ToDo : Shahid Ui for this controller
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: RoleModels
        public ActionResult Index(int page=1,int size=10,string search="")
        {
            IQueryable<RoleModel> List;
            if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search))
                List = db.RoleModels.Include(r=>r.Class).Where(i => i.Title.Contains(search));
            
            else
                List = db.RoleModels.Include(r => r.Class);
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            var res = List.OrderByDescending(i => i.Id).Skip((page - 1) * size).Take(size).ToList();
            return View(res.Select(i=> {
                return new RoleModel
                {
                    Class = i.Class,
                    ClassId = i.ClassId,
                    Content = i.Content,
                    Id = i.Id,
                    Title = i.Title ?? "",
                    ImageUrl = i.ImageUrl ?? "",
                    On = i.On,
                    VideoUrl = i.VideoUrl ?? ""
                };
            })
            );
            
          
        }

        // GET: RoleModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoleModel roleModel = db.RoleModels.Include(i=>i.Class).FirstOrDefault(i=>i.Id==id);
            if (roleModel == null)
            {
                return HttpNotFound();
            }
            return View(roleModel);
        }

        // GET: RoleModels/Create
        public ActionResult Create()
        {
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay");
            if (Request.IsAjaxRequest())
                return PartialView();
            return View();
        }

        // POST: RoleModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create( RoleModel roleModel)
        {
            if (ModelState.IsValid)
            {
                db.RoleModels.Add(roleModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", roleModel.ClassId);
            return View(roleModel);
        }

        // GET: RoleModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoleModel roleModel = db.RoleModels.Find(id);
            if (roleModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", roleModel.ClassId);
            if (Request.IsAjaxRequest())
                return PartialView();
            return View(roleModel);
        }

        // POST: RoleModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit( RoleModel roleModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roleModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", roleModel.ClassId);
            return View(roleModel);
        }

        // GET: RoleModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoleModel roleModel = db.RoleModels.Find(id);
            if (roleModel == null)
            {
                return HttpNotFound();
            }
            return View(roleModel);
        }

        // POST: RoleModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RoleModel roleModel = db.RoleModels.Find(id);
            db.RoleModels.Remove(roleModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
