﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.PlayReasoning;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class PlayController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: Play
        public ActionResult Index(int page = 1, int size = 30, string search = "")
        {
            IQueryable<PlayRoot> List;
            if (!string.IsNullOrEmpty(search))
                List = db.PlayRoots.Where(i =>  i.Title.Contains(search));
            else

                List = db.PlayRoots;
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
        }

        // GET: Play/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayRoot playRoot = db.PlayRoots.Find(id);
            if (playRoot == null)
            {
                return HttpNotFound();
            }
            return View(playRoot);
        }

        // GET: Play/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Play/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(PlayRoot playRoot)
        {
            if (ModelState.IsValid)
            {
                playRoot.AddedOn=DateTime.UtcNow;
                db.PlayRoots.Add(playRoot);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(playRoot);
        }

        // GET: Play/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayRoot playRoot = db.PlayRoots.Find(id);
            if (playRoot == null)
            {
                return HttpNotFound();
            }
            return View(playRoot);
        }

        // POST: Play/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(PlayRoot playRoot)
        {
            if (ModelState.IsValid)
            {
                //this.db.Entry(playRoot).Property(x => x.AddedOn).IsModified = false;
                playRoot.AddedOn=DateTime.UtcNow;
                db.Entry(playRoot).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(playRoot);
        }

        // GET: Play/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayRoot playRoot = db.PlayRoots.Find(id);
            if (playRoot == null)
            {
                return HttpNotFound();
            }
            return View(playRoot);
        }

        // POST: Play/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            PlayRoot playRoot = db.PlayRoots.Find(id);
            db.PlayRoots.Remove(playRoot);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
