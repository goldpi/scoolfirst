﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class ReportAbuseTitlesController : Controller
    {
        private ScoolfirstContext dc = new ScoolfirstContext();

        // GET: ReportAbuseTitles
        public ActionResult Index(int page=1,int size=20,string search="")
        {
            IQueryable<ReportAbuseTitle> List;
            if (!string.IsNullOrEmpty(search))
                List = dc.ReportAbuseTitle.Where(i => i.Title.Contains(search));
            else
                List = dc.ReportAbuseTitle;
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderBy(i=>i.EntryDate).Skip((page-1)*size).Take(size).ToList());
        }

      

               // GET: ReportAbuseTitles/Edit/5
        public ActionResult Edit(int id=0)
        {
            ReportAbuseTitle m = new ReportAbuseTitle { Id=0 };
            if (id != 0)
                m = dc.ReportAbuseTitle.Find(id);
            if (Request.IsAjaxRequest())
                return PartialView(m);

            return View(m);


        }

        // POST: ReportAbuseTitles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,EntryDate,UserID,Visible")] ReportAbuseTitle model)
        {
            var req = Request.IsAjaxRequest();
          var data=  Request.Form["Visible"];
            if (data.Equals("on", StringComparison.InvariantCultureIgnoreCase))
            {
                model.Visible = true;
                ModelState.Clear();
            }
            if (ModelState.IsValid)
            {
                if (model.Id != 0)
                    dc.Entry(model).State = System.Data.Entity.EntityState.Modified;
                else
                dc.ReportAbuseTitle.Add(model);
                dc.SaveChanges();
                if (Request.IsAjaxRequest())
                    return Json(new { success = 1, msg = "Saved Changes" }, JsonRequestBehavior.AllowGet);
                else
                {
                    TempData["Sucess"] = "Saved Changes";
                    return Redirect(Request.UrlReferrer.ToString());
                }

            }

            if (Request.IsAjaxRequest())
                return PartialView(model);
            return View(model);

        }

        // GET: ReportAbuseTitles/Delete/5
        public ActionResult Delete(int? id)
        {
           ReportAbuseTitle value = null;
            if (id == null)
                return new HttpStatusCodeResult(404, "Not Found");
            if ((value = dc.ReportAbuseTitle.Find(id)) != null)
            {
                dc.ReportAbuseTitle.Remove(value);
                dc.SaveChanges();
                return Json(new { success = 1, msg = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return new HttpStatusCodeResult(404, "Not Found");
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dc.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
