﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Entity;
//using System.Linq;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;
//using Scoolfirst.Model.Context;
//using Scoolfirst.Model.Notifications;

//namespace Scoolfirst.Admin.Controllers
//{
//    public class NotificationsController : Controller
//    {
//        private ScoolfirstContext db = new ScoolfirstContext();

//        // GET: Notifications
//        public ActionResult Index()
//        {
//            return View(db.Notification.ToList());
//        }

//        // GET: Notifications/Details/5
//        public ActionResult Details(string id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Notification notification = db.Notification.Find(id);
//            if (notification == null)
//            {
//                return HttpNotFound();
//            }
//            return View(notification);
//        }

//        // GET: Notifications/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: Notifications/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,User,Group,ImageUrl,Link,PostId,Title,Message,Date,Read,Module,Size")] Notification notification)
//        {
//            if (ModelState.IsValid)
//            {
//                db.Notification.Add(notification);
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }

//            return View(notification);
//        }

//        // GET: Notifications/Edit/5
//        public ActionResult Edit(string id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Notification notification = db.Notification.Find(id);
//            if (notification == null)
//            {
//                return HttpNotFound();
//            }
//            return View(notification);
//        }

//        // POST: Notifications/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,User,Group,ImageUrl,Link,PostId,Title,Message,Date,Read,Module,Size")] Notification notification)
//        {
//            if (ModelState.IsValid)
//            {
//                db.Entry(notification).State = EntityState.Modified;
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }
//            return View(notification);
//        }

//        // GET: Notifications/Delete/5
//        public ActionResult Delete(string id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Notification notification = db.Notification.Find(id);
//            if (notification == null)
//            {
//                return HttpNotFound();
//            }
//            return View(notification);
//        }

//        // POST: Notifications/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(string id)
//        {
//            Notification notification = db.Notification.Find(id);
//            db.Notification.Remove(notification);
//            db.SaveChanges();
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                db.Dispose();
//            }
//            base.Dispose(disposing);
//        }
//    }
//}
