﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.MCQ;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class QuestionSetsController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: QuestionSets
        public ActionResult Index(int page=1,int size=20,string search="")
        {
            IQueryable<QuestionSet> List;
            if (!string.IsNullOrEmpty(search))
                List = db.QuestionSets.Where(i => i.Title.Contains(search));
            else
                List = db.QuestionSets;
            ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
            return View(List.OrderBy(i => i.Id).Skip((page - 1) * size).Take(size).ToList());
        }

        // GET: QuestionSets/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuestionSet questionSet = db.QuestionSets.Find(id);
            if (questionSet == null)
            {
                return HttpNotFound();
            }
            return View(questionSet);
        }

        // GET: QuestionSets/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: QuestionSets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Id,Title,Description,Class,Type,Level,Active,Minutes")] QuestionSet questionSet)
        {
            if (ModelState.IsValid)
            {
                questionSet.Id = Guid.NewGuid();
                db.QuestionSets.Add(questionSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(questionSet);
        }

        // GET: QuestionSets/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuestionSet questionSet = db.QuestionSets.Find(id);
            if (questionSet == null)
            {
                return HttpNotFound();
            }
            return View(questionSet);
        }

        // POST: QuestionSets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,Title,Description,Class,Type,Level,Active,Minutes")] QuestionSet questionSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(questionSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(questionSet);
        }

        // GET: QuestionSets/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuestionSet questionSet = db.QuestionSets.Find(id);
            if (questionSet == null)
            {
                return HttpNotFound();
            }
            return View(questionSet);
        }

        // POST: QuestionSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            QuestionSet questionSet = db.QuestionSets.Find(id);
            db.QuestionSets.Remove(questionSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
