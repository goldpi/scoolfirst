﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.ViewModel.Common;
using System.Web.Script.Serialization;
using Scoolfirst.Model.Application_Setting;
using Scoolfirst.Model.Provider;
using Scoolfirst.Model.Context;
using Scoolfirst.ViewModel.WorkBook;
using System.Data.Entity;
using System.Net;
using System.IO;
using System.Text;
using Scoolfirst.ViewModel;
using System.Net.Http;
using System.Threading.Tasks;

namespace Scoolfirst.Admin.Controllers
{
  
    [Authorize]
    public class HomeController : Controller
    {
        private ScoolfirstContext dc = new ScoolfirstContext();
        private static readonly HttpClient client = new HttpClient();
        public ActionResult Index()
        {
            ViewBag.StudentCount = dc.Roles.Include(i => i.Users).FirstOrDefault(i => i.Name == "student").Users.Count();
            ViewBag.SchoolCount = dc.Roles.Include(i => i.Users).FirstOrDefault(i => i.Name.ToLower() == "school").Users.Count();
            ViewBag.TotalUnAnswerd = dc.Articles.Include(i=>i.User).Where(i => i.Answered == false).Count();
            var date = DateTime.UtcNow.AddDays(-1);
            ViewBag.Last = dc.Users.Count(i => i.RegisterdOn > date);
            var today = DateTimeOffset.Now;
            var last = today.AddDays(-10);
            var p = dc.ActivityLogger.Where(i => i.OnDateOffset >= last && i.OnDateOffset <= today);
            ViewBag.TActivity = p.Count();
            ViewBag.Mact = p.Count(i => i.Source == "Mobile");
            ViewBag.Web = p.Count(i => i.Source == "Web");
            last = today.AddMinutes(-5);
            ViewBag.Online = dc.ActivityLogger.Where(i => i.OnDateOffset >= last && i.OnDateOffset <= today).Select(i => i.UserName).Distinct().Count();
            return View();
        }

        public ActionResult Analytics()
        {
           
            return View();
        }

        public ActionResult GetNotice()
        {
            var url = (Request.IsLocal ? "http://localhost:57123" : HostingServerInfo.Api.GetCompleteUrl()) + "/Home/Notice";
            var model = new NoticeModel
            {
                Notice = Get(url)
            };
            return View(model);
        }
        [ValidateInput(false)]
        public async Task<ActionResult> Notice(NoticeModel model)
        {
            var url = (Request.IsLocal ? "http://localhost:57123" : HostingServerInfo.Api.GetCompleteUrl()) + "/api/TextNotice";
            var values = new Dictionary<string, string>
            {
               { "value", model.Notice }
            };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync(url, content);

            var responseString = await response.Content.ReadAsStringAsync();
            return RedirectToAction("getNotice");
        }

        public string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
        //public string Post(string uri, string data, string contentType, string method = "POST")
        //{
        //    byte[] dataBytes = Encoding.UTF8.GetBytes(data);

        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
        //    request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
        //    request.ContentLength = dataBytes.Length;
        //    request.ContentType = contentType;
        //    request.Method = method;

        //    using (Stream requestBody = request.GetRequestStream())
        //    {
        //        requestBody.Write(dataBytes, 0, dataBytes.Length);
        //    }

        //    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        //    using (Stream stream = response.GetResponseStream())
        //    using (StreamReader reader = new StreamReader(stream))
        //    {
        //        return reader.ReadToEnd();
        //    }
        //}

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";
        //    var dc = new ScoolfirstContext();
        //    var p = dc.Database
        //        .SqlQuery<QuestionVM2>(
        //        @"SELECT [t4].[Id], [t4].[Title], 
        //        [t6].[Name] AS [Chapter], [t4].[Answer], 
        //        [t4].[Refrence], [t4].[Subject], 
        //        [t7].[Title] AS [Book]
        //        FROM [SchoolClassSubjects] AS [t0]
        //        INNER JOIN [SchoolClassSubjectBooks] AS 
        //        [t1] ON [t0].[Id] = [t1].[SchoolClassSubject_Id]
        //        INNER JOIN [ChapterBooks] AS [t2] ON ((
        //        SELECT [t3].[Id]
        //        FROM [Books] AS [t3]
        //        WHERE [t3].[Id] = [t1].[Books_Id]
        //        )) = [t2].[Books_Id]
        //        INNER JOIN [Questions] AS [t4] ON [t2].[Chapter_Id] = [t4].[ChapterId]
        //        INNER JOIN [Subjects] AS [t5] ON [t5].[Id] = [t0].[SubjectId]
        //        INNER JOIN [Chapters] AS [t6] ON [t6].[Id] = [t2].[Chapter_Id]
        //        INNER JOIN [Books] AS [t7] ON [t7].[Id] = [t2].[Books_Id]
        //        WHERE ([t0].[ClassId] = @p0) AND 
        //        ([t0].[SchoolId] = @p1) AND ([t5].[SubjectName] = @p2)"
        //         , 1, 12, "");

        //    return View(p);
        //}

        public ActionResult YouTube(string Id)
        {
            ViewBag.Id = Id;
            return PartialView();
        }

        // todo implement invoice logo url
        public ActionResult Branding()
        {
            return null;
            //System.Configuration.Configuration config = null;
            //if (System.Web.HttpContext.Current != null)
            //{
            //    config =
            //        System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            //}
            //else
            //{
            //    config =
            //        ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //}

            //BrandingVM brandingvm = new BrandingVM();
           
            //if (ConfigurationManager.AppSettings["MobileLogo"] != null)
            //{

            //    brandingvm.MobileLogo = ConfigurationManager.AppSettings["MobileLogo"] != null ? ConfigurationManager.AppSettings["MobileLogo"].ToString(): "";
            //}
            //else
            //{
            //    config.AppSettings.Settings.Add("MobileLogo", "");
            //    config.Save(ConfigurationSaveMode.Modified);
            //    brandingvm.MobileLogo = ConfigurationManager.AppSettings["MobileLogo"] != null ? ConfigurationManager.AppSettings["MobileLogo"].ToString() : "";
                
            //}

            //if (ConfigurationManager.AppSettings["Adminlogo"] != null)
            //{

            //    brandingvm.Adminlogo = ConfigurationManager.AppSettings["Adminlogo"] != null ? ConfigurationManager.AppSettings["Adminlogo"].ToString() : "";
            //}
            //else
            //{
            //    config.AppSettings.Settings.Add("Adminlogo", "");
            //    config.Save(ConfigurationSaveMode.Modified);
            //    brandingvm.Adminlogo = ConfigurationManager.AppSettings["Adminlogo"] != null ? ConfigurationManager.AppSettings["Adminlogo"].ToString() : "";
                
            //}

            //if (ConfigurationManager.AppSettings["Weblogo"] != null)
            //{

            //    brandingvm.Weblogo = ConfigurationManager.AppSettings["Weblogo"] != null ? ConfigurationManager.AppSettings["Weblogo"].ToString() : "";
            //}
            //else
            //{
            //    config.AppSettings.Settings.Add("Weblogo", "");
            //    config.Save(ConfigurationSaveMode.Modified);
            //    // ConfigurationManager.RefreshSection("appSettings");
            //    brandingvm.Weblogo = ConfigurationManager.AppSettings["Weblogo"] != null ? ConfigurationManager.AppSettings["Weblogo"].ToString() : "";
               
            //}
            //return View(brandingvm);
        }

        [HttpPost]
        public ActionResult Branding(BrandingVM Model)
        {
            System.Configuration.Configuration config = null;
            if (System.Web.HttpContext.Current != null)
            {
                config =
                    System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            }
            else
            {
                config =
                    ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }

            if (Uri.IsWellFormedUriString(Model.Weblogo, UriKind.RelativeOrAbsolute))
                config.AppSettings.Settings["Weblogo"].Value = Model.Weblogo;
            if (Uri.IsWellFormedUriString(Model.Adminlogo, UriKind.RelativeOrAbsolute))
                config.AppSettings.Settings["Adminlogo"].Value = Model.Adminlogo;
            if (Uri.IsWellFormedUriString(Model.Adminlogo, UriKind.RelativeOrAbsolute))
                config.AppSettings.Settings["MobileLogo"].Value = Model.MobileLogo;
                config.Save(ConfigurationSaveMode.Modified);

            return View(Model);
        }



        [AllowAnonymous]
       //[OutputCache(Duration = 3600, VaryByParam = "none")]
        public ActionResult GetLogos()
        {
            BrandingVM Model = new BrandingVM();
            Model.MobileLogo =ConfigurationManager.AppSettings["MobileLogo"] != null ? ConfigurationManager.AppSettings["MobileLogo"].ToString() : "";
            Model.Adminlogo =ConfigurationManager.AppSettings["Adminlogo"] != null ? ConfigurationManager.AppSettings["Adminlogo"].ToString() : "";
            Model.Weblogo =ConfigurationManager.AppSettings["Weblogo"] != null ? ConfigurationManager.AppSettings["Weblogo"].ToString() : "";
            if (!Uri.IsWellFormedUriString(Model.Weblogo, UriKind.RelativeOrAbsolute))
                Model.Weblogo = string.Empty;
           
           
            if (!Uri.IsWellFormedUriString(Model.Adminlogo, UriKind.RelativeOrAbsolute))
                Model.Adminlogo = string.Empty;
            if (!Uri.IsWellFormedUriString(Model.MobileLogo, UriKind.RelativeOrAbsolute))
                Model.MobileLogo = string.Empty;
            return Json(Model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Setting()
        {
            var appSetting = new ApplicationSetting {WebSiteUrl = Request.UserHostName};
            return View(appSetting);
        }


       

    }

    
}