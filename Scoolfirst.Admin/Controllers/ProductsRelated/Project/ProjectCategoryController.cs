﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.Admin.Controllers
{
    [Authorize]
    public class ProjectCategoryController : Controller
    {
        private ScoolfirstContext dc = new ScoolfirstContext();

        // GET: /Category/
        public ActionResult Index(int page=1,int size=10,string search="")
        {
            IQueryable<ProjectCategory> List;
            if (!string.IsNullOrEmpty(search))          
            List = dc.ProjectCategories.Where(i => i.Name.Contains(search));           
           else
                List = dc.ProjectCategories;
                ViewBag.Pager = Pagers.Items(List.Count()).PerPage(size).Move(page).Segment(20).Center();
                return View(List.OrderBy(i=>i.Id).Skip((page-1)*size).Take(size).ToList());           
        }

       
       
        public ActionResult Edit(int id=0)
        {
           ProjectCategory m = new ProjectCategory { Id = 0 };
            if (id != 0)
                m = dc.ProjectCategories.Find(id);
            if (Request.IsAjaxRequest())
                return PartialView(m);

            return View(m);
        }

        // POST: Countries/Edit/5
        [HttpPost]
        public ActionResult Edit(ProjectCategory model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id != 0)
                    dc.Entry(model).State = System.Data.Entity.EntityState.Modified;
                else
                    dc.ProjectCategories.Add(model);
                dc.SaveChanges();
                if (Request.IsAjaxRequest())
                    return Json(new { success = 1, msg = "Saved Changes" }, JsonRequestBehavior.AllowGet);
                else
                {
                    TempData["Sucess"] = "Saved Changes";
                    //return Redirect(Request.UrlReferrer.ToString());
                    return RedirectToAction("Index");
                }

            }

            if (Request.IsAjaxRequest())
                return PartialView(model);
            return View(model);
        }
        public ActionResult Delete(int? id)
        {

            ProjectCategory value = null;
            if (id == null)
                return new HttpStatusCodeResult(404, "Not Found");
            if ((value = dc.ProjectCategories.Find(id)) != null)
            {
                dc.ProjectCategories.Remove(value);
                dc.SaveChanges();
                return Json(new { success = 1, msg = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return new HttpStatusCodeResult(404, "Not Found");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dc.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
