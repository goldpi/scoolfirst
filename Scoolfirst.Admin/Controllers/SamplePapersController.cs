﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.WorkBook;

namespace Scoolfirst.Admin.Controllers
{
    public class SamplePapersController : Controller
    {
        private ScoolfirstContext db = new ScoolfirstContext();

        // GET: SamplePapers
        public ActionResult Index()
        {
            var samplePapers = db.SamplePapers.Include(s => s.Class).Include(s => s.School).Include(s => s.Subject);
            return View(samplePapers.ToList());
        }

        // GET: SamplePapers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SamplePaper samplePaper = db.SamplePapers.Find(id);
            if (samplePaper == null)
            {
                return HttpNotFound();
            }
            return View(samplePaper);
        }

        // GET: SamplePapers/Create
        public ActionResult Create()
        {
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay");
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName");
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "SubjectName");
            return View();
        }

        // POST: SamplePapers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,SchoolId,ClassId,SubjectId,ImageUrl,Description,AddedOn,Active")] SamplePaper samplePaper)
        {
            if (ModelState.IsValid)
            {
                db.SamplePapers.Add(samplePaper);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", samplePaper.ClassId);
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName", samplePaper.SchoolId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "SubjectName", samplePaper.SubjectId);
            return View(samplePaper);
        }

        // GET: SamplePapers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SamplePaper samplePaper = db.SamplePapers.Find(id);
            if (samplePaper == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", samplePaper.ClassId);
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName", samplePaper.SchoolId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "SubjectName", samplePaper.SubjectId);
            return View(samplePaper);
        }

        // POST: SamplePapers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,SchoolId,ClassId,SubjectId,ImageUrl,Description,AddedOn,Active")] SamplePaper samplePaper)
        {
            if (ModelState.IsValid)
            {
                db.Entry(samplePaper).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClassId = new SelectList(db.Classes, "Id", "RomanDisplay", samplePaper.ClassId);
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "SchoolName", samplePaper.SchoolId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "SubjectName", samplePaper.SubjectId);
            return View(samplePaper);
        }

        // GET: SamplePapers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SamplePaper samplePaper = db.SamplePapers.Find(id);
            if (samplePaper == null)
            {
                return HttpNotFound();
            }
            return View(samplePaper);
        }

        // POST: SamplePapers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SamplePaper samplePaper = db.SamplePapers.Find(id);
            db.SamplePapers.Remove(samplePaper);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
