﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.MCQ
{
    public class Report
    {
        public Guid Id { get; set; }
        public Guid QuestionSetId { get; set; }
        public virtual User User { get; set; }
        public string UserId { get; set; }
        public virtual QuestionSet QuestionSet { get; set; }

        public virtual ICollection<Answered> Answered { get; set; }
    }
}
