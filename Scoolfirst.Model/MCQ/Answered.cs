﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.MCQ
{
    public class AnswerGiven
    {
        [Key,Column(Order =0)]
        public string UserId { get; set; }
        [Key,Column(Order =2)]
        public Guid QuestionSetId { get; set; }
        public virtual User User { get; set; }
        public virtual QuestionSet QuestionSet { get; set; }
        public virtual ICollection<Answered> Answers { get; set; }
    }
    public class Answered
    {
        public Guid Id { get; set; }
        [ForeignKey("AnsweredList"), Column(Order = 0)]
        public string UserId { get; set; }
        [ForeignKey("AnsweredList"), Column(Order = 1)]
        public Guid QuestionSetId { get; set; }
        public Guid McqQuestionId { get; set; }
        public Guid OptionsId { get; set; }
        public DateTime On { get; set; }
        public Guid ReportId { get; set; }
        public virtual AnswerGiven AnsweredList { get; set; }
        public virtual Options Options { get; set; }
        public virtual McqQuestion Question { get; set; }
        public virtual Report Report { get; set; }
        
    }


}
