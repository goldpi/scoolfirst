﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.MCQ
{
    public class McqQuestion
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        [Required(ErrorMessage ="This Field is required")]
        public string Unit { get; set; }
        [Required(ErrorMessage ="You have to enter the Question!")]
        public string Query { get; set; }
        public virtual ICollection<Options> Option { get; set; }
        public Guid SetId { get; set; }
        public virtual QuestionSet Set { get; set; }
    }
    
}
