﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.MCQ
{
    public class Options
    {
        public Guid Id { get; set; }
        public Guid McqQuestionId { get; set;}
        public string Answer { get; set; }
        public bool Correct { get; set; }
        public virtual McqQuestion Question { get; set; }
    }
}
