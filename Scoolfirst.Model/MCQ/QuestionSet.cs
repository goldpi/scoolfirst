﻿using Scoolfirst.Model.RNM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.MCQ
{


    public enum TYPEQuetionSet
    {
        NAO
      
    }

    public class QuestionSet
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Class { get; set; }
        [EnumDataType(typeof(TYPEQuetionSet))]
        public TYPEQuetionSet Type { get; set; }
        public int Level { get; set; }
        public bool Active { get; set; }
        public int Minutes { get; set; }
        public virtual ICollection<McqQuestion> Questions { get; set; }


    }
}
