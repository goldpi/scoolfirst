﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Scoolfirst.Model.WorkBook
{
   public  class SyllabusDate
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public int Class { get; set; }
        public int School { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public virtual ICollection<Quiz> Quiz { get; set; }
        public string Content { get; set; }
    }
}
