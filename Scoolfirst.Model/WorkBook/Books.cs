﻿using Scoolfirst.Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.WorkBook
{
    public class Books
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Display(Name ="Image")]
        public string ImageUrl { get; set; }
        public string Author { get; set; }
        public virtual ICollection<School> Schools { get; set; }
        [Display(Name ="Classes")]
        [Required]
        public int ClassesId { get; set; }
        public virtual ICollection<SchoolClassSubject> SchoolClassBooks { get; set; }
        public virtual Classes Classes { get; set; }

        [Required]
        public int SubjectId { get; set; }
        public virtual Subject Subject { get; set; }

        public int? BoardId { get; set; }
        public virtual Board Board { get; set; }

        public bool Global { get; set; }

        public virtual ICollection<Chapter> Chapters { get; set; }
        public virtual ICollection<Questions> Questions { get; set; }
        

    }

    public class BookClassSchoolSyllabus
    {
        [Key,Column(Order =1)]
        public int ClassId { get; set; }
        [Key,Column(Order =2)]
        public int SchoolId { get; set; }
        //[Key,Column(Order =3)]
        //public int BookId { get; set; }
        [Key, Column(Order = 3)]
        public int SubjectId { get; set; }
        public string Map { get; set; }
    }
}
