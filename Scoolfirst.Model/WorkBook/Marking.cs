﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.WorkBook
{
    public class Marking
    {
        public Guid ID { get; set; }
        public Guid QuizId { get; set; }
        public string UserId { get; set; }
        public virtual Quiz Quiz { get; set; }
        public Guid QuestionId { get; set;}
        public virtual Questions Question { get; set; }
        public DateTime On { get; set; }
        public int Points{get;set;}
    }

    public class Score
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Guid QuizId { get; set; }
        public virtual ICollection<Marking> Marks { get; set; }
    }
}
