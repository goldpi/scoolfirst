﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.WorkBook
{
    public class Trending
    {
        [Key]
        public int Id { get; set; }
        [EnumDataType(typeof(TypeOfTrend))]
        public TypeOfTrend TypeOfTrend { get; set; }
        public string Trend { get; set; } 
        public bool Display { get; set; }     
    }

    public enum TypeOfTrend
    {
        REVISION,
        PROGRESSIVE,
        HYBIRD
    }
}
