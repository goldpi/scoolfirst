﻿using Scoolfirst.Model.Identity;
using Scoolfirst.Model.PeriodicAssesment;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.WorkBook
{
    public class TestTracking
    {
        [Key, Column(Order = 0)]
        public string UserId { get; set; }
        public virtual User User { get; set; }
        [Key, Column(Order = 3)]
        public long TestId { get; set; }
        public virtual PARoot Test { get; set; }
        
        public float Month { get; set; }
        public float Score { get; set; }
        public bool ViewSolution { get; set; }
        public bool PointsGiven { get; set; }
        public bool IsPassable { get; set; }
    }
}
