﻿using Scoolfirst.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.WorkBook
{
    public class SamplePaper
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int SchoolId { get; set; }
        public virtual School School { get; set; }
        public int ClassId { get; set; }
        public virtual Classes Class { get; set; }
        public int SubjectId { get; set; }
        public virtual Subject Subject { get; set; }
        public string ImageUrl { get; set;  }
        public string Description { get; set; }
        public DateTimeOffset AddedOn { get; set; }
        public bool Active { get; set; }   
    }
}
