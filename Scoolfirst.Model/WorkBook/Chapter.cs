﻿using Scoolfirst.Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.WorkBook
{
    public class Chapter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [EnumDataType(typeof(Month))]
        public Month Month { get; set; }
        public bool GlobalTrending { get; set; }
        public bool Trending { get; set; }
        public bool Active { get; set; }
        [Display(Name = "Class")]
        public int ClassesId { get; set; }
        public string SearchKeyWord { get; set; }
        public virtual Classes Classes { get; set; }
        public virtual ICollection<Books> Books { get; set; }
        public virtual ICollection<Questions> Questions { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
    }

    public enum Month
    {
        April,
        May,
        June,
        July,
        August,
        Septemeber,
        October,
        November,
        December,
        January,
        Feburary,
        March
    }
}
