﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.WorkBook
{
    public class Note
    {
        public Guid Id { get; set; }
        public virtual Chapter Chapter { get; set; }
        public int ChapterId { get; set; }
        public bool Trending { get; set; }
        public bool GlobalTrending { get; set; }
        public string Topic { get; set; }
        public string RevisonMatter { get; set; }

    }
}
