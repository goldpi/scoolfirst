﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.WorkBook
{
    public class Quiz
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid SyllabusId { get; set; }
        public string Subject { get; set; }
        public string Details { get; set; }
        public int  NoOfQuestion { get; set; }
        public virtual SyllabusDate Syllabus { get; set; }
        public virtual ICollection<Questions> Questions { get; set; }
        public string Class { get; set; }
        public string School { get; set; }
        public DateTime On { get; set; }
    }
}
