﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.WorkBook
{
    public class Questions
    {
        public Guid Id { get; set; }
        public string QuestionType { get; set; }
        public string Title { get; set; }
        public string Answer { get; set; }
        public string Refrence { get; set; }
        public string School { get; set; }
        public string Class { get; set; }
        public string Subject { get; set; }
        public int TypeOrder { get; set; }
        public int Order { get; set; }
        public virtual ICollection<Quiz> Quiz { get; set; }
        public virtual Chapter Chapter { get; set; }
        public int ChapterId { get; set; }
        public virtual ICollection<Books> Books { get; set; }
    }
}
