﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.PlayReasoning
{
   public class PlayQuestion
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "You have to enter the Question!")]
        public string Query { get; set; }
        public string Solution { get; set; }
        public bool ShowSolution { get; set; }
        public virtual ICollection<PlayOptions> Option { get; set; }
        [Required]
        public long SetId { get; set; }
        public virtual PlayRoot Set { get; set; }
        public int ShortOrder { get; set; }
       
    }
}
