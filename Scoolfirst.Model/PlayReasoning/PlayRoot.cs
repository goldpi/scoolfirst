﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Scoolfirst.Model.Common;

namespace Scoolfirst.Model.PlayReasoning
{
   public class PlayRoot
    {
        public long Id { get; set; }

        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        [Display(Name = "Time In Minutes")]
        public int TimeInMinutes { get; set; } = 0;  
        [Display(Name = "Added/Edited On")]   
        public DateTime AddedOn { get; set; }
        public int PointsPerQuestion { get; set; }
        public virtual ICollection<PlayQuestion> QuestionsSet { get; set; }
        public int TestMinutes => TimeInMinutes * 60;
        [Display(Name = "Test Brief")]
        public string TestBrief { get; set; }
        public int Order { get; set; }
        public string Level { get; set; }
        public string Color { get; set; }
        [Display(Name = "Title Color")]
        public string TitleColor { get; set; }
        public string Category { get; set; }
        public string Subject { get; set; }

        public Boolean Active { get; set; } = false;
       
        public Boolean IsChallange { get; set; }
        [Display(Name = "Publish Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "dd-MM-yyyy hh:mm:ss", ApplyFormatInEditMode = true)]
        public DateTime? StartsOn { get; set; }
        public double? PrizeAmount { get; set; } = 0;
        public double? MaxCap { get; set; } = 0;


    }
}
