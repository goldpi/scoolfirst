﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.PlayReasoning
{
   public class PlayAnswers
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Guid OptionId { get; set; }
        public Guid QuestionId { get; set; }
        public long PlayId { get; set; }
        public Guid AttemptId { get; set; }
        public virtual User User { get; set; }
        public virtual PlayOptions Option { get; set; }
        public virtual PlayQuestion Question { get; set; }
        public virtual PlayRoot Play { get; set; }
        public virtual PlayAttempt Attempt { get; set; }
        
    }
}
