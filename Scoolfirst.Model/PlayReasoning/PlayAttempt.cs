﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.PlayReasoning
{
   public class PlayAttempt
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public long PlayId { get; set; }
        public DateTime On { get; set; }
        public virtual ICollection<PlayAnswers> Ans { get; set; }
        public virtual PlayRoot Play { get; set; }
        public int RightAns { get; set; }
        public int WrongAns { get; set; }
        public int SkippedAns { get; set; }
        public virtual User User { get; set; }
        public int Total => RightAns + WrongAns + SkippedAns;
        public int PointsEarned { get; set; }
        public int BoosterUsed { get; set; }
    }
}
