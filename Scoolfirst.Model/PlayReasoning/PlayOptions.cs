﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.PlayReasoning
{
   public class PlayOptions
    {
        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }
        public string Answer { get; set; }
        public bool Correct { get; set; }
        public int ShortOrder { get; set; }
        public virtual PlayQuestion Question { get; set; }
    }
}
