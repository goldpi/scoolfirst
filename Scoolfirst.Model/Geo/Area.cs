﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Geo
{
    public class Area
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PostBox { get; set; }
        public int CityId { get; set; }
        public virtual City City { get; set; }
    }
}
