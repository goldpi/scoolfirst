﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.KDC
{
    public class LikeArticle
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Guid ArticleId { get; set; }
        public virtual Article Article { get; set; }
        public virtual User User { get; set; }
        public DateTime On { get; set; }
    }

    public class FavArticle
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Guid ArticleId { get; set; }
        public virtual Article Article { get; set; }
        public virtual User User { get; set; }
        public DateTime On { get; set; }
    }


    public class ArticleComment
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Guid ArticleId { get; set; }
        public virtual Article Article { get; set; }
        public virtual User User { get; set; }
        public string Comment { get; set; }
        public DateTime On { get; set; }
    }

}
