﻿using Scoolfirst.Model.Common;
using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;



namespace Scoolfirst.Model.KDC
{
    public class Article
    {
        public string Title { get; set; }
        public string Solution { get; set; }
        public string ImageUrl { get; set; }
        public bool Approved { get; set; }
        public bool Deleted { get; set; }
        public DateTime On { get; set; }
        public Guid ID { get; set; }
        public string UserId { get; set; }
        public virtual User User { get; set; }
        public int? ClassesId { get; set; }
        public virtual Classes Classes { get; set; }
        public int? SubjectId { get; set; }
        public virtual Subject Subject { get; set; }
        public bool Answered { get;set; }
        public virtual ICollection<LikeArticle> Likes { get; set; }
        public virtual ICollection<FavArticle> Favs { get; set; }
        public virtual ICollection<ArticleComment> Comments { get; set; }
    }
}