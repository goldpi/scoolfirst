﻿using Scoolfirst.Model.Identity;
using System;

namespace Scoolfirst.Model.Feed
{

    public class FeedReply
    {
        public Guid Id { get; set; }
        public Guid CommentId { get; set; }
        public string UserId { get; set; }
        public string Reply { get; set; }
        public DateTime On { get; set; }
        public virtual FeedComment Comment { get; set; }
        public virtual User User { get; set; }
    }
}