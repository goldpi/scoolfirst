﻿using Scoolfirst.Model.Identity;
using System;

namespace Scoolfirst.Model.Feed
{

    public class FeedStar
    {
        public Guid Id { get; set; }
        public virtual User User { get; set; }
        public string UserId { get; set; }
        public DateTime On { get; set; }
        public Guid FeedId { get; set; }
        public virtual Feeds Feed { get; set; }
    }
}