﻿using System;

namespace Scoolfirst.Model.Feed
{
    public class PostImages
    {
        public Guid Id { get; set; }
        public string ImageUrl { get; set; }
        public string ImageTitle { get; set; }
        public long PostId { get; set; }
    }
}