﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scoolfirst.Model.Feed
{
    public class Post
    {
        public long Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        public bool Pinned { get; set; } = false;
        public string UserId { get; set; }
        public string Tags { get; set; }
        [NotMapped]
        public string[] TagsToArray => (!string.IsNullOrEmpty(Tags)) ? Tags.Split(',') : new string[0];
        public virtual ICollection<PostImages> Images { get; set; }
        public virtual ICollection<PostVideos> Videos { get; set; }
        [EnumDataType(typeof(PostType))]
        public PostType Type { get; set; }
        public DateTime OnDateTIme { get; set; }

    }
}
