﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;

namespace Scoolfirst.Model.Feed
{

    public class FeedComment
    {
        public Guid Id { get; set; }
        public Guid FeedId { get; set; }
        public string UserId { get; set; }
        public string Comment { get; set; }
        public DateTime On { get; set; }
        public virtual Feeds Feed { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<FeedReply> Replys { get; set; }

       
    }
}