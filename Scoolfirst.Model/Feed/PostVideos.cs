﻿using System;

namespace Scoolfirst.Model.Feed
{

    public class PostVideos
    {
        public Guid Id { get; set; }
        public string VideoUrl  { get; set; }
        public long PostId { get; set; }
    }
}