﻿using Scoolfirst.Model.Common;
using System;
using System.Collections.Generic;

namespace Scoolfirst.Model.Feed
{
    public class Feeds
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public bool Pinned { get; set; }
        public long PostId { get; set; }
        public int GroupId { get; set; }
        public virtual Group Group { get; set; }
        public virtual Post Post { get; set; }
        public DateTime OnDateTime { get; set; }
        public bool FeaturedOnPastFutre { get; set; }
        public virtual ICollection<FeedComment> Comments { get; set; }
        public virtual ICollection<FeedLike> Likes { get; set; }
        public virtual ICollection<FeedStar> Stared { get; set; }
        
    }
}