﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Scoolfirst.Model.Projects;

namespace Scoolfirst.Model.Common
{
    public class ProjectCategory
    {
       
        public int Id { get; set; }
        [Required(ErrorMessage="Name is required")]
        public string Name { get; set; }
        public bool HasParent { get; set; }
        public ICollection<ProjectCategory> Child { get; set; }
        public virtual ProjectCategory Parent { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}