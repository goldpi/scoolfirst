﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class Goals
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public int Points { get; set; }
        public DateTime StartDate { get; set; }
        [EnumDataType(typeof(GoalLevel))]
        public GoalLevel GoalLevel { get; set; }
        public int Days { get; set; }
        public DateTime EndDate => StartDate.AddDays(Days); 
    }
}
