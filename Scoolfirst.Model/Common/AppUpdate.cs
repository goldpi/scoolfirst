﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class AppUpdate
    {
        public Guid Id { get; set; }
        [Required]
        [MaxLength(140)]
        public string Title { get; set; }


        [MaxLength(20)]
        public string UpdateTitle1 { get; set; }
        [MaxLength(140)]
        public string Update1 { get; set; }
        public bool ShowUpdate1 { get; set; }
        [MaxLength(20)]
        public string UpdateTitle2 { get; set; }
        [MaxLength(140)]
        public string Update2 { get; set; }
        public bool ShowUpdate2 { get; set; }
        [MaxLength(20)]
        public string UpdateTitle3 { get; set; }
        [MaxLength(140)]
        public string Update3 { get; set; }
        public bool ShowUpdate3 { get; set; }
        [MaxLength(20)]
        public string UpdateTitle4 { get; set; }
        [MaxLength(140)]
        public string Update4 { get; set; }
        public bool ShowUpdate4 { get; set; }

        public DateTime AddedOn { get; set; }

    }
}
