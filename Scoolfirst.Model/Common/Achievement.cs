﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Scoolfirst.Model.Common
{
   public  class Achievement
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        [Display]
        public string AchievementDetails { get; set; }
        [Display]
        public DateTime OnDate { get; set; }
        public bool Status { get; set; }
    }
}
