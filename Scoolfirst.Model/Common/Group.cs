﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string School { get; set; }

        public virtual ICollection<School> Schools { get; set; }
        public virtual ICollection<Classes> Class { get; set; }
    }
}
