﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class RoleModel
    {
        public int Id { get; set; }
        public Classes Class { get; set; }
        public int? ClassId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Content { get; set; }
        [Url]
        public string ImageUrl { get; set; }
        [Url]
        public string VideoUrl { get; set; }
        public bool Pinned { get; set; }
        public DateTime On { get; set; }

        public string TypeOfRm { get; set; }
    }


    public class NewsGlobal
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Display(Name="Image Url")]
        [Url]
        public string ImageUrl { get; set; }
        [Display(Name ="Video Url")]
        [Url]
        public string VideoUrl { get; set; }
        public string Content { get; set; }
        public DateTime On { get; set; }

        public NewsType Type { get; set; }
    }
}
