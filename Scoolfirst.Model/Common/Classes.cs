﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class Classes
    {
        public int Id { get; set; }
        public string RomanDisplay { get; set; }
        public int NumericDisplay { get; set; }
        public virtual ICollection<Group> Groups { get; set; }
    }
}
