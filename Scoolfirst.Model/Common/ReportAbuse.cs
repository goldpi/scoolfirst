﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
   public  class ReportAbuseTitle
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name ="Message")]
        public string Title { get; set; }
        public DateTime EntryDate { get; set; }
        public bool Visible { get; set; }
    }


    public class ReportedAbused
    {
        public Guid Id { get; set; }
        [EnumDataType(typeof(TypeSource))]
        public TypeSource Source { get; set; }
        public string Text { get; set; }
        public string IdOfPost { get; set; }
    }
}
