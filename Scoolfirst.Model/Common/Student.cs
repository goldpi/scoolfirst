﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class Student
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string BoardName { get; set; }
        public virtual Classes Class { get; set; }
        public virtual School School { get; set; }
        public int ClassId { get; set; }
        public int? SchoolId { get; set; }
        public bool IsBrandAmbasdor { get; set; }
        [EnumDataType(typeof(Gender))]
        public Gender Gender { get; set; }
        public int? ParentId { get; set; }
        public virtual Parent Parent { get; set; }
        public virtual User User { get; set; }
        
    }
}
