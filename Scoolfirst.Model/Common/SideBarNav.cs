﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class SideBarNav
    {
        [Key]
        [StringLength(200)]
        [Required]
        public string Title { get; set; }
        [Required]
        public int Order { get; set; }
        [Display(Name = "Sub Menu Items")]
        [Required]
        public string SubMenu { get; set; }

        public string ModuleDataId { get; set; }
        public virtual ModuleData ModuleData { get; set; }
        public string[] Menu => (SubMenu??"").Split(',');
    }
}
