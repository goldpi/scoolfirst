﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
   public class GeneralKnowledge
    {
        public int Id { get; set; }
        public Classes Class { get; set; }
        public int? ClassId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Content { get; set; }
        [Url]
        public string ImageUrl { get; set; }
        [Url]
        public string VideoUrl { get; set; }
        public bool Pinned { get; set; }
        public DateTime On { get; set; }
        public string Subject { get; set; }
    }
}
