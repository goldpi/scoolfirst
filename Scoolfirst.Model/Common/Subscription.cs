﻿using Newtonsoft.Json;
using System;

namespace Scoolfirst.Model.Common
{

    public class SubscriptionError
    {
        public int Code { get; set; }
        public string Reason { get; set; }
    }

    public class Subscription
    {
        [JsonProperty(PropertyName = "Id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "Image")]
        public string Image { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Decription")]
        public string Decription { get; set; }

        [JsonProperty(PropertyName = "Price")]
        public decimal Price { get; set; }

        [JsonProperty(PropertyName = "OfferValidity")]
        public DateTime? OfferValidity { get; set; }

        [JsonProperty(PropertyName = "MRP")]
        public decimal MRP { get; set; }

        [JsonProperty(PropertyName = "BGColor")]
        public string BGColor { get; set; }

        [JsonProperty(PropertyName = "BtmColor")]
        public string BtmColor { get; set; }

        [JsonProperty(PropertyName = "BillingCycle")]
        public string BillingCycle { get; set; }

        [JsonProperty(PropertyName = "MostPopular")]
        public bool MostPopular { get; set; }

        [JsonProperty(PropertyName = "Days")]
        public int Days { get; set; }
    }
}
