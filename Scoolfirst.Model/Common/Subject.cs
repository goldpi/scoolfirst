﻿using Scoolfirst.Model.WorkBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class Subject
    {
        public int Id { get; set; }
        public string SubjectName { get; set; }
        public string DisplayName { get; set; }
        public string Color { get; set; }
        public bool ShowOnReasonibg { get; set; }
        
        public virtual ICollection<Books> Books { get; set; }
    }


    public class SchoolClassSubject
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public int ClassId { get; set; }
        public int SubjectId { get; set; }

        public virtual School School { get; set; }
        public virtual Classes Class { get; set; }
        public virtual Subject Subject { get; set; }

        public virtual ICollection<Books> Books { get; set; }
    }
}
