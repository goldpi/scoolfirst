﻿using Scoolfirst.Model.Geo;
using Scoolfirst.Model.WorkBook;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class School
    {
        public int Id { get; set; }
        public string SchoolName { get; set; }
        public int AreaId { get; set; }
        public string LocalArea { get; set; }
        public virtual Area Area { get; set; }
        public bool UseMasterContent { get; set; }
        public bool IsMasterContent { get; set; }
        public int? BoardId { get; set; }
        public Board Board { get; set; }
        public virtual ICollection<Group> Groups { get; set; }
        public virtual ICollection<Books> Book { get; set; }

        public string FoundationNotice { get; set; }
        public string AssementNotice { get; set; }
        public bool Active { get; set; }
        //[Required()]
        public string ReasonToDeactivate { get; set; }
    }
    
    public class SchoolRepresentativeProfile
    {
        public int Id { get; set; }
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public bool Active { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        public int SchoolId { get; set; }
        public virtual School School { get; set; }

        public Scoolfirst.Model.Identity.User AdminSchoolUserInstance(SchoolRepresentativeProfile model, string StudentAvtar, string ParentAvtar)
        {
            return new Scoolfirst.Model.Identity.User
            {
                UserName = model.Email ?? model.MobileNo,
                Email = model.Email ?? null,
                FullName = model.Name,
                Student_Parent_Admin = "School",
                PhoneNumber = model.MobileNo,
                SchoolId = model.SchoolId,
                RegisterdOn = DateTime.UtcNow,
                Expiry = DateTime.UtcNow,
               

            };
        }

        public Model.Identity.User NewUserInstance(SchoolRepresentativeProfile model, string studentAvtar, string parentAvtar)
        {
            throw new NotImplementedException();
        }
    }
}
