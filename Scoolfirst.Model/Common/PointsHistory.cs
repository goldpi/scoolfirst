﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scoolfirst.Model.Identity;
using Scoolfirst.Model.KDC;

namespace Scoolfirst.Model.Common
{
    public class PointsHistory
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public int Points { get; set; }
        public DateTime On { get; set; }
       
        public string RelatedId { get; set; }
        public string RelatedType { get; set; }
        public string ShortDescription { get; set; }
    }
}
