﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class Board
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Medium MediumLanguage { get; set; }
    }
}
