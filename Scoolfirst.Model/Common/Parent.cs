﻿using Scoolfirst.Model.Geo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class Parent
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        [EnumDataType(typeof(Gudrdian))]
        public Gudrdian GurdianType { get; set; }
        public string Street { get; set; }
        public Area Area { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}
