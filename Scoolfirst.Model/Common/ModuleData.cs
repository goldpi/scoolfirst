﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class ModuleData
    {
        [Key]
        public string ModuleName { get; set; }
        public bool Free { get; set; }
        public bool AuthRequired { get; set; }
        public bool Active { get; set; }
        public string Message { get; set; }
        public ICollection<ModuleControllers> ActionControllers { get; set; }
    }
    public class ModuleControllers
    {
        [Key]
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Controller { get; set; }
        public string Action { get; set; }
        public virtual ModuleData Module { get; set; }
        public string ModuleId { get; set; }
    }
}
