﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Common
{
    public class Summary
    {
        public Guid Id { get; set; }
        [EnumDataType(typeof(SummaryType))]
        public SummaryType Type { get; set; }
        public string Summarizer{ get; set; }
        public string Visualizer { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int Class { get; set; }
        public int School { get; set; }

    }


}
