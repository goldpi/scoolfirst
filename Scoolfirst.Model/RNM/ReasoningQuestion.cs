﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Scoolfirst.Model.RNM
{

    public class ReasoningQuestion
    {
        public Guid Id { get; set; }
      
        
        [Required(ErrorMessage = "You have to enter the Question!")]
        public string Query { get; set; }
        public virtual ICollection<ReasoningOptions> Option { get; set; }
        public long? SetId { get; set; }
        public virtual ReasoningRoot Set { get; set; }
        public int ShortOrder { get; set; }
        public string Module { get; set; }
        public string Solution { get; set; }
    }
}