﻿using Scoolfirst.Model.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scoolfirst.Model.RNM
{

    public class ReasoningTracker
    {
        [Key, Column(Order = 0)]
        public string UserId { get; set; }
        [Key, Column(Order = 1)]
        public long RnmPostId { get; set; }
        public DateTime OnDateTime { get; set; }
        public virtual User User { get; set; }
        public virtual ReasoningRoot RnmPost { get; set; }
        public string Level { get; set; }
       // public bool TypeExam { get; set; } = false;
        public bool TestPassed { get; set; } = false;
       // public bool CheckPassed => TypeExam?
        public int NoOfDays => RnmPost.Days;
        public bool Complete => (DateTime.UtcNow - OnDateTime).Days > NoOfDays || TestPassed;
        public int Days => Complete ? NoOfDays : (DateTime.UtcNow - OnDateTime).Days;
        public string Accepted { get; set; }
    }
}