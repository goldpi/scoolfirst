﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Scoolfirst.Model.RNM
{
   public class ReasoningRoot
    {
        public long Id { get; set; }
        public string Class { get; set; }
        public string Content { get; set; }
        public string ShortDiscription { get; set; }
        public string Title { get; set; }
        public int MinQuestionToPass { get; set; }
        public int TimeInMinutes { get; set; } = 0;
        public string Tips { get; set; }
        public DateTime AddedOn { get; set; }
        [EnumDataType(typeof(ReasoningRootType))]
        public ReasoningRootType IsLessonOrExam { get; set; }
        public int Days { get; set; }
        public long? PreviousPostId { get; set; }
        public virtual ReasoningRoot PreviousPost{ get; set; }
        public virtual ICollection<ReasoningQuestion> Questions { get; set; }
        public int TestMinutes => TimeInMinutes * 60;
        public string TestBrief { get; set; }
        public int Order { get; set; }
        public string Level { get; set; }
        public string Color { get; set; }
        public string TitleColor { get; set; }
        public string VideoUrl { get; set; }

    }
}
