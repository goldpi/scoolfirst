﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;

namespace Scoolfirst.Model.RNM
{


    public class ReasoningAnswers
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Guid OptionId { get; set; }
        public Guid QuestionId { get; set; }
        public long ReasoningId { get; set; }
        public Guid AttempId { get; set; }
        
        public virtual User User { get; set; }
        public virtual ReasoningOptions Option { get; set; }
        public virtual ReasoningQuestion Question { get; set; }
        public virtual ReasoningRoot Reasoning { get; set; }
        public virtual ReasoningAttemp Attemp { get; set; }
    }
    public class ReasoningAttemp
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public long ReasoningId { get; set; }
        public DateTime On { get; set; }
        public virtual ICollection<ReasoningAnswers> Ans { get; set; }
    }
}