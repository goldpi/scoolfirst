﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Scoolfirst.Model.RNM
{

    public class ReasoningOptions
    {

        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }
        public string Answer { get; set; }
        public bool Correct { get; set; }
        public int ShortOrder { get; set; }
        public virtual ReasoningQuestion Question { get; set; }

    }
}