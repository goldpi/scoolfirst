﻿namespace Scoolfirst.Model.RNM
{
    public enum ReasoningRootType
    {
        Lesson=1,
        Exam=2,
        Single=3,
        RevisionOfLesson = 4,
        ReviewOfExam=5,
        Assessment=6

    }
}