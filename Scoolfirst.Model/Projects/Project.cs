﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Scoolfirst.Model.Common;
namespace Scoolfirst.Model.Projects
{
    public class Project
    {
        
        public int Id { get; set; }
        public int CategoryId { get; set; }
        [Required(ErrorMessage="Please enter project name")]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public decimal Order { get; set; }
        [Display(Name="Abstract File")]
        public string FileUrl { get; set; }
        public bool Active { get; set; }
        public DateTime AddedOn { get; set; }
        public virtual ProjectCategory Category { get; set; }
    }
}