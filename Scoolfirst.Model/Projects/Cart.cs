﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Projects
{
    public class Cart
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public virtual User User { get; set; }
        public bool CheckOut { get; set; }
        public virtual ICollection<CartItems> Items { get; set; }

    }
    public class CartItems
    {
        public Guid Id { get; set; }
        public Guid CartId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public virtual Product Product { get; set; }
    }
}
