﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Scoolfirst.Model.Projects
{
    public class Order
    {
        //Invoice No pattern UJ+ yy+ mm + id

        public int Id { get; set; } = 0;
        [Display(Name ="User Id")]
        public string UserId { get; set; }
        [Display(Name = "Product Id")]
        public int ProductId { get; set; }
        //public virtual Project Project { get; set; }
        public virtual Product Product { get; set; }

        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }

        public DateTime OrderDate { get; set; } = DateTime.Now;
        public bool Shipped { get; set; }
        public decimal? ShippingCharges { get; set; }
        public string TrackingNo { get; set; } = "";

        public Guid TransactionId { get; set; }
        public virtual Payments.Transaction Transaction { get; set; }

        public string PrintInvoiceNo => string.Format("UJ{0:yyMM}{1}", OrderDate, Id.ToString("D"+InfoId.ToString()));

        public int InfoId => Id.ToString("D").Length + 5;
    }
}