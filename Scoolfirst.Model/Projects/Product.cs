﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Projects
{
    public class Product
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Video { get; set; }
        [Display(Name ="Product Description")]
        public string ProductDisc { get; set; }
        [Display(Name = "Short Description")]
        public string ShortDisc { get; set; }
        [EnumDataType(typeof(Reedeem))]
        public  Reedeem Reedeem { get; set; }
        public string ShowPoints { get; set; }
        public string ShowMoney { get; set; }
        public decimal Price { get; set; }
        public bool Active { get; set; }
        [Display(Name = "Added On")]
        public DateTime AddedOn { get; set; }
        public int CategoryId { get; set; }
        public virtual ProductCategory Category { get; set; }


    }

        public enum Reedeem
        {
            OnlyPoints,
            OnlyMoney
        }
}
