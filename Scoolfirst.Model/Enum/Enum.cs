﻿using System.ComponentModel.DataAnnotations;

public enum TypeSource
{
    FeedComment,
    KDCComment,
    KDC,
    Project,
    Feed,
    Bug,
    Genearal
}

public enum GoalLevel
{
    Expert,
    Engage,
    Explore,
    Encourage
}

public enum Gender
{
    Male,
    Female,
    Other
}

public enum Gudrdian
{
    Father,
    Mother,
    GrandMother,
    GrandFather,
    Gurdian
}

public enum Medium
{
    English,
    Hindi,
    Urdu,
    Regional
}

public enum UserType
{
    Parent,
    Student,
    School
}
public enum RegisterAs
{
    Guest,
    User
    
}

public enum QuizType
{
    ChapterWorkBook,
    TestWorkBook,
    ModuleWorkBook,
    ConnectedTest,
    FeedQuiz,
    Book
}

public enum PostType
{

    KNOWLEDGE_NUTRITION=0,
    [Display(Name = "Must remember tricks")]
    MUST_REMEMBER_TRICKS = 1,
    CREATIVE_CANVAS=2
    //BreakBrainFast,
    //ExperimentalLearning,
    //ReasoningAndMathematics,
    //Vocalabury
    
}

public enum NewsType
{
    News,
    CC
}

public enum SummaryType
{
    SCHOOL,UPGRADE,WORLD_AROUND
}