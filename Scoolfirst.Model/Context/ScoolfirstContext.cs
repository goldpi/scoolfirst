﻿using Microsoft.AspNet.Identity.EntityFramework;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Feed;
using Scoolfirst.Model.Geo;
using Scoolfirst.Model.Identity;
using Scoolfirst.Model.KDC;
using Scoolfirst.Model.NAO;
using Scoolfirst.Model.MCQ;
using Scoolfirst.Model.Notifications;
using Scoolfirst.Model.Projects;
using Scoolfirst.Model.Slides;
using Scoolfirst.Model.WorkBook;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Scoolfirst.Model.Application_Setting;
using Scoolfirst.Model.RNM;
using Scoolfirst.Model.Payments;

using Scoolfirst.Model.PeriodicAssesment;
using Scoolfirst.Model.PlayReasoning;
using Scoolfirst.Model.VirtualCompetetion;
using Scoolfirst.Model.DaliyTestNameSpace;

namespace Scoolfirst.Model.Context
{
    public class ScoolfirstContext : IdentityDbContext<User>
    {
        public ScoolfirstContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ScoolfirstContext Create()
        {
            return new ScoolfirstContext();
        }

        #region Common
        public virtual DbSet<Classes> Classes { get; set; }
        public virtual DbSet<Parent> Parents { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<School> Schools { get; set; }
        //public virtual DbSet<SchoolRepresentativeProfile> SchoolProfile { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<ReportAbuseTitle> ReportAbuseTitle { get; set; }
        public virtual DbSet<ReportedAbused> ReportAbused { get; set; }
        public virtual DbSet<RoleModel> RoleModels { get; set; }
        public virtual DbSet<NewsGlobal> News { get; set; }
        public virtual DbSet<Goals> Goals { get; set; }
        public virtual DbSet<Board> Boards { get; set; }
        public virtual DbSet<Summary> Summary { get; set; }
        public virtual DbSet<Achievement> Achievements { get; set; }
        public virtual DbSet<GeneralKnowledge> GeneralKnowledges { get; set; }
        #endregion
        #region Feed

        public virtual DbSet<Feeds> Feeds { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<PostImages> PostImages { get; set; }
        public virtual DbSet<PostVideos> PostVideo { get; set; }
        public virtual DbSet<FeedComment> FeedComments { get; set; }
        public virtual DbSet<FeedReply> FeedReply { get; set; }
        public virtual DbSet<FeedStar> FeedStars { get; set; }
        public virtual DbSet<FeedLike> FeedLike { get; set; }

        #endregion
        #region KDC
        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<LikeArticle> LikeArticles { get; set; }
        public virtual DbSet<FavArticle> FavArticles { get; set; }
        public virtual DbSet<ArticleComment> ArticleComments { get; set; }
        public virtual DbSet<PointsHistory> PointsHistory { get; set; }
        #endregion
        #region GEO
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Area> Areas { get; set; }
        #endregion
        #region Project
        public virtual DbSet<ProjectCategory> ProjectCategories { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }

        public virtual DbSet<Cart> Carts { get; set; }
        public virtual DbSet<CartItems> CartItems { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<Subcription> Subcriptions { get; set; }
        public virtual DbSet<SubcriptionPackage> SubcriptionPackages { get; set; }

        #endregion
        #region WorkBook

        public virtual DbSet<SchoolClassSubject> SchoolClassSubject { get; set; }
        public virtual DbSet<Questions> Questions { get; set; }
        public virtual DbSet<Quiz> Quizes { get; set; }
        public virtual DbSet<Marking> Marks { get; set; }
        public virtual DbSet<Score> Score { get; set; }
        public virtual DbSet<Books> Books { get; set; }
        public virtual DbSet<Chapter> Chapters { get; set; }
        public virtual DbSet<BookClassSchoolSyllabus> BookClassSchoolSyllabus { get; set; }

        public virtual DbSet<Note> Notes { get; set; }
        public virtual DbSet<Trending> Trending { get; set; }
        public virtual DbSet<SamplePaper> SamplePapers { get; set; }

        #endregion
        #region Slider
        public virtual DbSet<Slider> Sliders { get; set; }
        public virtual DbSet<SliderSlice> SliderSlices { get; set; }
        public virtual DbSet<SyllabusDate> SyllabusDates { get; set; }
        #endregion
        #region Noa
        public virtual DbSet<Olympiad> Olympiads { get; set; }
        #endregion
        #region MCQ
        public virtual DbSet<Answered> Answereds { get; set; }
        public virtual DbSet<McqQuestion> McqQuestions { get; set; }
        public virtual DbSet<Options> Options { get; set; }
        public virtual DbSet<QuestionSet> QuestionSets { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<AnswerGiven> AnswerGiven { get; set; }
        #endregion
        #region RNMPost
        public virtual DbSet<ReasoningRoot> ReasoningRoots { get; set; }
        public virtual DbSet<ReasoningQuestion> ReasoningQuestions { get; set; }
        public virtual DbSet<ReasoningOptions> ReasoningOptions { get; set; }
        public virtual DbSet<ReasoningTracker> ReasoningTracker { get; set; }
        public virtual DbSet<ReasoningAnswers> ReasoningAnswers { get; set; }
        public virtual DbSet<ReasoningAttemp> ReasoningAttemp { get; set; }
        #endregion
        #region PA
        public virtual DbSet<PARoot> PARoots { get; set; }
        public virtual DbSet<PAQuestion> PAQuestions { get; set; }
        public virtual DbSet<PAOptions> PAOptions { get; set; }
        //public virtual DbSet<PATracker> PATracker { get; set; }
        public virtual DbSet<PAAnswers> PAAnswers { get; set; }
        public virtual DbSet<PAAttemp> PAAttemp { get; set; }
        #endregion

        #region Play
        public virtual DbSet<PlayRoot> PlayRoots { get; set; }
        public virtual DbSet<PlayQuestion> PlayQuestions { get; set; }
        public virtual DbSet<PlayOptions> PlayOptions { get; set; }
        public virtual DbSet<PlayAnswers> PlayAnswers { get; set; }
        public virtual DbSet<PlayAttempt> PlayAttempts { get; set; }
        #endregion

       
        #region ApplicationDep
        public virtual DbSet<ApplicationSetting> ApplicationSetting { get; set; }
        public virtual DbSet<SideBarNav> SideBarNavigation { get; set; }
        public virtual DbSet<ModuleData> Modules { get; set; }
        public virtual DbSet<ModuleControllers> ControllerAction { get; set; }
        public virtual DbSet<ActivityLogger> ActivityLogger { get; set; }

        #endregion

       public virtual DbSet<Booster> Booster { get; set; }


        public virtual DbSet<Camp.CampCategory> CampCategories { get; set; }
        public virtual DbSet<Camp.CampPost> CampPosts { get; set; }
        public virtual DbSet<Camp.SubscribedCamp> SubscribedCamps { get; set; }
        public virtual DbSet<Camp.CampAnswers> CampAnswers { get; set; }
        public virtual DbSet<Camp.CampAttempt> CampAttempt { get; set; }
        public virtual DbSet<Camp.CampOptions> CampOptions { get; set; }
        public virtual DbSet<Camp.CampQuestion> CampQuestions { get; set; }

        public virtual DbSet<OfflinePayment> OfflinePayments { get; set; }
        public virtual DbSet<TestTracking> TestTrackings { get; set; }
        public virtual DbSet<AppUpdate> AppUpdates { get; set; }

        #region VirtualCOmpetetion

        public virtual DbSet<VirtualCompetetionAnswers> VirtualCompetetionAnswers { get; set; }
        public virtual DbSet<VirtualCompetetion.VirtualCompetetionMainCategory> VirtualCompetetionMainCategories { get; set; }
        public virtual DbSet<VirtualCompetetion.VirtualCompetetionOptions> VirtualCompetetionOptions { get; set; }
        public virtual DbSet<VirtualCompetetion.VirtualCompetetionQuestion> VirtualCompetetionQuestions { get; set; }
        public virtual DbSet<VirtualCompetitonSubCategory> VirtualCompetitonSubCategories { get; set; }
        public virtual DbSet<VirtualCompetetionAttempt> VirtualCompetetionAttempts { get; set; }


        #endregion

        public virtual DbSet<VirtualExam> VirtualExams { get; set; }


        #region DaliyTest
        public virtual DbSet<DaliyTest> DaliyTests { get; set; }
        public virtual DbSet<DTQuestionSet> DTQuestionSet { get; set; }
        public virtual DbSet<DTQuestion> DTQuestions { get; set; }
        public virtual DbSet<DTAnswer> DTAnswers { get; set; }
        public virtual DbSet<DTAppeared> DTAppearences { get; set; }
        public virtual DbSet<DTAnswered> DTAnswereds { get; set; }
        #endregion




    }


}
