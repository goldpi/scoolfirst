﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Payments
{
    public class Booster
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public int Factor { get; set; }
        public DateTime ExpiryOn { get; set; }
       
    }
}
