﻿using Scoolfirst.Model.Identity;
using Scoolfirst.Model.Projects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Payments
{
    public class Transaction
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public TransactionsType Type { get; set; } = TransactionsType.Subscription;
        public double Amount { get; set; }
        public string Description { get; set; }
        public Guid? CartId { get; set; }
        public virtual Cart Cart { get; set; }
        public bool IsForCart  => CartId.HasValue;
        public string UserId { get; set; }

        public Guid? SubcriptionId { get; set; }
        public Subcription Subcription { get; set; }
        public bool IsForSubcription => SubcriptionId.HasValue;
        public string TransactionResult { get; set; }
        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int? Points { get; set; }
        public  bool Redeem { get; set; }
        public DateTime On { get; set; } = DateTime.UtcNow;

        public string Email { get; set; }
        public string Address { get; set; }

    }

    public class SubcriptionPackage
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }
    }

    public class Subcription
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string UserId { get; set; }
        public DateTime Expiry { get; set; }
        public Guid SubcriptionPackageId { get; set; }
        public virtual SubcriptionPackage SubcriptionPackage { get; set; }
    }

    public class OfflinePayment
    {
        public Guid Id { get; set; }
        public double Amount { get; set; }
        public string UserId { get; set; }
        public string Description { get; set; }
        public bool Paid { get; set; } = false;
        public virtual User User { get; set; }
        public DateTimeOffset RequestDate { get; set; }
        public string PaidRemaks { get; set; }
        [NotMapped]
        public int Days { get; set; }

    }




    public enum TransactionsType
    {
        Subscription,
        Purchase,
        Refund,
        F4x,
        F8x,
        
    }

   
}
