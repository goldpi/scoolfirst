﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Provider.PaymentGateway
{
    public class PayUMoney : IGateWay
    {

        public string Host => $"http://localhost:{System.Web.HttpContext.Current.Request.Url.Port}";

        public string Key => System.Web.HttpContext.Current.Request.IsLocal? PyamentGatewaySettings.DevKey.Value():PyamentGatewaySettings.Key.Value();

        public string Salt => System.Web.HttpContext.Current.Request.IsLocal ? PyamentGatewaySettings.DevSalt.Value() : PyamentGatewaySettings.Salt.Value();

        public string EndPoint => System.Web.HttpContext.Current.Request.IsLocal ? PyamentGatewaySettings.TestUrl.Value() : PyamentGatewaySettings.LiveUrl.Value();

        public string SuccesUrl => System.Web.HttpContext.Current.Request.IsLocal ?  this.Host+PyamentGatewaySettings.SuccesUrl.Value() :HostingServerInfo.Application.GetCompleteUrl()+PyamentGatewaySettings.SuccesUrl.Value();

        public string FailedUrl => System.Web.HttpContext.Current.Request.IsLocal ? this.Host + PyamentGatewaySettings.FailedUrl.Value() : HostingServerInfo.Application.GetCompleteUrl() + PyamentGatewaySettings.FailedUrl.Value();

        public string MakePayment(string Txnid,string Amount,string ProductInfo,string Email,string Phone,string FirstName)
        {
            var Vm = new PayUMoneyVm
            {
                Amount = Amount,
                Email = Email,
                FirstName = FirstName,
                Furl = FailedUrl,
                Surl = SuccesUrl,
                Key = Key,
                Phone = Phone,
                ProductInfo = ProductInfo,
                Salt = Salt,
                Txnid = Txnid,
                ServiceProvider="payu"
            };
            return Vm.Post(EndPoint);
        }

        public string PaymentCanceled(dynamic Object)
        {
            throw new NotImplementedException();
        }

        public string PaymentFailed(dynamic Object)
        {
            throw new NotImplementedException();
        }

        public string PaymentSuccess(dynamic Object)
        {
            throw new NotImplementedException();
        }

        public string Generatehash512(string text)
        {

            byte[] message = Encoding.UTF8.GetBytes(text);

            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            SHA512Managed hashString = new SHA512Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;

        }
    }

    public class PayUMoneyVm
    {
        private NameValueCollection Collection = new NameValueCollection();
        public string Key { set { Collection.Add("key", value); } get { return Collection["key"]; } }
        public string Salt { get; set; }
        public string Txnid { set { Collection.Add("txnid", value); } get { return Collection["txnid"]; } }
        public string Amount { set { Collection.Add("amount", value); } get { return Collection["amount"]; } }
        public string ProductInfo { set { Collection.Add("productinfo", value); } get { return Collection["productinfo"]; } }
        public string FirstName { set { Collection.Add("firstname", value); } get { return Collection["firstname"]; } }
        public string Phone { set { Collection.Add("phone", value); } get { return Collection["phone"]; } }
        public string Email { set { Collection.Add("email", value); } get { return Collection["email"]; } }
        public string Surl { set { Collection.Add("surl", value); } get { return Collection["surl"]; } }
        public string Furl { set { Collection.Add("furl", value); } get { return Collection["furl"]; } }
        public string ServiceProvider { set {
                if(Collection["service_provider"]==null)
                Collection.Add("service_provider", "payu_paisa"); } get { return Collection["service_provider"]; } } 
        public string Hash
        {
            set
            {
                string hashString = Key + "|" + Txnid + "|" + Amount + "|" + ProductInfo + "|" + FirstName + "|" + Email + "|||||||||||" + Salt;
                Collection.Add("hash", Generatehash512(hashString));
            }
            get
            {
                return Collection["hash"];
            }
        }

        public string Generatehash512(string text)
        {

            byte[] message = Encoding.UTF8.GetBytes(text);

            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            SHA512Managed hashString = new SHA512Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;

        }

        public string Post(string Url)
        {
            this.ServiceProvider = "";
            this.Hash = "";
            

            StringBuilder str = new StringBuilder();
            str.Append("<html><head>");
            string FormName = "form" + new Random().Next(34, 50).ToString();
            str.Append(string.Format("</head><body onload=\"document.{0}.submit()\">",FormName));
            str.Append(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, "post", Url));
            for (int i = 0; i < Collection.Keys.Count; i++)
            {
                str.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", Collection.Keys[i], Collection[Collection.Keys[i]]));
            }
            str.Append("</form>");
            str.Append("</body></html>");
            return str.ToString();
        }
    }
}
