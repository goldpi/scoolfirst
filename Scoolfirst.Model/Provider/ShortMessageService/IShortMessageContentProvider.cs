﻿namespace Scoolfirst.Model.Provider
{
    public interface IShortMessageContentProvider
    {
        string GetMessage(MessageEvents e);

        string GetMessage(MessageEvents e, params string[] args);
    }
}
