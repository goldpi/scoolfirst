﻿using System.Threading.Tasks;

namespace Scoolfirst.Model.Provider
{
   public interface IShortMessageSender
    {
       //string User { get; }
       //string Password { get; }
       string BaseUrl { get; }
       string Parameters { get; }
       string Method { get; }
       string SuccessResponse { get; }
       bool SendMessage(string No, string Message);
       Task<bool> SendMessageAsync(string No, string Message);

       Task<bool> SendMessageAsync(string No, string Message, string Method);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentName"></param>
        /// <param name="OTP"></param>
        /// <param name="No"></param>
        /// <param name="Templatename">User Tamplete Name "Registration" to send OTP at the time of registration &&  Tamplete Name "Resend" to resend the OTP.</param>
        /// <returns></returns>
        Task<bool> SendOTPAsync(string studentName, string OTP, string No, string Templatename = "Registration");


    }
}