﻿using System;
using System.Collections.Generic;
using Owin;
using Ninject;

namespace Scoolfirst.Model.Provider
{
    public class ShortMessaeContentProvider : IShortMessageContentProvider
    {
        private ConfigReader ConfigReader;
        public ShortMessaeContentProvider(string path)
        {
            ConfigReader = new ConfigReader(path);
        }

        public string GetMessage(MessageEvents e)
        {
            return ConfigReader[e.ToString()];
        }
        public string GetMessage(MessageEvents e, params string[] args)
        {
            return string.Format(GetMessage(e), args);
        }

       
    }


    public static class ShortMessageContentProviderHelper
    {
        private static IShortMessageContentProvider _provider;
        private static IKernel _di;
        public static void Init(IKernel DI)
        {
            _di = DI;
            _provider = _di.Get<IShortMessageContentProvider>();
        }

        public static string GetMessage(this MessageEvents e,params string[] args)
        {
            return _provider.GetMessage(e, args);
        }
        public static string GetMessage(this MessageEvents e)
        {
            return _provider.GetMessage(e);
        }

    }
    

}