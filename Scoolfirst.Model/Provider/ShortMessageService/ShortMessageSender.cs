﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Scoolfirst.Model.Provider
{
    public static class SmsProviderInstance {
        private static IShortMessageSender _provider;
        private static IKernel _di;
        public static void Init(IKernel DI)
        {
            _di = DI;
            _provider = _di.Get<IShortMessageSender>();
        }


        public static bool SendMessage(string No, string Message)
        {
           return _provider.SendMessage(No, Message);
        }

        public static async Task<bool> SendMessageAsync(string No, string Message)
        {
            return await _provider.SendMessageAsync(No, Message);
        }

        public static async Task<bool> SendOTPAsync(string studentName, string OTP, string No, string Templatename = "Registration")
        {
            return await _provider.SendOTPAsync(studentName,OTP,No, Templatename);
        }
    }


    public class ShortMessageSender : IShortMessageSender
    {
        private ConfigReader _messageProviderConfig;

        public ShortMessageSender()
        {
                
        }

        public ShortMessageSender(string path)
        {
            _messageProviderConfig = new ConfigReader(path);
        }
        public string BaseUrl
        {
            get
            {
                return _messageProviderConfig["BaseUrl"];
            }
        }

        public string Method
        {
            get
            {
                return _messageProviderConfig["Method"];
            }
        }

        public string Parameters
        {
            get
            {
                return HttpUtility.HtmlDecode(_messageProviderConfig["Parameters"]);
            }
        }
        
        //public string Password
        //{
        //    get
        //    {
        //        return _messageProviderConfig["Password"];
        //    }
        //}

        public string SuccessResponse
        {
            get
            {
                return _messageProviderConfig["SuccessResponse"];
            }
        }

        //public string User
        //{
        //    get
        //    {
        //        return _messageProviderConfig["User"];
        //    }
        //}

        public bool SendMessage(string No, string Message)
        {
            try
            {
                string Posturl = BaseUrl + "?" + string.Format(Parameters,No,Message);

                WebRequest request = WebRequest.Create(Posturl);
                request.Method = Method;
                // If required by the server, set the credentials.
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.
                WebResponse response = request.GetResponse();

                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                if (responseFromServer.Contains(SuccessResponse))
                    return true;
                return false;
            }
            catch
            {

                return false;
            }
        }

        public async Task<bool> SendMessageAsync(string No, string Message)
        {
            try
            {
                string Posturl = BaseUrl + "?" + string.Format(Parameters, No, Message);

                WebRequest request = WebRequest.Create(Posturl);
                request.Method = Method;
                // If required by the server, set the credentials.
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.
                var response = await request.GetResponseAsync();
                
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                if (responseFromServer.Contains(SuccessResponse))
                    return true;
                return false;
            }
            catch
            {

                return false;
            }
        }

        public async Task<bool> SendMessageAsync(string No, string Message,string Method)
        {


           var result= await new Task<bool>(
                () =>
                {
                    try
                    {
                        string Posturl = $"http://2factor.in/API/V1/95384cdb-3278-11e8-a895-0200cd936042/ADDON_SERVICES/SEND/TSMS";
                        FormPost myremotepost = new FormPost();
                        //posting all the parameters required for integration.
                        myremotepost.Url = Posturl;
                        //Order ID generated from website.
                        myremotepost.AddField("From", "UPGRDE");
                        myremotepost.AddField("To", No);
                        myremotepost.AddField("Msg", Message);
                        myremotepost.Post();
                        return true;
                    }
                    catch
                    {

                        return false;
                    }
                });

            return result;     
        }

        public async Task<bool> SendOTPAsync(string studentName, string OTP, string No, string Templatename= "Registration")
        {
              try
                {
                    string Posturl = $"https://2factor.in/API/R1/?module=TRANS_SMS&apikey=95384cdb-3278-11e8-a895-0200cd936042&to={No}&from=UPGRDE&templatename={Templatename}&var1={studentName}&var2={OTP}";

                    WebRequest request = WebRequest.Create(Posturl);
                    request.Method = "GET";
                    // If required by the server, set the credentials.
                    request.Credentials = CredentialCache.DefaultCredentials;
                    // Get the response.
                    var response = await request.GetResponseAsync();

                    Stream dataStream = response.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    string responseFromServer = reader.ReadToEnd();
                    if (responseFromServer.Contains(SuccessResponse))
                        return true;
                    return false;
                }
                catch 
                {

                    return false;
                }
            
        }
        private static byte[] FormateData(NameValueCollection FormData)
        {
            List<string> sb = new List<string>();

            foreach (var data in FormData.AllKeys)
            {
                sb.Add(data + "=" + FormData[data]);
            }

            var str = string.Join("&", sb.ToArray());
            return Encoding.ASCII.GetBytes(str);
        }
    }
}