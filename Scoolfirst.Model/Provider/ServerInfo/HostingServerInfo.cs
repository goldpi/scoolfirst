﻿namespace Scoolfirst.Model.Provider
{

    public enum HostingServerInfo
    {
        Domain,
        Application,
        Api,
        Picture,
        File,
        Notification,
        Admin
    }
}