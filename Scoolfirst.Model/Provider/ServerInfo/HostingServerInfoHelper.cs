﻿using Ninject;
using System.Web;

namespace Scoolfirst.Model.Provider
{

    public static class HostingServerInfoHelper
        {
        private static IKernel _diContainer;
        private static IServerConfig _config;
        public static void Init(IKernel dIcontainer)
        {
            _diContainer = dIcontainer;
            _config = _diContainer.Get<IServerConfig>();
        }

        public static string Url(this HostingServerInfo info)
        {
            return _config.GetServer(info);
        }

        public static string GetCompleteUrl(this HostingServerInfo Info)
        {
            return $"http{(HttpContext.Current.Request.IsSecureConnection ? "s" : "")}://{Info.Url()}";
        }
            
        }
}