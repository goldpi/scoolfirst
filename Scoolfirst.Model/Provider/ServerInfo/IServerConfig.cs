﻿namespace Scoolfirst.Model.Provider
{
    public interface IServerConfig
    {
        string GetServer(HostingServerInfo e);
    }
}
