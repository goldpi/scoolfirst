﻿namespace Scoolfirst.Model.Provider
{

    public class ServerConfig : IServerConfig
    {
        private ConfigReader reader;
        public ServerConfig(string path)
        {
            reader = new ConfigReader(path);
        }
        public string GetServer(HostingServerInfo e)
        {
            if (HostingServerInfo.Domain == e)
                return reader.keyValues[e.ToString()];
            else
                return $"{reader.keyValues[e.ToString()]}.{reader.keyValues[HostingServerInfo.Domain.ToString()]}";
        }

    }
}