﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Utility
{
    public static class HtmlRemoval
    {
        /// <summary>
        /// Remove HTML from string with Regex.
        /// </summary>
        public static string StripTagsRegex(string source)
        {
            if(!string.IsNullOrEmpty(source) && source.Length>0)
            return Regex.Replace(source, "<.*?>", string.Empty);
            return source;
        }
        public static string StripUTF(string source)
        {

            if(!string.IsNullOrEmpty(source)&&source.Length>0)
            return Regex.Replace(source, @"\&.*?;", string.Empty);
            return source;
        }
        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string StripTagsRegexCompiled(string source)
        {
            if (!string.IsNullOrEmpty(source) && source.Length > 0)
                return _htmlRegex.Replace(source, string.Empty);
            return source;
        }

        /// <summary>
        /// Remove HTML tags from string using char array.
        /// </summary>
        public static string StripTagsCharArray(string source)
        {
            if (string.IsNullOrEmpty(source) && source.Length > 0)
                return source;
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }
}
