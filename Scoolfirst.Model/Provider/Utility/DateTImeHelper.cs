﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Utility
{
    public static class DateTimeHelper
    {
        public static TimeZoneInfo INDIAN_ZONE =
 TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        public static DateTime GetIST()
        {
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
        }

        public static DateTime GetIST(this DateTime Date)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(Date, INDIAN_ZONE);
        }

        public static string GetISTShort(this DateTime Date)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(Date, INDIAN_ZONE).ToString("dd-MM-yyyy");
        }
    }
}
