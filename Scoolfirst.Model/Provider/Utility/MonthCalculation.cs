﻿using Scoolfirst.Model.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Provider
{
   public class MonthCalculation
    {
        public DateTime NOW => DateTimeHelper.GetIST();
        public int PrevYear => NOW.Month > 3 ? NOW.Year : NOW.Year - 1;
        public int NextYear => NOW.Month > 3 ? NOW.Year + 1 : NOW.Year;
    }
}
