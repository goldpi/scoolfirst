﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Scoolfirst.Model.Provider
{
    public class Email
    {
        public static void SendEmail(string To,string From,string Sub,string Body,bool multiple=false){
            try
            {
               using (SmtpClient client = new SmtpClient())
               using (var message = new MailMessage())
               {
                   if (multiple)
                   {
                       var str = To.Split(',');
                       foreach (var item in str)
                       {
                           message.To.Add(new MailAddress(item));
                       }
                   }
                   else
                   {
                       message.To.Add(new MailAddress(To));
                   }
                   message.Subject = Sub;
                   message.IsBodyHtml = true;
                   message.Body = Body;
                   message.From = new MailAddress(From);
                   client.Send(message);
               }
            }
            catch
            {
                //ToDo : Logging
                //https://code.google.com/p/elmah/wiki/MVC
            }
        }
    }
}