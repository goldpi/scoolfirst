﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Scoolfirst.Model.Provider
{
    public  interface ILinkStripper
    {
        char[] GetStriper();
    }
    public class LinkStripper:ILinkStripper
    {
        ConfigReader config;
        public LinkStripper(string Path)
        {
            config = new ConfigReader(Path);
        }

        public char[] GetStriper()
        {
            return HttpUtility.HtmlDecode((config.keyValues.First().Value as string).ToString()).ToArray();
        }
    }

    public static class LinkStripperHelper
    {
        static ILinkStripper _striper;
        static IKernel _di;
        public static void Init(IKernel kernel)
        {
            _di = kernel;
            _striper = _di.Get<ILinkStripper>();
        }
        public static string StripUrl(this string str)
        {
            var cha = _striper.GetStriper();
            foreach (var i in cha)
                str = str.Replace(i, '-');
            return str;
        }

        public static string StripUrlcheck(this string t)
        {
            return new string( _striper.GetStriper());
        }
    }
}
