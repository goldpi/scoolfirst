﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Provider
{
    public class SMS
    {
        private string BaseUrl { get; set; }
        private string StaticParameter { get; set; }
       
       public  SMS(string to, string message)
        {
            BaseUrl = "http://bulksms.patnasms.com/app/smsapi/index.php";
            StaticParameter =
                string.Format("key=55714DADEB1816&routeid=271&type=text&senderid=UPGRDE&contacts={0}&msg={1}", to,message);
            
        }

       
        public  bool Send()
        {
            try
            {
                string Posturl = BaseUrl + "?" + StaticParameter;

                WebRequest request = WebRequest.Create(Posturl);
                // If required by the server, set the credentials.
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.
                WebResponse response = request.GetResponse();

                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                if (responseFromServer.Contains("SMS-SHOOT-ID"))
                    return true;
                return false;
            }
            catch 
            {

                return false;
            }
        }
    }
}
