﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.Model.Utility
{
    public static class HtmlHelperCustom
    {
        public static string RawTextMore(this HtmlHelper helper, string Text, int count = 200)
        {
            var tep = HtmlRemoval.StripUTF(HtmlRemoval.StripTagsRegex(helper.Raw(Text).ToString()));
            if (!string.IsNullOrEmpty(Text) && tep.Length > count)
                return string.Concat(tep.Take(count).AsEnumerable());
            else
                return tep;
        }

        public static MvcHtmlString BreakedText(this HtmlHelper Helper, string Text)
        {
            var BuilderStr = new StringBuilder();
            if (Text != null)
            {
                var t = Text.Split('\n');
                foreach (var i in t)
                {
                    BuilderStr.Append(i + "<br/>");
                }

            }
            return new MvcHtmlString(BuilderStr.ToString());
            }

        public static string TrimTitle(this HtmlHelper Helper,string Title,int count)
        {
            if (Title.Count() > count)
            {
                int cc = (Title.Count() - count);
                return Title.Remove(count, cc);
            }
            else return Title;
        }

        public static List<string> GetImagesInHTMLString(this HtmlHelper helper, string htmlString)
        {
            List<string> images = new List<string>();
            if (string.IsNullOrEmpty(htmlString))
                return images;

            string pattern = @"<(img)\b[^>]*>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(htmlString);

            for (int i = 0, l = matches.Count; i < l; i++)
            {
                images.Add(matches[i].Value);
            }

            return images;
        }

        public static MvcHtmlString IfHasImage(this HtmlHelper helper,string htmlString, Dictionary<string, string> attr, string @class = "img-responsive")
        {
            var img = helper.GetImagesInHTMLString(htmlString);
            if (img.Count > 0)
            {
                var  matchString = Regex.Match(img.First(), "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
                var tg = new TagBuilder("img");
                tg.AddCssClass(@class);
                tg.Attributes.Add("src", matchString);
                if (attr != null) {
                    foreach (var item in attr)
                    {
                        tg.Attributes.Add(item);
                    }
                    
                }
                return new MvcHtmlString(tg.ToString());
            }
                
            else
                return new MvcHtmlString("<span>No Image</span>");
        }
    }
}
