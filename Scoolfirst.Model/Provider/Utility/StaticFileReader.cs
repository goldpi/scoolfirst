﻿using System.IO;


namespace Scoolfirst.Model.Provider
{
   public static class StaticFileReader
    {

        public static string EmailerContent(string filepath)
        {
            StreamReader sr = new StreamReader(filepath);
            var line = sr.ReadToEnd();
            sr.Close();
            return line;
        }
    }
}
