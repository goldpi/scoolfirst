﻿using Ninject;

namespace Scoolfirst.Model.Provider
{
    public interface IApplicationConfig
    {
        string GetConfiguration(HostedApplicationConfig config);
    }
    public enum HostedApplicationConfig
    {
        Version,
        ApplicationName,
        ApplicationType,
        NoSchoolMessage,
        DefaultStudentAvtar,
        DefaultParentAvatar,
        MobileNo,
        Email
    }
    public class ApplicationConfig : IApplicationConfig
    {
        private ConfigReader reader;
        public ApplicationConfig(string path)
        {
            reader = new ConfigReader(path);
        }
        public string GetConfiguration(HostedApplicationConfig config)
        {
            return reader.keyValues[config.ToString()];
        }
    }

    public static class ApplicationConfigHelper
    {
        private static IApplicationConfig _config;
        private static IKernel _diResover;
        public static void Init(IKernel DIResolver)
        {
            _diResover = DIResolver;
            _config = _diResover.Get<IApplicationConfig>();
        }

        public static string Value(this HostedApplicationConfig e)
        {
            return _config.GetConfiguration(e);
        }
    }
}
