﻿using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Scoolfirst.Model.Provider
{
    public class UserLoggerAttribute : ActionFilterAttribute, IDisposable
    {
        private ScoolfirstContext dc;
        public UserLoggerAttribute(ScoolfirstContext DataConext)
        {
            dc = DataConext;
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var a=  filterContext.ControllerActionName();
            if(!filterContext.IsChildAction)
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                ActivityLogger logger = new ActivityLogger
                {    Url = filterContext.HttpContext.Request.Url.ToString(),
                    Action = a.Item1,
                    Controller = a.Item2,
                    Source = "Web",
                    UserName = filterContext.HttpContext.User.Identity.Name,
                    Id=Guid.NewGuid(),
                    OnDateOffset=DateTimeOffset.Now
                };
                dc.ActivityLogger.Add(logger);
                dc.SaveChanges();
            }
            base.OnActionExecuting(filterContext);  
        }
    }
}
