﻿using System;
using System.Web.Mvc;

namespace Scoolfirst.Model.Provider
{

    public static class ActionExecutingContextHelper
    {
        public static void View(this ActionExecutingContext filterContext,string page) {

            filterContext.Result = new ViewResult
            {
                ViewName = page,
                ViewData = filterContext.Controller.ViewData,
                TempData = filterContext.Controller.TempData,

            };
        }
        public static void View(this ActionExecutingContext filterContext, string page,object Model)
        {
            filterContext.Controller.ViewData.Model = Model;
            filterContext.Result = new ViewResult
            {
                ViewName = page,
                ViewData = filterContext.Controller.ViewData,
                TempData = filterContext.Controller.TempData,

            };
        }

        public static Tuple<string,string> ControllerActionName(this ActionExecutingContext filterContext)
        {
            var RouteData = filterContext.RouteData;
            string actionName = RouteData.Values["action"].ToString().ToUpper();
            string controllerName = RouteData.Values["controller"].ToString().ToUpper();
            return new Tuple<string, string>(actionName,controllerName);
        }
    }
}