﻿using System;

/// <summary>
/// Summary description for SubscriptionExtention
/// </summary>
/// namespace Scoolfirst.Model.Provider
/// 


namespace Scoolfirst.Model.Provider
{
    public static class SubscriptionHelper
    {

        public static DateTime? SubscriptionExpiry { get; private set; } = null;

        private static DateTime _CurrentIndianStandredDateTime;


        public static void SetSubscriptionExpiryDate(DateTime IndianStandaredDateTimeOfEexpiry)
        {
            SubscriptionExpiry = IndianStandaredDateTimeOfEexpiry;
            _CurrentIndianStandredDateTime = TimeZoneInfo.ConvertTimeFromUtc(
                DateTime.UtcNow,
                TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
        }


        public static bool ISAccountExpired(DateTime IndianStandaredDateTimeOfEexpiry)
        {
            if (SubscriptionExpiry == null)
            {
                throw new Exception("Please set subscription expiry date.");
            }
            _CurrentIndianStandredDateTime = TimeZoneInfo.ConvertTimeFromUtc(
                DateTime.UtcNow,
                TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
            //int daydiff = (int)((DateTime.UtcNow - ExpiryDateConvertedToUTC).Hours);

            if (IndianStandaredDateTimeOfEexpiry > _CurrentIndianStandredDateTime)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static SubscriptionSpan GetSubscriptionSpan()
        {

            return FindSubscriptionSpan();
        }

        public static string GetSubscriptionStatusMessage()
        {
            throw new NotImplementedException();
        }

        private static SubscriptionSpan FindSubscriptionSpan()
        {

            if ((SubscriptionExpiry.Value - _CurrentIndianStandredDateTime).TotalDays >= 1)
            {
                return
                    new SubscriptionSpan(
                        Math.Round(((SubscriptionExpiry.Value - _CurrentIndianStandredDateTime).TotalDays)),
                        SubscriptionRestHourDayMonthYear.Day);
            }


            if ((SubscriptionExpiry.Value - _CurrentIndianStandredDateTime).TotalHours >= 1)
            {
                return
                    new SubscriptionSpan(
                        Math.Round(((SubscriptionExpiry.Value - _CurrentIndianStandredDateTime).TotalHours)),
                        SubscriptionRestHourDayMonthYear.Hour);
            }

            if ((SubscriptionExpiry.Value - _CurrentIndianStandredDateTime).TotalMinutes >= 1)
            {
                return
                    new SubscriptionSpan(
                        Math.Round((SubscriptionExpiry.Value - _CurrentIndianStandredDateTime).TotalMinutes),
                        SubscriptionRestHourDayMonthYear.Minute);
            }

            var data =
                Math.Round(
                    double.Parse((SubscriptionExpiry.Value - _CurrentIndianStandredDateTime).Hours.ToString()));
            if (data < 0)
                return
                    new SubscriptionSpan(0, SubscriptionRestHourDayMonthYear.Exipred);
            else
            {
                return
               new SubscriptionSpan(0, SubscriptionRestHourDayMonthYear.Exipred);
            }
        }

    }


    public class SubscriptionSpan
    {
        public double Duration { get; set; }

        public SubscriptionRestHourDayMonthYear HourDayMonthYear { get; set; }

        public SubscriptionSpan()
        {

        }

        public SubscriptionSpan(double duration, SubscriptionRestHourDayMonthYear hourDayMonthYear)
        {
            this.Duration = duration;
            this.HourDayMonthYear = hourDayMonthYear;
        }
    }

    public enum SubscriptionRestHourDayMonthYear

    {
        Minute = 0,

        Hour = 1,

        Day = 2,

        Exipred = 3
    }


}