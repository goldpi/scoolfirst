﻿using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Scoolfirst.Model.Provider
{
    public class ModuleTrackerAttribute : ActionFilterAttribute, IDisposable
    {
        private ScoolfirstContext dc;
        private User UserData;
        private ModuleControllers Controller;
        public ModuleTrackerAttribute(ScoolfirstContext context)
        {
            dc = context;
        }
        public void Dispose()
        {
            dc.Dispose();
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (filterContext.IsChildAction)
            {
                return;
            }

            CheckUserCachingSession(filterContext, dc.Users);
            CheckModuleAcces(filterContext, dc.ControllerAction, dc.Modules);

        }


        public void CheckModuleAcces(ActionExecutingContext filterContext, IQueryable<ModuleControllers> Controllers, IQueryable<ModuleData> Modules)
        {


            if (filterContext.IsChildAction)
            {
                return;
            }


            var RouteData = filterContext.RouteData;
            string actionName = RouteData.Values["action"].ToString().ToUpper();
            string controllerName = RouteData.Values["controller"].ToString().ToUpper();
            if (controllerName.ToLower() == "payment")
                return;
            Controller = Controllers.FirstOrDefault(i => i.Action == actionName && i.Controller == controllerName);

            if (Controller == null)
            {
                Controller = new ModuleControllers
                {
                    Action = actionName,
                    Controller = controllerName,
                    Id = Guid.NewGuid().ToString(),
                    ModuleId = "FREE"
                };
                dc.ControllerAction.Add(Controller);
                dc.SaveChanges();
            }
            else
            {
                var mod = Modules.FirstOrDefault(i => i.ModuleName == Controller.ModuleId);
                if (mod == null)
                {
                    filterContext.View("Error");
                    return;
                }
                if (!mod.Active)
                {
                    filterContext.Controller.TempData["Message"] = string.IsNullOrEmpty(mod.Message) ? "<p>This module is inactive!</p>" : mod.Message;
                    filterContext.View("Inactive");
                    return;
                }
                if (!mod.Free && mod.AuthRequired)
                {
                    if (filterContext.HttpContext.User.Identity.IsAuthenticated)
                    {
                        if (!this.UserData.ModulesSubscribed.Any(i => i == mod.ModuleName))
                        {
                            filterContext.View("Payment", new Scoolfirst.Model.Common.SubscriptionError { Code = 1, Reason = "Subscription Limitation" });
                            return;
                        }
                    }
                }
                if (filterContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    var date = DateTime.UtcNow;

                    //set the expiry date of subscription of the user
                    SubscriptionHelper.SetSubscriptionExpiryDate(this.UserData.Expiry);

                    var Span = SubscriptionHelper.GetSubscriptionSpan();

                    if (Span.HourDayMonthYear == SubscriptionRestHourDayMonthYear.Exipred)
                    {
                        filterContext.View("Payment", new Scoolfirst.Model.Common.SubscriptionError { Code = 1, Reason = "Subscription Expired" });

                    }
                    if (this.UserData.Subscribed == false)
                    {
                        filterContext.Controller.TempData["Expiry"] = "Please Subscribe for full access";
                    }

                    //checking the time span of subscription
                    switch (Span.HourDayMonthYear)
                    {
                        case SubscriptionRestHourDayMonthYear.Minute:
                            {

                                filterContext.Controller.TempData["Expiry"] = MessageEvents.OnBeforeExpirationOfSubscription.GetMessage(new string[] { this.UserData.FullName, "Upgrade Jr", Span.Duration.ToString() + " " + Span.HourDayMonthYear });

                                break;
                            }
                        case SubscriptionRestHourDayMonthYear.Day:
                            {
                                if (Span.Duration <= 15)
                                {
                                    filterContext.Controller.TempData["Expiry"] = MessageEvents.OnBeforeExpirationOfSubscription.GetMessage(new string[] { this.UserData.FullName, "Upgrade Jr", Span.Duration.ToString() + " " + Span.HourDayMonthYear });

                                }
                                break;
                            }
                        case SubscriptionRestHourDayMonthYear.Hour:
                            {
                                filterContext.Controller.TempData["Expiry"] = MessageEvents.OnBeforeExpirationOfSubscription.GetMessage(new string[] { this.UserData.FullName, "Upgrade Jr", Span.Duration.ToString() + " " + Span.HourDayMonthYear });

                                break;
                            }
                    }



                }

            }
        }
        public void CheckUserCachingSession(ActionExecutingContext filterContext, IQueryable<User> Users)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
                if (filterContext.HttpContext.Session["UserDetails"] == null)
                {
                    var user = filterContext.HttpContext.User.Identity.Name;
                    var UserData1 = (from l in Users
                                     where l.UserName == user || l.Email == user

                                     select new { p = l, School = l.School, @class = l.Class }).FirstOrDefault();
                    UserData = UserData1.p;
                    UserData.School = UserData1.School;
                    UserData.Class = UserData1.@class;
                    if (UserData != null)
                    {
                        filterContext.HttpContext.Session["UserDetails"] = UserData;
                    }
                    else
                    {
                        filterContext.View("Error");
                    }

                }
                else
                {
                    UserData = filterContext.HttpContext.Session["UserDetails"] as User;
                }

        }



    }
}
