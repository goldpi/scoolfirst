﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Scoolfirst.Model.Provider
{
    public static class Flash
    {

        public static void Error(this Controller controller, string Title, string Message)
        {
           
            AlertMessage NewAlert = new AlertMessage();
            NewAlert.Type = "error";
            NewAlert.Title = Title;
            NewAlert.Message = Message;
            var json = new JavaScriptSerializer().Serialize(NewAlert);
            controller.TempData["Message"] = json;

        }
        public static void Warning(this Controller controller, string Title, string Message)
        {
            
            AlertMessage NewAlert = new AlertMessage();
            NewAlert.Type = "warning";
            NewAlert.Title = Title;
            NewAlert.Message = Message;
            var json = new JavaScriptSerializer().Serialize(NewAlert);
            controller.TempData["Message"] = json;

        }
        public static void Info(this Controller controller, string Title, string Message)
        {
            
            AlertMessage NewAlert = new AlertMessage();
            NewAlert.Type = "info";
            NewAlert.Title = Title;
            NewAlert.Message = Message;
            var json = new JavaScriptSerializer().Serialize(NewAlert);
            controller.TempData["Message"] = json;

        }
       
        public static void Success(this Controller controller, string Title, string Message)
        {
            
            AlertMessage NewAlert = new AlertMessage();
            NewAlert.Type = "success";
            NewAlert.Title = Title;
            NewAlert.Message = Message;
            var json = new JavaScriptSerializer().Serialize(NewAlert);
            controller.TempData["Message"] = json;

        }

        public static void AlertCollection(this Controller controller, List<AlertMessage> AlertMessageCollection)
        {
            var json = new JavaScriptSerializer().Serialize(AlertMessageCollection);
            controller.TempData["Message"] = json;

        }

    }
}