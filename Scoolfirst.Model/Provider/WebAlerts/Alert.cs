﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Scoolfirst.Model.Provider
{
    // to use cookie base flash message uncomment the class

    /* public static class Alert
    {
        
        public static void Error(string Title,string Message)
        {
            HttpCookie myCookie = new HttpCookie("Message");
            AlertMessage NewAlert = new AlertMessage();
            NewAlert.Type = "error";
            NewAlert.Title = Title;
            NewAlert.Message = Message;
            var json = new JavaScriptSerializer().Serialize(NewAlert);
            myCookie.Value = json;
            HttpContext.Current.Response.Cookies.Add(myCookie);

        }
        public static void Warning(string Title, string Message)
        {
            HttpCookie myCookie = new HttpCookie("Message");
            AlertMessage NewAlert = new AlertMessage();
            NewAlert.Type = "warning";
            NewAlert.Title = Title;
            NewAlert.Message = Message;
            var json = new JavaScriptSerializer().Serialize(NewAlert);
            myCookie.Value = json;
            HttpContext.Current.Response.Cookies.Add(myCookie);

        }
        public static void Info(string Title, string Message)
        {
            HttpCookie myCookie = new HttpCookie("Message");
            AlertMessage NewAlert = new AlertMessage();
            NewAlert.Type = "info";
            NewAlert.Title = Title;
            NewAlert.Message = Message;
            var json = new JavaScriptSerializer().Serialize(NewAlert);
            myCookie.Value = json;
            HttpContext.Current.Response.Cookies.Add(myCookie);

        }
        public static void Success(string Title, string Message)
        {
            HttpCookie myCookie = new HttpCookie("Message");
            AlertMessage NewAlert = new AlertMessage();
            NewAlert.Type = "success";
            NewAlert.Title = Title;
            NewAlert.Message = Message;
            var json = new JavaScriptSerializer().Serialize(NewAlert);
            myCookie.Value = json;
            HttpContext.Current.Response.Cookies.Add(myCookie);

        }

    }*/
    //=====================Alert Type==================
    //error
    //info
    //warning
    //success
    //=====================Alert Type End==================
    public class AlertMessage
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }
}