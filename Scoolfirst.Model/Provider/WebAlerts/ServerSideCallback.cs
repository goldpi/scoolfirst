﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoolfirst.Model.Provider
{
    public class ServerSideCallback<T> where T:class
    {
        public ServerSideCallback()
        {
            callbacks = new List<Function>();
        }
        public T Result { get; set; }
        [JsonProperty("callback")]
        public List<Function> callbacks { get; set; }
    }

    public class InvokeClient
    {
        [JsonProperty("callback")]
        public List<Function> callbacks { get; set; }
    }
    public class Function
    {
        [JsonProperty("function")]
        public string function { get; set; }
        [JsonProperty("parameters")]
        public object[] parameters { get; set; }
    }
}