﻿namespace Scoolfirst.Model.Provider
{
    public enum MessageEvents
    {
        InvoiceMatter,
        OnRegistrationStart,
        OnSelectionOfOtherSchoolAtTheTimeOfRegistration,
        OnRegistrationSuccessCompleted,
        OnVerificationCodeSend,
        OnVerificationCompleted,
        OnForgetPasswordProcessStart,
        OnForgetPasswordProcessEnd,
        OnChangeOfSchoolStart,
        OnChangeOfSchoolEnd,
        OnChangeOfClassStart,
        OnChangeOfClassEnd,
        OnAccountBlock,
        InPeriodOfDemo,
        OnLapsOfDemo,
        OnPurchaseOfSubscription,
        OnBeforeExpirationOfSubscription,
        OnExpirationOfSubscription,
        OnUpgradeOfSubscriptionStart,
        OnUpgradeOfSubscriptionCompleted,
        OnDowngradeOfSubscriptionStart,
        OnDowngradeOfSubscriptionCompleted,
        OnSendingInvoiceOnPurchaseDowngradeUpgradeOfSubcription,
        OnFeedbackProcessStart,
        OnFeedbackSend,
        OnFeedbackAcceptedApproved,
        OnPostOfDoubtClass,
        OnAnswerOfDoubt,
        OnNLOExamPurchaseStart,
        OnNLOExamPaymentReceived

    }
}