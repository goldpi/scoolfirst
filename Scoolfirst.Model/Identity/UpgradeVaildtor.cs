﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer.Utilities;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Identity
{
    public class  UpgradeVaildtor<TUser> : UserValidator<TUser> where TUser : User
    {
        private static readonly Regex EmailRegex = new Regex(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private readonly UserManager<TUser> Manager;

        //public UpgradeVaildtor():base()
        //{
           
        //}

        public UpgradeVaildtor(UserManager<TUser> manager):base(manager)
        {
            Manager = manager;
        }

        override public async Task<IdentityResult> ValidateAsync(TUser item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            var errors = new List<string>();
            await ValidateUserName(item, errors).WithCurrentCulture();
            ValidatePhone(item,errors);
            if (RequireUniqueEmail)
            {
                await ValidateEmailAsync(item, errors).WithCurrentCulture();
            }
            if (errors.Count > 0)
            {
                return IdentityResult.Failed(errors.ToArray());
            }
            return IdentityResult.Success;
        }

        private async Task ValidateUserName(TUser user, List<string> errors)
        {
            if (string.IsNullOrWhiteSpace(user.UserName))
            {
                errors.Add("To short username");
            }
            else if (AllowOnlyAlphanumericUserNames && !Regex.IsMatch(user.UserName, @"^[A-Za-z0-9@_\.]+$"))
            {
                // If any characters are not letters or digits, its an illegal user name
                errors.Add("Invalid User name, username must only contain a-z A-Z 0-9 _ ." );
            }
            else
            {
                var owner = await Manager.FindByNameAsync(user.UserName).WithCurrentCulture();
                if (owner != null && !(owner.Id== user.Id))
                {
                    errors.Add("User Exist.");
                }
            }
        }

        // make sure email is not empty, valid, and unique
        private async Task ValidateEmailAsync(TUser user, List<string> errors)
        {
            //Issue #24 Issue #22
            
            //Yusuf
            var email = user.Email;//await Manager.GetEmailAsync(user.Email).WithCurrentCulture();

            //var email = await Manager.GetEmailAsync(user.Id).WithCurrentCulture();
            if (!string.IsNullOrWhiteSpace(email))
            {
                try
                {
                    var m = new MailAddress(email);
                }
                catch (FormatException)
                {
                    errors.Add("Invalid email");
                    return;
                }

                var owner = await Manager.FindByEmailAsync(email).WithCurrentCulture();
                if (owner != null && !(owner.Id == user.Id))
                {
                    errors.Add("User Exist.");
                }
            }
           
           
        }

        private void ValidatePhone(TUser user, List<string> errors)
        {
            
            if (string.IsNullOrWhiteSpace(user.PhoneNumber))
            {
                errors.Add("Invalid Mobile No.");
                return;
            }

            var owner = Manager.Users.FirstOrDefault(i => i.PhoneNumber == user.PhoneNumber);
            if (owner != null && !(owner.Id == user.Id))
            {
                errors.Add("User Exist.");
            }
        }
    }
}
