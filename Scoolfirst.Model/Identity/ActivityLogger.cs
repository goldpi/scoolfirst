﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Identity
{
    public class ActivityLogger
    {
        [Key,Column(Order =1)]
        public Guid Id { get; set; } = Guid.NewGuid();
        public string UserName { get; set; }
        public string Url { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        [Key,Column(Order =2)]
        public DateTimeOffset OnDateOffset { get; set; } = DateTimeOffset.Now;

        public string Source { get; set; }

        public string Date => OnDateOffset.ToString("dd/MM/yyy HH");
    }
}
