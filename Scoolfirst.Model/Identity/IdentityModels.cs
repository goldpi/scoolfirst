﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using Scoolfirst.Model.Common;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scoolfirst.Model.Identity
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser
    {
        public string FullName { get; set; }
        public string Student_Parent_Admin { get; set; }
        public int ClassId { get; set; }
        public string Board { get; set; }
        public string PictureUrl { get; set; }
        public int Points { get; set; }
        public string AboutMe { get; set; }
        public string DOB { get; set; }
        public string Address { get; set; }
        public int? SchoolId { get; set; }
        public virtual School School { get; set; }
        public virtual Classes Class { get; set; }
        public bool Trial { get; set; }
        public bool Subscribed { get; set; }
        public DateTime Expiry { get; set; }
        public DateTime RegisterdOn { get; set; }
       // public bool Spo
        public bool Sponsered { get; set; }
        
        //RegisterAs use to track the registrationn type of user while signup for the app.
        public RegisterAs RegisterAs { get; set; }

        /// <summary>
        /// RequestedforBrandAmbesdor use to track the user has requested for brand ambesdor.
        /// </summary>
        public bool RequestedforBrandAmbesdor { get; set; }
        public bool IsBrandAmbesdor { get; set; } = false;
        public string Gender { get; set; }
        public string OtherSchool { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
       
        public string ConnectionId { get; set; }
        public string Modules { get; set; }
        public string RefferedBy { get; set; }
        public string RefferCode { get; set; }

        public string ParentsNo { get; set; }

        public int Factor { get; set; } = 2;
        [NotMapped]
        public string[] ModulesSubscribed
        {
            get { return Modules != null ? this.Modules.Split(',') : new string[0]; }
            set { this.Modules = string.Join(",", value); }
        }
        
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager userManager, string authenticationType)
        {
            var userIdentity = await userManager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }


}