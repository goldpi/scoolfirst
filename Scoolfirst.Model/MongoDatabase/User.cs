﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoRepository;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Scoolfirst.Model.Notifications;
using Scoolfirst.Model.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.MongoDatabase
{
    public class OnlineUser : Entity
    {
        public string UserId { get; set; }
        public string DeviceId { get; set; }
        public string ConnectionId { get; set; }
        public DateTime LastConnection { get; set; }
        public DateTime ConnectedOn { get; set; }
        public int Reconnected { get; set; }
    }


    public class NotificationAndroid : Entity
    {
        public string User { get; set; }
        public string Group { get; set; }
        [JsonProperty("ImageUrl")]
        public string ImageUrl { get; set; }
        [JsonProperty("Link")]
        public string Link { get; set; }
        [JsonProperty("PostId")]
        public string PostId { get; set; }
        [JsonProperty("Title")]
        public string Title { get; set; }
        [JsonProperty("Message")]
        public string Message { get; set; }
        [JsonProperty("Date")]
        //[BsonDateTimeOptions(Representation = BsonType.String)]
        public DateTime Date { get; set; }
        [JsonProperty("Read")]
        public bool Read { get; set; }
        [EnumDataType(typeof(ModType))]
        [JsonProperty("Module")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ModType Module { get; set; }
        [JsonProperty("Size")]
        public bool Size { get; set; }

    }
    public enum ModType
    {
        [Display(Name = "Knowledge Nutrition")]
        KN,
        [Display(Name = "Reasoning Root")]
        RR,
        [Display(Name = "Work Sheet")]
        WRKSHT,
        [Display(Name = "U inspire me")]
        UIM,
        [Display(Name = "Computer and common core resources")]
        CC,
        [Display(Name = "Knowledge donation camp")]
        KDC_APP
    }
    public class NotificationManagerRepo
    {
        private Scoolfirst.Model.Context.ScoolfirstContext Repo = new Scoolfirst.Model.Context.ScoolfirstContext();

        public void AddNotification(Notification notification)
        {
            Repo.Notification.Add(notification);
            Repo.SaveChanges();
        }

        //public void DeleteNotification(string Id)
        //{
        //    Repo.Delete(Id);
        //}

        public List<Notification> ListAllByGroup(string Gruop)
        {
            return Repo.Notification.Where(i => i.Group == Gruop).ToList();
        }
        public List<Notification> ListAll(int p=0,int size=20)
        {
            return Repo.Notification.OrderBy(i=>i.Date).Skip(p*size).Take(size).ToList();
        }
        public Tuple<List<Notification>, int> ListAllT(int p = 0, int size = 20)
        {
            var query = Repo.Notification.OrderByDescending(i => i.Date);
            return new Tuple<List<Notification>, int>(query.Skip(p * size).Take(size).ToList(), query.Count());
        }
        public List<Notification> ListAllPer(int p = 0, int size = 20)
        {
            var datTimeYesterda = DateTime.Now.AddDays(-1);
            var dateTimeTommorow = DateTime.Now.AddDays(1);
            var query = Repo.Notification.Where(i => i.Date <= dateTimeTommorow && i.Date >= datTimeYesterda);
            return query.OrderByDescending(i => i.Date).Skip(p * size).Take(size).ToList();
        }

        public Tuple<List<Notification>, int> ListAllPerT(int p = 0, int size = 20)
        {
            var datTimeYesterda = DateTime.Now.AddDays(-1);
            var dateTimeTommorow = DateTime.Now.AddDays(1);
            var query = Repo.Notification.Where(i => i.Date <= dateTimeTommorow && i.Date >= datTimeYesterda);
            return new Tuple<List<Notification>, int>(query.OrderByDescending(i => i.Date).Skip(p * size).Take(size).ToList(), query.Count());
        }

        public List<Notification> ListAllByUser(string User)
        {
            return Repo.Notification.Where(i => i.User == User).ToList();
        }
    }

    public class OnlinerUserRepo
    {
        static MongoRepository<OnlineUser> Repo = new MongoRepository<OnlineUser>();
        public OnlineUser AddOnlineUser(OnlineUser user)
        {
            return Repo.Add(user);
        }
        public List<OnlineUser> OnlineUser(string user)
        {
            return Repo.Where(i => i.UserId == user).ToList();
        }
        public void Delete(string user)
        {
            Repo.Delete(i => i.UserId == user);
        }
        public void LastActivity(string conID)
        {
            var userPr = Repo.FirstOrDefault(i => i.ConnectionId == conID);
            userPr.LastConnection = DateTime.Now.AddHours(5.5);
            Repo.Update(userPr);
        }

        public void DeleteByDievice(string connection,string user,string Divice)
        {
            Repo.Delete(i => i.ConnectionId != connection && i.DeviceId==Divice && i.UserId==user);
        }
        public void Reconnect(string conID)
        {
            var userPr = Repo.FirstOrDefault(i => i.ConnectionId == conID);
            userPr.Reconnected++;
            userPr.LastConnection = DateTime.Now.AddHours(5.5);
            Repo.Update(userPr);
        }
        public void DeleteByConection(string con)
        {
            Repo.Delete(i => i.ConnectionId == con);
        }
        public List<OnlineUser> All(int page = 0, int size = 20)
        {
            return Repo.OrderBy(i => i.UserId).Skip(page * size).Take(size).ToList();
        }
    }
    public class Group : Entity
    {
        public string GroupName { get; set; }
        public List<string> ClassId { get; set; }
        public List<string> ConnectionId { get; set; }
        public List<string> Schools { get; set; }

    }

    public class GroupManagerManogDB
    {
        static MongoRepository<Group> Repo = new MongoRepository<Group>();


        public void AddGroup(string name, string Class, string ConnectionId)
        {
            var grp = new Group
            {
                ClassId = new List<string>(),
                ConnectionId = new List<string>(),
                GroupName = name
            };
            grp.ConnectionId.Add(ConnectionId);
            grp.ClassId.Add(Class);
            Repo.Add(grp);
        }
        public void AddGroupList(string name, List<string> Class, List<String> School,string ConnectionId)
        {
            var grp = new Group
            {
                ClassId = new List<string>(),
                ConnectionId = new List<string>(),
                Schools=new List<string>(),
                GroupName = name
            };
            grp.ConnectionId.Add(ConnectionId);
            grp.ClassId = Class;
            grp.Schools = School;
            Repo.Add(grp);
        }
        public void AddToGroup(string groupName, string connetionId)
        {
            var grp = Repo.FirstOrDefault(i => i.GroupName == groupName);
            grp.ConnectionId.Add(connetionId);
            Repo.Update(grp);

        }
        public void RemoveFromGropu(string connetionId)
        {
            var grp = Repo.Where(i => i.ConnectionId.Contains(connetionId));
            foreach (var GP in grp)
            {
                GP.ConnectionId.Remove(connetionId);
                if(GP.ConnectionId.Count>0)
                Repo.Update(GP);
                else
                {
                    Repo.Delete(GP);
                }
            }
            

        }


        public List<Group> AllGrp(int p = 0, int size = 30)
        {
            return Repo.OrderBy(i => i.GroupName).Skip(p * size).Take(size).ToList();
        }
        public bool GroupExists(string groupName)
        {
            return Repo.Any(i => i.GroupName == groupName);
        }
    }

    public class NotificatonRecived:Entity
    {
        public string NotficationId { get; set; }
        public DateTime On { get; set; }
        public bool Read { get; set; }
        public bool Deleted { get; set; }
        public string User { get; set; }
    }

    public class NotificationRecivedManager
    {
        private MongoRepository<NotificatonRecived> Repo = new MongoRepository<NotificatonRecived>();
       
        public void Add (string id,string usr){
            if(!Repo.Any(i=>i.NotficationId==id&&i.User==usr))
            Repo.Add(new NotificatonRecived { NotficationId = id, User = usr, On = DateTime.UtcNow.AddHours(5.5) , Read=false});
        }

        public string MarkRead(string id,string usr)
        {
            var d = Repo.FirstOrDefault(i => i.NotficationId == id && i.User == usr);
            d.Read = true;
            Repo.Update(d);
            return d.NotficationId;
        }
        public string MarkDelete(string id, string usr)
        {
            var d = Repo.FirstOrDefault(i => i.NotficationId == id && i.User == usr);
            d.Deleted = true;
            Repo.Update(d);
            return d.NotficationId;

        }
        public IEnumerable<string> MarkAll(string user)
        {
            foreach(var d in Repo.Where(i =>  i.User == user))
            {
                d.Read = true;
                Repo.Update(d);
              yield return  d.NotficationId;
            }
           
        }


        public IEnumerable<Notification> UnreadNotification(string user,List<string> gr)
        {
            var Repoq = new Scoolfirst.Model.Context.ScoolfirstContext().Notification.ToList();
            var my = Repo.Where(i => i.User == user);
            List<Notification> pp = new List<Notification>();
            foreach(var e in gr)
            {
                pp.AddRange(Repoq.Where(i => i.Group.Contains(e)));
            }
            foreach(var d in pp)
            {
                if(my.Any(i=>i.NotficationId==d.Id&&i.Deleted==false))
                
                {
                    d.Read = my.First(i=>i.NotficationId==d.Id).Read;
                    yield return d;
                }

            }
            

        }

        public IEnumerable<Notification> DeliveredNotification(string user)
        {
            var Repoq = new Scoolfirst.Model.Context.ScoolfirstContext().Notification.ToList();
            var my = Repo.Where(i => i.User == user && i.Deleted==false);
            foreach (var i in my)
            {
                var re = Repoq.First(pi => pi.Id == i.NotficationId);
                re.Read = i.Read;
                yield return re;
            }


        }
    }

    
}
