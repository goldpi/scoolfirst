﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Application_Setting
{
    public class ApplicationSetting
    {
        [Key]
        public int Id { get; set; }
       
        public string Weblogo { get; set; }
        public string Adminlogo { get; set; }
        public string Invoicelogo { get; set; }
        public string InvoicePreFix { get; set; }
        public string BillingAddress { get; set; }


        public string CompanyName { get; set; }
        public string WebSiteUrl { get; set; }
        public string SaleEmail { get; set; }
        public string SupportEmail { get; set; }
        public string BillingEmail { get; set; }
        public string SupportMobile { get; set; }
        public string SaleMobile { get; set; }
        public string BillingMobile { get; set; }
       
        public string GeneralAddress { get; set; }
        public string GlobalNotice { get; set; }

    }
}
