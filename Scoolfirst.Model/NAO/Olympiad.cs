﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.NAO
{
    public class Olympiad
    {
        public long Id { get; set; }
        public int Level { get; set; }
        public string Subject { get; set; }
        public int Class { get; set; }
        public string Content { get; set; }
        public string Title { get; set; }
        public DateTime AddedOn { get; set; }
    }
}
