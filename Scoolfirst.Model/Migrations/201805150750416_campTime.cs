namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class campTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CampPosts", "Time", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CampPosts", "Time");
        }
    }
}
