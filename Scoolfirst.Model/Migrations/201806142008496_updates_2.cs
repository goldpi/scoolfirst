namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updates_2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppUpdates", "UpdateTitle1", c => c.String(maxLength: 20));
            AddColumn("dbo.AppUpdates", "UpdateTitle2", c => c.String(maxLength: 20));
            AddColumn("dbo.AppUpdates", "UpdateTitle3", c => c.String(maxLength: 20));
            AddColumn("dbo.AppUpdates", "UpdateTitle4", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AppUpdates", "UpdateTitle4");
            DropColumn("dbo.AppUpdates", "UpdateTitle3");
            DropColumn("dbo.AppUpdates", "UpdateTitle2");
            DropColumn("dbo.AppUpdates", "UpdateTitle1");
        }
    }
}
