namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class playChallange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayRoots", "IsChallange", c => c.Boolean(nullable: false));
            AddColumn("dbo.PlayRoots", "StartsOn", c => c.DateTimeOffset(precision: 7));
            AddColumn("dbo.PlayRoots", "PrizeAmount", c => c.Double());
            AddColumn("dbo.PlayRoots", "MaxCap", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlayRoots", "MaxCap");
            DropColumn("dbo.PlayRoots", "PrizeAmount");
            DropColumn("dbo.PlayRoots", "StartsOn");
            DropColumn("dbo.PlayRoots", "IsChallange");
        }
    }
}
