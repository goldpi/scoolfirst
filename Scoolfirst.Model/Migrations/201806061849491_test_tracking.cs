namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test_tracking : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TestTrackings",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        TestId = c.Long(nullable: false),
                        Month = c.Single(nullable: false),
                        Score = c.Single(nullable: false),
                        ViewSolution = c.Boolean(nullable: false),
                        PointsGiven = c.Boolean(nullable: false),
                        IsPassable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.TestId })
                .ForeignKey("dbo.PARoots", t => t.TestId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.TestId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TestTrackings", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TestTrackings", "TestId", "dbo.PARoots");
            DropIndex("dbo.TestTrackings", new[] { "TestId" });
            DropIndex("dbo.TestTrackings", new[] { "UserId" });
            DropTable("dbo.TestTrackings");
        }
    }
}
