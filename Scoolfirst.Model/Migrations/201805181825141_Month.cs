namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Month : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PARoots", "Month", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PARoots", "Month");
        }
    }
}
