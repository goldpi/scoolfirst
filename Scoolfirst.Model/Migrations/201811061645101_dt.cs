namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dt : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DaliyTests",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        StartsOn = c.DateTime(nullable: false),
                        EndsOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DTAnswereds",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DTQuestionSetId = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        AppearedId = c.Guid(nullable: false),
                        AnswerId = c.Guid(nullable: false),
                        Correct = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DTAnswers", t => t.AnswerId, cascadeDelete: false)
                .ForeignKey("dbo.DTAppeareds", t => t.AppearedId, cascadeDelete: false)
                .ForeignKey("dbo.DTQuestionSets", t => t.DTQuestionSetId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.DTQuestionSetId)
                .Index(t => t.UserId)
                .Index(t => t.AppearedId)
                .Index(t => t.AnswerId);
            
            CreateTable(
                "dbo.DTAnswers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        Answer = c.String(),
                        IsCoorect = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DTQuestions", t => t.QuestionId, cascadeDelete: false)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.DTQuestions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionSetId = c.Guid(nullable: false),
                        Question = c.String(),
                        Solution = c.String(),
                        Concept = c.String(),
                        ShowSloution = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DTQuestionSets", t => t.QuestionSetId, cascadeDelete: false)
                .Index(t => t.QuestionSetId);
            
            CreateTable(
                "dbo.DTQuestionSets",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DaliyTestId = c.Guid(nullable: false),
                        ClassId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DaliyTests", t => t.DaliyTestId, cascadeDelete: false)
                .Index(t => t.DaliyTestId);
            
            CreateTable(
                "dbo.DTAppeareds",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DaliyTestId = c.Guid(nullable: false),
                        DTQuestionSetId = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        On = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DaliyTests", t => t.DaliyTestId, cascadeDelete: false)
                .ForeignKey("dbo.DTQuestionSets", t => t.DTQuestionSetId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.DaliyTestId)
                .Index(t => t.DTQuestionSetId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DTAnswereds", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.DTAnswereds", "DTQuestionSetId", "dbo.DTQuestionSets");
            DropForeignKey("dbo.DTAppeareds", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.DTAppeareds", "DTQuestionSetId", "dbo.DTQuestionSets");
            DropForeignKey("dbo.DTAppeareds", "DaliyTestId", "dbo.DaliyTests");
            DropForeignKey("dbo.DTAnswereds", "AppearedId", "dbo.DTAppeareds");
            DropForeignKey("dbo.DTAnswereds", "AnswerId", "dbo.DTAnswers");
            DropForeignKey("dbo.DTQuestions", "QuestionSetId", "dbo.DTQuestionSets");
            DropForeignKey("dbo.DTQuestionSets", "DaliyTestId", "dbo.DaliyTests");
            DropForeignKey("dbo.DTAnswers", "QuestionId", "dbo.DTQuestions");
            DropIndex("dbo.DTAppeareds", new[] { "UserId" });
            DropIndex("dbo.DTAppeareds", new[] { "DTQuestionSetId" });
            DropIndex("dbo.DTAppeareds", new[] { "DaliyTestId" });
            DropIndex("dbo.DTQuestionSets", new[] { "DaliyTestId" });
            DropIndex("dbo.DTQuestions", new[] { "QuestionSetId" });
            DropIndex("dbo.DTAnswers", new[] { "QuestionId" });
            DropIndex("dbo.DTAnswereds", new[] { "AnswerId" });
            DropIndex("dbo.DTAnswereds", new[] { "AppearedId" });
            DropIndex("dbo.DTAnswereds", new[] { "UserId" });
            DropIndex("dbo.DTAnswereds", new[] { "DTQuestionSetId" });
            DropTable("dbo.DTAppeareds");
            DropTable("dbo.DTQuestionSets");
            DropTable("dbo.DTQuestions");
            DropTable("dbo.DTAnswers");
            DropTable("dbo.DTAnswereds");
            DropTable("dbo.DaliyTests");
        }
    }
}
