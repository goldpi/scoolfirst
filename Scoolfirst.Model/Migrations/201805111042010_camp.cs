namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class camp : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ARCA_BUILD", "VERSIONID", "dbo.ARCA_VERSION");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaBuildId", "dbo.ARCA_BUILD");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaCrashconfigurationId", "dbo.ARCA_CRASHCONFIGURATION");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaCustomdataId", "dbo.ARCA_CUSTOMDATA");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaDevicefeaturesId", "dbo.ARCA_DEVICEFEATURES");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaEnvironmentId", "dbo.ARCA_ENVIRONMENT");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaInitialconfigurationId", "dbo.ARCA_INITIALCONFIGURATION");
            DropIndex("dbo.ARCA_BUILD", new[] { "VERSIONID" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaCustomdataId" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaInitialconfigurationId" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaCrashconfigurationId" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaDevicefeaturesId" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaEnvironmentId" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaBuildId" });
            CreateTable(
                "dbo.CampCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisplayName = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CampPosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CampCategoryId = c.Int(nullable: false),
                        Title = c.String(),
                        Active = c.Boolean(nullable: false),
                        Page = c.String(),
                        VideoUrl = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SubscribedCamps",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        CampId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.CampId });
            
            AddColumn("dbo.PlayRoots", "Active", c => c.Boolean(nullable: false));
            AddColumn("dbo.RoleModels", "TypeOfRm", c => c.String());
            DropTable("dbo.ARCA_BUILD");
            DropTable("dbo.ARCA_VERSION");
            DropTable("dbo.ARCA_CRASHCONFIGURATION");
            DropTable("dbo.ARCA_CUSTOMDATA");
            DropTable("dbo.ARCA_DEVICEFEATURES");
            DropTable("dbo.ARCA_ENVIRONMENT");
            DropTable("dbo.ARCA_ErrorReportingMaster");
            DropTable("dbo.ARCA_INITIALCONFIGURATION");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ARCA_INITIALCONFIGURATION",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompatScreenHeightDp = c.Int(nullable: false),
                        CompatScreenWidthDp = c.Int(nullable: false),
                        CompatSmallestScreenWidthDp = c.Int(nullable: false),
                        DensityDpi = c.Int(nullable: false),
                        FontScale = c.String(),
                        HardKeyboardHidden = c.String(),
                        Keyboard = c.String(),
                        KeyboardHidden = c.String(),
                        Locale = c.String(),
                        Mcc = c.Int(nullable: false),
                        Mnc = c.Int(nullable: false),
                        Navigation = c.String(),
                        NavigationHidden = c.String(),
                        Orientation = c.String(),
                        ScreenHeightDp = c.Int(nullable: false),
                        ScreenLayout = c.String(),
                        ScreenWidthDp = c.Int(nullable: false),
                        Seq = c.Int(nullable: false),
                        SmallestScreenWidthDp = c.Int(nullable: false),
                        ThemeConfig = c.String(),
                        Touchscreen = c.String(),
                        UiMode = c.String(),
                        UserSetLocale = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_ErrorReportingMaster",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ReportStatus = c.Int(),
                        ReportPrority = c.Int(),
                        OrigilanLogFile = c.String(),
                        REPORTID = c.String(),
                        APPVERSIONCODE = c.Int(nullable: false),
                        APPVERSIONNAME = c.String(),
                        PACKAGENAME = c.String(),
                        FILEPATH = c.String(),
                        PHONEMODEL = c.String(),
                        ANDROIDVERSION = c.Double(nullable: false),
                        BRAND = c.String(),
                        PRODUCT = c.String(),
                        TOTALMEMSIZE = c.Long(nullable: false),
                        AVAILABLEMEMSIZE = c.String(),
                        STACKTRACE = c.String(),
                        USERCOMMENT = c.String(),
                        USERAPPSTARTDATE = c.DateTime(nullable: false),
                        USERCRASHDATE = c.DateTime(nullable: false),
                        DUMPSYSMEMINFO = c.String(),
                        LOGCAT = c.String(),
                        INSTALLATIONID = c.String(),
                        USEREMAIL = c.String(),
                        ArcaCustomdataId = c.Int(nullable: false),
                        ArcaInitialconfigurationId = c.Int(nullable: false),
                        ArcaCrashconfigurationId = c.Int(nullable: false),
                        ArcaDevicefeaturesId = c.Int(nullable: false),
                        ArcaEnvironmentId = c.Int(nullable: false),
                        ArcaBuildId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_ENVIRONMENT",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GetDataDirectory = c.String(),
                        GetDownloadCacheDirectory = c.String(),
                        GetEmulatedStorageObbSource = c.String(),
                        GetExternalStorageDirectory = c.String(),
                        GetExternalStoragePath = c.String(),
                        GetExternalStoragePathState = c.String(),
                        GetExternalStorageState = c.String(),
                        GetInternalStoragePath = c.String(),
                        GetInternalStoragePathState = c.String(),
                        GetLegacyExternalStorageDirectory = c.String(),
                        GetLegacyExternalStorageObbDirectory = c.String(),
                        GetMediaStorageDirectory = c.String(),
                        GetOemDirectory = c.String(),
                        GetRootDirectory = c.String(),
                        GetSecondaryStorageDirectory = c.String(),
                        GetSecureDataDirectory = c.String(),
                        GetStorageType = c.Int(nullable: false),
                        GetSystemSecureDirectory = c.String(),
                        GetVendorDirectory = c.String(),
                        IsEncryptedFilesystemEnabled = c.Boolean(nullable: false),
                        IsExternalStorageEmulated = c.Boolean(nullable: false),
                        IsExternalStorageRemovable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_DEVICEFEATURES",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AndroidHardwareSensorProximity = c.Boolean(nullable: false),
                        AndroidHardwareSensorAccelerometer = c.Boolean(nullable: false),
                        AndroidHardwareFaketouch = c.Boolean(nullable: false),
                        AndroidHardwareUsbAccessory = c.Boolean(nullable: false),
                        ComFihtdcNavigationFeatureStyleInfocus = c.Boolean(nullable: false),
                        AndroidSoftwareBackup = c.Boolean(nullable: false),
                        AndroidHardwareTouchscreen = c.Boolean(nullable: false),
                        AndroidHardwareTouchscreenMultitouch = c.Boolean(nullable: false),
                        ComFihtdcEncryptedOta = c.Boolean(nullable: false),
                        AndroidSoftwarePrint = c.Boolean(nullable: false),
                        ComFihtdcInlifeuiSettingsStyleTab = c.Boolean(nullable: false),
                        AndroidSoftwareVoiceRecognizers = c.Boolean(nullable: false),
                        AndroidHardwareBluetooth = c.Boolean(nullable: false),
                        AndroidHardwareCameraAutofocus = c.Boolean(nullable: false),
                        AndroidHardwareTelephonyGsm = c.Boolean(nullable: false),
                        ComFihtdcInlifeuiSettingsStyleDashboard = c.Boolean(nullable: false),
                        AndroidHardwareAudioOutput = c.Boolean(nullable: false),
                        AndroidHardwareCameraFlash = c.Boolean(nullable: false),
                        AndroidHardwareCameraFront = c.Boolean(nullable: false),
                        AndroidHardwareScreenPortrait = c.Boolean(nullable: false),
                        AndroidSoftwareHomeScreen = c.Boolean(nullable: false),
                        AndroidHardwareMicrophone = c.Boolean(nullable: false),
                        AndroidHardwareBluetoothLe = c.Boolean(nullable: false),
                        AndroidHardwareTouchscreenMultitouchJazzhand = c.Boolean(nullable: false),
                        AndroidSoftwareAppWidgets = c.Boolean(nullable: false),
                        AndroidSoftwareInputMethods = c.Boolean(nullable: false),
                        AndroidHardwareSensorLight = c.Boolean(nullable: false),
                        AndroidSoftwareDeviceAdmin = c.Boolean(nullable: false),
                        AndroidHardwareCamera = c.Boolean(nullable: false),
                        AndroidHardwareScreenLandscape = c.Boolean(nullable: false),
                        ComFihtdcInlifeuiFeature = c.Boolean(nullable: false),
                        AndroidSoftwareManagedUsers = c.Boolean(nullable: false),
                        AndroidSoftwareWebview = c.Boolean(nullable: false),
                        AndroidHardwareCameraAny = c.Boolean(nullable: false),
                        AndroidSoftwareConnectionservice = c.Boolean(nullable: false),
                        AndroidHardwareTouchscreenMultitouchDistinct = c.Boolean(nullable: false),
                        AndroidHardwareLocationNetwork = c.Boolean(nullable: false),
                        AndroidSoftwareLiveWallpaper = c.Boolean(nullable: false),
                        AndroidHardwareLocationGps = c.Boolean(nullable: false),
                        AndroidHardwareWifi = c.Boolean(nullable: false),
                        AndroidHardwareLocation = c.Boolean(nullable: false),
                        ComFihtdcInlifeuiSettingsSearch = c.Boolean(nullable: false),
                        ComFihtdcNavigationFeature = c.Boolean(nullable: false),
                        AndroidHardwareTelephony = c.Boolean(nullable: false),
                        GlEsVersion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_CUSTOMDATA",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_CRASHCONFIGURATION",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompatScreenHeightDp = c.Int(nullable: false),
                        CompatScreenWidthDp = c.Int(nullable: false),
                        CompatSmallestScreenWidthDp = c.Int(nullable: false),
                        DensityDpi = c.Int(nullable: false),
                        FontScale = c.String(),
                        HardKeyboardHidden = c.String(),
                        Keyboard = c.String(),
                        KeyboardHidden = c.String(),
                        Locale = c.String(),
                        Mcc = c.Int(nullable: false),
                        Mnc = c.Int(nullable: false),
                        Navigation = c.String(),
                        NavigationHidden = c.String(),
                        Orientation = c.String(),
                        ScreenHeightDp = c.Int(nullable: false),
                        ScreenLayout = c.String(),
                        ScreenWidthDp = c.Int(nullable: false),
                        Seq = c.Int(nullable: false),
                        SmallestScreenWidthDp = c.Int(nullable: false),
                        ThemeConfig = c.String(),
                        Touchscreen = c.String(),
                        UiMode = c.String(),
                        UserSetLocale = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_VERSION",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ACTIVECODENAMES = c.String(),
                        BASEOS = c.String(),
                        CODENAME = c.String(),
                        INCREMENTAL = c.String(),
                        RELEASE = c.Double(nullable: false),
                        RESOURCESSDKINT = c.Int(nullable: false),
                        SDK = c.Int(nullable: false),
                        SDKINT = c.Int(nullable: false),
                        SECURITYPATCH = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_BUILD",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BOARD = c.String(),
                        BOOTLOADER = c.String(),
                        BRAND = c.String(),
                        CPUABI = c.String(),
                        CPUABI2 = c.String(),
                        DEVICE = c.String(),
                        DISPLAY = c.String(),
                        FINGERPRINT = c.String(),
                        HARDWARE = c.String(),
                        HOST = c.String(),
                        BUILDID = c.String(),
                        ISDEBUGGABLE = c.Boolean(nullable: false),
                        MANUFACTURER = c.String(),
                        MODEL = c.String(),
                        PRODUCT = c.String(),
                        RADIO = c.String(),
                        SERIAL = c.String(),
                        SUPPORTED32BITABIS = c.String(),
                        SUPPORTED64BITABIS = c.String(),
                        SUPPORTEDABIS = c.String(),
                        TAGS = c.String(),
                        TIME = c.String(),
                        TYPE = c.String(),
                        UNKNOWN = c.String(),
                        USER = c.String(),
                        VERSIONID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.RoleModels", "TypeOfRm");
            DropColumn("dbo.PlayRoots", "Active");
            DropTable("dbo.SubscribedCamps");
            DropTable("dbo.CampPosts");
            DropTable("dbo.CampCategories");
            CreateIndex("dbo.ARCA_ErrorReportingMaster", "ArcaBuildId");
            CreateIndex("dbo.ARCA_ErrorReportingMaster", "ArcaEnvironmentId");
            CreateIndex("dbo.ARCA_ErrorReportingMaster", "ArcaDevicefeaturesId");
            CreateIndex("dbo.ARCA_ErrorReportingMaster", "ArcaCrashconfigurationId");
            CreateIndex("dbo.ARCA_ErrorReportingMaster", "ArcaInitialconfigurationId");
            CreateIndex("dbo.ARCA_ErrorReportingMaster", "ArcaCustomdataId");
            CreateIndex("dbo.ARCA_BUILD", "VERSIONID");
            AddForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaInitialconfigurationId", "dbo.ARCA_INITIALCONFIGURATION", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaEnvironmentId", "dbo.ARCA_ENVIRONMENT", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaDevicefeaturesId", "dbo.ARCA_DEVICEFEATURES", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaCustomdataId", "dbo.ARCA_CUSTOMDATA", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaCrashconfigurationId", "dbo.ARCA_CRASHCONFIGURATION", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaBuildId", "dbo.ARCA_BUILD", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ARCA_BUILD", "VERSIONID", "dbo.ARCA_VERSION", "Id", cascadeDelete: true);
        }
    }
}
