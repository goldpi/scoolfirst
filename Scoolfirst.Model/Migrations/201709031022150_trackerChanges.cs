namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class trackerChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReasoningTrackers", "TestPassed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReasoningTrackers", "TestPassed");
        }
    }
}
