namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class to_to : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.Questions", "TypeOrder", c => c.Int(nullable: false));
            //AddColumn("dbo.Transactions", "ProductId", c => c.Int());
            //AddColumn("dbo.Transactions", "Points", c => c.Int());
            //AddColumn("dbo.Transactions", "Redeem", c => c.Boolean(nullable: false));
            //CreateIndex("dbo.Transactions", "ProductId");
            //AddForeignKey("dbo.Transactions", "ProductId", "dbo.Products", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "ProductId", "dbo.Products");
            DropIndex("dbo.Transactions", new[] { "ProductId" });
            DropColumn("dbo.Transactions", "Redeem");
            DropColumn("dbo.Transactions", "Points");
            DropColumn("dbo.Transactions", "ProductId");
            DropColumn("dbo.Questions", "TypeOrder");
        }
    }
}
