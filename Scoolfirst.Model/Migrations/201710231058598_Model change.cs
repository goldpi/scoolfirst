namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modelchange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PAQuestions", "Solution", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PAQuestions", "Solution");
        }
    }
}
