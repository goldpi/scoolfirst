// <auto-generated />
namespace Scoolfirst.Model.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class campTime : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(campTime));
        
        string IMigrationMetadata.Id
        {
            get { return "201805150750416_campTime"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
