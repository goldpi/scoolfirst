namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dt_changes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DaliyTests", "Scroll", c => c.String());
            AddColumn("dbo.DaliyTests", "CheatCode", c => c.String());
            AddColumn("dbo.DTAnswers", "IsCorrect", c => c.Boolean(nullable: false));
            DropColumn("dbo.DTAnswers", "IsCoorect");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DTAnswers", "IsCoorect", c => c.Boolean(nullable: false));
            DropColumn("dbo.DTAnswers", "IsCorrect");
            DropColumn("dbo.DaliyTests", "CheatCode");
            DropColumn("dbo.DaliyTests", "Scroll");
        }
    }
}
