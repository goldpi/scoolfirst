namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class virtualcompetiton : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VirtualCompetetionAnswers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        OptionId = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        ExamId = c.Long(nullable: false),
                        AttemptId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VirtualCompetetionAttempts", t => t.AttemptId, cascadeDelete: false)
                .ForeignKey("dbo.VirtualExams", t => t.ExamId, cascadeDelete: false)
                .ForeignKey("dbo.VirtualCompetetionOptions", t => t.OptionId, cascadeDelete: false)
                .ForeignKey("dbo.VirtualCompetetionQuestions", t => t.QuestionId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.OptionId)
                .Index(t => t.QuestionId)
                .Index(t => t.ExamId)
                .Index(t => t.AttemptId);
            
            CreateTable(
                "dbo.VirtualCompetetionAttempts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        ExamId = c.Long(nullable: false),
                        On = c.DateTime(nullable: false),
                        RightAns = c.Int(nullable: false),
                        WrongAns = c.Int(nullable: false),
                        SkippedAns = c.Int(nullable: false),
                        PointsEarned = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VirtualExams", t => t.ExamId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ExamId);
            
            CreateTable(
                "dbo.VirtualExams",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Content = c.String(nullable: false),
                        TimeInMinutes = c.Int(nullable: false),
                        AddedOn = c.DateTime(nullable: false),
                        PointsPerQuestion = c.Int(nullable: false),
                        TestBrief = c.String(),
                        Order = c.Int(nullable: false),
                        Level = c.String(),
                        Color = c.String(),
                        TitleColor = c.String(),
                        CategoryId = c.Int(nullable: false),
                        Subject = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VirtualCompetitonSubCategories", t => t.CategoryId, cascadeDelete: false)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.VirtualCompetitonSubCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Details = c.String(),
                        VideoUrl = c.String(),
                        Active = c.Boolean(nullable: false),
                        AddedOn = c.DateTime(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VirtualCompetetionMainCategories", t => t.CategoryId, cascadeDelete: false)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.VirtualCompetetionMainCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Details = c.String(),
                        Position = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        AddedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VirtualCompetetionQuestions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Query = c.String(nullable: false),
                        Solution = c.String(),
                        ShowSolution = c.Boolean(nullable: false),
                        ExamId = c.Long(nullable: false),
                        ShortOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VirtualExams", t => t.ExamId, cascadeDelete: false)
                .Index(t => t.ExamId);
            
            CreateTable(
                "dbo.VirtualCompetetionOptions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        Answer = c.String(),
                        Correct = c.Boolean(nullable: false),
                        ShortOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VirtualCompetetionQuestions", t => t.QuestionId, cascadeDelete: false)
                .Index(t => t.QuestionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VirtualCompetetionAnswers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.VirtualCompetetionAnswers", "QuestionId", "dbo.VirtualCompetetionQuestions");
            DropForeignKey("dbo.VirtualCompetetionAnswers", "OptionId", "dbo.VirtualCompetetionOptions");
            DropForeignKey("dbo.VirtualCompetetionAnswers", "ExamId", "dbo.VirtualExams");
            DropForeignKey("dbo.VirtualCompetetionAttempts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.VirtualCompetetionAttempts", "ExamId", "dbo.VirtualExams");
            DropForeignKey("dbo.VirtualCompetetionOptions", "QuestionId", "dbo.VirtualCompetetionQuestions");
            DropForeignKey("dbo.VirtualCompetetionQuestions", "ExamId", "dbo.VirtualExams");
            DropForeignKey("dbo.VirtualExams", "CategoryId", "dbo.VirtualCompetitonSubCategories");
            DropForeignKey("dbo.VirtualCompetitonSubCategories", "CategoryId", "dbo.VirtualCompetetionMainCategories");
            DropForeignKey("dbo.VirtualCompetetionAnswers", "AttemptId", "dbo.VirtualCompetetionAttempts");
            DropIndex("dbo.VirtualCompetetionOptions", new[] { "QuestionId" });
            DropIndex("dbo.VirtualCompetetionQuestions", new[] { "ExamId" });
            DropIndex("dbo.VirtualCompetitonSubCategories", new[] { "CategoryId" });
            DropIndex("dbo.VirtualExams", new[] { "CategoryId" });
            DropIndex("dbo.VirtualCompetetionAttempts", new[] { "ExamId" });
            DropIndex("dbo.VirtualCompetetionAttempts", new[] { "UserId" });
            DropIndex("dbo.VirtualCompetetionAnswers", new[] { "AttemptId" });
            DropIndex("dbo.VirtualCompetetionAnswers", new[] { "ExamId" });
            DropIndex("dbo.VirtualCompetetionAnswers", new[] { "QuestionId" });
            DropIndex("dbo.VirtualCompetetionAnswers", new[] { "OptionId" });
            DropIndex("dbo.VirtualCompetetionAnswers", new[] { "UserId" });
            DropTable("dbo.VirtualCompetetionOptions");
            DropTable("dbo.VirtualCompetetionQuestions");
            DropTable("dbo.VirtualCompetetionMainCategories");
            DropTable("dbo.VirtualCompetitonSubCategories");
            DropTable("dbo.VirtualExams");
            DropTable("dbo.VirtualCompetetionAttempts");
            DropTable("dbo.VirtualCompetetionAnswers");
        }
    }
}
