namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updates : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AppUpdates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(nullable: false, maxLength: 140),
                        Update1 = c.String(maxLength: 140),
                        ShowUpdate1 = c.Boolean(nullable: false),
                        Update2 = c.String(maxLength: 140),
                        ShowUpdate2 = c.Boolean(nullable: false),
                        Update3 = c.String(maxLength: 140),
                        ShowUpdate3 = c.Boolean(nullable: false),
                        Update4 = c.String(maxLength: 140),
                        ShowUpdate4 = c.Boolean(nullable: false),
                        AddedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AppUpdates");
        }
    }
}
