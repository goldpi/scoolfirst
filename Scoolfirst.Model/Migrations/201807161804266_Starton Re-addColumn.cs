namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StartonReaddColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayRoots", "StartsOn", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlayRoots", "StartsOn");
        }
    }
}
