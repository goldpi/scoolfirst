namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StartonColumnDrop : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PlayRoots", "StartsOn");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PlayRoots", "StartsOn", c => c.DateTimeOffset(precision: 7));
        }
    }
}
