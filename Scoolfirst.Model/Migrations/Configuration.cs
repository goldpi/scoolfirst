namespace Scoolfirst.Model.Migrations
{
    using Common;
    using Geo;
    using Projects;
    using Slides;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Scoolfirst.Model.Application_Setting;
    using FizzWare.NBuilder;
    using WorkBook;
    using Feed;

    internal sealed class Configuration : DbMigrationsConfiguration<Scoolfirst.Model.Context.ScoolfirstContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Scoolfirst.Model.Context.ScoolfirstContext context)
        {
             return;
            /*
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            //context.ReportAbuseTitle.AddOrUpdate(
            //    p => p.Title,
            //    new ReportAbuseTitle { Title = "It's annoying or not interesting", EntryDate = DateTime.UtcNow, Visible = true },
            //    new ReportAbuseTitle { Title = "I'm in this photo and I don't like it", EntryDate = DateTime.UtcNow, Visible = true },
            //    new ReportAbuseTitle { Title = "I think it shouldn't be on ScoolFirst.com", EntryDate = DateTime.UtcNow, Visible = true },
            //    new ReportAbuseTitle { Title = "It's spam", EntryDate = DateTime.UtcNow, Visible = true },
            //    new ReportAbuseTitle { Title = "Adult content", EntryDate = DateTime.UtcNow, Visible = true },
            //    new ReportAbuseTitle { Title = "Heart specific cummunity", EntryDate = DateTime.UtcNow, Visible = true }

            //    );
            #region GEO
            //context.Countries.AddOrUpdate(
            //p => p.Name,
            //new Country
            //{
            //    Name = "India",
            //    States = new List<State>
            //    {
            //                    new State
            //                    {
            //                    Name ="Jharkhand",
            //                    Cities =new List<City>
            //                    {
            //                        new City
            //                        {
            //                        Name ="Ranchi",
            //                        Areas =new List<Area>
            //                            {

            //                                new Area { Name="Argora", PostBox="834001" },
            //                                new Area { Name="Ashok Nagar", PostBox="834001" },
            //                                new Area { Name="Bahu Bazaar", PostBox="834001" },
            //                                new Area { Name="Bariatu", PostBox="834001" },
            //                                new Area { Name="Booty More", PostBox="834001" },
            //                                new Area { Name="Bundu", PostBox="834001" },
            //                                new Area { Name="Dhanai Soso", PostBox="834001" },
            //                                new Area { Name="Dhurwa", PostBox="834001" },
            //                                new Area { Name="Dipatoli", PostBox="834001" },
            //                                new Area { Name="Dipatoli Cantt", PostBox="834001" },
            //                                new Area { Name="Doranda", PostBox="834001" },
            //                                new Area { Name="H.E.C. Sector-3", PostBox="834001" },
            //                                new Area { Name="H.E.C. Sector-4", PostBox="834001" },
            //                                new Area { Name="Harmu", PostBox="834001" },
            //                                new Area { Name="Harmu Housing Colony", PostBox="834001" },
            //                                new Area { Name="Hehal", PostBox="834001" },
            //                                new Area { Name="Hesag", PostBox="834001" },
            //                                new Area { Name="Hindpiri", PostBox="834001" },
            //                                new Area { Name="Hinoo", PostBox="834001" },
            //                                new Area { Name="Hulhundu", PostBox="834001" },
            //                                new Area { Name="Itki", PostBox="834001" },
            //                                new Area { Name="Jagganathpur", PostBox="834001" },
            //                                new Area { Name="Jail road", PostBox="834001" },
            //                                new Area { Name="Kanke", PostBox="834001" },
            //                                new Area { Name="Kanke road", PostBox="834001" },
            //                                new Area { Name="Kantatoli", PostBox="834001" },
            //                                new Area { Name="Kanthitand", PostBox="834001" },
            //                                new Area { Name="Khelgaon", PostBox="834001" },
            //                                new Area { Name="Lalpur", PostBox="834001" },
            //                                new Area { Name="Morabadi", PostBox="834001" },
            //                                new Area { Name="Namkum", PostBox="834001" },
            //                                new Area { Name="Purulia road", PostBox="834001" },
            //                                new Area { Name="Ranchi G.P.O", PostBox="834001" },
            //                                new Area { Name="Ratu road", PostBox="834001" },
            //                                new Area { Name="Samlong", PostBox="834001" },
            //                                new Area { Name="Shahid Chowk", PostBox="834001" },
            //                                new Area { Name="Tatisilwai", PostBox="834001" },
            //                                new Area { Name="Tupudana", PostBox="834001" },
            //                                new Area { Name="Dashmile", PostBox="834001" },
            //                                new Area { Name="Harmu", PostBox="834001" },
            //                                new Area { Name="Kadru", PostBox="834001" },
            //                                new Area { Name="Meshra", PostBox="834001" },
            //                                new Area { Name="Namkum", PostBox="834001" },
            //                                new Area { Name="Over Bridge", PostBox="834001" },
            //                                new Area { Name="P. P.  Compund", PostBox="834001" },
            //                                new Area { Name="Purlia Road", PostBox="834001" }
            //                        }
            ////area end
            //                         }
            //                       }//cities end
            //                    },
            //    }//states end
            //}//country end

            //);


            #endregion
            
            #region School

                        //context.Schools.AddOrUpdate
                        //    (
                        //    p => p.Id,
                        //new School { SchoolName = "Army School", LocalArea = "Samlong",  Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Assisi High School", LocalArea = "Samlong", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Baby's World  Pre School", LocalArea = "Doranda", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Bishop Westcott Boys' School", LocalArea = "Namkum", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Bishop Westcott Girls' School", LocalArea = "Namkum", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Bishop Westcott Girls' School", LocalArea = "Doranda", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Bishop's School", LocalArea = "Bahubazar", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Bridgeford School", LocalArea = "Tupudana", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Cambrian Public School", LocalArea = "Kanke road", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Cambrian Public School", LocalArea = "Bariatu", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Cambridge School", LocalArea = "Tatisilwai", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Carmel School", LocalArea = "Samlong", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Central Academy", LocalArea = "Bariyatu", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Chhotanagpur Public School", LocalArea = "Buti Road", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Children's Educational Centre", LocalArea = "Harmu Housing Colony", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Children's Paradise School", LocalArea = "Tatisilwai", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Chiranjeevee Play School", LocalArea = "Morabadi", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Chiranjeevee Public School", LocalArea = "Morabadi", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Creative Kids Play School", LocalArea = "Kanthitand", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Creative Kids Play School", LocalArea = "Khelgaon", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "DAV Kapildeo Public School", LocalArea = "Kadru", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "DAV Nandraj Public School", LocalArea = "Lalpur", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "DAV Public School", LocalArea = "Hehal", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "DAV Public School", LocalArea = "Bariatu", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "DAV Public School", LocalArea = "Kanke road", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "DAV Public School", LocalArea = "H.E.C.Sector - 3", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Delhi Public School", LocalArea = "Ranchi", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Don Bosco School", LocalArea = "Hesag", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Fatima Girls' Academy", LocalArea = "Itki", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Firayalal Public School", LocalArea = "Over Bridge", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "G & H High School", LocalArea = "Jail Road", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Gouri Dutt Mandelia High School", LocalArea = "Ratu Road", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Guru Nanak School", LocalArea = "P.P.Compund", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "IIPS - Direct Learning School", LocalArea = "Kantatoli Chowk", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Jamia public school", LocalArea = "Hindpiri", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Jawahar Vidya Mandir", LocalArea = "Doranda", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Kairali School", LocalArea = "H.E.C.Sector-4", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Kendriya Vidyalaya", LocalArea = "Dipatoli Cantt", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Kendriya Vidyalaya", LocalArea = "Hinoo", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Kendriya Vidyalaya", LocalArea = "Ratu road", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "L. A.Garden High School", LocalArea = "Samlong Ghat Road", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Lala Lajpat Rai Bal Mandir", LocalArea = "Kadru", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Little Genius Preschool", LocalArea = "Bariatu Road", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Little Learners Preschool", LocalArea = "Ashok Nagar", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Loreto Convent", LocalArea = "Doranda", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Manan Vidya", LocalArea = "Booty More", Area = context.Areas.First(i => i.Name == "Bariatu") },
                        //new School { SchoolName = "Prabhat Tara High School", LocalArea = "Dhurwa", Area = context.Areas.First(i => i.Name == "Bariatu") }



                        //);

                        #endregion

            #region Class
                        //context.Classes.AddOrUpdate
                        //       (
                        //        p => p.RomanDisplay,


                        //        new Classes { NumericDisplay = "3", RomanDisplay = "Class III" },
                        //        new Classes { NumericDisplay = "4", RomanDisplay = "Class IV" },
                        //        new Classes { NumericDisplay = "5", RomanDisplay = "Class V" },
                        //        new Classes { NumericDisplay = "6", RomanDisplay = "Class VI" }


                        //        );
                        #endregion

            #region ProjectCategories
                        //context.ProjectCategories.AddOrUpdate(
                        //    p => p.Name,
                        //    new ProjectCategory { Name = "Class I" },
                        //     new ProjectCategory { Name = "Class II" },
                        //      new ProjectCategory { Name = "Class III" },
                        //       new ProjectCategory { Name = "Class IV" },
                        //        new ProjectCategory { Name = "Class V" },
                        //         new ProjectCategory { Name = "Class VI" },
                        //          new ProjectCategory { Name = "Class VII" },
                        //          new ProjectCategory { Name = "Class VIII" }
                        //    );
                        #endregion

            #region 
                        //context.ProductCategories.AddOrUpdate(cate => cate.Title,
                        //    new ProductCategory { Title = "Science Projects", Description = "", Image = "http://placehold.it/100x100&text=Projects", ShortDescription = "" },
                        //    new ProductCategory { Title = "Daily Pocket Learner", Description = "", Image = "http://placehold.it/100x100&text=DPL", ShortDescription = "" },
                        //    new ProductCategory { Title = "Visual Learning Board", Description = "", Image = "http://placehold.it/100x100&text=VLB", ShortDescription = "" },
                        //    new ProductCategory { Title = "Practice Booklet", Description = "", Image = "http://placehold.it/100x100&text=Booklet", ShortDescription = "" }
                        //    );
                        #endregion

            #region Slider
                        //context.Sliders.AddOrUpdate
                        //      (
                        //       p => p.SliderName,
                        //       new Slider { SliderName = "Android Dashboard", UpdateOn = DateTime.UtcNow, Status = true, IsSytemDefine = true },
                        //       new Slider { SliderName = "Android RoleModel", UpdateOn = DateTime.UtcNow, Status = true, IsSytemDefine = true },
                        //       new Slider { SliderName = "Android Computer & Creative", UpdateOn = DateTime.UtcNow, Status = true, IsSytemDefine = true },
                        //       new Slider { SliderName = "Android Product Category", UpdateOn = DateTime.UtcNow, Status = true, IsSytemDefine = true }

                        //       );
                        #endregion
                      
            #region ApplicationSetting
                        //context.ApplicationSetting.AddOrUpdate
                        //      (
                        //       p => p.CompanyName,
                        //       new ApplicationSetting {BillingMobile = "",SupportMobile = "",SaleMobile = "",SaleEmail = "",SupportEmail = "",BillingEmail = "",BillingAddress = "", GeneralAddress = "",WebSiteUrl = "",Adminlogo = "",Weblogo = "",Invoicelogo = "",CompanyName = "Upgrade Jr"}

                        //       );

                        #endregion

            #region Books

                        var books = Builder<Scoolfirst.Model.WorkBook.Books>.CreateListOfSize(1000)
                    .All()
                        .With(o => o.ClassesId = Pick<int>.RandomItemFrom(context.Classes.Select(i=>i.Id).ToList()))
                        .With(o => o.SubjectId = Pick<int>.RandomItemFrom(context.Subjects.Select(i => i.Id).ToList()))
                        .With(o => o.Title = Faker.Name.First())
                        .With(o=>o.Author=Faker.Name.First())
                        .With(o=>o.ImageUrl= "http://static.upgrde.net/source/Thumbnails.jpg")
                    .Build();

                        context.Books.AddOrUpdate(o => o.Id, books.ToArray());
                        #endregion

            #region Chapters
                        var chapters = Builder<Scoolfirst.Model.WorkBook.Chapter>.CreateListOfSize(1000)
                   .All()
                       .With(o => o.ClassesId = Pick<int>.RandomItemFrom(context.Classes.Select(i => i.Id).ToList()))
                       .With(o => o.Books = context.Books.OrderBy(i=>Guid.NewGuid()).Take(1).ToList())
                       .With(o => o.Name = Faker.Name.First())          
                   .Build();
                        context.Chapters.AddOrUpdate(o => o.Id, chapters.ToArray());
                        #endregion

            #region Notes
                        var notes = Builder<Note>.CreateListOfSize(1000)
                   .All()
                       .With(o => o.ChapterId = Pick<int>.RandomItemFrom(context.Chapters.Select(i => i.Id).ToList()))
                       .With(o => o.Topic = Faker.Name.First())
                       .With(o => o.RevisonMatter = Faker.Lorem.Paragraph())
                   .Build();
                        context.Notes.AddOrUpdate(o => o.Id, notes.ToArray());
                        #endregion

            #region RoleModel
                        var dategenerator = new RandomGenerator();
                        var rolemodel = Builder<RoleModel>.CreateListOfSize(1000)
                  .All()
                      .With(o => o.ClassId = Pick<int>.RandomItemFrom(context.Classes.Select(i => i.Id).ToList()))
                      .With(o => o.Title = Faker.Name.First())
                      .With(o => o.Content = Faker.Lorem.Paragraph())
                      .With(o=>o.ImageUrl= "http://static.upgrde.net/source/Thumbnails.jpg")
                      .With(o=>o.VideoUrl= "https://youtu.be/svVSim1xils")
                      .With(o=>o.On=DateTime.Now.AddDays(-dategenerator.Next(1,100)))
                  .Build();
                        context.RoleModels.AddOrUpdate(o => o.Id, rolemodel.ToArray());
                        #endregion
        
            #region Post
            var rgen = new RandomGenerator();
            var post = Builder<Feed.Post>.CreateListOfSize(1000)
     .All()

         .With(o => o.Title = Faker.Name.First())
         .With(o => o.Content = Faker.Lorem.Paragraph())
         .With(o => o.UserId = "4dcc9e8f-2c37-4b00-aceb-f47a1279683e")
         .With(o => o.Tags=Faker.Company.Name())
         .With(o=>o.Type=(PostType)(rgen.Next(0,3)))
         .With(o=>o.Videos=(o.Type==PostType.KNOWLEDGE_NUTRITION?new List<PostVideos>() { new PostVideos {  Id=Guid.NewGuid(), PostId=o.Id, VideoUrl= "https://youtu.be/svVSim1xils" } }:new List<PostVideos>()))
         .With(o=>o.Images= (o.Type == PostType.CREATIVE_CANVAS ? new List<PostImages>() { new PostImages { Id = Guid.NewGuid(), PostId = o.Id, ImageUrl = "http://static.upgrde.net/source/Thumbnails.jpg" } }:new List<PostImages>()))
         .Build();
            context.Posts.AddOrUpdate(o => o.Id, post.ToArray());
            #endregion

            #region Feed
            //if (!System.Diagnostics.Debugger.IsAttached)
            //    System.Diagnostics.Debugger.Launch();
            var feeddate = new RandomGenerator();
            var feed = Builder<Feed.Feeds>.CreateListOfSize(1000)
      .All()

          .With(o => o.Title = Faker.Name.First())
          .With(o => o.OnDateTime = DateTime.Now.AddDays(-feeddate.Next(1, 200)))
          .With(o => o.PostId = Pick<long>.RandomItemFrom(context.Posts.Select(i => i.Id).ToList()))
          .With(o => o.GroupId = Pick<int>.RandomItemFrom(context.Groups.Select(i => i.Id).ToList()))
          .Build();
            context.Feeds.AddOrUpdate(o => o.Id, feed.ToArray());
            #endregion
              */
        }
    }
}