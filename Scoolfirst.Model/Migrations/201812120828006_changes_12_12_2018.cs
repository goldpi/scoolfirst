namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changes_12_12_2018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ParentsNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "ParentsNo");
        }
    }
}
