namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Articles", "ClassesId", "dbo.Classes");
            DropForeignKey("dbo.Articles", "SubjectId", "dbo.Subjects");
            DropIndex("dbo.Articles", new[] { "ClassesId" });
            DropIndex("dbo.Articles", new[] { "SubjectId" });
            AlterColumn("dbo.Articles", "ClassesId", c => c.Int());
            AlterColumn("dbo.Articles", "SubjectId", c => c.Int());
            CreateIndex("dbo.Articles", "ClassesId");
            CreateIndex("dbo.Articles", "SubjectId");
            AddForeignKey("dbo.Articles", "ClassesId", "dbo.Classes", "Id");
            AddForeignKey("dbo.Articles", "SubjectId", "dbo.Subjects", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Articles", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Articles", "ClassesId", "dbo.Classes");
            DropIndex("dbo.Articles", new[] { "SubjectId" });
            DropIndex("dbo.Articles", new[] { "ClassesId" });
            AlterColumn("dbo.Articles", "SubjectId", c => c.Int(nullable: false));
            AlterColumn("dbo.Articles", "ClassesId", c => c.Int(nullable: false));
            CreateIndex("dbo.Articles", "SubjectId");
            CreateIndex("dbo.Articles", "ClassesId");
            AddForeignKey("dbo.Articles", "SubjectId", "dbo.Subjects", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Articles", "ClassesId", "dbo.Classes", "Id", cascadeDelete: true);
        }
    }
}
