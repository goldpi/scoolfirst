namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class user_offline_payment_date : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OfflinePayments", "RequestDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AddColumn("dbo.OfflinePayments", "PaidRemaks", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OfflinePayments", "PaidRemaks");
            DropColumn("dbo.OfflinePayments", "RequestDate");
        }
    }
}
