namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class camp_relations : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.CampPosts", "CampCategoryId");
            AddForeignKey("dbo.CampPosts", "CampCategoryId", "dbo.CampCategories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CampPosts", "CampCategoryId", "dbo.CampCategories");
            DropIndex("dbo.CampPosts", new[] { "CampCategoryId" });
        }
    }
}
