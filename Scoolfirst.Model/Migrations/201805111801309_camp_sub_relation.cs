namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class camp_sub_relation : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.SubscribedCamps", "UserId");
            CreateIndex("dbo.SubscribedCamps", "CampId");
            AddForeignKey("dbo.SubscribedCamps", "CampId", "dbo.CampCategories", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SubscribedCamps", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubscribedCamps", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SubscribedCamps", "CampId", "dbo.CampCategories");
            DropIndex("dbo.SubscribedCamps", new[] { "CampId" });
            DropIndex("dbo.SubscribedCamps", new[] { "UserId" });
        }
    }
}
