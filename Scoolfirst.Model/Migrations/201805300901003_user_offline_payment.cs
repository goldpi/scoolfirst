namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class user_offline_payment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OfflinePayments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Amount = c.Double(nullable: false),
                        UserId = c.String(maxLength: 128),
                        Description = c.String(),
                        Paid = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OfflinePayments", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.OfflinePayments", new[] { "UserId" });
            DropTable("dbo.OfflinePayments");
        }
    }
}
