namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class virtualcompetiton_q : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VirtualCompetetionQuestions", "QueryActual", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VirtualCompetetionQuestions", "QueryActual");
        }
    }
}
