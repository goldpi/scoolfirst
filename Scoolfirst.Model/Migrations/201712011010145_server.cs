namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class server : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Boards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MediumLanguage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BookClassSchoolSyllabus",
                c => new
                    {
                        ClassId = c.Int(nullable: false),
                        SchoolId = c.Int(nullable: false),
                        BookId = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                        Map = c.String(),
                    })
                .PrimaryKey(t => new { t.ClassId, t.SchoolId, t.BookId, t.SubjectId });
            
            AddColumn("dbo.Schools", "BoardId", c => c.Int());
            CreateIndex("dbo.Schools", "BoardId");
            AddForeignKey("dbo.Schools", "BoardId", "dbo.Boards", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Schools", "BoardId", "dbo.Boards");
            DropIndex("dbo.Schools", new[] { "BoardId" });
            DropColumn("dbo.Schools", "BoardId");
            DropTable("dbo.BookClassSchoolSyllabus");
            DropTable("dbo.Boards");
        }
    }
}
