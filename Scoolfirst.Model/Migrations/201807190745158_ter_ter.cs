namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ter_ter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chapters", "Active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Chapters", "Active");
        }
    }
}
