namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class passable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PARoots", "IsPassable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PARoots", "IsPassable");
        }
    }
}
