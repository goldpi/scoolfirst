namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Achievements",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(),
                        AchievementDetails = c.String(),
                        OnDate = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ActivityLoggers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserName = c.String(),
                        Url = c.String(),
                        Controller = c.String(),
                        Action = c.String(),
                        OnDateOffset = c.DateTimeOffset(nullable: false, precision: 7),
                        Source = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Answereds",
                c => new
                    {
                        UserId = c.String(maxLength: 128),
                        QuestionSetId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false),
                        McqQuestionId = c.Guid(nullable: false),
                        OptionsId = c.Guid(nullable: false),
                        On = c.DateTime(nullable: false),
                        ReportId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AnswerGivens", t => new { t.UserId, t.QuestionSetId })
                .ForeignKey("dbo.Options", t => t.OptionsId, cascadeDelete: true)
                .ForeignKey("dbo.McqQuestions", t => t.McqQuestionId, cascadeDelete: true)
                .ForeignKey("dbo.Reports", t => t.ReportId, cascadeDelete: true)
                .Index(t => new { t.UserId, t.QuestionSetId })
                .Index(t => t.McqQuestionId)
                .Index(t => t.OptionsId)
                .Index(t => t.ReportId);
            
            CreateTable(
                "dbo.AnswerGivens",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        QuestionSetId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.QuestionSetId })
                .ForeignKey("dbo.QuestionSets", t => t.QuestionSetId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.QuestionSetId);
            
            CreateTable(
                "dbo.QuestionSets",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        Class = c.String(),
                        Type = c.Int(nullable: false),
                        Level = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Minutes = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.McqQuestions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Subject = c.String(),
                        Unit = c.String(nullable: false),
                        Query = c.String(nullable: false),
                        SetId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionSets", t => t.SetId, cascadeDelete: true)
                .Index(t => t.SetId);
            
            CreateTable(
                "dbo.Options",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        McqQuestionId = c.Guid(nullable: false),
                        Answer = c.String(),
                        Correct = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.McqQuestions", t => t.McqQuestionId, cascadeDelete: true)
                .Index(t => t.McqQuestionId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FullName = c.String(),
                        Student_Parent_Admin = c.String(),
                        ClassId = c.Int(nullable: false),
                        Board = c.String(),
                        PictureUrl = c.String(),
                        Points = c.Int(nullable: false),
                        AboutMe = c.String(),
                        DOB = c.String(),
                        Address = c.String(),
                        SchoolId = c.Int(),
                        Trial = c.Boolean(nullable: false),
                        Subscribed = c.Boolean(nullable: false),
                        Expiry = c.DateTime(nullable: false),
                        RegisterdOn = c.DateTime(nullable: false),
                        Sponsered = c.Boolean(nullable: false),
                        RegisterAs = c.Int(nullable: false),
                        RequestedforBrandAmbesdor = c.Boolean(nullable: false),
                        IsBrandAmbesdor = c.Boolean(nullable: false),
                        Gender = c.String(),
                        OtherSchool = c.String(),
                        ConnectionId = c.String(),
                        Modules = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassId, cascadeDelete: true)
                .ForeignKey("dbo.Schools", t => t.SchoolId)
                .Index(t => t.ClassId)
                .Index(t => t.SchoolId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RomanDisplay = c.String(),
                        NumericDisplay = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        School = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Schools",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SchoolName = c.String(),
                        AreaId = c.Int(nullable: false),
                        LocalArea = c.String(),
                        UseMasterContent = c.Boolean(nullable: false),
                        IsMasterContent = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Areas", t => t.AreaId, cascadeDelete: true)
                .Index(t => t.AreaId);
            
            CreateTable(
                "dbo.Areas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PostBox = c.String(),
                        CityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.States", t => t.StateId, cascadeDelete: true)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        ImageUrl = c.String(),
                        Author = c.String(),
                        ClassesId = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassesId, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.ClassesId)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.Chapters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        GlobalTrending = c.Boolean(nullable: false),
                        Trending = c.Boolean(nullable: false),
                        ClassesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassesId, cascadeDelete: true)
                .Index(t => t.ClassesId);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ChapterId = c.Int(nullable: false),
                        Trending = c.Boolean(nullable: false),
                        GlobalTrending = c.Boolean(nullable: false),
                        Topic = c.String(),
                        RevisonMatter = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chapters", t => t.ChapterId, cascadeDelete: true)
                .Index(t => t.ChapterId);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionType = c.String(),
                        Title = c.String(),
                        Answer = c.String(),
                        Refrence = c.String(),
                        School = c.String(),
                        Class = c.String(),
                        Subject = c.String(),
                        ChapterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chapters", t => t.ChapterId, cascadeDelete: true)
                .Index(t => t.ChapterId);
            
            CreateTable(
                "dbo.Quizs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SyllabusId = c.Guid(nullable: false),
                        Subject = c.String(),
                        Details = c.String(),
                        NoOfQuestion = c.Int(nullable: false),
                        Class = c.String(),
                        School = c.String(),
                        On = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SyllabusDates", t => t.SyllabusId, cascadeDelete: true)
                .Index(t => t.SyllabusId);
            
            CreateTable(
                "dbo.SyllabusDates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        Class = c.Int(nullable: false),
                        School = c.Int(nullable: false),
                        From = c.DateTime(nullable: false),
                        To = c.DateTime(nullable: false),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SchoolClassSubjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SchoolId = c.Int(nullable: false),
                        ClassId = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassId, cascadeDelete: true)
                .ForeignKey("dbo.Schools", t => t.SchoolId, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.SchoolId)
                .Index(t => t.ClassId)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SubjectName = c.String(),
                        DisplayName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionSetId = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionSets", t => t.QuestionSetId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.QuestionSetId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ApplicationSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Weblogo = c.String(),
                        Adminlogo = c.String(),
                        Invoicelogo = c.String(),
                        InvoicePreFix = c.String(),
                        BillingAddress = c.String(),
                        CompanyName = c.String(),
                        WebSiteUrl = c.String(),
                        SaleEmail = c.String(),
                        SupportEmail = c.String(),
                        BillingEmail = c.String(),
                        SupportMobile = c.String(),
                        SaleMobile = c.String(),
                        BillingMobile = c.String(),
                        GeneralAddress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_BUILD",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BOARD = c.String(),
                        BOOTLOADER = c.String(),
                        BRAND = c.String(),
                        CPUABI = c.String(),
                        CPUABI2 = c.String(),
                        DEVICE = c.String(),
                        DISPLAY = c.String(),
                        FINGERPRINT = c.String(),
                        HARDWARE = c.String(),
                        HOST = c.String(),
                        BUILDID = c.String(),
                        ISDEBUGGABLE = c.Boolean(nullable: false),
                        MANUFACTURER = c.String(),
                        MODEL = c.String(),
                        PRODUCT = c.String(),
                        RADIO = c.String(),
                        SERIAL = c.String(),
                        SUPPORTED32BITABIS = c.String(),
                        SUPPORTED64BITABIS = c.String(),
                        SUPPORTEDABIS = c.String(),
                        TAGS = c.String(),
                        TIME = c.String(),
                        TYPE = c.String(),
                        UNKNOWN = c.String(),
                        USER = c.String(),
                        VERSIONID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ARCA_VERSION", t => t.VERSIONID, cascadeDelete: true)
                .Index(t => t.VERSIONID);
            
            CreateTable(
                "dbo.ARCA_VERSION",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ACTIVECODENAMES = c.String(),
                        BASEOS = c.String(),
                        CODENAME = c.String(),
                        INCREMENTAL = c.String(),
                        RELEASE = c.Double(nullable: false),
                        RESOURCESSDKINT = c.Int(nullable: false),
                        SDK = c.Int(nullable: false),
                        SDKINT = c.Int(nullable: false),
                        SECURITYPATCH = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_CRASHCONFIGURATION",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompatScreenHeightDp = c.Int(nullable: false),
                        CompatScreenWidthDp = c.Int(nullable: false),
                        CompatSmallestScreenWidthDp = c.Int(nullable: false),
                        DensityDpi = c.Int(nullable: false),
                        FontScale = c.String(),
                        HardKeyboardHidden = c.String(),
                        Keyboard = c.String(),
                        KeyboardHidden = c.String(),
                        Locale = c.String(),
                        Mcc = c.Int(nullable: false),
                        Mnc = c.Int(nullable: false),
                        Navigation = c.String(),
                        NavigationHidden = c.String(),
                        Orientation = c.String(),
                        ScreenHeightDp = c.Int(nullable: false),
                        ScreenLayout = c.String(),
                        ScreenWidthDp = c.Int(nullable: false),
                        Seq = c.Int(nullable: false),
                        SmallestScreenWidthDp = c.Int(nullable: false),
                        ThemeConfig = c.String(),
                        Touchscreen = c.String(),
                        UiMode = c.String(),
                        UserSetLocale = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_CUSTOMDATA",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_DEVICEFEATURES",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AndroidHardwareSensorProximity = c.Boolean(nullable: false),
                        AndroidHardwareSensorAccelerometer = c.Boolean(nullable: false),
                        AndroidHardwareFaketouch = c.Boolean(nullable: false),
                        AndroidHardwareUsbAccessory = c.Boolean(nullable: false),
                        ComFihtdcNavigationFeatureStyleInfocus = c.Boolean(nullable: false),
                        AndroidSoftwareBackup = c.Boolean(nullable: false),
                        AndroidHardwareTouchscreen = c.Boolean(nullable: false),
                        AndroidHardwareTouchscreenMultitouch = c.Boolean(nullable: false),
                        ComFihtdcEncryptedOta = c.Boolean(nullable: false),
                        AndroidSoftwarePrint = c.Boolean(nullable: false),
                        ComFihtdcInlifeuiSettingsStyleTab = c.Boolean(nullable: false),
                        AndroidSoftwareVoiceRecognizers = c.Boolean(nullable: false),
                        AndroidHardwareBluetooth = c.Boolean(nullable: false),
                        AndroidHardwareCameraAutofocus = c.Boolean(nullable: false),
                        AndroidHardwareTelephonyGsm = c.Boolean(nullable: false),
                        ComFihtdcInlifeuiSettingsStyleDashboard = c.Boolean(nullable: false),
                        AndroidHardwareAudioOutput = c.Boolean(nullable: false),
                        AndroidHardwareCameraFlash = c.Boolean(nullable: false),
                        AndroidHardwareCameraFront = c.Boolean(nullable: false),
                        AndroidHardwareScreenPortrait = c.Boolean(nullable: false),
                        AndroidSoftwareHomeScreen = c.Boolean(nullable: false),
                        AndroidHardwareMicrophone = c.Boolean(nullable: false),
                        AndroidHardwareBluetoothLe = c.Boolean(nullable: false),
                        AndroidHardwareTouchscreenMultitouchJazzhand = c.Boolean(nullable: false),
                        AndroidSoftwareAppWidgets = c.Boolean(nullable: false),
                        AndroidSoftwareInputMethods = c.Boolean(nullable: false),
                        AndroidHardwareSensorLight = c.Boolean(nullable: false),
                        AndroidSoftwareDeviceAdmin = c.Boolean(nullable: false),
                        AndroidHardwareCamera = c.Boolean(nullable: false),
                        AndroidHardwareScreenLandscape = c.Boolean(nullable: false),
                        ComFihtdcInlifeuiFeature = c.Boolean(nullable: false),
                        AndroidSoftwareManagedUsers = c.Boolean(nullable: false),
                        AndroidSoftwareWebview = c.Boolean(nullable: false),
                        AndroidHardwareCameraAny = c.Boolean(nullable: false),
                        AndroidSoftwareConnectionservice = c.Boolean(nullable: false),
                        AndroidHardwareTouchscreenMultitouchDistinct = c.Boolean(nullable: false),
                        AndroidHardwareLocationNetwork = c.Boolean(nullable: false),
                        AndroidSoftwareLiveWallpaper = c.Boolean(nullable: false),
                        AndroidHardwareLocationGps = c.Boolean(nullable: false),
                        AndroidHardwareWifi = c.Boolean(nullable: false),
                        AndroidHardwareLocation = c.Boolean(nullable: false),
                        ComFihtdcInlifeuiSettingsSearch = c.Boolean(nullable: false),
                        ComFihtdcNavigationFeature = c.Boolean(nullable: false),
                        AndroidHardwareTelephony = c.Boolean(nullable: false),
                        GlEsVersion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_ENVIRONMENT",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GetDataDirectory = c.String(),
                        GetDownloadCacheDirectory = c.String(),
                        GetEmulatedStorageObbSource = c.String(),
                        GetExternalStorageDirectory = c.String(),
                        GetExternalStoragePath = c.String(),
                        GetExternalStoragePathState = c.String(),
                        GetExternalStorageState = c.String(),
                        GetInternalStoragePath = c.String(),
                        GetInternalStoragePathState = c.String(),
                        GetLegacyExternalStorageDirectory = c.String(),
                        GetLegacyExternalStorageObbDirectory = c.String(),
                        GetMediaStorageDirectory = c.String(),
                        GetOemDirectory = c.String(),
                        GetRootDirectory = c.String(),
                        GetSecondaryStorageDirectory = c.String(),
                        GetSecureDataDirectory = c.String(),
                        GetStorageType = c.Int(nullable: false),
                        GetSystemSecureDirectory = c.String(),
                        GetVendorDirectory = c.String(),
                        IsEncryptedFilesystemEnabled = c.Boolean(nullable: false),
                        IsExternalStorageEmulated = c.Boolean(nullable: false),
                        IsExternalStorageRemovable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ARCA_ErrorReportingMaster",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ReportStatus = c.Int(),
                        ReportPrority = c.Int(),
                        OrigilanLogFile = c.String(),
                        REPORTID = c.String(),
                        APPVERSIONCODE = c.Int(nullable: false),
                        APPVERSIONNAME = c.String(),
                        PACKAGENAME = c.String(),
                        FILEPATH = c.String(),
                        PHONEMODEL = c.String(),
                        ANDROIDVERSION = c.Double(nullable: false),
                        BRAND = c.String(),
                        PRODUCT = c.String(),
                        TOTALMEMSIZE = c.Long(nullable: false),
                        AVAILABLEMEMSIZE = c.String(),
                        STACKTRACE = c.String(),
                        USERCOMMENT = c.String(),
                        USERAPPSTARTDATE = c.DateTime(nullable: false),
                        USERCRASHDATE = c.DateTime(nullable: false),
                        DUMPSYSMEMINFO = c.String(),
                        LOGCAT = c.String(),
                        INSTALLATIONID = c.String(),
                        USEREMAIL = c.String(),
                        ArcaCustomdataId = c.Int(nullable: false),
                        ArcaInitialconfigurationId = c.Int(nullable: false),
                        ArcaCrashconfigurationId = c.Int(nullable: false),
                        ArcaDevicefeaturesId = c.Int(nullable: false),
                        ArcaEnvironmentId = c.Int(nullable: false),
                        ArcaBuildId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ARCA_BUILD", t => t.ArcaBuildId, cascadeDelete: true)
                .ForeignKey("dbo.ARCA_CRASHCONFIGURATION", t => t.ArcaCrashconfigurationId, cascadeDelete: true)
                .ForeignKey("dbo.ARCA_CUSTOMDATA", t => t.ArcaCustomdataId, cascadeDelete: true)
                .ForeignKey("dbo.ARCA_DEVICEFEATURES", t => t.ArcaDevicefeaturesId, cascadeDelete: true)
                .ForeignKey("dbo.ARCA_ENVIRONMENT", t => t.ArcaEnvironmentId, cascadeDelete: true)
                .ForeignKey("dbo.ARCA_INITIALCONFIGURATION", t => t.ArcaInitialconfigurationId, cascadeDelete: true)
                .Index(t => t.ArcaCustomdataId)
                .Index(t => t.ArcaInitialconfigurationId)
                .Index(t => t.ArcaCrashconfigurationId)
                .Index(t => t.ArcaDevicefeaturesId)
                .Index(t => t.ArcaEnvironmentId)
                .Index(t => t.ArcaBuildId);
            
            CreateTable(
                "dbo.ARCA_INITIALCONFIGURATION",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompatScreenHeightDp = c.Int(nullable: false),
                        CompatScreenWidthDp = c.Int(nullable: false),
                        CompatSmallestScreenWidthDp = c.Int(nullable: false),
                        DensityDpi = c.Int(nullable: false),
                        FontScale = c.String(),
                        HardKeyboardHidden = c.String(),
                        Keyboard = c.String(),
                        KeyboardHidden = c.String(),
                        Locale = c.String(),
                        Mcc = c.Int(nullable: false),
                        Mnc = c.Int(nullable: false),
                        Navigation = c.String(),
                        NavigationHidden = c.String(),
                        Orientation = c.String(),
                        ScreenHeightDp = c.Int(nullable: false),
                        ScreenLayout = c.String(),
                        ScreenWidthDp = c.Int(nullable: false),
                        Seq = c.Int(nullable: false),
                        SmallestScreenWidthDp = c.Int(nullable: false),
                        ThemeConfig = c.String(),
                        Touchscreen = c.String(),
                        UiMode = c.String(),
                        UserSetLocale = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ArticleComments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        ArticleId = c.Guid(nullable: false),
                        Comment = c.String(),
                        On = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articles", t => t.ArticleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ArticleId);
            
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Title = c.String(),
                        Solution = c.String(),
                        ImageUrl = c.String(),
                        Approved = c.Boolean(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        On = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                        ClassesId = c.Int(),
                        SubjectId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Classes", t => t.ClassesId)
                .ForeignKey("dbo.Subjects", t => t.SubjectId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ClassesId)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.FavArticles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        ArticleId = c.Guid(nullable: false),
                        On = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articles", t => t.ArticleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ArticleId);
            
            CreateTable(
                "dbo.LikeArticles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        ArticleId = c.Guid(nullable: false),
                        On = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articles", t => t.ArticleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ArticleId);
            
            CreateTable(
                "dbo.CartItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CartId = c.Guid(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Carts", t => t.CartId, cascadeDelete: true)
                .Index(t => t.CartId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Image = c.String(),
                        Video = c.String(),
                        ProductDisc = c.String(),
                        ShortDisc = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Active = c.Boolean(nullable: false),
                        AddedOn = c.DateTime(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductCategories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        ShortDescription = c.String(),
                        Image = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Carts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        CheckOut = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ModuleControllers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Controller = c.String(),
                        Action = c.String(),
                        ModuleId = c.String(),
                        Module_ModuleName = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ModuleDatas", t => t.Module_ModuleName)
                .Index(t => t.Module_ModuleName);
            
            CreateTable(
                "dbo.ModuleDatas",
                c => new
                    {
                        ModuleName = c.String(nullable: false, maxLength: 128),
                        Free = c.Boolean(nullable: false),
                        AuthRequired = c.Boolean(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ModuleName);
            
            CreateTable(
                "dbo.FeedComments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FeedId = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        Comment = c.String(),
                        On = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Feeds", t => t.FeedId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.FeedId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Feeds",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        Pinned = c.Boolean(nullable: false),
                        PostId = c.Long(nullable: false),
                        GroupId = c.Int(nullable: false),
                        OnDateTime = c.DateTime(nullable: false),
                        FeaturedOnPastFutre = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.GroupId, cascadeDelete: true)
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .Index(t => t.PostId)
                .Index(t => t.GroupId);
            
            CreateTable(
                "dbo.FeedLikes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FeedId = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        On = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Feeds", t => t.FeedId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.FeedId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Content = c.String(nullable: false),
                        Pinned = c.Boolean(nullable: false),
                        UserId = c.String(),
                        Tags = c.String(),
                        Type = c.Int(nullable: false),
                        OnDateTIme = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PostImages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ImageUrl = c.String(),
                        ImageTitle = c.String(),
                        PostId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .Index(t => t.PostId);
            
            CreateTable(
                "dbo.PostVideos",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        VideoUrl = c.String(),
                        PostId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .Index(t => t.PostId);
            
            CreateTable(
                "dbo.FeedStars",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        On = c.DateTime(nullable: false),
                        FeedId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Feeds", t => t.FeedId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.FeedId);
            
            CreateTable(
                "dbo.FeedReplies",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CommentId = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        Reply = c.String(),
                        On = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FeedComments", t => t.CommentId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.CommentId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Goals",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(),
                        Title = c.String(),
                        Points = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        GoalLevel = c.Int(nullable: false),
                        Days = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Markings",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuizId = c.Guid(nullable: false),
                        UserId = c.String(),
                        QuestionId = c.Guid(nullable: false),
                        On = c.DateTime(nullable: false),
                        Points = c.Int(nullable: false),
                        Score_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .ForeignKey("dbo.Quizs", t => t.QuizId, cascadeDelete: true)
                .ForeignKey("dbo.Scores", t => t.Score_Id)
                .Index(t => t.QuizId)
                .Index(t => t.QuestionId)
                .Index(t => t.Score_Id);
            
            CreateTable(
                "dbo.NewsGlobals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        ImageUrl = c.String(),
                        VideoUrl = c.String(),
                        Content = c.String(),
                        On = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        User = c.String(),
                        Group = c.String(),
                        ImageUrl = c.String(),
                        Link = c.String(),
                        PostId = c.String(),
                        Title = c.String(),
                        Message = c.String(),
                        Date = c.DateTime(nullable: false),
                        Read = c.Boolean(nullable: false),
                        Module = c.Int(nullable: false),
                        Size = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Olympiads",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Level = c.Int(nullable: false),
                        Subject = c.String(),
                        Class = c.Int(nullable: false),
                        Content = c.String(),
                        Title = c.String(),
                        AddedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ProductId = c.Int(nullable: false),
                        Project_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.Project_Id)
                .Index(t => t.Project_Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        Remarks = c.String(),
                        Order = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FileUrl = c.String(),
                        Active = c.Boolean(nullable: false),
                        AddedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProjectCategories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.ProjectCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        HasParent = c.Boolean(nullable: false),
                        Parent_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProjectCategories", t => t.Parent_Id)
                .Index(t => t.Parent_Id);
            
            CreateTable(
                "dbo.PAAnswers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        OptionId = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        ReasoningId = c.Long(nullable: false),
                        AttempId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PAAttemps", t => t.AttempId, cascadeDelete: true)
                .ForeignKey("dbo.PAOptions", t => t.OptionId, cascadeDelete: true)
                .ForeignKey("dbo.PAQuestions", t => t.QuestionId, cascadeDelete: true)
                .ForeignKey("dbo.PARoots", t => t.ReasoningId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.OptionId)
                .Index(t => t.QuestionId)
                .Index(t => t.ReasoningId)
                .Index(t => t.AttempId);
            
            CreateTable(
                "dbo.PAAttemps",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        ReasoningId = c.Long(nullable: false),
                        On = c.DateTime(nullable: false),
                        RightAns = c.Int(nullable: false),
                        WrongAns = c.Int(nullable: false),
                        SkippedAns = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PARoots", t => t.ReasoningId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ReasoningId);
            
            CreateTable(
                "dbo.PARoots",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Content = c.String(nullable: false),
                        ShortDiscription = c.String(nullable: false),
                        MinQuestionToPass = c.Int(nullable: false),
                        TimeInMinutes = c.Int(nullable: false),
                        Tips = c.String(),
                        AddedOn = c.DateTime(nullable: false),
                        TypeOfAssement = c.Int(nullable: false),
                        DisplayAfterDate = c.DateTime(nullable: false),
                        TestBrief = c.String(),
                        Order = c.Int(nullable: false),
                        SchoolId = c.Int(nullable: false),
                        ClassId = c.Int(nullable: false),
                        SubjectId = c.Int(),
                        Level = c.String(),
                        Color = c.String(),
                        TitleColor = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassId, cascadeDelete: true)
                .ForeignKey("dbo.Schools", t => t.SchoolId, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.SubjectId)
                .Index(t => t.SchoolId)
                .Index(t => t.ClassId)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.PAQuestions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Query = c.String(nullable: false),
                        SetId = c.Long(nullable: false),
                        ShortOrder = c.Int(nullable: false),
                        Module = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PARoots", t => t.SetId, cascadeDelete: true)
                .Index(t => t.SetId);
            
            CreateTable(
                "dbo.PAOptions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        Answer = c.String(),
                        Correct = c.Boolean(nullable: false),
                        ShortOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PAQuestions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.Parents",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FullName = c.String(),
                        GurdianType = c.Int(nullable: false),
                        Street = c.String(),
                        Area_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Areas", t => t.Area_Id)
                .Index(t => t.Area_Id);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        FullName = c.String(),
                        BoardName = c.String(),
                        ClassId = c.Int(nullable: false),
                        SchoolId = c.Int(),
                        IsBrandAmbasdor = c.Boolean(nullable: false),
                        Gender = c.Int(nullable: false),
                        ParentId = c.Int(),
                        Parent_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassId, cascadeDelete: true)
                .ForeignKey("dbo.Parents", t => t.Parent_Id)
                .ForeignKey("dbo.Schools", t => t.SchoolId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ClassId)
                .Index(t => t.SchoolId)
                .Index(t => t.Parent_Id);
            
            CreateTable(
                "dbo.PointsHistories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        Points = c.Int(nullable: false),
                        On = c.DateTime(nullable: false),
                        RelatedId = c.String(),
                        RelatedType = c.String(),
                        ShortDescription = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ReasoningAnswers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        OptionId = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        ReasoningId = c.Long(nullable: false),
                        AttempId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ReasoningAttemps", t => t.AttempId, cascadeDelete: true)
                .ForeignKey("dbo.ReasoningOptions", t => t.OptionId, cascadeDelete: true)
                .ForeignKey("dbo.ReasoningQuestions", t => t.QuestionId, cascadeDelete: true)
                .ForeignKey("dbo.ReasoningRoots", t => t.ReasoningId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.OptionId)
                .Index(t => t.QuestionId)
                .Index(t => t.ReasoningId)
                .Index(t => t.AttempId);
            
            CreateTable(
                "dbo.ReasoningAttemps",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(),
                        ReasoningId = c.Long(nullable: false),
                        On = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReasoningOptions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        Answer = c.String(),
                        Correct = c.Boolean(nullable: false),
                        ShortOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ReasoningQuestions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.ReasoningQuestions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Query = c.String(nullable: false),
                        SetId = c.Long(),
                        ShortOrder = c.Int(nullable: false),
                        Module = c.String(),
                        Solution = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ReasoningRoots", t => t.SetId)
                .Index(t => t.SetId);
            
            CreateTable(
                "dbo.ReasoningRoots",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Class = c.String(),
                        Content = c.String(),
                        ShortDiscription = c.String(),
                        Title = c.String(),
                        MinQuestionToPass = c.Int(nullable: false),
                        TimeInMinutes = c.Int(nullable: false),
                        Tips = c.String(),
                        AddedOn = c.DateTime(nullable: false),
                        IsLessonOrExam = c.Int(nullable: false),
                        Days = c.Int(nullable: false),
                        PreviousPostId = c.Long(),
                        TestBrief = c.String(),
                        Order = c.Int(nullable: false),
                        Level = c.String(),
                        Color = c.String(),
                        TitleColor = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ReasoningRoots", t => t.PreviousPostId)
                .Index(t => t.PreviousPostId);
            
            CreateTable(
                "dbo.ReasoningTrackers",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RnmPostId = c.Long(nullable: false),
                        OnDateTime = c.DateTime(nullable: false),
                        Level = c.String(),
                        TestPassed = c.Boolean(nullable: false),
                        Accepted = c.String(),
                    })
                .PrimaryKey(t => new { t.UserId, t.RnmPostId })
                .ForeignKey("dbo.ReasoningRoots", t => t.RnmPostId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RnmPostId);
            
            CreateTable(
                "dbo.ReportedAbuseds",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Source = c.Int(nullable: false),
                        Text = c.String(),
                        IdOfPost = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReportAbuseTitles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        EntryDate = c.DateTime(nullable: false),
                        Visible = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RoleModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClassId = c.Int(),
                        Title = c.String(nullable: false),
                        Content = c.String(),
                        ImageUrl = c.String(),
                        VideoUrl = c.String(),
                        Pinned = c.Boolean(nullable: false),
                        On = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassId)
                .Index(t => t.ClassId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Scores",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(),
                        QuizId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SideBarNavs",
                c => new
                    {
                        Title = c.String(nullable: false, maxLength: 200),
                        Order = c.Int(nullable: false),
                        SubMenu = c.String(nullable: false),
                        ModuleDataId = c.String(),
                        ModuleData_ModuleName = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Title)
                .ForeignKey("dbo.ModuleDatas", t => t.ModuleData_ModuleName)
                .Index(t => t.ModuleData_ModuleName);
            
            CreateTable(
                "dbo.Sliders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SliderName = c.String(),
                        Status = c.Boolean(nullable: false),
                        UpdateOn = c.DateTime(nullable: false),
                        IsAlbum = c.Boolean(nullable: false),
                        IsSytemDefine = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SliderSlice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Link = c.String(),
                        Target = c.String(),
                        Image = c.String(),
                        slidingOrder = c.Int(nullable: false),
                        UpdateOn = c.DateTime(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        IsHead = c.Boolean(nullable: false),
                        SliderID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sliders", t => t.SliderID, cascadeDelete: true)
                .Index(t => t.SliderID);
            
            CreateTable(
                "dbo.SubcriptionPackages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(),
                        Duration = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Subcriptions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(),
                        Expiry = c.DateTime(nullable: false),
                        SubcriptionPackageId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubcriptionPackages", t => t.SubcriptionPackageId, cascadeDelete: true)
                .Index(t => t.SubcriptionPackageId);
            
            CreateTable(
                "dbo.Summaries",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Summarizer = c.String(),
                        Visualizer = c.String(),
                        Month = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        School = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Amount = c.Double(nullable: false),
                        Description = c.String(),
                        CartId = c.Guid(),
                        SubcriptionId = c.Guid(),
                        TransactionResult = c.String(),
                        On = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Carts", t => t.CartId)
                .ForeignKey("dbo.Subcriptions", t => t.SubcriptionId)
                .Index(t => t.CartId)
                .Index(t => t.SubcriptionId);
            
            CreateTable(
                "dbo.Trendings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeOfTrend = c.Int(nullable: false),
                        Trend = c.String(),
                        Display = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GroupClasses",
                c => new
                    {
                        Group_Id = c.Int(nullable: false),
                        Classes_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Group_Id, t.Classes_Id })
                .ForeignKey("dbo.Groups", t => t.Group_Id, cascadeDelete: true)
                .ForeignKey("dbo.Classes", t => t.Classes_Id, cascadeDelete: true)
                .Index(t => t.Group_Id)
                .Index(t => t.Classes_Id);
            
            CreateTable(
                "dbo.ChapterBooks",
                c => new
                    {
                        Chapter_Id = c.Int(nullable: false),
                        Books_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Chapter_Id, t.Books_Id })
                .ForeignKey("dbo.Chapters", t => t.Chapter_Id, cascadeDelete: true)
                .ForeignKey("dbo.Books", t => t.Books_Id, cascadeDelete: true)
                .Index(t => t.Chapter_Id)
                .Index(t => t.Books_Id);
            
            CreateTable(
                "dbo.QuestionsBooks",
                c => new
                    {
                        Questions_Id = c.Guid(nullable: false),
                        Books_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Questions_Id, t.Books_Id })
                .ForeignKey("dbo.Questions", t => t.Questions_Id, cascadeDelete: true)
                .ForeignKey("dbo.Books", t => t.Books_Id, cascadeDelete: true)
                .Index(t => t.Questions_Id)
                .Index(t => t.Books_Id);
            
            CreateTable(
                "dbo.QuizQuestions",
                c => new
                    {
                        Quiz_Id = c.Guid(nullable: false),
                        Questions_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Quiz_Id, t.Questions_Id })
                .ForeignKey("dbo.Quizs", t => t.Quiz_Id, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.Questions_Id, cascadeDelete: true)
                .Index(t => t.Quiz_Id)
                .Index(t => t.Questions_Id);
            
            CreateTable(
                "dbo.SchoolClassSubjectBooks",
                c => new
                    {
                        SchoolClassSubject_Id = c.Int(nullable: false),
                        Books_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SchoolClassSubject_Id, t.Books_Id })
                .ForeignKey("dbo.SchoolClassSubjects", t => t.SchoolClassSubject_Id, cascadeDelete: true)
                .ForeignKey("dbo.Books", t => t.Books_Id, cascadeDelete: true)
                .Index(t => t.SchoolClassSubject_Id)
                .Index(t => t.Books_Id);
            
            CreateTable(
                "dbo.BooksSchools",
                c => new
                    {
                        Books_Id = c.Int(nullable: false),
                        School_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Books_Id, t.School_Id })
                .ForeignKey("dbo.Books", t => t.Books_Id, cascadeDelete: true)
                .ForeignKey("dbo.Schools", t => t.School_Id, cascadeDelete: true)
                .Index(t => t.Books_Id)
                .Index(t => t.School_Id);
            
            CreateTable(
                "dbo.SchoolGroups",
                c => new
                    {
                        School_Id = c.Int(nullable: false),
                        Group_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.School_Id, t.Group_Id })
                .ForeignKey("dbo.Schools", t => t.School_Id, cascadeDelete: true)
                .ForeignKey("dbo.Groups", t => t.Group_Id, cascadeDelete: true)
                .Index(t => t.School_Id)
                .Index(t => t.Group_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "SubcriptionId", "dbo.Subcriptions");
            DropForeignKey("dbo.Transactions", "CartId", "dbo.Carts");
            DropForeignKey("dbo.Subcriptions", "SubcriptionPackageId", "dbo.SubcriptionPackages");
            DropForeignKey("dbo.SliderSlice", "SliderID", "dbo.Sliders");
            DropForeignKey("dbo.SideBarNavs", "ModuleData_ModuleName", "dbo.ModuleDatas");
            DropForeignKey("dbo.Markings", "Score_Id", "dbo.Scores");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.RoleModels", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.ReasoningTrackers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ReasoningTrackers", "RnmPostId", "dbo.ReasoningRoots");
            DropForeignKey("dbo.ReasoningAnswers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ReasoningAnswers", "ReasoningId", "dbo.ReasoningRoots");
            DropForeignKey("dbo.ReasoningAnswers", "QuestionId", "dbo.ReasoningQuestions");
            DropForeignKey("dbo.ReasoningAnswers", "OptionId", "dbo.ReasoningOptions");
            DropForeignKey("dbo.ReasoningQuestions", "SetId", "dbo.ReasoningRoots");
            DropForeignKey("dbo.ReasoningRoots", "PreviousPostId", "dbo.ReasoningRoots");
            DropForeignKey("dbo.ReasoningOptions", "QuestionId", "dbo.ReasoningQuestions");
            DropForeignKey("dbo.ReasoningAnswers", "AttempId", "dbo.ReasoningAttemps");
            DropForeignKey("dbo.PointsHistories", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Students", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Students", "SchoolId", "dbo.Schools");
            DropForeignKey("dbo.Students", "Parent_Id", "dbo.Parents");
            DropForeignKey("dbo.Students", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.Parents", "Area_Id", "dbo.Areas");
            DropForeignKey("dbo.PAAnswers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PAAnswers", "ReasoningId", "dbo.PARoots");
            DropForeignKey("dbo.PAAnswers", "QuestionId", "dbo.PAQuestions");
            DropForeignKey("dbo.PAAnswers", "OptionId", "dbo.PAOptions");
            DropForeignKey("dbo.PAAttemps", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PAAttemps", "ReasoningId", "dbo.PARoots");
            DropForeignKey("dbo.PARoots", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.PARoots", "SchoolId", "dbo.Schools");
            DropForeignKey("dbo.PAQuestions", "SetId", "dbo.PARoots");
            DropForeignKey("dbo.PAOptions", "QuestionId", "dbo.PAQuestions");
            DropForeignKey("dbo.PARoots", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.PAAnswers", "AttempId", "dbo.PAAttemps");
            DropForeignKey("dbo.Orders", "Project_Id", "dbo.Projects");
            DropForeignKey("dbo.Projects", "CategoryId", "dbo.ProjectCategories");
            DropForeignKey("dbo.ProjectCategories", "Parent_Id", "dbo.ProjectCategories");
            DropForeignKey("dbo.Markings", "QuizId", "dbo.Quizs");
            DropForeignKey("dbo.Markings", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.FeedComments", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FeedReplies", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FeedReplies", "CommentId", "dbo.FeedComments");
            DropForeignKey("dbo.FeedStars", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FeedStars", "FeedId", "dbo.Feeds");
            DropForeignKey("dbo.Feeds", "PostId", "dbo.Posts");
            DropForeignKey("dbo.PostVideos", "PostId", "dbo.Posts");
            DropForeignKey("dbo.PostImages", "PostId", "dbo.Posts");
            DropForeignKey("dbo.FeedLikes", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FeedLikes", "FeedId", "dbo.Feeds");
            DropForeignKey("dbo.Feeds", "GroupId", "dbo.Groups");
            DropForeignKey("dbo.FeedComments", "FeedId", "dbo.Feeds");
            DropForeignKey("dbo.ModuleControllers", "Module_ModuleName", "dbo.ModuleDatas");
            DropForeignKey("dbo.Carts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CartItems", "CartId", "dbo.Carts");
            DropForeignKey("dbo.CartItems", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Products", "CategoryId", "dbo.ProductCategories");
            DropForeignKey("dbo.ArticleComments", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Articles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Articles", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.LikeArticles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.LikeArticles", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.FavArticles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FavArticles", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.ArticleComments", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.Articles", "ClassesId", "dbo.Classes");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaInitialconfigurationId", "dbo.ARCA_INITIALCONFIGURATION");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaEnvironmentId", "dbo.ARCA_ENVIRONMENT");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaDevicefeaturesId", "dbo.ARCA_DEVICEFEATURES");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaCustomdataId", "dbo.ARCA_CUSTOMDATA");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaCrashconfigurationId", "dbo.ARCA_CRASHCONFIGURATION");
            DropForeignKey("dbo.ARCA_ErrorReportingMaster", "ArcaBuildId", "dbo.ARCA_BUILD");
            DropForeignKey("dbo.ARCA_BUILD", "VERSIONID", "dbo.ARCA_VERSION");
            DropForeignKey("dbo.Reports", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Reports", "QuestionSetId", "dbo.QuestionSets");
            DropForeignKey("dbo.Answereds", "ReportId", "dbo.Reports");
            DropForeignKey("dbo.Answereds", "McqQuestionId", "dbo.McqQuestions");
            DropForeignKey("dbo.Answereds", "OptionsId", "dbo.Options");
            DropForeignKey("dbo.Answereds", new[] { "UserId", "QuestionSetId" }, "dbo.AnswerGivens");
            DropForeignKey("dbo.AnswerGivens", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "SchoolId", "dbo.Schools");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.SchoolGroups", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.SchoolGroups", "School_Id", "dbo.Schools");
            DropForeignKey("dbo.BooksSchools", "School_Id", "dbo.Schools");
            DropForeignKey("dbo.BooksSchools", "Books_Id", "dbo.Books");
            DropForeignKey("dbo.SchoolClassSubjects", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Books", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.SchoolClassSubjects", "SchoolId", "dbo.Schools");
            DropForeignKey("dbo.SchoolClassSubjects", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.SchoolClassSubjectBooks", "Books_Id", "dbo.Books");
            DropForeignKey("dbo.SchoolClassSubjectBooks", "SchoolClassSubject_Id", "dbo.SchoolClassSubjects");
            DropForeignKey("dbo.Books", "ClassesId", "dbo.Classes");
            DropForeignKey("dbo.Quizs", "SyllabusId", "dbo.SyllabusDates");
            DropForeignKey("dbo.QuizQuestions", "Questions_Id", "dbo.Questions");
            DropForeignKey("dbo.QuizQuestions", "Quiz_Id", "dbo.Quizs");
            DropForeignKey("dbo.Questions", "ChapterId", "dbo.Chapters");
            DropForeignKey("dbo.QuestionsBooks", "Books_Id", "dbo.Books");
            DropForeignKey("dbo.QuestionsBooks", "Questions_Id", "dbo.Questions");
            DropForeignKey("dbo.Notes", "ChapterId", "dbo.Chapters");
            DropForeignKey("dbo.Chapters", "ClassesId", "dbo.Classes");
            DropForeignKey("dbo.ChapterBooks", "Books_Id", "dbo.Books");
            DropForeignKey("dbo.ChapterBooks", "Chapter_Id", "dbo.Chapters");
            DropForeignKey("dbo.Schools", "AreaId", "dbo.Areas");
            DropForeignKey("dbo.States", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Cities", "StateId", "dbo.States");
            DropForeignKey("dbo.Areas", "CityId", "dbo.Cities");
            DropForeignKey("dbo.GroupClasses", "Classes_Id", "dbo.Classes");
            DropForeignKey("dbo.GroupClasses", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AnswerGivens", "QuestionSetId", "dbo.QuestionSets");
            DropForeignKey("dbo.McqQuestions", "SetId", "dbo.QuestionSets");
            DropForeignKey("dbo.Options", "McqQuestionId", "dbo.McqQuestions");
            DropIndex("dbo.SchoolGroups", new[] { "Group_Id" });
            DropIndex("dbo.SchoolGroups", new[] { "School_Id" });
            DropIndex("dbo.BooksSchools", new[] { "School_Id" });
            DropIndex("dbo.BooksSchools", new[] { "Books_Id" });
            DropIndex("dbo.SchoolClassSubjectBooks", new[] { "Books_Id" });
            DropIndex("dbo.SchoolClassSubjectBooks", new[] { "SchoolClassSubject_Id" });
            DropIndex("dbo.QuizQuestions", new[] { "Questions_Id" });
            DropIndex("dbo.QuizQuestions", new[] { "Quiz_Id" });
            DropIndex("dbo.QuestionsBooks", new[] { "Books_Id" });
            DropIndex("dbo.QuestionsBooks", new[] { "Questions_Id" });
            DropIndex("dbo.ChapterBooks", new[] { "Books_Id" });
            DropIndex("dbo.ChapterBooks", new[] { "Chapter_Id" });
            DropIndex("dbo.GroupClasses", new[] { "Classes_Id" });
            DropIndex("dbo.GroupClasses", new[] { "Group_Id" });
            DropIndex("dbo.Transactions", new[] { "SubcriptionId" });
            DropIndex("dbo.Transactions", new[] { "CartId" });
            DropIndex("dbo.Subcriptions", new[] { "SubcriptionPackageId" });
            DropIndex("dbo.SliderSlice", new[] { "SliderID" });
            DropIndex("dbo.SideBarNavs", new[] { "ModuleData_ModuleName" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.RoleModels", new[] { "ClassId" });
            DropIndex("dbo.ReasoningTrackers", new[] { "RnmPostId" });
            DropIndex("dbo.ReasoningTrackers", new[] { "UserId" });
            DropIndex("dbo.ReasoningRoots", new[] { "PreviousPostId" });
            DropIndex("dbo.ReasoningQuestions", new[] { "SetId" });
            DropIndex("dbo.ReasoningOptions", new[] { "QuestionId" });
            DropIndex("dbo.ReasoningAnswers", new[] { "AttempId" });
            DropIndex("dbo.ReasoningAnswers", new[] { "ReasoningId" });
            DropIndex("dbo.ReasoningAnswers", new[] { "QuestionId" });
            DropIndex("dbo.ReasoningAnswers", new[] { "OptionId" });
            DropIndex("dbo.ReasoningAnswers", new[] { "UserId" });
            DropIndex("dbo.PointsHistories", new[] { "UserId" });
            DropIndex("dbo.Students", new[] { "Parent_Id" });
            DropIndex("dbo.Students", new[] { "SchoolId" });
            DropIndex("dbo.Students", new[] { "ClassId" });
            DropIndex("dbo.Students", new[] { "UserId" });
            DropIndex("dbo.Parents", new[] { "Area_Id" });
            DropIndex("dbo.PAOptions", new[] { "QuestionId" });
            DropIndex("dbo.PAQuestions", new[] { "SetId" });
            DropIndex("dbo.PARoots", new[] { "SubjectId" });
            DropIndex("dbo.PARoots", new[] { "ClassId" });
            DropIndex("dbo.PARoots", new[] { "SchoolId" });
            DropIndex("dbo.PAAttemps", new[] { "ReasoningId" });
            DropIndex("dbo.PAAttemps", new[] { "UserId" });
            DropIndex("dbo.PAAnswers", new[] { "AttempId" });
            DropIndex("dbo.PAAnswers", new[] { "ReasoningId" });
            DropIndex("dbo.PAAnswers", new[] { "QuestionId" });
            DropIndex("dbo.PAAnswers", new[] { "OptionId" });
            DropIndex("dbo.PAAnswers", new[] { "UserId" });
            DropIndex("dbo.ProjectCategories", new[] { "Parent_Id" });
            DropIndex("dbo.Projects", new[] { "CategoryId" });
            DropIndex("dbo.Orders", new[] { "Project_Id" });
            DropIndex("dbo.Markings", new[] { "Score_Id" });
            DropIndex("dbo.Markings", new[] { "QuestionId" });
            DropIndex("dbo.Markings", new[] { "QuizId" });
            DropIndex("dbo.FeedReplies", new[] { "UserId" });
            DropIndex("dbo.FeedReplies", new[] { "CommentId" });
            DropIndex("dbo.FeedStars", new[] { "FeedId" });
            DropIndex("dbo.FeedStars", new[] { "UserId" });
            DropIndex("dbo.PostVideos", new[] { "PostId" });
            DropIndex("dbo.PostImages", new[] { "PostId" });
            DropIndex("dbo.FeedLikes", new[] { "UserId" });
            DropIndex("dbo.FeedLikes", new[] { "FeedId" });
            DropIndex("dbo.Feeds", new[] { "GroupId" });
            DropIndex("dbo.Feeds", new[] { "PostId" });
            DropIndex("dbo.FeedComments", new[] { "UserId" });
            DropIndex("dbo.FeedComments", new[] { "FeedId" });
            DropIndex("dbo.ModuleControllers", new[] { "Module_ModuleName" });
            DropIndex("dbo.Carts", new[] { "UserId" });
            DropIndex("dbo.Products", new[] { "CategoryId" });
            DropIndex("dbo.CartItems", new[] { "ProductId" });
            DropIndex("dbo.CartItems", new[] { "CartId" });
            DropIndex("dbo.LikeArticles", new[] { "ArticleId" });
            DropIndex("dbo.LikeArticles", new[] { "UserId" });
            DropIndex("dbo.FavArticles", new[] { "ArticleId" });
            DropIndex("dbo.FavArticles", new[] { "UserId" });
            DropIndex("dbo.Articles", new[] { "SubjectId" });
            DropIndex("dbo.Articles", new[] { "ClassesId" });
            DropIndex("dbo.Articles", new[] { "UserId" });
            DropIndex("dbo.ArticleComments", new[] { "ArticleId" });
            DropIndex("dbo.ArticleComments", new[] { "UserId" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaBuildId" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaEnvironmentId" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaDevicefeaturesId" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaCrashconfigurationId" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaInitialconfigurationId" });
            DropIndex("dbo.ARCA_ErrorReportingMaster", new[] { "ArcaCustomdataId" });
            DropIndex("dbo.ARCA_BUILD", new[] { "VERSIONID" });
            DropIndex("dbo.Reports", new[] { "UserId" });
            DropIndex("dbo.Reports", new[] { "QuestionSetId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.SchoolClassSubjects", new[] { "SubjectId" });
            DropIndex("dbo.SchoolClassSubjects", new[] { "ClassId" });
            DropIndex("dbo.SchoolClassSubjects", new[] { "SchoolId" });
            DropIndex("dbo.Quizs", new[] { "SyllabusId" });
            DropIndex("dbo.Questions", new[] { "ChapterId" });
            DropIndex("dbo.Notes", new[] { "ChapterId" });
            DropIndex("dbo.Chapters", new[] { "ClassesId" });
            DropIndex("dbo.Books", new[] { "SubjectId" });
            DropIndex("dbo.Books", new[] { "ClassesId" });
            DropIndex("dbo.States", new[] { "CountryId" });
            DropIndex("dbo.Cities", new[] { "StateId" });
            DropIndex("dbo.Areas", new[] { "CityId" });
            DropIndex("dbo.Schools", new[] { "AreaId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "SchoolId" });
            DropIndex("dbo.AspNetUsers", new[] { "ClassId" });
            DropIndex("dbo.Options", new[] { "McqQuestionId" });
            DropIndex("dbo.McqQuestions", new[] { "SetId" });
            DropIndex("dbo.AnswerGivens", new[] { "QuestionSetId" });
            DropIndex("dbo.AnswerGivens", new[] { "UserId" });
            DropIndex("dbo.Answereds", new[] { "ReportId" });
            DropIndex("dbo.Answereds", new[] { "OptionsId" });
            DropIndex("dbo.Answereds", new[] { "McqQuestionId" });
            DropIndex("dbo.Answereds", new[] { "UserId", "QuestionSetId" });
            DropTable("dbo.SchoolGroups");
            DropTable("dbo.BooksSchools");
            DropTable("dbo.SchoolClassSubjectBooks");
            DropTable("dbo.QuizQuestions");
            DropTable("dbo.QuestionsBooks");
            DropTable("dbo.ChapterBooks");
            DropTable("dbo.GroupClasses");
            DropTable("dbo.Trendings");
            DropTable("dbo.Transactions");
            DropTable("dbo.Summaries");
            DropTable("dbo.Subcriptions");
            DropTable("dbo.SubcriptionPackages");
            DropTable("dbo.SliderSlice");
            DropTable("dbo.Sliders");
            DropTable("dbo.SideBarNavs");
            DropTable("dbo.Scores");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.RoleModels");
            DropTable("dbo.ReportAbuseTitles");
            DropTable("dbo.ReportedAbuseds");
            DropTable("dbo.ReasoningTrackers");
            DropTable("dbo.ReasoningRoots");
            DropTable("dbo.ReasoningQuestions");
            DropTable("dbo.ReasoningOptions");
            DropTable("dbo.ReasoningAttemps");
            DropTable("dbo.ReasoningAnswers");
            DropTable("dbo.PointsHistories");
            DropTable("dbo.Students");
            DropTable("dbo.Parents");
            DropTable("dbo.PAOptions");
            DropTable("dbo.PAQuestions");
            DropTable("dbo.PARoots");
            DropTable("dbo.PAAttemps");
            DropTable("dbo.PAAnswers");
            DropTable("dbo.ProjectCategories");
            DropTable("dbo.Projects");
            DropTable("dbo.Orders");
            DropTable("dbo.Olympiads");
            DropTable("dbo.Notifications");
            DropTable("dbo.NewsGlobals");
            DropTable("dbo.Markings");
            DropTable("dbo.Goals");
            DropTable("dbo.FeedReplies");
            DropTable("dbo.FeedStars");
            DropTable("dbo.PostVideos");
            DropTable("dbo.PostImages");
            DropTable("dbo.Posts");
            DropTable("dbo.FeedLikes");
            DropTable("dbo.Feeds");
            DropTable("dbo.FeedComments");
            DropTable("dbo.ModuleDatas");
            DropTable("dbo.ModuleControllers");
            DropTable("dbo.Carts");
            DropTable("dbo.ProductCategories");
            DropTable("dbo.Products");
            DropTable("dbo.CartItems");
            DropTable("dbo.LikeArticles");
            DropTable("dbo.FavArticles");
            DropTable("dbo.Articles");
            DropTable("dbo.ArticleComments");
            DropTable("dbo.ARCA_INITIALCONFIGURATION");
            DropTable("dbo.ARCA_ErrorReportingMaster");
            DropTable("dbo.ARCA_ENVIRONMENT");
            DropTable("dbo.ARCA_DEVICEFEATURES");
            DropTable("dbo.ARCA_CUSTOMDATA");
            DropTable("dbo.ARCA_CRASHCONFIGURATION");
            DropTable("dbo.ARCA_VERSION");
            DropTable("dbo.ARCA_BUILD");
            DropTable("dbo.ApplicationSettings");
            DropTable("dbo.Reports");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.Subjects");
            DropTable("dbo.SchoolClassSubjects");
            DropTable("dbo.SyllabusDates");
            DropTable("dbo.Quizs");
            DropTable("dbo.Questions");
            DropTable("dbo.Notes");
            DropTable("dbo.Chapters");
            DropTable("dbo.Books");
            DropTable("dbo.Countries");
            DropTable("dbo.States");
            DropTable("dbo.Cities");
            DropTable("dbo.Areas");
            DropTable("dbo.Schools");
            DropTable("dbo.Groups");
            DropTable("dbo.Classes");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Options");
            DropTable("dbo.McqQuestions");
            DropTable("dbo.QuestionSets");
            DropTable("dbo.AnswerGivens");
            DropTable("dbo.Answereds");
            DropTable("dbo.ActivityLoggers");
            DropTable("dbo.Achievements");
        }
    }
}
