namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class solutionAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReasoningQuestions", "Solution", c => c.String());
            AddColumn("dbo.ReasoningTrackers", "Accepted", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReasoningTrackers", "Accepted");
            DropColumn("dbo.ReasoningQuestions", "Solution");
        }
    }
}
