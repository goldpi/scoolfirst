namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class attempsForPA : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PAAttemps", "RightAns", c => c.Int(nullable: false));
            AddColumn("dbo.PAAttemps", "WrongAns", c => c.Int(nullable: false));
            AddColumn("dbo.PAAttemps", "SkippedAns", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PAAttemps", "SkippedAns");
            DropColumn("dbo.PAAttemps", "WrongAns");
            DropColumn("dbo.PAAttemps", "RightAns");
        }
    }
}
