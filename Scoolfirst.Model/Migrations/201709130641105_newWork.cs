namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newWork : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PAAttemps", "RightAns", c => c.Int(nullable: false));
            AddColumn("dbo.PAAttemps", "WrongAns", c => c.Int(nullable: false));
            AddColumn("dbo.PAAttemps", "SkippedAns", c => c.Int(nullable: false));
            AddColumn("dbo.ReasoningQuestions", "Solution", c => c.String());
            AddColumn("dbo.ReasoningTrackers", "TestPassed", c => c.Boolean(nullable: false));
            AddColumn("dbo.ReasoningTrackers", "Accepted", c => c.String());
            AlterColumn("dbo.PAAttemps", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.PAAttemps", "UserId");
            CreateIndex("dbo.PAAttemps", "ReasoningId");
            AddForeignKey("dbo.PAAttemps", "ReasoningId", "dbo.PARoots", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PAAttemps", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PAAttemps", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PAAttemps", "ReasoningId", "dbo.PARoots");
            DropIndex("dbo.PAAttemps", new[] { "ReasoningId" });
            DropIndex("dbo.PAAttemps", new[] { "UserId" });
            AlterColumn("dbo.PAAttemps", "UserId", c => c.String());
            DropColumn("dbo.ReasoningTrackers", "Accepted");
            DropColumn("dbo.ReasoningTrackers", "TestPassed");
            DropColumn("dbo.ReasoningQuestions", "Solution");
            DropColumn("dbo.PAAttemps", "SkippedAns");
            DropColumn("dbo.PAAttemps", "WrongAns");
            DropColumn("dbo.PAAttemps", "RightAns");
        }
    }
}
