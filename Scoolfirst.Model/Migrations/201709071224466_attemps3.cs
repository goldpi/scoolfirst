namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class attemps3 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.PAAttemps", "ReasoningId");
            AddForeignKey("dbo.PAAttemps", "ReasoningId", "dbo.PARoots", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PAAttemps", "ReasoningId", "dbo.PARoots");
            DropIndex("dbo.PAAttemps", new[] { "ReasoningId" });
        }
    }
}
