namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class attemps2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PAAttemps", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.PAAttemps", "UserId");
            AddForeignKey("dbo.PAAttemps", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PAAttemps", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.PAAttemps", new[] { "UserId" });
            AlterColumn("dbo.PAAttemps", "UserId", c => c.String());
        }
    }
}
