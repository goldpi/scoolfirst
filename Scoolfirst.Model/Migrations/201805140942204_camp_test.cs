namespace Scoolfirst.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class camp_test : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CampAnswers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        OptionId = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        CampId = c.Long(nullable: false),
                        AttemptId = c.Guid(nullable: false),
                        Camp_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CampAttempts", t => t.AttemptId, cascadeDelete: false)
                .ForeignKey("dbo.CampPosts", t => t.Camp_Id)
                .ForeignKey("dbo.CampOptions", t => t.OptionId, cascadeDelete: false)
                .ForeignKey("dbo.CampQuestions", t => t.QuestionId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.OptionId)
                .Index(t => t.QuestionId)
                .Index(t => t.AttemptId)
                .Index(t => t.Camp_Id);
            
            CreateTable(
                "dbo.CampAttempts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        CampPostId = c.Long(nullable: false),
                        On = c.DateTime(nullable: false),
                        RightAns = c.Int(nullable: false),
                        WrongAns = c.Int(nullable: false),
                        SkippedAns = c.Int(nullable: false),
                        PointsEarned = c.Int(nullable: false),
                        BoosterUsed = c.Int(nullable: false),
                        CampPost_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CampPosts", t => t.CampPost_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.CampPost_Id);
            
            CreateTable(
                "dbo.CampQuestions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Query = c.String(nullable: false),
                        Solution = c.String(),
                        ShowSolution = c.Boolean(nullable: false),
                        SetId = c.Long(nullable: false),
                        ShortOrder = c.Int(nullable: false),
                        Set_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CampPosts", t => t.Set_Id)
                .Index(t => t.Set_Id);
            
            CreateTable(
                "dbo.CampOptions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        Answer = c.String(),
                        Correct = c.Boolean(nullable: false),
                        ShortOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CampQuestions", t => t.QuestionId, cascadeDelete: false)
                .Index(t => t.QuestionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CampAnswers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CampAnswers", "QuestionId", "dbo.CampQuestions");
            DropForeignKey("dbo.CampAnswers", "OptionId", "dbo.CampOptions");
            DropForeignKey("dbo.CampAnswers", "Camp_Id", "dbo.CampPosts");
            DropForeignKey("dbo.CampAttempts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CampAttempts", "CampPost_Id", "dbo.CampPosts");
            DropForeignKey("dbo.CampQuestions", "Set_Id", "dbo.CampPosts");
            DropForeignKey("dbo.CampOptions", "QuestionId", "dbo.CampQuestions");
            DropForeignKey("dbo.CampAnswers", "AttemptId", "dbo.CampAttempts");
            DropIndex("dbo.CampOptions", new[] { "QuestionId" });
            DropIndex("dbo.CampQuestions", new[] { "Set_Id" });
            DropIndex("dbo.CampAttempts", new[] { "CampPost_Id" });
            DropIndex("dbo.CampAttempts", new[] { "UserId" });
            DropIndex("dbo.CampAnswers", new[] { "Camp_Id" });
            DropIndex("dbo.CampAnswers", new[] { "AttemptId" });
            DropIndex("dbo.CampAnswers", new[] { "QuestionId" });
            DropIndex("dbo.CampAnswers", new[] { "OptionId" });
            DropIndex("dbo.CampAnswers", new[] { "UserId" });
            DropTable("dbo.CampOptions");
            DropTable("dbo.CampQuestions");
            DropTable("dbo.CampAttempts");
            DropTable("dbo.CampAnswers");
        }
    }
}
