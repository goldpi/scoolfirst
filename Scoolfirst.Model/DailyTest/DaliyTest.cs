﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Scoolfirst.Model.DaliyTestNameSpace
{
    public class DaliyTest
    {
        public Guid Id { get; set; }
        public DateTime StartsOn { get; set; }
        public DateTime EndsOn { get; set; }
        [DataType(DataType.MultilineText)]
        public string Scroll { get; set; }
        [DataType(DataType.MultilineText),AllowHtml]
        public string CheatCode { get; set; }


    }

    public class DTQuestionSet
    {
        public Guid Id { get; set; }
        public Guid DaliyTestId { get; set; }
        public virtual DaliyTest DaliyTest { get; set; }
        public int ClassId { get; set; }
        public ICollection<DTQuestion> Questions { get; set; }

    }

    public class DTQuestion
    {
        public Guid Id { get; set; }
        public Guid QuestionSetId { get; set; }
        public virtual DTQuestionSet QuestionSet { get; set; }

        [AllowHtml]
        public string Question { get; set; }
        [AllowHtml]
        public string Solution { get; set; }
        [AllowHtml]
        public string Concept   { get; set; }
        public bool ShowSloution { get; set; }
         
        public ICollection<DTAnswer> Answers { get; set; }

    }
    public class DTAnswer
    {
        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }
        public virtual DTQuestion Question { get; set; }

        [AllowHtml]
        public string Answer { get; set; }
        public bool IsCorrect { get; set; }
        public int Order { get; set; }
    }


    public class DTAppeared
    {
        public Guid Id { get; set; }
        public Guid DaliyTestId { get; set; }
        public virtual DaliyTest DaliyTest { get; set; }
        public Guid DTQuestionSetId { get; set; }
        public string UserId { get; set; }
        public virtual User User { get; set;}
        public virtual DTQuestionSet DTQuestionSet { get; set; }
        public DateTime On { get; set; }
        public ICollection<DTAnswered> Answereds { get; set; }
    }
    public class DTAnswered
    {
        public Guid Id { get; set; }
        public Guid DTQuestionSetId { get; set; }
        public string UserId { get; set; }
        public virtual User User { get; set; }
        public virtual DTQuestionSet DTQuestionSet { get; set; }
        public Guid AppearedId { get; set; }
        public virtual DTAppeared Appeared { get; set; }
        public Guid AnswerId { get; set; }
        public virtual DTAnswer Answer { get; set; }
        public bool Correct { get; set; }
    }
}
