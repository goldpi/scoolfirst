﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Notifications
{
    public class Notification
    {
        public Notification()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }
        public string User { get; set; }
        public string Group { get; set; }
        [JsonProperty("ImageUrl")]
        public string ImageUrl { get; set; }
        [JsonProperty("Link")]
        public string Link { get; set; }
        [JsonProperty("PostId")]
        public string PostId { get; set; }
        [JsonProperty("Title")]
        public string Title { get; set; }
        [JsonProperty("Message")]
        public string Message { get; set; }
        [JsonProperty("Date")]
        
        public DateTime Date { get; set; }
        [JsonProperty("Read")]
        public bool Read { get; set; }
        [EnumDataType(typeof(ModType))]
        [JsonProperty("Module")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ModType Module { get; set; }
        [JsonProperty("Size")]
        public bool Size { get; set; }


    }
    public enum ModType
    {
        [Display(Name = "Knowledge Nutrition")]
        KN,
        [Display(Name = "Reasoning Root")]
        RR,
        [Display(Name = "Work Sheet")]
        WRKSHT,
        [Display(Name = "U inspire me")]
        UIM,
        [Display(Name = "Computer and common core resources")]
        CC,
        [Display(Name = "Knowledge donation camp")]
        KDC_APP
    }
}
