﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Notifications
{
    public class NotificationHandler
    {
        public IEnumerable<Notification> Notifications { get; set; }
        public int UnreadCount { get; set; }
        public void GetNotification(Scoolfirst.Model.Context.ScoolfirstContext Context,string User)
        {
            //Notifications = Context.Notification.Where(i => i.TargetUserId == User).OrderByDescending(i => i.RaisedOn).Take(10);
            //UnreadCount = Context.Notification.Count(i => i.Read == false && i.TargetUserId == User);
        }

        //public void RaiseNotification(Scoolfirst.Model.Context.ScoolfirstContext Context,Notification Notifications)
        //{
        //    Notifications.Id = Guid.NewGuid();
        //    Notifications.Read = false;
        //    Notifications.RaisedOn = DateTime.UtcNow;
        //    Context.Notification.Add(Notifications);
        //    Context.SaveChanges();
        //}

        public void MarkRead(Scoolfirst.Model.Context.ScoolfirstContext Context,Guid Id)
        {
            var not = Context.Notification.Find(Id);
            not.Read = true;
            Context.Entry(not).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
        }
    }
}
