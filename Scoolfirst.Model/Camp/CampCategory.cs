﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Scoolfirst.Model.Camp
{
    public class CampCategory
    {

        public int Id { get; set; }
        public string DisplayName { get; set; }
        public bool Active { get; set; }
        public ICollection<CampPost> CampPosts { get; set; }
        [AllowHtml]
        public string Description { get; set; }
    }
    
}
