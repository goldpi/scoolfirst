﻿using System.Collections.Generic;

namespace Scoolfirst.Model.Camp
{
    public class CampPost
    {
        public int Id { get; set; }
        public int CampCategoryId { get; set; }
        public string Title { get; set; }
        public bool Active { get; set; }
        public string Page { get; set; }
        public string VideoUrl { get; set; }
        public int Time { get; set; }
        public virtual CampCategory CampCategory { get; set; }

        public virtual ICollection<CampQuestion> QuestionsSet { get; set; }
    }
    
}
