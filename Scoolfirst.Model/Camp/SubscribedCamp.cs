﻿using Scoolfirst.Model.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scoolfirst.Model.Camp
{
    public class SubscribedCamp
    {
        [Key, Column(Order = 0)] 
        public string UserId { get; set; }
        [Key, Column(Order = 1)]
        public int CampId { get; set; }

        public virtual User User { get; set; }
        public virtual CampCategory Camp { get; set; }
    }
    
}
