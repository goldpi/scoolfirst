﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Camp
{
   public class CampAnswers
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Guid OptionId { get; set; }
        public Guid QuestionId { get; set; }
        public long CampId { get; set; }
        public Guid AttemptId { get; set; }
        public virtual User User { get; set; }
        public virtual CampOptions Option { get; set; }
        public virtual CampQuestion Question { get; set; }
        public virtual CampPost Camp { get; set; }
        public virtual CampAttempt Attempt { get; set; }
        
    }
}
