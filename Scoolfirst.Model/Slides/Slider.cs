﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Slides
{
    public class Slider
    {
        
        public int Id { get; set; }
        [Display(Name ="Slider Name")]
        public string SliderName { get; set; }
        public bool Status { get; set; }
        [Display(Name = "Update On")]
        public DateTime UpdateOn { get; set; }
        public bool IsAlbum { get; set; }
        public bool IsSytemDefine { get; set; }
        public virtual ICollection<SliderSlice> Slices { get; set; }
    }

   
}
