﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.Slides
{
    public partial class SliderSlice
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string Target { get; set; }
        public string Image { get; set; }
        [Display(Name = "Item Order")]
        public int slidingOrder { get; set; }
        [Display(Name = "Update On")]
        public DateTime UpdateOn { get; set; }
        [Display(Name = "Create On")]
        public DateTime CreatedOn { get; set; }
        public bool IsHead { get; set; }
        public int SliderID { get; set; }
        public virtual Slider Slider { get; set; }


    }
}
