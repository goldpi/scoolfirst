﻿using Scoolfirst.Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Scoolfirst.Model.PeriodicAssesment
{
    public class PARoot
    {
        public long Id { get; set; }

        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string ShortDiscription { get; set; }

        public int MinQuestionToPass { get; set; } = 0;
        public int TimeInMinutes { get; set; } = 0;
        public string Tips { get; set; }
        public DateTime AddedOn { get; set; }

        [EnumDataType(typeof(PAType))]
        public PAType TypeOfAssement { get; set; } = PAType.WorkBook;

        public DateTime DisplayAfterDate { get; set; } = DateTime.UtcNow;

        public virtual ICollection<PAQuestion> QuestionsSet { get; set; }


        public int TestMinutes => TimeInMinutes * 60;

        public string TestBrief { get; set; }

        public int Order { get; set; }

        public int SchoolId { get; set; }
        public int ClassId { get; set; }
        public int? SubjectId { get; set; }
        public string Month { get; set; }

        public virtual School School { get; set; }
        public virtual Classes Class { get; set; }
        public virtual Subject Subject { get; set; }

        public string Level { get; set; }
        public string Color { get; set; }
        public string TitleColor { get; set; }
        public string Solution { get; set; }
        public bool IsPassable { get; set; }

    }
}
