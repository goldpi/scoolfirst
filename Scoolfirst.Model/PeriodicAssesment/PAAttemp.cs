﻿using Scoolfirst.Model.Identity;
using System;
using System.Collections.Generic;

namespace Scoolfirst.Model.PeriodicAssesment
{
    public class PAAttemp
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public long ReasoningId { get; set; }
        public DateTime On { get; set; }
        public virtual ICollection<PAAnswers> Ans { get; set; }
        public virtual PARoot Reasoning { get; set; }
        public int RightAns { get; set; }
        public int WrongAns { get; set; }
        public int SkippedAns { get; set; }
        public virtual User User { get; set; }
        public int Total => RightAns + WrongAns + SkippedAns;
    }
}
