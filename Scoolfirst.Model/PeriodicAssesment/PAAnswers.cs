﻿using Scoolfirst.Model.Identity;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Model.PeriodicAssesment
{
    public class PAAnswers
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Guid OptionId { get; set; }
        public Guid QuestionId { get; set; }
        public long ReasoningId { get; set; }
        public Guid AttempId { get; set; }
        public virtual User User { get; set; }
        public virtual PAOptions Option { get; set; }
        public virtual PAQuestion Question { get; set; }
        public virtual PARoot Reasoning { get; set; }
        public virtual PAAttemp Attemp { get; set; }
    }
}
