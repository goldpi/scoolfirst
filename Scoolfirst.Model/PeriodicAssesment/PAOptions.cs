﻿using System;

namespace Scoolfirst.Model.PeriodicAssesment
{
    public class PAOptions
    {

        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }
        public string Answer { get; set; }
        public bool Correct { get; set; }
        public int ShortOrder { get; set; }
        public virtual PAQuestion Question { get; set; }
    }
}
