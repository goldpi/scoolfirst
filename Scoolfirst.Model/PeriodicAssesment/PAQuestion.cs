﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Scoolfirst.Model.PeriodicAssesment
{
    public class PAQuestion
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "You have to enter the Question!")]
        public string Query { get; set; }
        public virtual ICollection<PAOptions> Option { get; set; }
        [Required]
        public long SetId { get; set; }
        public virtual PARoot Set { get; set; }
        public int ShortOrder { get; set; }
        public string Module { get; set; }
        public string Solution { get; set; }
    }
}
