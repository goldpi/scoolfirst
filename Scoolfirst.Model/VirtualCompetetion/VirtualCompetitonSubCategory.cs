﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Scoolfirst.Model.VirtualCompetetion
{
    public class VirtualCompetitonSubCategory
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Details { get; set; }
        public string VideoUrl { get; set; }
        public bool Active { get; set; }
        public DateTime AddedOn { get; set; }
        public int CategoryId { get; set; }

        public virtual VirtualCompetetionMainCategory Category { get; set; }
        public virtual ICollection<VirtualExam> Exams { get; set; }
    }
}