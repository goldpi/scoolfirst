﻿using Scoolfirst.Model.Identity;
using System;

namespace Scoolfirst.Model.VirtualCompetetion
{
    public class VirtualCompetetionAnswers
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Guid OptionId { get; set; }
        public Guid QuestionId { get; set; }
        public long ExamId { get; set; }
        public Guid AttemptId { get; set; }
        public virtual User User { get; set; }
        public virtual VirtualCompetetionOptions Option { get; set; }
        public virtual VirtualCompetetionQuestion Question { get; set; }
        public virtual VirtualExam Exam { get; set; }
        public virtual VirtualCompetetionAttempt Attempt { get; set; }
    }
}