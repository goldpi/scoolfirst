﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Scoolfirst.Model.VirtualCompetetion
{
    public class VirtualCompetetionQuestion
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "You have to enter the Question!"),AllowHtml]
        public string Query { get; set; }
        [Required(ErrorMessage = "You have to enter the Question!"), AllowHtml]
        public string QueryActual { get; set; }
        [AllowHtml]
        public string Solution { get; set; }
        public bool ShowSolution { get; set; }
        public virtual ICollection<VirtualCompetetionOptions> Options { get; set; }
        [Required]
        public long ExamId { get; set; }
        public virtual VirtualExam Exam { get; set; }
        public int ShortOrder { get; set; }
    }
}