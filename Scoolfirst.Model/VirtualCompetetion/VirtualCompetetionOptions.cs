﻿using System;

namespace Scoolfirst.Model.VirtualCompetetion
{
    public class VirtualCompetetionOptions
    {
        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }
        public string Answer { get; set; }
        public bool Correct { get; set; }
        public int ShortOrder { get; set; }
        public virtual VirtualCompetetionQuestion Question { get; set; }
    }
}