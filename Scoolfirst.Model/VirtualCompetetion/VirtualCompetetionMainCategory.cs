﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Scoolfirst.Model.VirtualCompetetion
{
    public class VirtualCompetetionMainCategory
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public int Position { get; set; }
        public bool Active { get; set; }
        public DateTime AddedOn { get; set; }

        //public virtual ICollection<VirtualCompetitonSubCategory> SubCategories { get; set; }
    }
}