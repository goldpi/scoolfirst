﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Scoolfirst.Model.VirtualCompetetion
{
    public class VirtualExam
    {
        public long Id { get; set; }

        [Required]
        public string Title { get; set; }
        [Required,AllowHtml]
        public string Content { get; set; }
        [Display(Name = "Time In Minutes")]
        public int TimeInMinutes { get; set; } = 0;
        [Display(Name = "Added/Edited On")]
        public DateTime AddedOn { get; set; }
        public int PointsPerQuestion { get; set; }
        public virtual ICollection<VirtualCompetetionQuestion> QuestionsSet { get; set; }
        public int TestMinutes => TimeInMinutes * 60;
        [Display(Name = "Test Brief")]
        public string TestBrief { get; set; }
        public int Order { get; set; }
        public string Level { get; set; }
        public string Color { get; set; }
        [Display(Name = "Title Color")]
        public string TitleColor { get; set; }
        public int CategoryId { get; set; }
        public string Subject { get; set; }

        public Boolean Active { get; set; } = false;

       
        public virtual VirtualCompetitonSubCategory Category { get; set; }
    }
}
