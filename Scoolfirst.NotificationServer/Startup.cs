﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Scoolfirst.Model.Identity;
using System;

[assembly: OwinStartupAttribute(typeof(Scoolfirst.NotificationServer.Startup))]
namespace Scoolfirst.NotificationServer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            
            
          
        }
    }
}
