﻿using Microsoft.AspNet.SignalR;
using Scoolfirst.NotificationServer.Hubs;
using Scoolfirst.NotificationServer.Models;
using Scoolfirst.NotificationServer.TaskSheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Scoolfirst.NotificationServer
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            UserOnline.OnlineUser = new Dictionary<string, string>();
            
            RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            RouteTable.Routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Online", action = "Index", id = UrlParameter.Optional }
            );
            GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromMinutes(11);
            GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromMinutes(3);
            GlobalHost.Configuration.KeepAlive = TimeSpan.FromMinutes(1);
            JobSheduler.Start();
        }
        void Application_Error(object sender, EventArgs e)
        {
            var Hubcontext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            Exception exception = Server.GetLastError();
            var str = Newtonsoft.Json.JsonConvert.SerializeObject(exception);
            Hubcontext.Clients.All.broadcastMessage("Error",str);
        }
    }
}
