﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Scoolfirst.NotificationServer.Models;
using System.Globalization;
using Scoolfirst.Model.MongoDatabase;

namespace Scoolfirst.NotificationServer.Hubs
{
    [HubName("notificationHub")]
    [OAuth]
    public class NotificationHub : Hub
    {
        Scoolfirst.Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();
        GroupManagerManogDB GM = new GroupManagerManogDB();
        OnlinerUserRepo OnlineManger = new OnlinerUserRepo();


        
        public override Task OnConnected()
        {

            if (Context.Request.Environment["server.User"] != null)
            {

                var Hubcontext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();

                Hubcontext.Clients.All.broadcastMessage(Context.Request.Environment["User"].ToString(), "Connected");
                var ass = Context.Request.Environment["User"].ToString();
                var user = dc.Users.FirstOrDefault(i => i.UserName == ass);
                AddUser(user);
                AddToGruop(user);           
                    
            }           
            return base.OnConnected();
        }

        private void AddToGruop(Model.Identity.User user)
        {
          
            var Groups = dc.Classes.Find(user.ClassId).Groups.Select(i => new { Name = i.Name, Classes = i.Class,i.Schools });
            foreach (var item in Groups)
            {
                if (GM.GroupExists(item.Name))
                {
                    GM.AddToGroup(item.Name, Context.ConnectionId);
                }
                else
                {
                    GM.AddGroupList(item.Name, 
                        item.Classes.Select(i => i.RomanDisplay).ToList(),
                        item.Schools.Select(i=>i.SchoolName).ToList(),
                        Context.ConnectionId);
                }
                this.Groups.Add(this.Context.ConnectionId, item.Name);
            }
 
        }

        private void AddUser(Model.Identity.User user)
        {

            
            OnlineManger.AddOnlineUser(new OnlineUser
            {
                ConnectionId = Context.ConnectionId,
                UserId = user.UserName,
                DeviceId = Context.Request.Headers["User-Agent"] ?? "Unkonw",
                ConnectedOn = DateTime.UtcNow.AddHours(5.5),
                LastConnection = DateTime.UtcNow.AddHours(5.5),
                Reconnected = 0
            });
        }

        public void ClearOldPhoneCon()
        {

            var Hubcontext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
           
            Hubcontext.Clients.All.broadcastMessage(Context.Request.Environment["User"].ToString(),"Previous Connection Recevied");
            OnlineManger.LastActivity(Context.ConnectionId);
            var con = OnlineManger.OnlineUser(Context.Request.Environment["User"].ToString()).Where(i => i.ConnectionId != Context.ConnectionId);
            foreach (var item in con)
                GM.RemoveFromGropu(item.ConnectionId);
            
            OnlineManger.DeleteByDievice(Context.ConnectionId, Context.Request.Environment["User"].ToString(), Context.Request.Headers["User-Agent"] ?? "Unkonw");
        }


        public override Task OnReconnected()
        {
            if (Context.Request.Environment["server.User"] != null)
            {
                var ass = Context.Request.Environment["User"].ToString();
                var Hubcontext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();

                Hubcontext.Clients.All.broadcastMessage(Context.Request.Environment["User"].ToString(), "rconnect");
                if (OnlineManger.OnlineUser(ass).Any(i => i.ConnectionId == Context.ConnectionId))
                {
                    OnlineManger.Reconnect(Context.ConnectionId);
                }
                else
                {
                    var user = dc.Users.FirstOrDefault(i => i.UserName == ass);
                    AddUser(user);
                }
                ////var user = dc.Users.FirstOrDefault(i => i.UserName == ass);
            }
            return base.OnReconnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {

            OnlineManger.DeleteByConection(Context.ConnectionId);
            GM.RemoveFromGropu(Context.ConnectionId);
            var Hubcontext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            
            Hubcontext.Clients.All.broadcastMessage(Context.Request.Environment["User"].ToString(),  "Discconedt");
            return base.OnDisconnected(stopCalled);
        }
        public void MarkReadAll()
        {
            NotificationRecivedManager NRM = new NotificationRecivedManager();

           var ret= NRM.MarkAll(Context.Request.Environment["User"].ToString()).ToList();
            Clients.Caller.Read(ret,"A");
        }

        public void MarkRead(string Id)
        {
            NotificationRecivedManager NRM = new NotificationRecivedManager();
            var ls = new List<string>
            {
                NRM.MarkRead(Id, Context.Request.Environment["User"].ToString())
            };
            Clients.Caller.Read(ls,"R");
        }


        public void MarkDelete(string Id)
        {
            NotificationRecivedManager NRM = new NotificationRecivedManager();
            var ls = new List<string>
            {
                NRM.MarkDelete(Id, Context.Request.Environment["User"].ToString())
            };
            Clients.Caller.Read(ls,"D");
        }


        public void MyUnreadNotification()
        {
            try
            {
                NotificationRecivedManager NRM = new NotificationRecivedManager();
                var us = Context.Request.Environment["User"].ToString();
                var user = dc.Users.FirstOrDefault(i => i.UserName == us);
                var Groups = dc.Classes.Find(user.ClassId).Groups.Select(i => new { Name = i.Name, Classes = i.Class, i.Schools });

                var list = NRM.UnreadNotification(Context.Request.Environment["User"].ToString(), Groups.Select(i => i.Name).ToList()).ToList();
                //  var lis = list.Where(i => Groups.Any(p => i.Group.Contains(p.Name)));
                Clients.Caller.notification(list);
            }
            catch(Exception ex)
            {
                var Hubcontext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
               
                var str = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                Hubcontext.Clients.All.broadcastMessage("Error", str);
            }
        }

        public void MyDiliveredNotification()
        {
            NotificationRecivedManager NRM = new NotificationRecivedManager();
            var dd=NRM.DeliveredNotification(Context.Request.Environment["User"].ToString()).ToList();
            Clients.Caller.notification(dd);
        }


        public void Received(string Id)
        {
            var Hubcontext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            NotificationRecivedManager NRM = new NotificationRecivedManager();
            NRM.Add(Id, Context.Request.Environment["User"].ToString());
            Hubcontext.Clients.All.broadcastMessage(Context.Request.Environment["User"].ToString(), Id.ToString()+ " Recevied"  );
        }
    }
}