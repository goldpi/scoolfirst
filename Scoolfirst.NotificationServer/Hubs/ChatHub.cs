﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Scoolfirst.NotificationServer.Models;
using Scoolfirst.Model.MongoDatabase;

namespace Scoolfirst.NotificationServer.Hubs
{
    public class AppHub : Hub
    {
        public void sendToall()
        {
            Clients.All.messgae("hi----");
        }
    }
  
    //[OAuth]
    public class ChatHub : Hub
    {

        public void sendToall()
        {
            Clients.All.messgae("hi----");
        }

        //public override Task OnConnected()
        //{
            
        //    if (Context.Headers["at"] != null)
        //    {
        //        var message = " Contetd android"; ;
        //        Clients.All.android(new Msg2 { Message = message, UserName = "android" });
        //        Clients.All.broadcastMessage("Android", message);
        //    }
        //    if (Context.Request.Environment["server.User"] != null)
        //        Clients.All.android(new Msg2 { Message = Context.Request.Environment["User"].ToString(), UserName = "android" });
        //    var messageq = " Contetd android esauth" + Context.Request.Environment["User"] + " tok :" + Context.Request.Environment["token"];
        //    Clients.All.android(new Msg2 { Message = messageq, UserName = "android" });
        //    return base.OnConnected();
        //}
        public void messagetoGroup(string msg, string grp)
        {
            var ls = new List<string>();
            ls.Add(grp);
            var th = new List<Scoolfirst.NotificationServer.Models.Msg>();
            th.Add(new Models.Msg { Id = 0, Message = msg, Read = false });
            var Hubcontext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            var lss = new NotificationManagerRepo().ListAll(0,20);
            Hubcontext.Clients.Groups(ls).notification(lss);
            Clients.All.Notification(lss);
        }
        public void Send(string name, string message)
        {
            try
            {

                if (Context.User.Identity.IsAuthenticated)
                    message += " " + Context.User.Identity.Name;
              //  Call the broadcastMessage method to update clients.
               Clients.All.broadcastMessage(name, message);
               Clients.All.android(new Msg2 { Message = message, UserName = name });
            }
            catch(Exception e)
            {
                Clients.All.broadcastMessage(name , e.Message);
            }
            
        }

        public override Task OnConnected()
        {
            Clients.All.messgae("Connected " + DateTime.Now);
            return base.OnConnected();
            
        }
    }
}