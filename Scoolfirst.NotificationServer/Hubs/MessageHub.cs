﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Security.Claims;
using Scoolfirst.NotificationServer.Models;

namespace Scoolfirst.NotificationServer.Hubs
{
    [OAuth]
    public class MessageHub : Hub
    {
        Scoolfirst.Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();

        public override Task OnConnected()
        {

            if (Context.Request.Environment["server.User"] != null)
            {
                var ass = Context.Request.Environment["User"].ToString();
                var user = dc.Users.FirstOrDefault(i => i.UserName == ass);
                user.ConnectionId = Context.ConnectionId;
                dc.SaveChanges();
             
             
                var Groups = dc.Classes.Find(user.ClassId).Groups.Select(i => i.Name);
                string gn = "";
                foreach (var item in Groups)
                {
                    gn += item + " ";
                    this.Groups.Add(this.Context.ConnectionId, item);
                }

            }
            return base.OnConnected();
        }
        public void MyUnreadMessages(string uid)
        {
            var th = new List<Scoolfirst.NotificationServer.Models.Msg>();
            th.Add(new Models.Msg { Id = 0, Message = "mess", Read = false });
            th.Add(new Models.Msg { Id = 0, Message = "mess", Read = false });
            Clients.Caller.messages(th);
        }

        
        public void sendToAll(string message)
        {
            var iid = Context.ConnectionId;
            var th = new List<Scoolfirst.NotificationServer.Models.Msg>();
            th.Add(new Models.Msg { Id = 0, Message = message, Read = false });
            Clients.AllExcept(iid).messages(th);
        }
        /// <summary>
        /// Mark All read
        /// </summary>
        public void MarkReadAll()
        {
            Clients.Caller.markedRead("Sucess");
        }
        /// <summary>
        /// MArk Single Notification As Read
        /// </summary>
        /// <param name="uid"></param>
        public void MarkRead(string uid)
        {
            Clients.Caller.markedRead("Sucess");
        }



    }
}