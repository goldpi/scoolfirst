﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
/// <summary>
/// To handle the Authorization Token
/// imran@goldpi.com
/// </summary>

namespace Scoolfirst.NotificationServer.Models
{

    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNet.SignalR.Hubs;
    using System.Security.Claims;
    using Microsoft.AspNet.SignalR.Owin;


    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class OAuthAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Authorization Of Hub
        /// </summary>
        /// <param name="hubDescriptor"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public override bool AuthorizeHubConnection(HubDescriptor hubDescriptor, IRequest request)
        {
            var token = request.QueryString.Get("access_token");
            var ticket = Startup.OAuthOptions.AccessTokenFormat.Unprotect(token);
            request.Environment["token"] = token;
            request.Environment["ticket"] = ticket != null? ticket.Identity.IsAuthenticated:false;

            if (ticket != null && ticket.Identity != null && ticket.Identity.IsAuthenticated)
            {
               // request.GetHttpContext().User = new ClaimsPrincipal(ticket.Identity);
                // set the authenticated user principal into environment so that it can be used in the future
                request.Environment["server.User"] = new ClaimsPrincipal(ticket.Identity);
                request.Environment["User"] = ticket.Identity.Name;
                return true;
            }

            return false;
           
        }
        
    }
}
