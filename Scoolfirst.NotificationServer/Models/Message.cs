﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.NotificationServer.Models
{
    public class Msg
    {
        [JsonProperty("Id")]
        public int Id { get; set; }
        [JsonProperty("Message")]
        public string Message { get; set; }
        //[JsonProperty("UserName")]
        //public string UserName { get; set; }
        [JsonProperty("Read")]
        public bool Read { get; set; }

    }
    public class Msg2
    {
        [JsonProperty("Message")]
        public string Message { get; set; }
        [JsonProperty("UserName")]
        public string UserName { get; set; }
    }
}
