﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.NotificationServer.Models
{
    static class UserOnline
    {
        public static Dictionary<string,string> OnlineUser { get; set; }
    }
}
