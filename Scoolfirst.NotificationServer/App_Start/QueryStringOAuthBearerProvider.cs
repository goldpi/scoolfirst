﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;

namespace Scoolfirst.NotificationServer
{
    internal class QueryStringOAuthBearerProvider : IOAuthBearerAuthenticationProvider
    {
        public Task ApplyChallenge(OAuthChallengeContext context)
        {
            return null;
        }

        public Task RequestToken(OAuthRequestTokenContext context)
        {
            var request = context.Request;
            var token = request.Query.Get("access_token");
            var ticket = Startup.OAuthOptions.AccessTokenFormat.Unprotect(token);
            request.Environment["token"] = token;
            request.Environment["ticket"] = ticket != null ? ticket.Identity.IsAuthenticated : false;

            if (ticket != null && ticket.Identity != null && ticket.Identity.IsAuthenticated)
            {
                // request.GetHttpContext().User = new ClaimsPrincipal(ticket.Identity);
                // set the authenticated user principal into environment so that it can be used in the future
                request.Environment["server.User"] = new ClaimsPrincipal(ticket.Identity);
                request.Environment["User"] = ticket.Identity.Name;
                return Task.FromResult<object>(true);
            }

            

            return Task.FromResult<object>(false);
        }

        public Task ValidateIdentity(OAuthValidateIdentityContext context)
        {
            var request = context.Request;
            var token = request.Query.Get("access_token");
            var ticket = Startup.OAuthOptions.AccessTokenFormat.Unprotect(token);
            request.Environment["token"] = token;
            request.Environment["ticket"] = ticket != null ? ticket.Identity.IsAuthenticated : false;

            if (ticket != null && ticket.Identity != null && ticket.Identity.IsAuthenticated)
            {
                // request.GetHttpContext().User = new ClaimsPrincipal(ticket.Identity);
                // set the authenticated user principal into environment so that it can be used in the future
                request.Environment["server.User"] = new ClaimsPrincipal(ticket.Identity);
                request.Environment["User"] = ticket.Identity.Name;
                return Task.FromResult<object>(true);
            }



            return Task.FromResult<object>(false);
        }
    }
}