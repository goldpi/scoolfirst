﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using Microsoft.Owin.Security.OAuth;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using System.Security.Claims;
using Microsoft.Owin.Cors;
using Scoolfirst.Model.Provider;
using Microsoft.AspNet.SignalR;

namespace Scoolfirst.NotificationServer
{
    //public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    //{
    //    private readonly string _publicClientId;

    //    public ApplicationOAuthProvider(string publicClientId)
    //    {
    //        if (publicClientId == null)
    //        {
    //            throw new ArgumentNullException("publicClientId");
    //        }

    //        _publicClientId = publicClientId;
    //    }
       
    //    public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
    //    {
    //        var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

    //        User user = await userManager.FindAsync(context.UserName, context.Password);

    //        if (user == null)
    //        {
    //            context.SetError("invalid_grant", "The user name or password is incorrect.");
    //            return;
    //        }

    //        ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
    //           OAuthDefaults.AuthenticationType);
    //        ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
    //            CookieAuthenticationDefaults.AuthenticationType);

    //        AuthenticationProperties properties = CreateProperties(user.UserName);
    //        AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
    //        context.Validated(ticket);
    //        context.Request.Context.Authentication.SignIn(cookiesIdentity);
    //    }

    //    public override Task TokenEndpoint(OAuthTokenEndpointContext context)
    //    {
    //        foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
    //        {
    //            context.AdditionalResponseParameters.Add(property.Key, property.Value);
    //        }

    //        return Task.FromResult<object>(null);
    //    }

    //    public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
    //    {
    //        // Resource owner password credentials does not provide a client ID.
    //        if (context.ClientId == null)
    //        {
    //            context.Validated();
    //        }

    //        return Task.FromResult<object>(null);
    //    }

    //    public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
    //    {
    //        if (context.ClientId == _publicClientId)
    //        {
    //            Uri expectedRootUri = new Uri(context.Request.Uri, "/");

    //            if (expectedRootUri.AbsoluteUri == context.RedirectUri)
    //            {
    //                context.Validated();
    //            }
    //        }

    //        return Task.FromResult<object>(null);
    //    }

    //    public static AuthenticationProperties CreateProperties(string userName)
    //    {
    //        IDictionary<string, string> data = new Dictionary<string, string>
    //        {
    //            { "userName", userName }
    //        };
    //        return new AuthenticationProperties(data);
    //    }
    //}
    public partial class Startup
    {
        //public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ScoolfirstContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            //Enable the application to use a cookie to store information for the signed in user

            //and to use a cookie to temporarily store information about a user logging in with a third party login provider

                       app.UseCookieAuthentication(new CookieAuthenticationOptions
                       {
                           AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                           LoginPath = new PathString("/Account/Login"),


#if RELEASE
                            CookieDomain =$".{HostingServerInfo.Domain.Url()}" ,
                            CookieName = "xauc",
#endif
                            Provider = new CookieAuthenticationProvider
                           {
                                // Enables the application to validate the security stamp when the user logs in.
                                // This is a security feature which is used when you change a password or add an external login to your account.  
                                OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, User>(
                                    validateInterval: TimeSpan.FromMinutes(9),
                                    regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                           }
                       });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            //app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            //app.Use(async (context, next) =>
            //{
            //    IOwinRequest req = context.Request;
            //    IOwinResponse res = context.Response;
            //    if (req.Path.StartsWithSegments(new PathString("/signalr/negotiate")))
            //    {
            //        var origin = req.Headers.Get("Origin");
            //        if (!string.IsNullOrEmpty(origin))
            //        {
            //            res.Headers.Set("Access-Control-Allow-Origin", origin);
            //        }
            //        if (req.Method == "OPTIONS")
            //        {
            //            res.StatusCode = 200;
            //            res.Headers.AppendCommaSeparatedValues("Access-Control-    Allow-Methods", "GET", "POST");
            //            res.Headers.AppendCommaSeparatedValues("Access-Control-    Allow-Headers", "authorization", "content-type");
            //            return;
            //        }
            //    }
            //    await next();
            //});
            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(15),
                RefreshTokenProvider = new UpgrdeRefreshTokenProvier(),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true
            };
            
            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);
            app.Map("/signalr", map =>
            {
                // Setup the CORS middleware to run before SignalR.
                // By default this will allow all origins. You can 
                // configure the set of origins and/or http verbs by
                // providing a cors options with a different policy.
               // map.UseCors(CorsOptions.AllowAll);
               
                var hubConfiguration = new HubConfiguration
                {

                    // You can enable JSONP by uncommenting line below.
                    // JSONP requests are insecure but some older browsers (and some
                    // versions of IE) require JSONP to work cross domain
                    EnableJSONP = true,
                    EnableDetailedErrors=true,
                    EnableJavaScriptProxies=true
                     
                };

                map.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions()
                {
                    Provider = new QueryStringOAuthBearerProvider()
                });
                hubConfiguration.EnableDetailedErrors = true;

                map.RunSignalR(hubConfiguration);

            });
        }
        }
}
