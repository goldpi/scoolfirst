﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.NotificationServer.Controllers
{
    public class authController : Controller
    {
        // GET: auth
        public ActionResult Index(string returnUrl="/online")
        {
            return Redirect($"http://{HostingServerInfo.Application.Url()}/account/login?returnUrl=http://{HostingServerInfo.Notification.Url()}{returnUrl}");
        }
    }
}