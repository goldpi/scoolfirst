﻿using Microsoft.AspNet.SignalR;
using Scoolfirst.Model.MongoDatabase;
using Scoolfirst.Model.Notifications;
using Scoolfirst.NotificationServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Scoolfirst.NotificationServer.Controllers
{
 
    
    public class OnlineController : Controller
    {
        // GET: Online
        public ActionResult Index()
        {
            var dc = new Scoolfirst.Model.Context.ScoolfirstContext();

            return View(dc.Groups.Select(i=>i.Name));
        }

        public ActionResult Groups(int size=20,int page=0)
        {
            GroupManagerManogDB gm = new GroupManagerManogDB();
            return View(gm.AllGrp(page, size));
        }

        public ActionResult Users(int size = 20, int page = 0)
        {
            OnlinerUserRepo gm = new OnlinerUserRepo();
            return View(gm.All(page, size));
        }


        public ActionResult Notification()
        {
            var dc = new Scoolfirst.Model.Context.ScoolfirstContext();
            ViewBag.Group = new SelectList(dc.Groups, "Name", "Name");
            var p  = new NotificationManagerRepo().ListAll(0,20).First();
            return View(p);
        }

        [HttpPost]
        public ActionResult Notification(Notification Android)
        {
            Android.Id = Guid.NewGuid().ToString().Replace('-','0');
            var dc = new Scoolfirst.Model.Context.ScoolfirstContext();
            ViewBag.Group = new SelectList(dc.Groups, "Name", "Name");
            var Hubcontext = GlobalHost.ConnectionManager.GetHubContext<Hubs.NotificationHub>();
            var Hubcontext2 = GlobalHost.ConnectionManager.GetHubContext<Hubs.ChatHub>();
            var str = Newtonsoft.Json.JsonConvert.SerializeObject(Android);
            ViewBag.str = str;
            Hubcontext.Clients.All.broadcastMessage("System", str + " Send");
            List<Notification> ls = new List<Notification>();
            ls.Add( Android);
            Hubcontext.Clients.All.notification(ls);
            Hubcontext.Clients.Group(Android.Group).notification(ls);
            return View();
        }


        public string SendToAll()
        {
            var Hubcontext2 = GlobalHost.ConnectionManager.GetHubContext<Hubs.ChatHub>();
            Hubcontext2.Clients.All.message("hi");
            var Hubcontext3 = GlobalHost.ConnectionManager.GetHubContext<Hubs.AppHub>();
            Hubcontext3.Clients.All.message("hi");
            return "ss";
        }
    }
}