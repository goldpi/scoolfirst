﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.NotificationServer.TaskSheduler
{
    public class JobSheduler
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();
            IJobDetail job = JobBuilder.Create<NotificationJob>().Build();
            ITrigger trigger = TriggerBuilder.Create()
            .WithIdentity("trigger1", "group1")
            .StartNow()
            .WithSimpleSchedule(x => x
            .WithIntervalInSeconds(20)
            .RepeatForever())
            .Build();
            scheduler.ScheduleJob(job, trigger);
            IJobDetail job1 = JobBuilder.Create<TestJob>().Build();
            ITrigger trigger1 = TriggerBuilder.Create()
            .WithIdentity("trigger2", "group2")
            .StartNow()
            .WithSimpleSchedule(x => x
            .WithIntervalInSeconds(2)
            .RepeatForever())
            .Build();
            scheduler.ScheduleJob(job1, trigger1);

        }
    }
}
