﻿using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Quartz;
using Scoolfirst.Model.DaliyTestNameSpace;
using Scoolfirst.Model.MongoDatabase;
using Scoolfirst.Model.Notifications;
using Scoolfirst.Model.Utility;
using Scoolfirst.NotificationServer.Hubs;
using Scoolfirst.NotificationServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.NotificationServer.TaskSheduler
{
    public class NotificationJob : IJob
    {
        Scoolfirst.Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();

        public void Execute(IJobExecutionContext context)
        {
            //#20 fixed
            var Hubcontext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
         
            var notificationtest = new NotificationManagerRepo().ListAllPer();
            var Hubcontext2 = GlobalHost.ConnectionManager.GetHubContext<Hubs.ChatHub>();
            var Hubcontext3 = GlobalHost.ConnectionManager.GetHubContext<Hubs.AppHub>();
          //  Hubcontext2.Clients.All.broadcastMessage("JobLog:"+DateTime.Now.ToLongDateString());
         
            foreach (var i in notificationtest)
            {
                var dat = DateTimeHelper.GetIST();
                var dat2 = DateTimeHelper.GetIST().Subtract(new TimeSpan(0, 0, 20));
               
                Hubcontext2.Clients.All.broadcastMessage("JobLog:", $" comparison Date \n <br> {i.Date} \n  <br>DateIST {dat.ToString()} \n <br> dateIST(20) {dat2.ToString()}  Comparison {i.Date <= dat && i.Date >= dat2}");
                if (i.Date <= dat && i.Date >= dat2)
                {
                    var p = new List<Notification>();
                    p.Add(i);
                    Hubcontext.Clients.Group(i.Group).notification(p);
                    Hubcontext.Clients.All.notification(p);
                }
                
            }
          

        }
    }

    public class TestJob : IJob
    {
        Scoolfirst.Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();
        IHubContext Hubcontext = GlobalHost.ConnectionManager.GetHubContext<Hubs.ChatHub>();
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");

               
                var date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                DaliyTest data = null;
                if(dc.DaliyTests.Any(i => i.EndsOn > date))
                data = dc.DaliyTests.OrderBy(i=>i.EndsOn).FirstOrDefault(i => i.EndsOn > date);
                // var dateEx = (data != null ? data.StartsOn.ToUniversalTime() : date);
               
                var TimeLeft = (data != null && (date < data.StartsOn) ? data.StartsOn - date : date - date);

                if (data != null)
                {
                    if ((int)TimeLeft.TotalSeconds != 0)
                        Hubcontext.Clients.All.test(false, data.StartsOn.ToString("dd MMM yyyy h:mm:ss tt") + " " +data.EndsOn.ToString("h:mm:ss tt"), TimeLeft.TotalSeconds);
                    else
                        Hubcontext.Clients.All.test(true, data.StartsOn.ToString("dd MMM yyyy h:mm:ss tt") + " " + data.EndsOn.ToString("h:mm:ss tt"), TimeLeft.TotalSeconds);
                   
                }
                else
                {
                    if (TimeLeft.TotalSeconds == 0 && dc.DaliyTests.Any(i => i.StartsOn < date && i.EndsOn > date))
                    {
                        data = dc.DaliyTests.First(i => i.StartsOn < date && i.EndsOn > date);
                    }
                    if (data == null)
                        Hubcontext.Clients.All.test(false, date.AddDays(1).ToString("dd MMM yyyy h:mm:ss tt"), 600);
                    else
                        Hubcontext.Clients.All.test(true, data.StartsOn.ToString("dd MMM yyyy h:mm:ss tt") + " " + data.EndsOn.ToString("h:mm:ss tt"), TimeLeft.TotalSeconds);

                }
            }
            catch(Exception ex)
            {
                Hubcontext.Clients.All.test(true, "Error " +ex.Message, 700);
            }
            


        }
    }
}
