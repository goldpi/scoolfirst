﻿using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Scoolfirst.Tests.MocksContext
{
    class MockHelper
    {

        public static Mock<ControllerContext> MockUser(string Username)
        {
            var mocks = new MockRepository(MockBehavior.Default);
           
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal
                .SetupGet(p => p.Identity.Name)
                .Returns(Username);
            mockPrincipal
                .Setup(p => p.IsInRole("User"))
                .Returns(true);
            var mockContext = new Mock<ControllerContext>();
            mockContext
                .SetupGet(p => p.HttpContext.User)
                .Returns(mockPrincipal.Object);
            mockContext
                .SetupGet(p => p.HttpContext.Request.IsAuthenticated)
                .Returns(true);
            mockContext.Setup(i => i.HttpContext.Session).Returns(new Mock<HttpSessionStateBase>().Object);
            return mockContext;
        }
        public static Mock<ControllerContext> MockUser(string Username, RouteData data)
        {
            
            var mocks = new MockRepository(MockBehavior.Default);

            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal
                .SetupGet(p => p.Identity.Name)
                .Returns(Username);
            mockPrincipal
                .SetupGet(p => p.Identity.IsAuthenticated)
                .Returns(true);
            mockPrincipal
                .Setup(p => p.IsInRole("User"))
                .Returns(true);
            var mockContext = new Mock<ControllerContext>();
            mockContext
                .SetupGet(p => p.HttpContext.User)
                .Returns(mockPrincipal.Object);
            mockContext
                .SetupGet(p => p.HttpContext.Request.IsAuthenticated)
                .Returns(true);
            mockContext.Setup(i => i.RouteData).Returns(data);
            var session = new Mock<HttpSessionStateBase>();
            //session.Setup(i => i).Returns();
            mockContext.Setup(i => i.HttpContext.Session).Returns(new FakeSession());
            return mockContext;
        }
        public static Mock<ControllerContext> MockUserWithAjax(string Username)
        {
            var mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal
                .SetupGet(p => p.Identity.Name)
                .Returns(Username);
            mockPrincipal
                .Setup(p => p.IsInRole("User"))
                .Returns(true);
            var mockContext = new Mock<ControllerContext>();
            mockContext
                .SetupGet(p => p.HttpContext.User)
                .Returns(mockPrincipal.Object);
            mockContext
                .SetupGet(p => p.HttpContext.Request.IsAuthenticated)
                .Returns(true);
            mockContext.SetupGet(x => x.HttpContext.Request.Headers).Returns(
        new WebHeaderCollection() {
            {"X-Requested-With", "XMLHttpRequest"}
        });
            mockContext.Setup(i => i.HttpContext.Session).Returns(new Mock<HttpSessionStateBase>().Object);
            return mockContext;
        }
        public static DbSet<T> GetQueryableMockDbSet<T>(params T[] sourceList) where T : class
        {
            var queryable = sourceList.AsQueryable();
            var dbSet = new Mock<DbSet<T>>();
            dbSet.As<IQueryable<T>>()
                .Setup(m => m.Provider)
                .Returns(queryable.Provider);
            dbSet.As<IQueryable<T>>()
                .Setup(m => m.Expression)
                .Returns(queryable.Expression);
            dbSet.As<IQueryable<T>>()
                .Setup(m => m.ElementType)
                .Returns(queryable.ElementType);
            dbSet.As<IQueryable<T>>()
                .Setup(m => m.GetEnumerator())
                .Returns(queryable.GetEnumerator());
            return dbSet.Object;
        }

        public static Mock<DbSet<T>> GetQueryableMockedDbSet<T>(params T[] sourceList) where T : class
        {
            var queryable = sourceList.AsQueryable();
            var dbSet = new Mock<DbSet<T>>();
            dbSet.As<IQueryable<T>>()
                .Setup(m => m.Provider)
                .Returns(queryable.Provider);
            dbSet.As<IQueryable<T>>()
                .Setup(m => m.Expression)
                .Returns(queryable.Expression);
            dbSet.As<IQueryable<T>>()
                .Setup(m => m.ElementType)
                .Returns(queryable.ElementType);
            dbSet.As<IQueryable<T>>()
                .Setup(m => m.GetEnumerator())
                .Returns(queryable.GetEnumerator());
            return dbSet;
        }
    }
}
