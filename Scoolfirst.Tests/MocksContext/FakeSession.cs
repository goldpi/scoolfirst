﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace Scoolfirst.Tests.MocksContext
{
    public class FakeSession :HttpSessionStateBase
    {
        private readonly IDictionary<string, object> _yourDictionary = new Dictionary<string, object>();
        public override  object this[string key]
        {
            // returns value if exists
            get { return _yourDictionary.Keys.Any(i=>i==key)? _yourDictionary[key]:null; }

            // updates if exists, adds if doesn't exist
            set { _yourDictionary[key] = value; }
        }

        public override int Count => _yourDictionary.Count;
    }
}
