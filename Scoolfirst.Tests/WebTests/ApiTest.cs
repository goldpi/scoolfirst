﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Scoolfirst.Web.Controllers;
using System.Collections;
using System.Linq;
using Scoolfirst.Model.Context;
using Moq;
using Scoolfirst.Model.WorkBook;
using System.Data.Entity;
using Scoolfirst.Tests.MocksContext;
using FizzWare.NBuilder;
using Scoolfirst.ViewModel.StringHelper;

namespace Scoolfirst.Tests.WebTests
{
    [TestClass]
    public class ApiTest
    {

        
        [TestMethod]
        public void TestStags()
        {
            
            List<string> strings =
             new List<string> {
                "Test",
                "test 2",
                "test"
            };
           
            var result = StringHelper.sTags(strings as IEnumerable<string>);
            Assert.AreEqual(4, result.ToList().Count());
        }

        //[TestMethod]
        //public void HybridTest()
        //{
        //    FakeContext = new Mock<ScoolfirstContext>();
        //    FakeContext.Setup(i => i.Chapters).Returns(MockHelper.GetQueryableMockDbSet<Chapter>(
        //         Builder<Chapter>.CreateListOfSize(40)
        //        .All()
        //        .With(i => i.Questions = Builder<Questions>.CreateListOfSize(10).All().Build().ToList())
        //        .Build()
        //        .ToArray()
        //        ));
        //    var con = new AllApiController(FakeContext.Object);
        //    var result = con.("r", 1);
        //    Assert.AreEqual(10, result.ToList().Count());

        //}

        
    }
}
