﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scoolfirst.Model.Context;
using Moq;
using Scoolfirst.Model.NAO;
using FizzWare.NBuilder;
using System.Linq;
using Scoolfirst.Tests.MocksContext;
using Scoolfirst.Web.Controllers;
using System.Collections.Generic;
using System.Web.Mvc;
using Scoolfirst.Model.MCQ;

namespace Scoolfirst.Tests.WebTests
{
    [TestClass]
    public class NLOControllerTest
    {
        private Mock<ScoolfirstContext> FakeContext;
        public NLOControllerTest()
        {
            FakeContext = new Mock<ScoolfirstContext>();
         }
        [TestMethod]
        public void IndexTest()
        {
            var re = Builder<Olympiad>.
                CreateListOfSize(10)
                .All()
                .Build()
                .ToArray();
            FakeContext.Setup(i => i.Olympiads).Returns(MockHelper.GetQueryableMockDbSet<Olympiad>(re));
            var con = new NLOController(FakeContext.Object)
            { ControllerContext = MockHelper.MockUser("user").Object };

           ViewResult result = (con.Index() as ViewResult);
            var Model = (IEnumerable<Olympiad>)result.Model;
            Assert.AreEqual(3, Model.Count());
        }
        [TestMethod]
        public void IndexAjaxTest()
        {
            var re = Builder<Olympiad>.
               CreateListOfSize(10)
               .All()
               .Build()
               .ToArray();
            FakeContext.Setup(i => i.Olympiads).Returns(MockHelper.GetQueryableMockDbSet<Olympiad>(re));
            var con = new NLOController(FakeContext.Object)
            { ControllerContext = MockHelper.MockUserWithAjax("user").Object };

            PartialViewResult result = (con.Index() as PartialViewResult);
            var Model = (IEnumerable<Olympiad>)result.Model;
            Assert.AreEqual(3, Model.Count());
        }


        [TestMethod]
        public void TestTest()
        {
            var re = Builder<QuestionSet>.
               CreateListOfSize(10)
               .All()
               .Build()
               .ToArray();
            var id = re.First();
            FakeContext.Setup(i => i.QuestionSets).Returns(MockHelper.GetQueryableMockDbSet<QuestionSet>(re));
            var con = new NLOController(FakeContext.Object)
            { ControllerContext = MockHelper.MockUser("user").Object };

            ViewResult result = (con.Test(id.Id) as ViewResult);
            var Model = (QuestionSet)result.Model;
            Assert.AreEqual(id, Model);
        }
    }
}
