﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Scoolfirst.Model.WorkBook;
using FizzWare.NBuilder;
using System.Linq;
using Scoolfirst.Model.Context;
using Moq;
using Scoolfirst.Tests.MocksContext;
using Scoolfirst.Web.Controllers;
using System.Web.Mvc;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Identity;

namespace Scoolfirst.Tests.WebTests
{
    [TestClass]
    public class TossTopicTest
    {
        private Mock<ScoolfirstContext> FakeContext;
        public TossTopicTest()
        {
            FakeContext = new Mock<ScoolfirstContext>();
            
            FakeContext.Setup(i => i.Books).Returns(MockHelper.GetQueryableMockDbSet<Books>(
                Builder<Books>.CreateListOfSize(40)
                .All()
                .With(i => i.Questions = Builder<Questions>.CreateListOfSize(10).All().Build().ToList())
                .Build()
                .ToArray()));
            //FakeContext.
            FakeContext.Setup(i => i.Classes).Returns(MockHelper.GetQueryableMockDbSet<Classes>(
                Builder<Classes>.CreateListOfSize(5).All().Build().ToArray()
                ));
            FakeContext.Setup(i => i.Questions).Returns(MockHelper.GetQueryableMockDbSet<Questions>(Builder<Questions>.CreateListOfSize(10)
                .All().Build().ToArray()));
            FakeContext.Setup(i => i.Users).Returns(MockHelper.GetQueryableMockDbSet<User>(
               new User
               {
                    ClassId=1,
                    UserName="UserName",
                     
               }
                ));
        }
        
        [TestMethod]
        public void ProgressiveLearning()
        {
            
            IQueryable<Classes> mockClasses = Builder<Classes>.CreateListOfSize(10).All().Build().ToList().AsQueryable();
            IQueryable<Chapter> mockChapter = Builder<Chapter>.CreateListOfSize(40)
                .All()
                .With(i => i.Questions = Builder<Questions>.CreateListOfSize(10).All().Build().ToList())
                .Build().ToList().AsQueryable();
            var result = Scoolfirst.ViewModel.WorkBook.TossTopic.ProgressiveLearing.ProgressiveQuestion(mockClasses, mockChapter, 6); 
            Assert.AreEqual(40,result.Count());
        }

        [TestMethod]
        public void HybridLearning()
        {
            var result = Scoolfirst.ViewModel.WorkBook.TossTopic.HybridLearning.HybridLearningQuestions(1, Builder<Chapter>.CreateListOfSize(40)
                .All()
                .With(i => i.Questions = Builder<Questions>.CreateListOfSize(10).All().Build().ToList())
                .Build().ToList().AsQueryable());
            Assert.AreEqual(10, result.Count());
        }


       
    }


   

}
