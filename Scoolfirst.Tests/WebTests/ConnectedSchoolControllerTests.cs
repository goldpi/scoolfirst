﻿using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.Tests.MocksContext;
using Scoolfirst.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Scoolfirst.Tests.WebTests
{
    [TestClass()]
    public class ConnectedSchoolControllerTests
    {
        private Mock<ScoolfirstContext> FakeContext;
        public ConnectedSchoolControllerTests()
        {
            FakeContext = new Mock<ScoolfirstContext>();
        }
        [TestMethod()]
        public void IndexTest()
        {
            
            Assert.AreEqual(1, 1);
        }

        //[TestMethod()]
        //public void SubjectsTest()
        //{
        //    Assert.Pass();
        //}

        //[TestMethod()]
        //public void LoadRegistationViewDataTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void UnitWiseTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void TestWorkbookTest()
        //{
        //    var re = Builder<Quiz>.
        //        CreateListOfSize(10)
        //        .All()
        //        .Build()
        //        .ToArray();
        //    FakeContext.Setup(i => i.Quizes).Returns(MockHelper.GetQueryableMockDbSet<Quiz>(re));
        //    var con = new ConnectedSchoolController(FakeContext.Object) {ControllerContext= MockHelper.MockUser("user").Object };

        //    PartialViewResult result = (con.TestWorkbook("School1", "Class1") as PartialViewResult);
        //    var Model = (IEnumerable<Quiz>)result.Model;
        //    Assert.AreEqual(1,Model.Count());

        //}

        //[TestMethod()]
        //public void TestTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void SyllabusTest()
        //{
        //    var queryable = Builder<SyllabusDate>.
        //       CreateListOfSize(10)
        //       .All()
        //       .With(i=>i.Quiz=Builder<Quiz>.CreateListOfSize(10).All().Build().ToList())
        //       .Build()
        //       .ToArray();
            
        //    FakeContext.Setup(i => i.SyllabusDates).Returns(MockHelper.GetQueryableMockDbSet<SyllabusDate>(queryable));
        //    var con = new ConnectedSchoolController(FakeContext.Object) { ControllerContext = MockHelper.MockUser("user").Object };

        //    PartialViewResult result = (con.Syllabus(1, 1) as PartialViewResult);
        //    var Model = (IEnumerable<SyllabusDate>)result.Model;
        //    Assert.AreEqual(1, Model.Count());
        //}




    }
}