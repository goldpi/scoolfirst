﻿using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Projects;
using Scoolfirst.Tests.MocksContext;
using Scoolfirst.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Scoolfirst.WebTests
{
    [TestClass()]
    public class ProductsControllerTests
    {
        private Mock<ScoolfirstContext> FakeContext;
        public ProductsControllerTests()
        {
            FakeContext = new Mock<ScoolfirstContext>();
        }
        [TestMethod()]
        public void IndexTest()
        {
            FakeContext.Setup(i => i.Products).Returns(MockHelper.GetQueryableMockDbSet<Product>(Builder<Product>.CreateListOfSize(100).All().Build().ToArray()));

            var con = new ProductsController(FakeContext.Object) { ControllerContext = MockHelper.MockUser("User").Object };
            var result = con.Index(0) as ViewResult;
            var countFirst = result.Model as IEnumerable<Product>
                ;
            Assert.AreEqual(12, countFirst.Count());

        }
        [TestMethod()]
        public void IndexTestAjax()
        {
            FakeContext.Setup(i => i.Products).Returns(MockHelper.GetQueryableMockDbSet<Product>(Builder<Product>.CreateListOfSize(100).All().Build().ToArray()));
            var con = new ProductsController(FakeContext.Object) { ControllerContext = MockHelper.MockUserWithAjax("User").Object };
            var result = con.Index(0) as PartialViewResult;
            var countFirst = result.Model as IEnumerable<Product>;
            Assert.AreEqual(12, countFirst.Count());

        }
    }
}