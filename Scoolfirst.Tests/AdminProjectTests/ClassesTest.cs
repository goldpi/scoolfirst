﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scoolfirst.Admin.Controllers;
using System.Web.Mvc;
using Scoolfirst.Model.Context;
using Moq;
using System.Collections.Generic;
using Scoolfirst.Model.Common;
using System.Data.Entity;
using System.Linq;
using Scoolfirst.Tests.MocksContext;

namespace Scoolfirst.Tests
{
    [TestClass]
    public class AdminClasses
    {
        private Mock<ScoolfirstContext> FakeContext;
        public AdminClasses()
        {
            FakeContext = new Mock<ScoolfirstContext>() { CallBase = true };
            var data = new List<Classes>
            {
                new Classes
                {
                     Id=1, NumericDisplay=1, RomanDisplay="STD I",Groups=new List<Group>()
                },new Classes
                {
                     Id=2, NumericDisplay=1, RomanDisplay="STD II",Groups=new List<Group>()
                },new Classes
                {
                     Id=3, NumericDisplay=1, RomanDisplay="STD III",Groups=new List<Group>()
                },new Classes
                {
                     Id=4, NumericDisplay=1, RomanDisplay="STD IV",Groups=new List<Group>()
                },new Classes
                {
                     Id=5, NumericDisplay=1, RomanDisplay="STD V",Groups=new List<Group>()
                }
            };
            FakeContext.Setup(i => i.Classes).Returns(MockHelper.GetQueryableMockDbSet<Classes>(data.ToArray()));
        }
        [TestMethod]
        public void AdminClassesIndexUserLogged()
        {
            //Preapration
            var ClasObj = new ClassesController(FakeContext.Object) { ControllerContext=MockHelper.MockUser("user").Object  };
            // E
            var RESULT = ClasObj.Index(1, 2);
            ViewResult re = RESULT as ViewResult;
            Assert.AreEqual(2, (re.Model as IEnumerable<Classes>).Count());

        }

        [TestMethod]
        public void EditClasses()
        {
            var ClasObj = new ClassesController(FakeContext.Object) { ControllerContext = MockHelper.MockUser("user").Object };
            ViewResult re = ClasObj.Edit(1) as ViewResult;
            var MOD = re.Model as Classes;
            Assert.AreEqual(1, MOD.Id);
            Assert.AreEqual(0, MOD.Groups.Count);
            Assert.AreEqual(1, MOD.NumericDisplay);
            Assert.AreEqual("STD I", MOD.RomanDisplay);
        }
        [TestMethod]
        public void EditNotFound()
        {
            var ClasObj = new ClassesController(FakeContext.Object) { ControllerContext = MockHelper.MockUser("user").Object };
            HttpNotFoundResult re = ClasObj.Edit(9) as HttpNotFoundResult;
            Assert.AreEqual(404, re.StatusCode);
            
        }
        [TestMethod]
        public void CreateClases()
        {
            var ClasObj = new ClassesController(FakeContext.Object) { ControllerContext = MockHelper.MockUser("user").Object };
            ViewResult re = ClasObj.Edit(0) as ViewResult;
            var MOD = re.Model as Classes;
            Assert.AreEqual(0, MOD.Id);
            Assert.AreEqual(null, MOD.Groups);
            Assert.AreEqual(0, MOD.NumericDisplay);
            Assert.AreEqual(null, MOD.RomanDisplay);

        }
        [TestMethod]
        public void DeleteNotFound()
        {
            var ClasObj = new ClassesController(FakeContext.Object) { ControllerContext = MockHelper.MockUser("user").Object };
            HttpNotFoundResult re = ClasObj.Delete(9) as HttpNotFoundResult;
            Assert.AreEqual(404, re.StatusCode);

        }
        //[TestMethod]
        //public void DeleteFound()
        //{
        //    var ClasObj = new ClassesController(FakeContext.Object) { ControllerContext = MockHelper.MockUser("user").Object };
        //    JsonResult re = ClasObj.Delete(1) as JsonResult;
        //    int Mod = (re.Data as dynamic).success;
        //    Assert.AreEqual(1, Mod);

        //}
    }
}
