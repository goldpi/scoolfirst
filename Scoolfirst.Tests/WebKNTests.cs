﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scoolfirst.Model.Context;
using Moq;
using Scoolfirst.Web.Controllers;
using System.Web.Mvc;

namespace Scoolfirst.Tests
{
    [TestClass]
    public class WebTestKnowlodgeNutrition
    {
        [TestMethod]
        public void IndexPageUserNotLogedIn()
        {
            var fakeContext= new Mock<ScoolfirstContext>();
            var KNC = new KnowledgeNutritionController(fakeContext.Object);
            var re = KNC.Index();
            RedirectToRouteResult result = (RedirectToRouteResult)re;
            Assert.AreEqual("login", result.RouteValues["action"]);
        }
    }
}
