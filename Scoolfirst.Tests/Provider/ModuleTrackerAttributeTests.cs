﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Identity;
using Scoolfirst.Model.Provider;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.Tests.MocksContext;
using Scoolfirst.ViewModel.WorkBook.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace Scoolfirst.Model.Provider.Tests
{
    [TestClass()]
    public class ModuleTrackerAttributeTests
    {
        private Mock<ScoolfirstContext> FakeContext;

        private ActionExecutingContext Filter;
        private List<Identity.User> User = new List<Identity.User>
        {
             new Identity.User
             {
                 FullName ="test",
                 AboutMe="Test",
                 AccessFailedCount=0,
                 Address="",
                 Board="",
                 Class=new Common.Classes
                      {
                      Groups=new List<Group>(),
                      Id=1,
                      NumericDisplay=1,
                      RomanDisplay="1"
                      },
                Id="User123",
                ClassId=1,
                ConnectionId="",
                DOB="1/1/2001",
                Email="someemail@domain.com",
                EmailConfirmed=false,
                Expiry=DateTime.Now.AddDays(1),
                Gender="Male",
                LockoutEnabled=false,
                LockoutEndDateUtc=DateTime.Now.AddDays(-1),
                PasswordHash="sadasd",
                PhoneNumber ="9090909090",
                PhoneNumberConfirmed=false,
                PictureUrl="",
                Points=1,
                RegisterAs=RegisterAs.User,
                RegisterdOn=DateTime.Now.AddYears(-1),
                RequestedforBrandAmbesdor=false,
                School= new School { AreaId=1, Book=new List<Books>(), LocalArea="some AREA" , SchoolName="Some School", Groups= new List<Group>(), Id=1},
                SchoolId=1,
                Sponsered=false,
                Student_Parent_Admin="Student",
                Subscribed=false,
                Trial=true,
                TwoFactorEnabled=false,
                UserName="UserName1",
                Modules="FREE"
             }
        };
        private List<ModuleControllers> ControllersSet;
        private List<ModuleData> ModeuleDataList;
        public ModuleTrackerAttributeTests()
        {
            FakeContext = new Mock<ScoolfirstContext>();
            FakeContext.Setup(i => i.Users).Returns(MockHelper.GetQueryableMockDbSet<User>(User.ToArray()));
            var controller = new Mock<Controller>();
            var Data = new RouteData();
            Data.Values.Add("action", "MOCK");
            Data.Values.Add("controller", "TEST");

            Filter = new ActionExecutingContext(MockHelper.MockUser("UserName1", Data).Object,
                new Mock<ActionDescriptor>().Object,
                new Dictionary<string, object>())
            { Result= new Mock<ActionResult>().Object ,
             Controller= new Mock<ControllerBase>().Object };

            
            ControllersSet = new List<ModuleControllers>()
        {
            new ModuleControllers
            {
                 Action="MOCK", Controller="TEST", Id="ID", ModuleId="FREE" , Module=new ModuleData
            {
                  ModuleName="Free", Active=true, AuthRequired=true, Free=true, Message="x"
            }
            }
        };
            ModeuleDataList = new List<ModuleData>()
        {
            new ModuleData
            {
                  ModuleName="Free", Active=true, ActionControllers= ControllersSet, AuthRequired=true, Free=true, Message="x"
            }
        };
        }
        

        

        [TestMethod()]
        public void OnActionExecutingTest()
        {
            var testSubject = new ModuleTrackerAttribute(FakeContext.Object);

        }

        [TestMethod()]
        public void AddSideBarTest()
        {
            
        }

        [TestMethod()]
        public void CheckModuleAccesTest()
        {
            var testSubject = new ModuleTrackerAttribute(FakeContext.Object);
            testSubject.CheckModuleAcces(Filter, this.ControllersSet.AsQueryable(), this.ModeuleDataList.AsQueryable());
            Assert.AreEqual("Error", (Filter.Result as ViewResult).ViewName);
           
        }
        [TestMethod]
        public void CheckModuleAcces_SubscripeTest()
        {
            FakeContext = new Mock<ScoolfirstContext>();
            var l = User[0];
            User = new List<Identity.User>();
            l.Expiry = DateTime.UtcNow.AddDays(-1);
            User.Add(l);
            FakeContext.Setup(i => i.Users).Returns(MockHelper.GetQueryableMockDbSet<User>(User.ToArray()));
            var testSubject = new ModuleTrackerAttribute(FakeContext.Object);

            ModeuleDataList.Add(new ModuleData
            {
                ModuleName = "FREE",
                Active = true,
                ActionControllers = ControllersSet,
                AuthRequired = true,
                Free = false,
                Message = "x"
            });
            testSubject.CheckUserCachingSession(Filter, User.AsQueryable());
            testSubject.CheckModuleAcces(Filter, this.ControllersSet.AsQueryable(), this.ModeuleDataList.AsQueryable());
            Assert.AreEqual("Payment", (Filter.Result as ViewResult).ViewName);

        }
        [TestMethod()]
        public void CheckModuleAcces_InactiveTest()
        {

            var testSubject = new ModuleTrackerAttribute(FakeContext.Object);
           
            ModeuleDataList.Add(new ModuleData
            {
                ModuleName = "FREE",
                Active = false,
                ActionControllers = ControllersSet,
                AuthRequired = true,
                Free = true,
                Message = "x"
            });
            testSubject.CheckModuleAcces(Filter, this.ControllersSet.AsQueryable(), this.ModeuleDataList.AsQueryable());
            Assert.AreEqual("Inactive", (Filter.Result as ViewResult).ViewName);
        }
        [TestMethod()]
        public void CheckUserCachingSessionTest()
        {

            var testSubject = new ModuleTrackerAttribute(FakeContext.Object);
            testSubject.CheckUserCachingSession(Filter, User.AsQueryable());
            var user = User.First();
            Assert.AreEqual(1, Filter.HttpContext.Session.Count);
            Assert.AreEqual(user, Filter.HttpContext.Session["UserDetails"] as User);
          
        }
    }
}