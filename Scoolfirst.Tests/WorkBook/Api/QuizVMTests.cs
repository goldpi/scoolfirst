﻿using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.WorkBook;
using Scoolfirst.Tests.MocksContext;
using Scoolfirst.ViewModel.WorkBook.Api;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.ViewModel.WorkBook.Api.Tests
{
    [TestClass]
    public class QuizVMTests
    {

        private Mock<ScoolfirstContext> FakeContext;
        public QuizVMTests()
        {
            FakeContext = new Mock<ScoolfirstContext>(); 
        }
        [TestMethod]
        public void ToQMTest()
        {
            var queryable = Builder<Questions>.
             CreateListOfSize(10)
             .All()
             .Build()
             .AsEnumerable();
            var result = QuizVM.ToQM(queryable);
            Assert.AreEqual(10, result.Count());
            Assert.IsInstanceOfType(result,typeof(IEnumerable<QuestionVM>));
            
        }

        [TestMethod]
        public void BookQuestionTest()
        {
             var queryable = Builder<Books>.
             CreateListOfSize(10)
             .All()
             .With(i=>i.SchoolClassBooks=Builder<SchoolClassSubject>.CreateListOfSize(10).All().Build().ToList())
             .With(i=>i.Chapters=Builder<Chapter>.CreateListOfSize(10).All().Build().ToList())
             .With(i=>i.Classes= new Model.Common.Classes { Groups=new List<Model.Common.Group>(), Id=i.ClassesId, NumericDisplay=1,RomanDisplay="STD I"  })
             .With(i=>i.Questions=Builder<Questions>.CreateListOfSize(10).All().Build().ToList())
             .Build()
             .ToArray();
            var q = queryable.First();
            Console.Write(q.Id);
           
            FakeContext.Setup(i => i.Books).Returns(MockHelper.GetQueryableMockDbSet<Books>(queryable));
            var result = QuizVM.BookQuestion(FakeContext.Object,q.Id);
            Assert.AreEqual(10, result.Questions.Count());
            Assert.IsInstanceOfType(result,typeof(QuizVM));
           
        }

        [TestMethod]
        public void QuizTest()
        {
            var queryable = Builder<Quiz>.
                       CreateListOfSize(10)
                       .All()
                       .With(i=>i.Questions= Builder<Questions>.CreateListOfSize(10).All().Build().ToList())
                       .Build()
                       .ToArray();
            var dbset = MockHelper.GetQueryableMockedDbSet<Quiz>(queryable);
         
            var q = queryable.First();

            FakeContext.Setup(i => i.Quizes).Returns(dbset.Object);
            Console.Write(q.Id);

            
            var result = QuizVM.Quiz(FakeContext.Object, q.Id);
            Assert.AreEqual(10, result.Questions.Count());
            Assert.IsInstanceOfType(result, typeof(QuizVM));
        }
    }
}