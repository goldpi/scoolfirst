﻿using Moq;
using Scoolfirst.Model.Context;
using Scoolfirst.Model.Feed;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoolfirst.Tests
{
    class FakeContext
    {
        public FakeContext()
        {
            var feeds = new List<Feeds>
            {
                new Feeds { Id= Guid.NewGuid(),   },
                new Feeds { Id= Guid.NewGuid(),  },
            };
            
            FakeContextObject.Setup(i => i.Feeds).Returns(Feeds.Object);
        }
        public Mock<ScoolfirstContext> FakeContextObject => new Mock<ScoolfirstContext>();

        public Mock<DbSet<Feeds>> Feeds => new Mock<DbSet<Model.Feed.Feeds>>();

        public ScoolfirstContext Context => FakeContextObject.Object;


    }
}
