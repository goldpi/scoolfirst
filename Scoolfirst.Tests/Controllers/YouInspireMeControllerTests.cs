﻿using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Scoolfirst.Model.Common;
using Scoolfirst.Model.Context;
using Scoolfirst.Tests.MocksContext;
using Scoolfirst.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Scoolfirst.Web.Controllers.Tests
{
    
    [TestClass]
    public class YouInspireMeControllerTests
    {
        private Mock<ScoolfirstContext> FakeContext;
        public YouInspireMeControllerTests()
        {
            FakeContext = new Mock<ScoolfirstContext>();
        }
        [TestMethod]
        public void IndexTest()
        {
            var re = Builder<RoleModel>.
               CreateListOfSize(10)
               .All()
               .Build()
               .ToArray();
            FakeContext.Setup(i => i.RoleModels).Returns(MockHelper.GetQueryableMockDbSet<RoleModel>(re));
            var con = new YouInspireMeController(FakeContext.Object)
            { ControllerContext = MockHelper.MockUser("user").Object };

            ViewResult result = (con.Index() as ViewResult);
            var Model = (IEnumerable<RoleModel>)result.Model;
            Assert.AreEqual(4, Model.Count());
        }
        [TestMethod]
        public void IndexAjaxTest()
        {
            var re = Builder<RoleModel>.
               CreateListOfSize(10)
               .All()
               .Build()
               .ToArray();
            FakeContext.Setup(i => i.RoleModels).Returns(MockHelper.GetQueryableMockDbSet<RoleModel>(re));
            var con = new YouInspireMeController(FakeContext.Object)
            { ControllerContext = MockHelper.MockUserWithAjax("user").Object };

            PartialViewResult result = (con.Index() as PartialViewResult);
            var Model = (IEnumerable<RoleModel>)result.Model;
            Assert.AreEqual(4, Model.Count());
        }
    }
}