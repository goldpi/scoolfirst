﻿using Scoolfirst.Model.Identity;
using Scoolfirst.Model.KDC;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.Model.Provider;
using Scoolfirst.PictureManager.Models;

namespace Scoolfirst.PictureManager.Controllers
{
    public class AndroidImageController : Controller
    {
        public AndroidImageController(Scoolfirst.Model.Context.ScoolfirstContext context)
        {
            dc = context;
        }

        //  private string AppServer;
        private string PictureServer = HostingServerInfo.Picture.Url();//ConfigurationManager.AppSettings["pictureServer"];

        private string ApiUrl = HostingServerInfo.Api.Url();
        private Scoolfirst.Model.Context.ScoolfirstContext dc;
        private User UserInfo;

        //protected override async void OnActionExecuting(ActionExecutingContext filterContext)
        //{
        //    AppServer = ConfigurationManager.AppSettings["AppServer"];
        //    PictureServer = filterContext.RequestContext.HttpContext.Request.Url.Host;

        //    try { var AuthHeader = filterContext.RequestContext.HttpContext.Request.Headers["Authorization"].ToString();

        //        UserInfo = await GetUser(AuthHeader);
        //    }
        //    catch
        //    {
        //        filterContext.Result = this.HttpNotFound();
        //    }

        //    base.OnActionExecuting(filterContext);
        //}
        private async Task<User> GetUser(string token)
        {
            var user = await GetUserInfo(token);
            var userInfo = dc.Users.FirstOrDefault(i => i.Id == user);
            return userInfo;
        }

        private async Task<string> GetUserInfo(string token)
        {
            if (Request.IsSecureConnection)
                ApiUrl = "https://" + ApiUrl;
            else
                ApiUrl = "http://" + ApiUrl;
            var client = new HttpClient { BaseAddress = new Uri(ApiUrl) };
            client.DefaultRequestHeaders.Add("Authorization", token);
            var response = await client.GetStringAsync("api/account/UserEmail");
            return response;
        }

        // GET: AndroidImage
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddKDC(int? classId, int? subjectId, string text, string Title, string token)
        {
            var owin = Request.GetOwinContext();
            var user = owin.Authentication.User.Identities.First().Name;
            user.Debug();
            if (string.IsNullOrEmpty(user))
                throw new HttpException(401, "Users Not Authorized");
            UserInfo = dc.Users.FirstOrDefault(i => i.UserName == token || i.UserName == user);// await GetUser(token);
            if (UserInfo == null)
                throw new HttpException(401, "Users Not Found");
            var article = new Article();
            article.ID = Guid.NewGuid();
            if (classId != null)
                article.ClassesId = classId;
            if (subjectId != null)
                article.SubjectId = subjectId;
            article.Title = Title;
            article.Solution = text;
            article.UserId = UserInfo.Id;
            article.On = DateTime.Now;
            dc.Articles.Add(article);
            dc.SaveChanges();
            string.Format("files count ->" + Request.Files.Count.ToString()).Debug();
            if (Request.Files.Count > 0)
            {
                var image = Request.Files["image"];
                if (image != null)
                {
                    var servarPath = Server.MapPath("~/Content/KDC/" + UserInfo.Id);
                    var Dir = new DirectoryInfo(servarPath);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    var fileName = Guid.NewGuid() + image.FileName;
                    image.SaveAs(servarPath + "\\" + fileName);
                    article.ImageUrl = "http://" + PictureServer + "/content/Kdc/" + UserInfo.Id + "/" + fileName;
                    await dc.SaveChangesAsync();
                }
            }

            return Json(new { article.ID }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> AddKDC1(int classId, int subjectId, string text, string Title, string token)
        {
            if (string.IsNullOrEmpty(token))
                return Json(new
                {
                    Error = "Not Authorized"
                }, JsonRequestBehavior.AllowGet);
            UserInfo = await GetUser(token);
            if (UserInfo == null)
                return Json(new
                {
                    Error = "NO user found"
                }, JsonRequestBehavior.AllowGet);

            var article = new Article();
            article.ID = Guid.NewGuid();
            article.ClassesId = classId;
            article.SubjectId = subjectId;
            article.Title = Title;
            article.Solution = text;
            article.UserId = UserInfo.Id;
            article.On = DateTime.Now;
            dc.Articles.Add(article);
            dc.SaveChanges();
            if (Request.Files.Count > 0)
            {
                var image = Request.Files["image"];
                if (image != null)
                {
                    var servarPath = Server.MapPath("~/Content/KDC/" + UserInfo.Id);
                    var Dir = new DirectoryInfo(servarPath);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    var fileName = Guid.NewGuid() + image.FileName;
                    image.SaveAs(servarPath + "\\" + fileName);
                    article.ImageUrl = "http://" + PictureServer + "/content/Kdc/" + UserInfo.Id + "/" + fileName;
                    dc.SaveChanges();
                }
            }

            return Json(article, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadPicture(HttpPostedFileWrapper File, string user)
        {
            var userdata = dc.Users.FirstOrDefault(i => i.UserName == user || i.Email == user);
            if (File != null)
            {
                // var user = User.Identity.Name;

                if (userdata != null)
                {
                    var servarPath = Server.MapPath("~/Content/User/");
                    var Dir = new DirectoryInfo(servarPath + userdata.Id);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    var fileName = Guid.NewGuid() + File.FileName;
                    File.SaveAs(servarPath + userdata.Id + "\\" + fileName);

                    userdata.PictureUrl = "http://" + PictureServer + "/content/user/" + userdata.Id + "/" + fileName;
                    dc.SaveChanges();
                }
            }
            return Json(new { Picture = userdata.PictureUrl });
        }
    }
}