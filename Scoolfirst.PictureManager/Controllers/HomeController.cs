﻿using Scoolfirst.Model.KDC;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scoolfirst.ViewModel.Abuse;
using Scoolfirst.Model.Provider;

namespace Scoolfirst.PictureManager.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private string AppServer;
        private string PictureServer;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            AppServer = HostingServerInfo.Application.Url(); //ConfigurationManager.AppSettings["AppServer"];
            PictureServer = HostingServerInfo.Picture.Url();
            base.OnActionExecuting(filterContext);
        }

        private Scoolfirst.Model.Context.ScoolfirstContext dc = new Model.Context.ScoolfirstContext();

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UploadPicture()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult UploadPicture(HttpPostedFileWrapper File)
        {
            if (File != null)
            {
                var user = User.Identity.Name;
                var userdata = dc.Users.FirstOrDefault(i => i.UserName == user || i.Email == user);
                if (userdata != null)
                {
                    var servarPath = Server.MapPath("~/Content/User/");
                    var Dir = new DirectoryInfo(servarPath + userdata.Id);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    var fileName = Guid.NewGuid() + File.FileName;
                    File.SaveAs(servarPath + userdata.Id + "\\" + fileName);

                    userdata.PictureUrl = "http://" + PictureServer + "/content/user/" + userdata.Id + "/" + fileName;
                    dc.SaveChanges();
                }
            }
            return Redirect(AppServer + "/profile?message=Picture+changed+!&mes=true");
        }

        public ActionResult Picture()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity.Name;
                var userdata = dc.Users.FirstOrDefault(i => i.UserName == user || i.Email == user);
                if (userdata != null)
                {
                    var servarPath = Server.MapPath("~/Content/User/" + userdata.Id);
                    ViewBag.Path = "/content/user/" + userdata.Id + "/";
                    DirectoryInfo dir = new DirectoryInfo(servarPath);
                    if (!dir.Exists)
                    {
                        dir.Create();
                    }
                    var files = dir.GetFiles().AsEnumerable();
                    if (Request.IsAjaxRequest())
                        return PartialView(files);
                    else return View(files);
                }
            }
            return Content("404");
        }

        public ActionResult Change(string id)
        {
            var user = User.Identity.Name;
            var userdata = dc.Users.FirstOrDefault(i => i.UserName == user || i.Email == user);
            if (userdata != null)
            {
                userdata.PictureUrl = "http://" + PictureServer + "/content/user/" + userdata.Id + "/" + id;
                dc.SaveChanges();
            }
            return Redirect(AppServer + "/profile?message=Picture+changed+!&mes=true");
        }

        public ActionResult Kdc()
        {
            ViewBag.ClassesId = new SelectList(dc.Classes, "Id", "RomanDisplay");
            ViewBag.SubjectId = new SelectList(dc.Subjects, "Id", "SubjectName");
            return View();
        }

        public ActionResult EditKDC(Guid Id)
        {
            var kdc = dc.Articles.Find(Id);
            if (kdc != null)
            {
                if (User.Identity.IsAuthenticated)
                {
                    var user = User.Identity.Name;
                    var userdata = dc.Users.FirstOrDefault(i => i.UserName == user || i.Email == user);
                    if (kdc.UserId == userdata.Id)
                    {
                        ViewBag.ClassesId = new SelectList(dc.Classes, "Id", "RomanDisplay");
                        ViewBag.SubjectId = new SelectList(dc.Subjects, "Id", "SubjectName");
                        return View(kdc);
                    }
                }
            }
            return Content(@"404: Closing window  <script>

                        window.close();
                        </script>");
        }

        [HttpPost]
        public ActionResult EditKDC(Article Article, HttpPostedFileWrapper image)
        {
            ViewBag.ClassesId = new SelectList(dc.Classes, "Id", "RomanDisplay");
            ViewBag.SubjectId = new SelectList(dc.Subjects, "Id", "SubjectName");
            if (!ModelState.IsValid)
                return View(Article);

            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity.Name;
                var userdata = dc.Users.FirstOrDefault(i => i.UserName == user || i.Email == user);
                if (userdata != null)
                {
                    Article.ID = Guid.NewGuid();
                    Article.UserId = userdata.Id;

                    dc.Articles.Add(Article);
                    dc.SaveChanges();

                    if (image != null)
                    {
                        var servarPath = Server.MapPath("~/Content/KDC/" + userdata.Id);
                        var Dir = new DirectoryInfo(servarPath);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        var fileName = Guid.NewGuid() + image.FileName;
                        image.SaveAs(servarPath + "\\" + fileName);
                        Article.ImageUrl = "http://" + PictureServer + "/content/Kdc/" + userdata.Id + "/" + fileName;
                        dc.SaveChanges();
                    }
                    return Content(@"<script>

                        window.close();
                        </script>");
                }
            }
            return View(Article);
        }

        [HttpPost]
        public ActionResult Kdc(Article Article, HttpPostedFileWrapper image)
        {
            ViewBag.ClassesId = new SelectList(dc.Classes, "Id", "RomanDisplay");
            ViewBag.SubjectId = new SelectList(dc.Subjects, "Id", "SubjectName");
            if (!ModelState.IsValid)
                return View(Article);

            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity.Name;
                var userdata = dc.Users.FirstOrDefault(i => i.UserName == user || i.Email == user);
                if (userdata != null)
                {
                    Article.ID = Guid.NewGuid();
                    Article.UserId = userdata.Id;

                    dc.Articles.Add(Article);
                    dc.SaveChanges();

                    if (image != null)
                    {
                        var servarPath = Server.MapPath("~/Content/KDC/" + userdata.Id);
                        var Dir = new DirectoryInfo(servarPath);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        var fileName = Guid.NewGuid() + image.FileName;
                        image.SaveAs(servarPath + "\\" + fileName);
                        Article.ImageUrl = "http://" + PictureServer + "/content/Kdc/" + userdata.Id + "/" + fileName;
                        dc.SaveChanges();
                    }
                    return Content(@"<script>

                        window.close();
                        </script>");
                }
            }
            return View(Article);
        }

        public ActionResult Report(TypeSource type, string PostId)
        {
            ViewBag.Reason = new SelectList(dc.ReportAbuseTitle, "Title", "Title");
            return View(new ReportAbuseModel { Source = type, PostId = PostId });
        }

        [HttpPost]
        public ActionResult Report(ReportAbuseModel model)
        {
            if (ModelState.IsValid)
            {
                dc.ReportAbused.Add(new Model.Common.ReportedAbused { Id = Guid.NewGuid(), IdOfPost = model.PostId, Source = model.Source, Text = model.Reason });
                dc.SaveChanges();
                return Content(@"<script>

                        window.close();
                        </script>");
            }
            ViewBag.Reason = new SelectList(dc.ReportAbuseTitle, "Title", "Title", model.Reason);
            return View(model);
        }
    }
}