﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Scoolfirst.PictureManager.Startup))]
namespace Scoolfirst.PictureManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
