﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoolfirst.PictureManager.Models
{
    public class DebugHub : Hub
    {
    }

    public static class DebugHubHelper
    {
        public static void Debug(this object Helper)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<DebugHub>();

            //var debug = Newtonsoft.Json.JsonConvert.SerializeObject(Helper);
            if (HttpContext.Current.User.Identity.IsAuthenticated)
                context.Clients.All.show($"----------------{HttpContext.Current.User.Identity.Name}------------------------");
            else
                context.Clients.All.show($"----A-N-A----------{DateTime.UtcNow}------------------------");

            context.Clients.All.show(Helper);

            context.Clients.All.show("-------------------------------------------------------");
        }

        public static void UserDetails(this object Helper)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<DebugHub>();
            context.Clients.All.userdata(Helper, DateTime.UtcNow);
        }
    }
}